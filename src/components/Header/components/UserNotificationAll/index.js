import React, {Component} from 'react';
import {Button, Icon, Modal, Table} from "antd";
import {GetData, PostData} from "../../../../services/api";

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            data: [],
            isLoading: true
        };
    }


    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Judul',
                dataIndex: 'notification_title',
                key: 'notification_title',
                width: '30%'
            },
            {
                title: 'Deskripsi',
                dataIndex: 'notification_description',
                key: 'notification_description',
                width: '55%'
            },
            {
                title: 'Waktu',
                dataIndex: 'created_at',
                key: 'created_at',
                width: '15%'
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 60,
                render: (text, record) => {
                    return (
                        <Button type={"primary"} icon={"arrow-right"} size={"small"}
                                onClick={this.props.notificationClick(record.notification_id, record.notification_type)}></Button>
                    )
                }
            },
        ];

        const data = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        // const notificationClick = this.props.notificationClick;

        return (
            <div>
                <div className={"text-center"}>
                    <Button type={"link"} onClick={this.modalStatus(true)}>
                        Semua Notifikasi <Icon type="arrow-right"/>
                    </Button>
                </div>

                <Modal
                    title={
                        <span>
                            <Icon type="bell"/> Semua Notifikasi
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={1000}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                    className={"modal-notification-all"}
                    zIndex={10000}
                >

                    <Table columns={columns}
                           dataSource={data}
                           className={"table-notification"}
                           loading={this.state.isLoading}
                           // onRow={(record) => ({
                           //     onClick: () => {
                           //         console.log(record);
                           //         this.change(record.notification_id, record.notification_type);
                           //     }
                           // })}
                           onChange={(pagination, filters, sorter, extra) => {
                               let to = parseInt(pagination.current) * parseInt(pagination.pageSize);
                               let from = parseInt(to) - parseInt(pagination.pageSize) + parseInt(1);
                               let notification_id_see_no_arr = [];
                               let no = 0;

                               extra.currentDataSource.map((item, key) => {
                                   //console.log(item);
                                   if (item.no >= from && item.no <= to && item.notification_see === 'no') {
                                       notification_id_see_no_arr[no] = item.notification_id;
                                       no++;
                                   }
                               });

                               // console.log('notification_id_no', notification_id_see_no_arr);

                               if (notification_id_see_no_arr.length > 0) {
                                   let data = {
                                       notification_id: notification_id_see_no_arr
                                   };
                                   PostData('/notification-see-all', data).then((result) => {

                                       this.props.notificationTotalChange(result.data.data.total);

                                       this.setState({
                                           data: result.data.data.list
                                       })
                                   });
                               }

                           }}
                           rowClassName={(record, index) => {
                               return record.notification_read === 'no' ? 'not-yet-read' : ''
                           }}
                           size='small'/>

                </Modal>
            </div>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    change = (notification_id, notification_type) => e => {
        console.log('change');
        this.props.notificationClick(notification_id, notification_type);
    };


    list() {
        this.setState({
            isLoading: true
        });
        GetData('/notification-all')
            .then((result) => {
                this.setState({
                    data: result.data.data,
                    isLoading: false,
                    listRefresh: false,
                });
            });
    }
}