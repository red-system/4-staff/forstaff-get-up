import React, {Component} from 'react';
import {Avatar, Dropdown, Icon, Menu} from "antd";
import {Redirect} from "react-router-dom";
import {Link} from "react-router-dom";
import GENERALDATA from "../../../../constants/generalData";
import './styles.scss';
import {log} from "async";

let menuProfile = GENERALDATA.primaryMenu.personal_menu.sub_menu["1"];
//let menuSettings = GENERALDATA.primaryMenu.settings;
let menuLogout = GENERALDATA.primaryMenu.logout;


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            userMenuVisibleChange: props.userMenuVisibleChange,
            redirectLogin: false
        };

        this.logout = this.logout.bind(this);
    }

    logout() {
        localStorage.clear();
        this.setState({
            redirectLogin: true
        });
    }

    render() {

        const {match, redirectLogin} = this.state;
        let loginData = localStorage.getItem('loginData');
        loginData = JSON.parse(loginData);

        if(redirectLogin) {
            return <Redirect to={""}/>
        }

        return (

            <Dropdown overlay={() => (
                <Menu className="user-menu-dropdown-list">
                    <Menu.Item key="4" className="hide-desktop hide-tablet"> Login sebagai <strong>{loginData.staff_name}</strong>
                    </Menu.Item>
                    <Menu.Divider className="hide-desktop hide-tablet"/>
                    <Menu.Item>
                        <Link to={`${match.url}/${menuProfile.route}`}>
                            <Icon type={"user"}/> {menuProfile.label}
                        </Link>
                    </Menu.Item>
                    <Menu.Divider/>
                    <Menu.Item>
                        <div className="app-logout" onClick={this.logout}>
                            <Icon type={menuLogout.icon}/> {menuLogout.label}
                        </div>
                    </Menu.Item>
                </Menu>
            )}
                      placement="bottomRight"
                      className="user-menu-dropdown"
                      trigger={['click']}>
                <span className="header-menu-right-item">
                    <Avatar
                        src={loginData.staff_avatar_url}
                        size="small"
                        className="app-user-avatar"/>
                    <span className="hide-mobile user-label">
                        {loginData.staff_name}
                    </span>
                </span>
            </Dropdown>
        )
    }

}

export default Index