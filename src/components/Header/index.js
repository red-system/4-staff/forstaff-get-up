import React, {Component} from 'react';
import {Col, Icon, Layout, Row} from "antd";
import UserMenu from './components/UserMenu';
import UserNotifications from './components/UserNotifications';
import './styles.scss';

const {Header} = Layout;

class AppHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            sidebarCollapsed: props.sidebarCollapsed,
            sidebarDrawerEvent: props.sidebarDrawerEvent,
            sidebarEvent: props.sidebarEvent,
        }
    }

    render() {

        const { match, sidebarDrawerEvent, sidebarEvent } = this.state;
        const sidebarCollapsedIcon = this.props.sidebarCollapsed ? 'menu-unfold' : 'menu-fold';

        return (
            <Header className="app-header">
                <Row type="flex" justify="end">
                    <Col span={2}>
                        <Icon className="trigger hide-mobile-portrait hide-mobile-landscape hide-tablet-portrait hide-tablet-portrait"
                              type={sidebarCollapsedIcon}
                              onClick={sidebarEvent} />
                    </Col>
                    <Col className="right-text" span={20} offset={2}>
                        <UserNotifications />

                        <UserMenu match={match} />

                        <Icon type="menu" className="header-menu-right-item hide-desktop" onClick={sidebarDrawerEvent}/>
                    </Col>
                </Row>

            </Header>
        )

    }

}

export default AppHeader;