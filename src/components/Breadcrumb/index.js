import {Breadcrumb, Icon} from "antd";
import React, {Component} from "react";
import {Link} from 'react-router-dom';
import GeneralData from "../../constants/generalData";

import MetaTags from 'react-meta-tags';
import {GetData} from "../../services/api";

class BreadcrumbComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            total_notification: 0,
        }
    }

    componentWillMount() {
        this.total_notification();
    }

    // async componentDidMount() {
    //     try {
    //         setInterval(async () => {
    //             this.total_notification();
    //         }, 60000);
    //     } catch (e) {
    //         // console.log(e);
    //     }
    // }

    render() {
        const {data} = this.state;
        let metaTitle = '';
        let breadcrumbLabel = [];
        let appUriUniq = GeneralData.appUriUniq;

        if (data) {
            data.map((row, index) => {
                breadcrumbLabel[index] = row.label;

                return null;
            });
        }

        breadcrumbLabel.reverse().map((row, index) => {
            metaTitle += `${row} < `;
            return null;
        });
        metaTitle += ' Dashboard';

        let countData = 0;
        if(data) {
            data.map((row, key) => {
                countData++;
            });
        }


        const breadcrumbList = !data ? null : data.map((row, key) => {
            if((key + 1) == countData) {
                return (
                    <Breadcrumb.Item key={key}>
                            {row.label}
                    </Breadcrumb.Item>
                )
            } else {
                return (
                    <Breadcrumb.Item key={key}>
                        <Link to={`../../${appUriUniq}/${row.route}`} key={key}>
                            {row.label}
                        </Link>
                    </Breadcrumb.Item>
                )
            }
        });

        let meta_notification = '';
        if(this.state.total_notification > 0) {
            if(this.state.total_notification > 99) {
                meta_notification = `(99+) `;
            } else {
                meta_notification = `(${this.state.total_notification}) `;
            }
        }

        return (
            <div>
                <MetaTags>
                    <title>{meta_notification}{metaTitle}</title>
                </MetaTags>
                <Breadcrumb className="app-breadcrumb">
                    <Breadcrumb.Item key="-2">
                        <Link to='/'><Icon type="home" /> Dashboard</Link>
                    </Breadcrumb.Item>
                    { breadcrumbList }
                </Breadcrumb>
            </div>
        )
    }

    // notification_data() {
    //     GetData('/notification-total').then((result) => {
    //         this.setState({
    //             total_notification: result.data.data.total
    //         })
    //     })
    // }

    total_notification = () => {
        GetData('/notification-total').then((result) => {
            this.setState({
                total_notification: result.data.data.total
            })
        })
    };

}

export default BreadcrumbComponent;
