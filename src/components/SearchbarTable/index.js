import React, { Component } from "react";
import { Icon, Input } from "antd";
import { map, get } from "lodash";

const Search = Input.Search;

class SearchbarTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            columnName: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            data: nextProps.data,
            dataSearched: nextProps.dataSearched,
            columnName: nextProps.columnName,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        console.log("reg: " + reg);
        var dataArray = [];
        var isTrue = false;
        var columnLength = this.state.columnName.length;
        const filteredData = map(this.state.data, (record) => {
            console.log("columnLength: " + columnLength);
            map(this.state.columnName, (columnRecord, index) => {
                console.log("index " + index);
                dataArray[index] = false;
                if (get(record, columnRecord) != null) {
                    dataArray[index] = get(record, columnRecord).toString().match(reg);
                }
                // console.log("dataArray: " + dataArray[index]);
                if (dataArray[index]) {
                    console.log("is true");
                    isTrue = true;
                    return;
                }
                // return false;
            });
            // for (let i = 0; i < columnLength; i++) {
            //     console.log("columnName: " + this.state.columnName[i]);
            //     dataArray[this.state.columnName[i]] = false;
            //     if (get(record, this.state.columnName[i]) != null) {
            //         dataArray[this.state.columnName[i]] = get(record, this.state.columnName[i]).toString().match(reg);
            //     }
            //     if (dataArray[this.state.columnName[i]]) {
            //         return record;
            //     }
            //     return null;
            // }
            // const nameMatch = get(record, "staff.staff_name").match(reg);
            // const departmentMath = get(record, "staff.position.department.department_title").match(reg);
            // const positionMatch = get(record, "staff.position.structure_name").match(reg);
            // if (!nameMatch && !departmentMath && !positionMatch) {
            //     return null;
            // }
            // return record;
            if (isTrue) {
                return record;
            }
            return null;
        }).filter((record) => !!record);
        console.log("filteredData: " + filteredData);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    render() {
        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        return (
            <div className="row mb-1">
                <div className="col-9"></div>
                <div className="col-3 pull-right">
                    <Search
                        size="default"
                        ref={(ele) => (this.searchText = ele)}
                        suffix={suffix}
                        onChange={this.onSearch}
                        placeholder="Search Records"
                        value={this.state.searchText}
                        onPressEnter={this.onSearch}
                    />
                </div>
            </div>
        );
    }
}

export default SearchbarTable;
