import React, { Component } from "react";
import { Drawer, Icon, Layout, Menu, Modal, Button, Input } from "antd";
import { Link } from "react-router-dom";
import renderHTML from "react-render-html";
import GENERALDATA from "../../constants/generalData";
import appLogoLandscape from "../../images/app-logo-landscape-light.png";
import appLogoSquare from "../../images/app-logo-square-light.png";
import bgHelp from "../../images/covers/luca-bravo-198062-unsplash-mirror.jpg";

import "../../styles/styles_sidebar.scss";
import { map, get } from "lodash";
const Search = Input.Search;

let menuDashboard = GENERALDATA.primaryMenu.dashboard;
let menuPersonalMenu = GENERALDATA.primaryMenu.personal_menu;
let menuSopManagement = GENERALDATA.primaryMenu.sop_management;
let menuStaffManagement = GENERALDATA.primaryMenu.staff_management;
let menuStaffAttendence = GENERALDATA.primaryMenu.staff_attendence;
let menuGeneralData = GENERALDATA.primaryMenu.general_data;
let releaseNote = GENERALDATA.primaryMenu.releaseNote;
//let officialWeb = GENERALDATA.primaryMenu.officialWeb;
let ERROR_REPORT = GENERALDATA.primaryMenu.errorReport;
let BRAND_COMPANY = GENERALDATA.appBrandCompany;

let PHONE = GENERALDATA.contact.phone;
let EMAIL = GENERALDATA.contact.email;

let INSTAGRAM = GENERALDATA.socialMedia.instagram;
let FACEBOOK = GENERALDATA.socialMedia.facebook;
let LINKEDIN = GENERALDATA.socialMedia.linkedin;

const sidebarWidthCollapsed = 200;
const { Sider } = Layout;

class AppSidebar extends Component {
    state = {
        sidebarDrawerVisible: this.props.sidebarDrawerVisible,
        sidebarCollapsed: this.props.sidebarCollapsed,
        match: null,
        visible: false,
        menuData: [],
        searchMenuData: [],
        searchMenuText: "",
    };

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            sidebarDrawerVisible: nextProps.sidebarDrawerVisible,
            sidebarCollapsed: nextProps.sidebarCollapsed,
            match: nextProps.match,
        });
    }

    componentWillMount() {
        const menuView = this.menuView();
        this.setState({ searchMenuData: menuView, menuData: menuView });
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };
    handleCloseModalDrawer = (e) => {
        this.props.sidebarDrawerEventClose();
        this.setState({
            visible: false,
        });
    };
    handleCloseModal = (e) => {
        this.setState({
            visible: false,
        });
    };

    onInputChange = (e) => {
        this.setState({ searchMenuText: e.target.value });
    };

    onSearchMenu = (e) => {
        const menuView = this.menuView();

        const reg = new RegExp(e.target.value, "gi");
        const filteredMenuData = map(menuView, (record) => {
            var findedMenu = false;
            var findedSubMenu = false;
            if (get(record, "label") != null) {
                findedMenu = get(record, "label").toString().match(reg);
            }
            console.log("findedMenu: " + findedMenu);
            console.log("------------------");
            // console.table("record: " + Object.keys(record.sub_menu));
            console.log("sub_menu length " + record.sub_menu);
            map(record.sub_menu, (recordSub) => {
                var findInSub = false;
                // console.log("object key sub: " + Object.values(recordSub));
                // console.log("label subMenu: " + recordSub.label);
                console.log(get(recordSub, "label"));
                if (get(recordSub, "label") != null) {
                    findInSub = get(recordSub, "label").toString().match(reg);
                }
                if (findInSub) {
                    findedSubMenu = true;
                    return;
                }
            });
            console.log("findedSubMenu: " + findedSubMenu);
            if (!findedMenu && !findedSubMenu) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        // filteredMenuData.map((item) => console.log("filtered: " + item.));
        console.log("filteredMenu: " + filteredMenuData);

        this.setState({
            searchMenuText: e.target.value,
            searchMenuData: e.target.value ? filteredMenuData : this.state.menuData,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    render() {
        let menuView = this.menuView();
        let appLogo = this.state.sidebarCollapsed ? appLogoSquare : appLogoLandscape;

        const { searchMenuText, searchMenuData } = this.state;
        const suffix = searchMenuText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;
        const { sidebarDrawerEvent, match } = this.props;
        let menuActive = this.menuActive();
        let menuList = searchMenuData.map((item, key) => {
            return (
                <Menu.Item key={item.variable}>
                    <Link to={`${match.url}/${item.route}`}>
                        <Icon type={item.icon} />
                        <span>{item.label}</span>
                    </Link>
                </Menu.Item>
            );
        });

        return (
            <Sider
                className="app-sidebar hide-mobile-portrait hide-mobile-landscape hide-tablet-portrait hide-tablet-portrait hide-tablet-medium-landscape hide-tablet-medium-portrait"
                collapsed={this.state.sidebarCollapsed}
                trigger={null}
                breakpoint="lg"
                width={sidebarWidthCollapsed}
            >
                {/* {this.sidebarDrawer(sidebarDrawerEvent, match, menuActive, this.showModal, menuList)} */}
                {this.sidebarDrawer(sidebarDrawerEvent, match, menuActive, this.showModal, searchMenuData)}

                <div className="app-logo">
                    <img src={appLogo} height={"44"} alt="forStaff" />
                </div>
                <div>
                    <Search
                        size="default"
                        ref={(ele) => (this.searchMenuText = ele)}
                        suffix={suffix}
                        onChange={this.onSearchMenu}
                        placeholder="Search Records"
                        value={this.state.searchMenuText}
                        onPressEnter={this.onSearchMenu}
                    />
                </div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={[menuActive]} className="app-sidebar-menu">
                    {menuList}
                </Menu>

                <div className="app-info" onClick={this.showModal}>
                    <div className="uncollapse">
                        <h1>
                            <Icon type="question-circle" /> Informasi & Bantuan
                        </h1>
                    </div>
                    <div className="collapse">
                        <h1>
                            <Icon type="question-circle" />
                        </h1>
                    </div>
                </div>

                <Modal visible={this.state.visible} onCancel={this.handleCloseModal} footer={null} className="custom-modal-v1" centered>
                    <article className="img-card-v2 mb-4" style={{ height: "500px" }}>
                        <div className="img-card__cover" style={{ backgroundImage: `url('${bgHelp}')` }}></div>
                        <div className="img-card__body img-card__body--left">
                            <h2 className="img-card__title">{renderHTML(GENERALDATA.appBrandHtml)}</h2>
                            <small>v1.0.0</small>
                            <p className="img-card__desc lead mb-6">
                                {renderHTML(GENERALDATA.appBrandHtml)} adalah sistem yang membantu perusahaan untuk manajemen staff dengan rapi, efisien dan
                                profesional
                                <br />
                                <br />
                                <div>{BRAND_COMPANY}</div>
                                <div className="app-social-list">
                                    <a href={PHONE.route}>
                                        <small>
                                            <Icon type={PHONE.icon} /> {PHONE.label}
                                        </small>
                                    </a>
                                    &nbsp;
                                    <a href={EMAIL.route}>
                                        <small>
                                            <Icon type={EMAIL.icon} /> {EMAIL.label}
                                        </small>
                                    </a>
                                </div>
                                <div className="app-social-list">
                                    <small>
                                        <a href={INSTAGRAM.route} target="_blank" rel="noopener noreferrer">
                                            <Icon type={INSTAGRAM.icon} theme="filled" /> {INSTAGRAM.label}
                                        </a>
                                    </small>
                                    &nbsp;
                                    <small>
                                        <a href={FACEBOOK.route} target="_blank" rel="noopener noreferrer">
                                            <Icon type={FACEBOOK.icon} theme="filled" /> {FACEBOOK.label}
                                        </a>
                                    </small>
                                    &nbsp;
                                    <small>
                                        <a href={LINKEDIN.route} target="_blank" rel="noopener noreferrer">
                                            <Icon type={LINKEDIN.icon} theme="filled" /> {LINKEDIN.label}
                                        </a>
                                    </small>
                                </div>
                            </p>
                            <Link to={`${match.url}/${releaseNote.route}`} onClick={this.handleCloseModalDrawer}>
                                <Button type="primary" className="btn-cta" size="small">
                                    <Icon type={releaseNote.icon} /> {releaseNote.label}
                                </Button>
                            </Link>
                            &nbsp;
                            <Link to={`${match.url}/${ERROR_REPORT.route}`} onClick={this.handleCloseModalDrawer}>
                                <Button type="warning" className="btn-cta" size="small">
                                    <Icon type={ERROR_REPORT.icon} /> {ERROR_REPORT.label}
                                </Button>
                            </Link>
                        </div>
                    </article>
                </Modal>
            </Sider>
        );
    }

    sidebarDrawer(sidebarDrawerEvent, match, menuActive, showModal, menuList) {
        return (
            <Drawer
                title={renderHTML(GENERALDATA.appBrand)}
                placement="right"
                closable={true}
                onClose={sidebarDrawerEvent}
                visible={this.state.sidebarDrawerVisible}
                className="app-drawer-menu"
            >
                <Menu onClick={sidebarDrawerEvent} defaultSelectedKeys={[menuActive]} mode="inline" theme="dark" inlineCollapsed={false}>
                    {menuList}
                </Menu>
                <div className="footer" onClick={showModal}>
                    <Icon type="question-circle" /> Informasi & Bantuan
                </div>
            </Drawer>
        );
    }

    menuView = () => {
        let roles = localStorage.getItem("roles");
        roles = JSON.parse(roles);
        let menu = [];
        let menuView = [];

        Object.keys(roles).map((item, key) => {
            Object.keys(roles[item]).map((item2, key2) => {
                Object.keys(roles[item][item2]).map((item3, key3) => {
                    Object.keys(roles[item][item2][item3]).map((item4, key4) => {
                        Object.keys(roles[item][item2][item3][item4]).map((item5, key5) => {
                            let status = roles[item][item2][item3][item4][item5];
                            //console.log(item, item2, item3, item4, item5, status);
                            if (status) {
                                menu[key] = item;
                            }
                        });
                    });
                });
            });
        });

        Object.keys(menu).map((item, key) => {
            let menuVariable = menu[item];
            menuView[key] = GENERALDATA.primaryMenu[menuVariable];
        });

        return menuView;
    };

    menuActive = () => {
        let menuView = this.menuView();
        let url = window.location.href;
        let menuActive = menuView[0].variable;

        Object.keys(menuView).map((item, key) => {
            if (url.includes(menuView[item].route)) {
                menuActive = menuView[item].variable;
            } else {
                Object.keys(menuView[item].sub_menu).map((item2, key2) => {
                    if (url.includes(menuView[item].sub_menu[item2].route)) {
                        menuActive = menuView[item].variable;
                    }
                });
            }
        });

        return menuActive;
    };
}

export default AppSidebar;
