import React from 'react';

const Loader = () => (
    <div style={{textAlign: 'center', paddingTop: '15vh'}}>
        <img src={require('../../images/forstaff_loading.gif')} alt={"Loading ..."} />
    </div>
);

export default Loader;
