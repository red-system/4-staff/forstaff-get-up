import React, { Component } from 'react';
import {Layout} from "antd";
import './styles.scss';
import GENERALDATA from "../../constants/generalData";
import renderHTML from 'react-render-html';

const {Footer} = Layout;
let appBrandHtml = GENERALDATA.appBrandHtml;

class AppFooter extends Component {

    render() {
        let newDate = new Date();
        const year = newDate.getFullYear();

        return(
            <Footer style={{ textAlign: 'center' }}>
                {renderHTML(appBrandHtml)} ©{year} Created by <a href="http://www.redsystem.id" target="_blank" rel="noopener noreferrer"> PT. Guna Teknologi Nusantara</a>
            </Footer>
        );
    }

}

export default AppFooter;