import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Loadable from "react-loadable";
import Loading from "../components/Loading";
import GeneralData from "../constants/generalData";
import NotificationRedirect from '../components/NotificationRedirect';

const delay = 0;

const AsyncAppLayout = Loadable({
    loader: () => import('./AppLayout'),
    loading: Loading,
    delay: delay
});
// const AsyncLayoutStaff = Loadable({
//     loader: () => import('./LayoutStaff'),
//     loading: Loading
// });
const AsyncLogin = Loadable({
    loader: () => import('./Login'),
    loading: Loading,
    delay: delay
});
const AsyncSignup = Loadable({
    loader: () => import('./Signup'),
    loading: Loading,
    delay: delay
});
const AsyncForgotPassword = Loadable({
    loader: () => import('./ForgotPassword'),
    loading: Loading,
    delay: delay
});
const AsyncRegisterStaff = Loadable({
    loader: () => import('./RegisterStaff'),
    loading: Loading,
    delay: delay
});



class Index extends Component {

    render() {

        let appUriUniq = GeneralData.appUriUniq;

        return (
            <Router>
                <Switch>
                    <Route path={`/${appUriUniq}`} component={AsyncAppLayout}/>
                    {/*<Route path='/staff' component={AsyncLayoutStaff}/>*/}
                    <Route path='/sign-up' component={AsyncSignup}/>
                    <Route path='/forgot-password' component={AsyncForgotPassword}/>
                    <Route path='/register' component={AsyncRegisterStaff}/>
                    <Route path='/logout' render={() => {localStorage.clear(); window.location.href = '../'}} />
                    <Route path='/notification-redirect/:notification_id/:notification_type' component={NotificationRedirect} />
                    <Route path='/' component={AsyncLogin}/>
                </Switch>
            </Router>
        )
    }


}

export default Index;