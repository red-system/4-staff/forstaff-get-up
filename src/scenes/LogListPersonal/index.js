import React, {Component} from "react";
import {Icon, Spin} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';
import {GetData} from "../../services/api";

import '../../styles/ui/_card-icon.scss';

let LOG_LIST_PERSONAL = GENERALDATA.primaryMenu.logListPersonal;

const breadcrumb = [
    {
        label: LOG_LIST_PERSONAL.label,
        route: `${LOG_LIST_PERSONAL.route}`
    },
];

export default class LogListPersonal extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: {},
            isLoading: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });

        GetData('/log/activity/personal').then((result) => {
            let data = result.data;
            this.setState({
                data: data.data,
                isLoading: false
            });
        });
    }

    render() {

        let {data, isLoading} = this.state;

        const dataLog = Object.keys(data).map(function (key) {

            return (
                <div key={key}>
                    <article className="tl-item">
                        <div className="tl-body">
                            <div className="tl-entry">
                                <div className="tl-caption">
                                    <button className="btn btn-primary btn-block"><Icon type={"calendar"}/> {key}</button>
                                </div>
                            </div>
                        </div>
                    </article>

                    {
                        Object.keys(data[key]).map(function(key2) {

                            let data_level1 = data[key];
                            let data_level2 = data_level1[key2];
                            let iconColor = '',
                                iconType = '',
                                titleColor = '';

                            switch(data_level2['log_activity_type']) {
                                case "get":
                                    iconColor = "btn-info";
                                    iconType = "ordered-list";
                                    titleColor = "text-info";
                                    break;
                                case "insert":
                                    iconColor = "btn-primary";
                                    iconType = "save";
                                    titleColor = "text-primary";
                                    break;
                                case "update":
                                    iconColor = "btn-success";
                                    iconType = "cloud-upload";
                                    titleColor = "text-success";
                                    break;
                                case "delete":
                                    iconColor = "btn-danger";
                                    iconType = "close";
                                    titleColor = "text-danger";
                                    break;
                            }

                            return (
                                <article className="tl-item" key={key}>
                                    <div className="tl-body">
                                        <div className="tl-entry">
                                            <div className="tl-time">{data_level2['created_at']}</div>
                                            <div className={`tl-icon icon-btn-round icon-btn icon-btn-thin ${iconColor}`}><Icon
                                                type={iconType}/></div>
                                            <div className="tl-content">
                                                <h4 className={`tl-title ${titleColor}`}>{`${data_level2['log_activity_title']}`} - <small>({data_level2['menu_label']})</small></h4>
                                                <p>{data_level2['log_activity_description']}</p>

                                                <small><Icon type={"user"}/> - {data_level2['staff_name']}</small>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            );

                        })
                    }

                </div>
            )
        });


        return (
            <div>
                {this.state.breadcrumbComponent}

                <Spin tip={"Loading ..."} spinning={isLoading}>
                    <div className="container-fluid container-mw-md py-3">
                        <section className="ui-timeline ui-timline-left">


                            {dataLog}


                        </section>
                    </div>
                </Spin>

            </div>
        )

    }

}