import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Row, Col, Radio, Select, message} from "antd";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";


const { Option } = Select;

class Create extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            staff: [],
            rewardGuide: [],

            reward_guide_id: '',
            staff_id: '',
            reward_publish: 'yes'
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            staff: nextProps.staff,
            rewardGuide: nextProps.rewardGuide
        });
    }

    render() {

        const {modalVisible, isLoading, staff, rewardGuide} = this.state;
        let menuCreate = menuActionViewAccess('sop_management', 'reward', 'create_reward_staff');

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Staff"
                            {...this.state.formError.staff_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Staf"
                                optionFilterProp="children"
                                defaultValue={this.state.staff_id}
                                onChange={this.onChange('staff_id')}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    staff.map((item, key) => {
                                        return <Option value={item["staff_id"]} key={key}>{item['staff_name']}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Kinerja"
                            {...this.state.formError.reward_guide_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Kinerja"
                                optionFilterProp="children"
                                defaultValue={this.state.reward_guide_id}
                                onChange={this.onChange('reward_guide_id')}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    rewardGuide.map((item, key) => {
                                        return <Option value={item["reward_guide_id"]} key={key}>{item['rewardg_title']}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Publish"
                            {...this.state.formError.reward_publish}>
                            <Radio.Group defaultValue={this.state.reward_publish} onChange={this.onChange('reward_publish')} buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={isLoading} onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };

    onChange = name => value => {
        if(typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            [name]: value
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });
        PostData('/reward', this.state)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        staff_id:null,
                        reward_guide_id:null,
                        reward_publish: null
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

export default Create;