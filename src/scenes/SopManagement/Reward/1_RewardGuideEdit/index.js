import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, Modal, Row, Radio, message} from "antd";
import {PostData, PutData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        let {record} = this.props;
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            isLoading: false,
            formError: [],

            reward_guide_id: record.key,
            rewardg_title: record.rewardg_title,
            rewardg_description: record.rewardg_description,
            rewardg_publish: record.rewardg_publish
        };
    }

    render() {
        const {modalVisible, isLoading} = this.state;
        const {rewardg_title, rewardg_description, rewardg_publish} = this.state;
        const {rewardg_title_error, rewardg_description_error, rewardg_publish_error} = this.state.formError;

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Kinerja"
                        {...rewardg_title_error}>
                        <Input defaultValue={rewardg_title} onChange={this.onChange('rewardg_title')}/>
                    </Form.Item>
                    <Form.Item
                        label="Penghargaan"
                        {...rewardg_description_error}>
                        <Input defaultValue={rewardg_description} onChange={this.onChange('rewardg_description')}/>
                    </Form.Item>
                    <Form.Item
                        label="Publish"
                        {...rewardg_publish_error}>
                        <Radio.Group defaultValue={rewardg_publish} onChange={this.onChange('rewardg_publish')}
                                     buttonStyle="solid">
                            <Radio.Button value="yes">Ya</Radio.Button>
                            <Radio.Button value="no">Tidak</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={isLoading}
                                onClick={this.update}
                        >
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }


    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            [name]: value
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };

    update = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PostData(`/reward-guide/${this.state.reward_guide_id}`, this.state)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}_error`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });
        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;