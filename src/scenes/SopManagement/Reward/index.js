import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import RewardGuide from "./1_RewardGuide";
import Reward from "./2_RewardData";
import {menuActionViewAccess} from "../../../services/app/General";

class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            tabChange: false,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        let menuListReward = menuActionViewAccess('sop_management', 'reward', 'list_reward');
        let menuListRewardGuide = menuActionViewAccess('sop_management', 'reward', 'list_reward_staff');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="panduan-reward">
                                <TabPane
                                    className={menuListReward.class}
                                        tab={<span><Icon type="ordered-list" /> Panduan Penghargaan</span>}
                                         key="panduan-reward">
                                    <RewardGuide {...this.state}/>
                                </TabPane>
                                <TabPane
                                    className={menuListRewardGuide.class}
                                    tab={<span><Icon type="gift" /> Penghargaan untuk Staf</span>} key="reward">
                                    <Reward {...this.state} />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    };

    handleChange = activeKey => {
        this.setState({
            activeKey,
            tabChange: true
        });
    };

}

let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["6"];
let pageParent = GENERALDATA.primaryMenu.sop_management;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default RewardTabs;