import React, {Component} from 'react';

import Create from '../2_RewardDataCreate';
import List from '../2_RewardDataList';
import {GetData} from "../../../../services/api";


class RewardGuide extends Component {

    constructor(props) {
        super(props);
        this.state = {
            staff: [],
            rewardGuide: [],
            listRefresh: props.listRefresh,
            listRefreshRun: props.listRefreshRun
        }
    }

    async componentWillMount() {
        await GetData('/staff').then((result) => {
            this.setState({
                staff: result.data.data
            })
        });

        await GetData('/reward-guide').then((result) => {
            this.setState({
                rewardGuide: result.data.data
            })
        });
    }

    render() {

        return (
            <div>
                <Create {...this.state}/>
                <List {...this.state}/>
            </div>
        )
    }

}

export default RewardGuide;