import React, { Component } from "react";
import { Row, Col, Spin } from "antd";
import { GetData } from "../../../../services/api";

import OrgChart from "react-orgchart";
import "react-orgchart/index.css";
import { menuActionViewAccess } from "../../../../services/app/General";

class Overview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: [],
        };
    }

    componentWillMount() {
        this.data();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.data();
        }
    }

    render() {
        let menuPreview = menuActionViewAccess("my_company", "organization_structure", "preview");

        const { data } = this.state;

        const MyNodeComponent = ({ node }) => {
            if (node.staff_name === null) {
                return (
                    <div className="initechNode">
                        <Row>
                            <Col span={24}>
                                <div className="job-title">{node.structure_name}</div>
                            </Col>
                        </Row>
                    </div>
                );
            } else {
                return (
                    <div className="initechNode">
                        <Row>
                            <Col span={24}>
                                <div className="job-title">{node.structure_name}</div>
                                <div className="job-desc">{node.staff_name}</div>
                            </Col>
                        </Row>
                    </div>
                );
            }
        };

        let render = null;
        if (data.length !== 0) {
            render = (
                <div className="overview-wrapper">
                    <Spin spinning={this.state.isLoading} tip={"Loading ..."} className={menuPreview.class}>
                        <OrgChart tree={data} NodeComponent={MyNodeComponent} />
                    </Spin>
                </div>
            );
        }

        return render;
    }

    data() {
        GetData("/structure-staff-overview").then((result) => {
            const json = result.data;

            this.setState({
                data: json.data,
                isLoading: false,
            });
        });
    }
}

export default Overview;
