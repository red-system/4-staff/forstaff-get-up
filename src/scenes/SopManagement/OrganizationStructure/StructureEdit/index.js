import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, message, Select } from "antd";
import {PostData, PutData} from "../../../../services/api";


export default class Edit extends Component {

    constructor(props) {
        super(props);

        let department_id = props.node.department_id === 0 ? '': props.node.department_id;
        let {node} = props;

        this.state = {
            isLoading: false,
            modalVisible: false,
            formError: [],
            formValue: {
                department_id: node.department_id,
                structure_parent_id: node.structure_parent_id,
                structure_name: node.structure_name,
                structure_description: node.structure_description,
                structure_worktime: node.structure_worktime,
                structure_type: node.structure_type
            },
            listDepartment: props.listDepartment
        };

        this.update = this.update.bind(this);
    }

    render() {
        let department;

        if (this.state.formValue.structure_parent_id === "1" && this.state.formValue.structure_type === 'structure') {
            department = (
                <Form.Item
                    label="Nama Departemen"
                    {...this.state.formError.department_id}>
                    <Select
                        showSearch
                        placeholder="Pilih Departemen"
                        optionFilterProp="children"
                        defaultValue={this.state.formValue.department_id}
                        onChange={this.onChange('department_id')}
                        filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        } >
                        {
                            this.state.listDepartment.map((item, key) => {
                                return (
                                    <Select.Option value={item.department_id} key={key}>{item.department_title}</Select.Option>
                                )
                            })
                        }
                    </Select>
                </Form.Item>
            )
        }


        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)} icon={"edit"} size={"small"}>
                    Edit
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout} onSubmit={this.update}>
                        {department}
                        <Form.Item
                            label="Nama Posisi"
                            {...this.state.formError.structure_name}>
                            <Input onChange={this.onChange("structure_name")}
                                   value={this.state.formValue.structure_name}/>
                        </Form.Item>
                        <Form.Item
                            label="Struktur Work Time"
                            {...this.state.formError.structure_worktime}>
                            <Select
                                showSearch
                                placeholder="Pilih Struktur Worktime"
                                optionFilterProp="children"
                                defaultValue={this.state.formValue.structure_worktime}
                                onChange={this.onChange("structure_worktime")}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                <Select.Option key="normal" value="normal">Normal</Select.Option>
                                <Select.Option key="shift" value="shift">Shift</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Keterangan" {...this.state.formError.structure_description}>
                            <Input.TextArea onChange={this.onChange("structure_description")}
                                   value={this.state.formValue.structure_description}/>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType="submit" icon={"check"}
                                    loading={this.state.isLoading} onClick={this.update}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </span>
        )
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value
        });
    };

    update(e) {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PutData(`/structure/${this.props.node.structure_id}`, this.state.formValue)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }


}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 7},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 17},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};