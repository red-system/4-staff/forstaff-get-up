import React, {Component} from 'react';
import {Button, Table, Spin} from "antd";
import {GetData} from "../../../../services/api";

import Create from '../DepartmentDataCreate';
import Edit from '../DepartmentDataEdit';
import Delete from '../DepartmentDataDelete';
import {menuActionViewAccess} from "../../../../services/app/General";

class DepartmentData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            parent: [],
            isLoading: true
        };

        this.list = this.list.bind(this);
    }

    componentWillMount() {
        this.list()
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    render() {

        let menuList = menuActionViewAccess('sop_management', 'organization_structure', 'list_department');

        const columns = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Nama Departmen', dataIndex: 'department_title', key: 'department_title'},
            {title: 'Deskripsi Departemen', dataIndex: 'department_description', key: 'department_description'},
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', width: 200, render: (text, record) => {
                    return (
                        <Button.Group size={"small"}>
                            <Edit listRefreshRun={this.props.listRefreshRun}
                                  record={record}/>
                            <Delete record={record}
                                    listRefreshRun={this.props.listRefreshRun}/>
                        </Button.Group>
                    )
                }
            }
        ];
        const data_list = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            };
        });


        return (
            <Spin spinning={this.state.isLoading} tip={"Loading ..."}>
                <Create
                    listRefresh={this.props.listRefresh}
                    listRefreshRun={this.props.listRefreshRun}/>
                <Table className={menuList.class}
                       columns={columns}
                       bordered={true}
                       dataSource={data_list}
                       isLoading={this.state.isLoading}
                       size='small'/>
            </Spin>
        )
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData('/department').then(res => {
            const data = res.data.data;
            this.setState({
                data: data,
                isLoading: false
            });
        })
    }
}

export default DepartmentData;