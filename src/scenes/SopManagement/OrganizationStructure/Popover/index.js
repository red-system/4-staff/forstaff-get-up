import React, {Component} from 'react';
import {Row, Col, Spin, Popover, Button, Alert} from "antd";
import {GetData} from "../../../../services/api";

import OrgChart from 'react-orgchart';
import 'react-orgchart/index.css';

import StructureCreate from "../StructureCreate";
import StructureEdit from "../StructureEdit";
import StructureDelete from "../StructureDelete";
import StructureListStaff from "../StructureListStaff";
import {menuActionViewAccess} from "../../../../services/app/General";

class Popover extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: [],
            listDepartment: [],
            visible: []
        };
    }


    componentWillMount() {
        this.data();
        this.listDepartment();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.data();
        }
    }

    render() {
        const {data} = this.state;
        let render = null;

        let menuListPosition = menuActionViewAccess('sop_management', 'organization_structure', 'list_position');
        let menuCreatePosition = menuActionViewAccess('sop_management', 'organization_structure', 'create_position');
        let menuEditPosition = menuActionViewAccess('sop_management', 'organization_structure', 'edit_position');
        let menuListStaffPosition = menuActionViewAccess('sop_management', 'organization_structure', 'list_staff_position');
        let menuDeletePosition = menuActionViewAccess('sop_management', 'organization_structure', 'delete_position');

        return (
            <div className="initechNode-posisi">
                <Popover content={this.content(node, menuCreatePosition, menuListStaffPosition, menuEditPosition, menuDeletePosition)}
                         title="" trigger="hover" onVisibleChange={this.handleVisibleChange}>
                    <Row>
                        <Col span={24}>
                            <div className="job-title">
                                {node.structure_name}
                            </div>
                        </Col>
                    </Row>
                </Popover>
            </div>
        );
    }

    hidePopover = () => {
        this.setState({
            visible: false,
        });
    };

    handleVisibleChange = ({id, visible}) => {
        this.setState({ visible });
    };

    data() {
        GetData('/structure-overview').then((result) => {
            const json = result.data;

            this.setState({
                data: json.data,
                isLoading: false,
            });
        });
    }

    listDepartment() {
        GetData('/department').then((result) => {
            this.setState({
                listDepartment: result.data.data
            })
        })
    }

    content = (node, menuCreatePosition, menuListStaffPosition, menuEditPosition, menuDeletePosition) => {
        let btn_delete;

        if (node.structure_type !== 'ceo') {
            btn_delete = (
                <span className={menuDeletePosition.class}>
                    <StructureDelete
                        node={node}
                        listDepartment={this.state.listDepartment}
                        listRefreshRun={this.props.listRefreshRun}
                        onClick={this.hidePopover}
                    />
                    </span>
            );
        }


        return (
            <div>
                <span className={menuCreatePosition.class}>
                    <StructureCreate
                        node={node}
                        listDepartment={this.state.listDepartment}
                        listRefreshRun={this.props.listRefreshRun}
                        label={"Tambah Sub"}
                        onClick={this.hidePopover}
                    />
                </span>
                &nbsp;
                <span className={menuListStaffPosition.class}>
                    <StructureListStaff
                        className={menuListStaffPosition.class}
                        node={node}
                        listDepartment={this.state.listDepartment}
                        listRefreshRun={this.props.listRefreshRun}
                        onClick={this.hidePopover}
                    />
                </span>
                &nbsp;
                <span className={menuEditPosition.class}>
                    <StructureEdit
                        className={menuEditPosition.class}
                        node={node}
                        listDepartment={this.state.listDepartment}
                        listRefreshRun={this.props.listRefreshRun}/>
                </span>
                &nbsp;
                {btn_delete}
            </div>
        )
    };

}

export default Popover;