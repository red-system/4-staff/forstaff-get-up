import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal,  message} from "antd";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

const {TextArea} = Input;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: {
                rulesr_title: '',
                rulesr_decsription: '',
                rulesr_filename: ''
            }
        };
    }

    render() {

        let menuCreate = menuActionViewAccess('sop_management', 'rules_regulation', 'create');

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul"
                            {...this.state.formError.rulesr_title}>
                            <Input defaultValue={this.state.formValue.rulesr_title} onChange={this.onChange("rulesr_title")}/>
                        </Form.Item>
                        <Form.Item
                            label="Deksripsi"
                            {...this.state.formError.rulesr_description}>
                            <TextArea defaultValue={this.state.formValue.rulesr_description} onChange={this.onChange("rulesr_description")}/>
                        </Form.Item>
                        <Form.Item
                            label="File Deksripsi Pekerjaan"
                            {...this.state.formError.rulesr_filename}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                rulesr_filename: event.target.files[0]
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        formData.append('rulesr_title', this.state.formValue.rulesr_title);
        formData.append('rulesr_description', this.state.formValue.rulesr_description);
        formData.append('rulesr_filename', this.state.formValue.rulesr_filename);

        PostData('/rules-regulation', formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;