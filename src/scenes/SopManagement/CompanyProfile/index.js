import React, {Component} from "react";
import {
    Row, Col, Icon, Button, Input, Form,
    Select, DatePicker, Spin, message
} from 'antd';

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {Link} from 'react-router-dom';
import {PostData, GetData} from "../../../services/api";
import moment from 'moment';
import {
    dateFormatApp,
    dateFormatApi,
    dateFormatterApp,
    dateFormatterApi,
    menuActionViewAccess
} from '../../../services/app/General';

import '../../../styles/ui/_card-icon.scss';

const FormItem = Form.Item;
const {Option} = Select;
const {TextArea} = Input;

class ConfigHrd extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            isLoadingDistrict: false,
            form_error: {},
            province: [],
            district: [],

            company_name: '',
            company_inisial: '',
            company_logo: null,
            company_logo_url: '',
            company_logo_banner: null,
            company_logo_banner_url: '',
            company_banner: null,
            company_banner_url: '',
            company_website: '',
            company_description: '',
            company_found_date: '1991-12-11',
            company_telp_1: '',
            company_telp_2: '',
            company_fax: '',
            company_email_1: '',
            company_email_2: '',
            province_id: '',
            district_id: '',
            company_address: '',
            company_map_iframe: ''

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.handleChange3 = this.handleChange3.bind(this);
        this.onChange1 = this.onChange1.bind(this);
        this.onChange2 = this.onChange2.bind(this);
        this.update = this.update.bind(this);
    }

    async componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });

        const data = await GetData('/company-profile').then((result) => {
            let data = result.data.data;
            return data;
        });

        const companyProfile = data['company'];
        const province = data['province'];
        const district = data['district'];

        this.setState({
            isLoading: false,
            company_name: companyProfile['company_name'],
            company_inisial: companyProfile['company_inisial'],
            //company_logo: companyProfile['company_logo'],
            company_logo_url: companyProfile['company_logo_url'],
            company_banner_url: companyProfile['company_banner_url'],
            company_logo_banner_url: companyProfile['company_logo_banner_url'],
            company_website: companyProfile['company_website'],
            company_description: companyProfile['company_description'],
            company_found_date: companyProfile['company_found_date'],
            company_telp_1: companyProfile['company_telp_1'],
            company_telp_2: companyProfile['company_telp_2'],
            company_fax: companyProfile['company_fax'],
            company_email_1: companyProfile['company_email_1'],
            company_email_2: companyProfile['company_email_2'],
            province_id: companyProfile['province_id'],
            district_id: companyProfile['district_id'],
            company_address: companyProfile['company_address'],
            company_map_iframe: companyProfile['company_map_iframe'],
        });

        this.setState({
            province: province,
            district: district,
        });
    }

    render() {

        const {
            isLoading,
            isLoadingDistrict,
            province,
            district,

            company_name,
            company_inisial,
            company_logo,
            company_logo_url,
            company_banner,
            company_banner_url,
            company_logo_banner_url,
            company_website,
            company_description,
            company_found_date,
            company_telp_1,
            company_telp_2,
            company_fax,
            company_email_1,
            company_email_2,
            province_id,
            district_id,
            company_address,
            company_map_iframe,
        } = this.state;

        const {

            company_name_field,
            company_inisial_field,
            company_logo_field,
            company_banner_field,
            company_logo_banner_field,
            company_website_field,
            company_description_field,
            company_found_date_field,
            company_telp_1_field,
            company_telp_2_field,
            company_fax_field,
            company_email_1_field,
            company_email_2_field,
            province_id_field,
            district_id_field,
            company_address_field,
            company_map_iframe_field
        } = this.state.form_error;

        let menuList = menuActionViewAccess('sop_management', 'company_profile', 'list');
        let menuUpdate = menuActionViewAccess('sop_management', 'company_profile', 'update');

        return (
            <Spin tip={"Loading ..."} spinning={isLoading} className={menuList.class}>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Form onSubmit={this.update}>
                                    <FormItem
                                        {...formItemLayout}
                                        {...company_name_field}
                                        label="Nama Perusahaan"
                                        required={true}
                                    >
                                        <Input value={company_name} onChange={this.onChange2("company_name")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        {...company_inisial_field}
                                        label="Inisial Perusahaan"
                                        required={true}
                                    >
                                        <Input value={company_inisial} onChange={this.onChange2("company_inisial")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_logo_field}
                                        label="Logo Perusahaan"
                                        required={true}
                                    >
                                        <img src={company_logo_url} className={"image-preview"}/>
                                        <br/>
                                        <div className="upload-btn-wrapper">
                                            <button className="btn-file">Browse logo</button>
                                            <input type="file" name="myfile" onChange={this.handleChange}/>
                                        </div>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_banner_field}
                                        label="Banner Perusahaan"
                                        required={true}
                                    >
                                        <img src={company_banner_url} className={"image-preview"}/>
                                        <br/>
                                        <div className="upload-btn-wrapper">
                                            <button className="btn-file">Browse banner</button>
                                            <input type="file" name="myfile" onChange={this.handleChange2}/>
                                        </div>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_logo_banner_field}
                                        label="Banner Perusahaan"
                                        required={true}
                                    >
                                        <img src={company_logo_banner_url} className={"image-preview"}/>
                                        <br/>
                                        <div className="upload-btn-wrapper">
                                            <button className="btn-file">Browse banner logo</button>
                                            <input type="file" name="myfile" onChange={this.handleChange3}/>
                                        </div>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_website_field}
                                        required={true}
                                        label="Website"
                                    >
                                        <Input value={company_website}
                                               onChange={this.onChange2("company_website")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        {...company_description_field}
                                        label="Deskripsi Perusahaan"
                                        required={true}
                                    >
                                        <TextArea value={company_description}
                                                  onChange={this.onChange2("company_description")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_found_date_field}
                                        label="Tanggal Perusahaan Berdiri"
                                        required={true}
                                    >
                                        <DatePicker key={company_found_date}
                                                    value={moment(dateFormatterApp(company_found_date), dateFormatApp())}
                                                    onChange={this.onChange1("company_found_date")}
                                                    format={dateFormatApp()}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_telp_1_field}
                                        label="No Telepon 1"
                                        required={true}
                                    >
                                        <Input value={company_telp_1} onChange={this.onChange2("company_telp_1")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_telp_2_field}
                                        label="No Telepon 2"
                                    >
                                        <Input value={company_telp_2} onChange={this.onChange2("company_telp_2")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_fax_field}
                                        label="No Fax"
                                    >
                                        <Input value={company_fax} onChange={this.onChange2("company_fax")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_email_1_field}
                                        label="Email 1"
                                        required={true}
                                    >
                                        <Input value={company_email_1} onChange={this.onChange2("company_email_1")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...company_email_2_field}
                                        label="Email 2"
                                    >
                                        <Input value={company_email_2} onChange={this.onChange2("company_email_2")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...province_id_field}
                                        label="Provinsi"
                                        required={true}
                                    >
                                        <Select
                                            showSearch
                                            style={{width: 200}}
                                            placeholder="Pilih Provinsi"
                                            optionFilterProp="children"
                                            defaultValue={province_id}
                                            onChange={this.onChange1("province_id")}
                                            key={province_id}
                                            filterOption={(input, option) =>
                                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }
                                        >
                                            {
                                                Object.keys(province).map((key) => {
                                                    return (
                                                        <Option value={province[key]['province_id']}
                                                                key={key}>{province[key]['name']}</Option>
                                                    );
                                                })
                                            }
                                        </Select>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_upload}
                                        {...district_id_field}
                                        label="Kabupaten"
                                        required={true}
                                    >
                                        <Select
                                            showSearch
                                            style={{width: 200}}
                                            placeholder="Pilih Kabupaten"
                                            optionFilterProp="children"
                                            defaultValue={district_id}
                                            key={district_id}
                                            onChange={this.onChange1("district_id")}
                                            loading={isLoadingDistrict}
                                            filterOption={(input, option) =>
                                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }
                                        >
                                            {
                                                Object.keys(district).map((key) => {
                                                    return (
                                                        <Option value={district[key]['district_id']}
                                                                key={key}>{district[key]['name']}</Option>
                                                    );
                                                })
                                            }
                                        </Select>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        {...company_address_field}
                                        label="Alamat Perusahaan"
                                        required={true}
                                    >
                                        <TextArea value={company_address} onChange={this.onChange2("company_address")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayoutTextArea}
                                        {...company_map_iframe_field}
                                        label="Posisi Peta Perusahaan (iframe)"
                                    >
                                        <TextArea value={company_map_iframe}
                                                  onChange={this.onChange2("company_map_iframe")}/>
                                    </FormItem>
                                    <Form.Item {...tailFormItemLayout}>
                                        <Button className={menuUpdate.class} type="primary" htmlType="submit" onClick={this.update} icon={"check"}
                                                loading={isLoading}>
                                            Perbarui Data
                                        </Button>
                                        &nbsp;
                                        <Link to={`../${pageParent.route}`}>
                                            <Button type="warning" htmlType="submit" icon={"rollback"}>
                                                Kembali
                                            </Button>
                                        </Link>
                                    </Form.Item>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Spin>
        )
    }

    handleChange(event) {
        this.setState({
            company_logo_url: URL.createObjectURL(event.target.files[0]),
            company_logo: event.target.files[0]
        })
    }
    handleChange2(event) {
        this.setState({
            company_banner_url: URL.createObjectURL(event.target.files[0]),
            company_banner: event.target.files[0]
        })
    }
    handleChange3(event) {
        this.setState({
            company_logo_banner_url: URL.createObjectURL(event.target.files[0]),
            company_logo_banner: event.target.files[0]
        })
    }

    onChange1 = name => value => {

        if (name === 'province_id') {
            this.setState({
                isLoadingDistrict: true,
                district_id: ''
            });
            PostData('/district', {province_id: value}).then((result) => {
                const data = result.data;
                this.setState({
                    district: data.data,
                    district_id: data.data[0]['district_id'],
                    isLoadingDistrict: false
                });
            });
        } else if(name === 'company_found_date') {
            value = value.format(dateFormatApi());
        }

        this.setState({
            [name]: value
        });
    };

    onChange2 = name => e => {
        this.setState({
            [name]: e.target.value
        });
    };

    update(e) {
        e.preventDefault();
        const {
            company_name,
            company_inisial,
            company_logo,
            company_banner,
            company_logo_banner,
            company_website,
            company_description,
            company_found_date,
            company_telp_1,
            company_telp_2,
            company_fax,
            company_email_1,
            company_email_2,
            province_id,
            district_id,
            company_address,
            company_map_iframe
        } = this.state;

        this.setState({
            isLoading: true,
            form_error: {}
        });

        const formData = new FormData();
        formData.append('company_name', company_name);
        formData.append('company_inisial', company_inisial);
        formData.append('company_website', company_website);
        formData.append('company_description', company_description);
        formData.append('company_found_date', dateFormatterApi(company_found_date));
        formData.append('company_telp_1', company_telp_1);
        formData.append('company_telp_2', company_telp_2);
        formData.append('company_fax', company_fax);
        formData.append('company_email_1', company_email_1);
        formData.append('company_email_2', company_email_2);
        formData.append('province_id', province_id);
        formData.append('district_id', district_id);
        formData.append('company_address', company_address);
        formData.append('company_map_iframe', company_map_iframe);
        if(company_logo) {
            formData.append('company_logo', company_logo);
        }
        if(company_banner) {
            formData.append('company_banner', company_banner);
        }
        if(company_logo_banner) {
            formData.append('company_logo_banner', company_logo_banner);
        }

        PostData('/company-profile', formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false
                });
            } else {
                let errors = data.errors;
                let form_error = [];
                message.error(data.message);

                Object.keys(errors).map(function (key) {
                    form_error[`${key}_field`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                console.log(form_error);

                this.setState({
                    form_error: form_error,
                    isLoading: false
                });
            }
        });

        return false;
    }

}

export default ConfigHrd;



let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["2"];
let pageParent = GENERALDATA.primaryMenu.sop_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayoutTextArea = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayout_upload = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 12,
            offset: 6,
        },
    },
};

const dateFormat = dateFormatApp();