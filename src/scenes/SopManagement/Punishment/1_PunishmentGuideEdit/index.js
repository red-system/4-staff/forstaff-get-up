import React, { Component } from "react";
import { Button, Form, Icon, Input, Modal, Radio, message } from "antd";
import { PostData, PutData } from "../../../../services/api";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
        };
    }

    render() {
        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit" />
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data
                    </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={false}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Pelanggaran" {...this.state.formError.punishmentg_title}>
                        <Input defaultValue={this.state.formValue.punishmentg_title} onChange={this.onChange("punishmentg_title")} />
                    </Form.Item>
                    <Form.Item label="Punishment" {...this.state.formError.punishmentg_description}>
                        <Input defaultValue={this.state.formValue.punishmentg_description} onChange={this.onChange("punishmentg_description")} />
                    </Form.Item>
                    <Form.Item label="Publish" {...this.state.formError.punishmentg_publish}>
                        <Radio.Group
                            defaultValue={this.state.formValue.punishmentg_publish}
                            onChange={this.onChange("punishmentg_publish")}
                            buttonStyle="solid"
                        >
                            <Radio.Button value="yes">Ya</Radio.Button>
                            <Radio.Button value="no">Tidak</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.update}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
                formValue: [],
            });
        }
    };

    update = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData(`/punishment-guide/${this.state.formValue.key}`, this.state.formValue).then((result) => {
            const data = result.data;

            if (data.status === "success") {
                message.success(data.message);
                this.props.listRefreshRun(true);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[key] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;
