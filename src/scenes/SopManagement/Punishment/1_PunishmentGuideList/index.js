import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Create from "../1_PunishmentGuideCreate";
import Edit from "../1_PunishmentGuideEdit";
import Delete from "../1_PunishmentGuideDelete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class PunishmentGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listRefresh: props.listRefresh,
            listRefreshRun: props.listRefreshRun,
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var punishmentTitle = false;
            if (get(record, "punishmentg_title") != null) {
                punishmentTitle = get(record, "punishmentg_title").toString().match(reg);
            }
            var punishmentDescription = false;
            if (get(record, "punishmentg_description") != null) {
                punishmentDescription = get(record, "punishmentg_description").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!punishmentTitle && !punishmentDescription && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuCreate = menuActionViewAccess("sop_management", "punishment", "create_punishment");
        let menuEdit = menuActionViewAccess("sop_management", "punishment", "edit_punishment");
        let menuDelete = menuActionViewAccess("sop_management", "punishment", "delete_punishment");
        let menuList = menuActionViewAccess("sop_management", "punishment", "list_punishment");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Pelanggaran", dataIndex: "punishmentg_title", key: "punishmentg_title" },
            { title: "Keterangan", dataIndex: "punishmentg_description", key: "punishmentg_description" },
            {
                title: "Publish",
                dataIndex: "punishmentg_publish",
                key: "punishmentg_publish",
                render: (text, record) => {
                    return status_view(record.punishmentg_publish);
                },
            },
            { title: "Waktu Permbaruan", dataIndex: "updated_at", key: "updated_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} {...this.state} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} {...this.state} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["punishment_guide_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <span className={menuCreate.class}>
                    <Create {...this.state} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} size="small" bordered={true} loading={this.state.isLoading} />
            </div>
        );
    }

    list() {
        GetData("/punishment-guide").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default PunishmentGuide;
