import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import PunishmentGuide from "./1_PunishmentGuideList";
import Punishment from "./2_PunishmentDataList";
import {menuActionViewAccess} from "../../../services/app/General";

let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["7"];
let pageParent = GENERALDATA.primaryMenu.sop_management;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {
        let menuListPunishment = menuActionViewAccess('sop_management', 'punishment', 'list_punishment');
        let menuListPunishmentStaff = menuActionViewAccess('sop_management', 'punishment', 'list_punishment_staff');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="panduan-punishment">
                                <TabPane
                                    className={menuListPunishment.class}
                                        tab={<span><Icon type="ordered-list" /> Panduan Sanksi</span>}
                                         key="panduan-punishment">
                                    <PunishmentGuide {...this.state} />
                                </TabPane>
                                <TabPane
                                    className={menuListPunishmentStaff.class}
                                    tab={<span><Icon type="thunderbolt" /> Sanksi untuk Staf</span>} key="punishment">
                                    <Punishment {...this.state} />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    };
}

export default RewardTabs;