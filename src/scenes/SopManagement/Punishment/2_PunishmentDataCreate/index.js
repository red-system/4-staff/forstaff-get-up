import React, {Component} from 'react';
import {Button, Form, Icon, DatePicker, Modal, Radio, Select, message} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatterApi, dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";


const { Option } = Select;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: {
                punishment_date: dateToDay()
            }
        }
    }

    render() {

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Staff"
                            {...this.state.formError.staff_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Staf"
                                optionFilterProp="children"
                                defaultValue={this.state.formValue.staff_id}
                                onChange={this.onChange('staff_id')}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    this.props.dataStaff.map((item, key) => {
                                        return <Option value={item['staff_id']} key={key}>{item['staff_name']}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Tanggal"
                            {...this.state.formError.punishment_date}>
                            <DatePicker
                                style={{width: "100%"}}
                                value={moment(dateFormatterApp(this.state.formValue.punishment_date), dateFormatApp())}
                                format={dateFormatApp()}
                                onChange={this.onChange("punishment_date")} />
                        </Form.Item>
                        <Form.Item
                            label="Pelanggaran"
                            {...this.state.formError.punishment_guide_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Kinerja"
                                optionFilterProp="children"
                                defaultValue={this.state.formValue.punishment_guide_id}
                                onChange={this.onChange('punishment_guide_id')}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    this.props.dataPunishmentGuide.map((item, key) => {
                                        return <Option value={item['punishment_guide_id']} key={key}>{item['punishmentg_title']}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Publish"
                            {...this.state.formError.punishment_publish}>
                            <Radio.Group defaultValue={this.state.formValue.punishment_publish} onChange={this.onChange('punishment_publish')} buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    handleDatePickerChange = (date, dateString, id) => {
        this.setState({
            formValue: {
                punishment_date: dateFormatterApi(dateString)
            }
        });
    };

    onChange = name => value => {
        if (typeof value == 'object' && name != 'punishment_date') {
            value = value.target.value;
        }
        if(name == 'punishment_date') {
            value = value.format(dateFormatApi());
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
                formValue: [],
            })
        }
    };

    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });

        PostData('/punishment', this.state.formValue)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        formError: []
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[key] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });
        return false;
    }

}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;