import React, {Component} from "react";
import {Row, Col} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';
import {Link} from 'react-router-dom';

import '../../styles/ui/_card-icon.scss';
import {menuViewAccess} from "../../services/app/General";

let menuSopManagement = GENERALDATA.primaryMenu.sop_management;
let subSopManagement = GENERALDATA.primaryMenu.sop_management.sub_menu;
let gutter = GENERALDATA.gutter;

const menuGrid = {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 3
};

const breadcrumb = [
    {
        label: menuSopManagement.label,
        route: `${menuSopManagement.route}`
    },
];

class SopManagement extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="-1"/>)]
        })
    }

    render() {

        const menuList = Object.keys(subSopManagement).map(function (key) {
            let menuView = menuViewAccess(menuSopManagement.variable, subSopManagement[key].variable);

            if (menuView) {
                return (
                    <Col {...menuGrid} className="text-center" key={key}>
                        <Link to={subSopManagement[key].route}>
                            <img src={subSopManagement[key].img} alt={subSopManagement[key].label}/>
                            <h1>{subSopManagement[key].label}</h1>
                        </Link>
                    </Col>
                )
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row gutter={gutter} className="menu-list" type="flex" align="top">
                    {menuList}
                </Row>
            </div>
        )

    }

}

export default SopManagement;