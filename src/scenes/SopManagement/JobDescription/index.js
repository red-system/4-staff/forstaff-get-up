import React, {Component} from 'react';
import {Col, Row} from "antd";

import Create from './Create';
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {menuActionViewAccess} from "../../../services/app/General";

class JobDescription extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            lifeRefresh: true
        };

        this.listRefreshRun = this.listRefreshRun.bind(this);
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        const {listRefresh} = this.state;
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Create formLayout={formLayout} listRefreshRun={this.listRefreshRun} />
                                <List
                                    listRefreshRun={this.listRefreshRun}
                                    listRefresh={listRefresh} />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun(value) {
        this.setState({
            listRefresh: value
        })
    }

}



const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 18,
        offset: 6
    },
    'lg': {
        span: 18,
        offset: 6
    }
};

let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.sop_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default JobDescription;