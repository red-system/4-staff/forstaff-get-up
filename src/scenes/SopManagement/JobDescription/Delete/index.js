import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";

class Delete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            record: props.record,
            listRefreshRun: props.listRefreshRun
        }
    }


    render() {

        const {record, listRefreshRun} = this.state;

        const text = 'Yakin hapus data ini ?';

        function confirm() {
            let job_description_id = record.key;
            DeleteData(`/job-desc/${job_description_id}`)
                .then((result) => {
                    let data = result.data;
                    message.success(data.message);
                    listRefreshRun(true);
                })
        }

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger">
                    <Icon type="close"/>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }
}

export default Delete;