import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Edit from "../Edit";
import Delete from "../Delete";
import ViewFile from "../ViewFile";

import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};

class JobDescription extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };

        this.list = this.list.bind(this);
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var jobTitle = false;
            if (get(record, "jobd_title") != null) {
                jobTitle = get(record, "jobd_title").toString().match(reg);
            }
            var jobDescription = false;
            if (get(record, "jobd_description") != null) {
                jobDescription = get(record, "jobd_description").toString().match(reg);
            }
            if (!jobTitle && !jobDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        const { dataSearched } = this.state;
        let menuList = menuActionViewAccess("sop_management", "job_description", "list");
        let menuViewFile = menuActionViewAccess("sop_management", "job_description", "view_file");
        let menuEdit = menuActionViewAccess("sop_management", "job_description", "edit");
        let menuDelete = menuActionViewAccess("sop_management", "job_description", "delete");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Judul Deskripsi Pekerjaan", dataIndex: "jobd_title", key: "jobd_title" },
            { title: "Catatan Deskripsi Pekerjaan", dataIndex: "jobd_description", key: "jobd_description" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 250,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuViewFile.class}>
                            <ViewFile record={record} />
                        </span>
                        <span className={menuEdit.class}>
                            <Edit formLayout={formLayout} record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data_list = dataSearched.map((item, key) => {
            return {
                key: item["job_description_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} loading={this.state.isLoading} bordered={true} dataSource={data_list} size="small" />;
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/job-desc").then((res) => {
            const data = res.data.data;
            this.setState({
                data: data,
                dataSearched: data,
                isLoading: false,
            });
        });
    }
}

export default JobDescription;
