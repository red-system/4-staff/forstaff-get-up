import React, {Component} from 'react';
import {Button, Col, Icon, Modal, Row} from "antd";
import {renderFileType} from "../../../../services/app/General";


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,

            record: props.record
        };
    }

    render() {

        const {modalVisible, formLayout, record} = this.state;

        return [
            <Button type="file" onClick={this.modalStatus(true)}>
                <Icon type="eye"/> View File
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="eye"/> View File
                        </span>
                }
                visible={modalVisible}
                width={900}
                style={{top: 20}}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row style={{textAlign: 'center'}} key="1">
                        <Col {...formLayout}>
                            <a href={record.workf_filename_url} target="_blank">
                                <Button key="submit" type="primary">
                                    <Icon type="download"/> Download File
                                </Button>
                            </a>
                            &nbsp;

                            <Button key="back" type="warning" onClick={this.modalStatus(false)}>
                                <Icon type="rollback"/> Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                {renderFileType(record.workf_filename_type, record.workf_filename_url)}
            </Modal>
        ]
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };
}

export default ViewFile;