import React, {Component} from 'react';
import {Button, Form, Icon, Modal, DatePicker, TimePicker, Select, message, Radio, Popover} from "antd";
import moment from 'moment';
import {GetData, PostData} from "../../../../services/api";
import ImportTemplateView from "../2_AbsenceManageImportTemplateView";
import ModalErrorTime from "../2_AbsenceManageErrorTime";

let formValue = {
    template_id: "",
    template_url: "",
    absence_replace: "no",
    absence_file: ''
};

message.config({
    top: 24,
    duration: 10
});

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: formValue,
            importTemplate: [],
            modalVisibleErrorTime: false,
            errorTimeData: {
                staff_id_label: [],
                absence_year: '',
                absence_month: '',
                data: [],
                days: [],
                days_count: 1
            }
        };

        this.modalStatus = this.modalStatus.bind(this);

    }

    componentWillMount() {
        this.listImportTemplate();
    }

    render() {

        let importTemplate = this.state.importTemplate;

        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)}>
                    <Icon type="import"/> Import Absen File
                </Button>
                <Modal
                    title={
                        <span>
                            <Icon type="import"/> Import Data Absen
                        </span>
                    }
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Template Import"
                            {...this.state.formError.template_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Template Import"
                                optionFilterProp="children"
                                value={this.state.formValue.template_id}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                                onChange={this.onChange('template_id')}
                            >
                                {
                                    Object.keys(importTemplate).map((index) => {
                                        return <Select.Option key={index} value={""+index}>{importTemplate[index]['name']}</Select.Option>
                                    })
                                }
                            </Select>
                            <ImportTemplateView template_url={this.state.formValue.template_url}/>
                        </Form.Item>
                        <Form.Item
                            label="Replace Absence jika Sama"
                            {...this.state.formError.absence_replace}>
                            <Radio.Group
                                value={this.state.formValue.absence_replace}
                                buttonStyle="solid"
                                onChange={this.onChange('absence_replace')}>
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                            &nbsp;
                            <Popover
                                content={
                                    <span>
                                        <ul>
                                            <li>
                                                <strong>Ya</strong> : Akan menggantikan absensi yang sudah ada sebelumnya dengan data absensi yang ada di file import,<br />
                                                namun dengan absensi yang dalam status <strong>belum publish</strong>.
                                            </li>
                                            <li>
                                                <strong>Tidak</strong> : Tidak melakukan penggantian absensi, jika file import ada absensi staff yang sama,<br />
                                                namun file import harus <strong>diperbaiki</strong> terlebih dahulu, karena terdapat absensi yang sama.
                                            </li>
                                        </ul>
                                    </span>
                                }
                                title="Keterangan Replace Absen jika Sama">
                                <Button type={"warning"} icon={"question-circle"}>Keterangan</Button>
                            </Popover>
                        </Form.Item>
                        <Form.Item
                            label="File Absen"
                            {...this.state.formError.absence_file}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File Absence</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                    loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>

                <ModalErrorTime
                    modalVisibleErrorTime={this.state.modalVisibleErrorTime}
                    modalStatusErrorTimeClose={this.modalStatusErrorTimeClose}
                    errorTimeData={this.state.errorTimeData}
                    listRefreshRun={this.props.listRefreshRun}
                    modalImportClose={this.modalImportClose}/>

            </span>
        )
    }

    listImportTemplate() {
        GetData('/absence-import-template').then((result) => {
            this.setState({
                importTemplate: result.data.data
            })
        });
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
                template_url: name === 'template_id' ? this.state.importTemplate[value]['file_url'] : ''
            }
        });
    };

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                absence_file: event.target.files[0],
            }
        })
    };

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });

        if(!value) {
            this.setState({
                formError: [],
                modalVisibleErrorTime: false
            })
        }
    };

    modalStatusErrorTimeClose = e => {
        this.setState({
            formError: [],
            modalVisibleErrorTime: false
        })
    };

    modalImportClose = e => {
        this.setState({
            modalVisible: false
        })
    };


    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        formData.append('template_id', this.state.formValue.template_id);
        formData.append('absence_replace', this.state.formValue.absence_replace);
        formData.append('absence_file', this.state.formValue.absence_file);

        PostData('/absence-import', formData)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.destroy();
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        formError: [],
                        formValue: {
                            ...this.state.formValue,
                            template_id: "",
                            template_url: "",
                            absence_replace: "no",
                        }
                    });
                } else if(data.status === 'error_time') {
                    message.warning(data.message);
                    this.setState({
                        modalVisibleErrorTime: true,
                        isLoading: false,
                        errorTimeData: data.errors
                    })
                } else {
                    let errors = data.errors;
                    let formError = [];
                    message.warning(data.message);

                    Object.keys(errors).map(function (key) {
                        formError[key] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });
        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 10,
        },
    },
};