import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import AbsenceSummaryDetail from "../1_AbsenceSummaryDetail";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoadingStaff: props.isLoadingStaff,
            dataStaff: props.dataStaff,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {}

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.setState({
                dataStaff: nextProps.dataStaff,
                isLoadingStaff: nextProps.isLoadingStaff,
            });
            console.log(nextProps.dataStaff);
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataStaff, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "position.department.department_title") != null) {
                departmentTitle = get(record, "position.department.department_title").toString().match(reg);
            }
            var positionName = false;
            if (get(record, "position.structure_name") != null) {
                positionName = get(record, "position.structure_name").toString().match(reg);
            }
            if (!staffName && !departmentTitle && !positionName) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataStaff,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataStaff,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "absence", "list_summary");
        let menuDetail = menuActionViewAccess("staff_attendence", "absence", "detail_summary");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Departemen",
                dataIndex: "position.department.department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "position.structure_name",
                key: "structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 90,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <AbsenceSummaryDetail record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} bordered={true} dataSource={data} loading={this.state.isLoadingStaff} size="small" />;
            </div>
        );
    }
}
