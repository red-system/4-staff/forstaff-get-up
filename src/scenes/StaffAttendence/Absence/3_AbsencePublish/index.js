import React, { Component } from "react";
import { Button, Table, Icon, message, Tag, Input } from "antd";
import { GetData, PostData, PutData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            formValue: {
                data: [],
            },
            selectedRowKeys: [],
            selectAllStatus: false,
            buttonPublishDisabledStatus: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var asbsenceDate = false;
            if (get(record, "absence_date_format") != null) {
                asbsenceDate = get(record, "absence_date_format").toString().match(reg);
            }
            var presentHour = false;
            if (get(record, "present_hour") != null) {
                presentHour = get(record, "present_hour").toString().match(reg);
            }
            var breakIn = false;
            if (get(record, "break_in") != null) {
                breakIn = get(record, "break_in").toString().match(reg);
            }
            var breakOut = false;
            if (get(record, "break_out") != null) {
                breakOut = get(record, "break_out").toString().match(reg);
            }
            var returnHour = false;
            if (get(record, "return_hour") != null) {
                returnHour = get(record, "return_hour").toString().match(reg);
            }
            if (!staffName && !asbsenceDate && !presentHour && !returnHour && !breakIn && !breakOut) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let status_absence_on_break = this.props.absenceOnBreak === "yes" ? "" : "hide";
        let total_data = 0;
        let menuList = menuActionViewAccess("staff_attendence", "absence", "list_publish");
        let menuDo = menuActionViewAccess("staff_attendence", "absence", "do_publish");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Tanggal",
                dataIndex: "absence_date_format",
                key: "absence_date_format",
            },
            {
                title: "Jam Hadir",
                dataIndex: "present_hour",
                key: "present_hour",
            },
            {
                title: "Mulai Istirahat",
                dataIndex: "break_in",
                key: "break_in",
                className: status_absence_on_break,
            },
            {
                title: "Selesai Istirahat",
                dataIndex: "break_out",
                key: "break_out",
                className: status_absence_on_break,
            },
            {
                title: "Jam Pulang",
                dataIndex: "return_hour",
                key: "return_hour",
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            total_data++;
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys, selectedRows) => {
                let selectRowNow = [];
                const absence_id = selectedRows.map((item, key) => {
                    selectRowNow[key] = item["key"];
                    return {
                        absence_id: item["absence_id"],
                        staff_id: item["staff_id"],
                    };
                });

                let buttonPublishDisabledStatus = selectRowNow.length > 0 ? false : true;

                console.log(buttonPublishDisabledStatus);

                this.setState({
                    formValue: {
                        data: absence_id,
                    },
                    selectedRowKeys: selectRowNow,
                    buttonPublishDisabledStatus: buttonPublishDisabledStatus,
                });
            },
        };

        return (
            <div>
                {this.state.selectAllStatus ? (
                    <Button type={"warning"} icon={"close-square"} loading={this.state.isLoading} onClick={this.checkAll(total_data)}>
                        Tidak Pilih Semua Data
                    </Button>
                ) : (
                    <Button type={"primary"} icon={"check-square"} loading={this.state.isLoading} onClick={this.checkAll(total_data)}>
                        Pilih Semua Data
                    </Button>
                )}
                &nbsp;
                <Button
                    className={menuDo.class}
                    type={"success"}
                    icon={"check"}
                    loading={this.state.isLoading}
                    disabled={this.state.buttonPublishDisabledStatus}
                    onClick={this.publish}
                >
                    Publish
                </Button>
                <br />
                <br />
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuList.class}
                    columns={columns}
                    title={() => {
                        return (
                            <Tag color={"blue"}>
                                <Icon type={"check-square"} /> Terpilih {this.state.selectedRowKeys.length}/{total_data}
                            </Tag>
                        );
                    }}
                    dataSource={data}
                    rowSelection={rowSelection}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/absence/no-publish").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }

    checkAll = (total_data) => (e) => {
        let { selectAllStatus } = this.state;
        let selectAllStatusChange = !selectAllStatus;
        let selectedRowKeys = selectAllStatusChange ? [...Array(total_data).keys()] : [];
        let absence_id = [];

        if (selectAllStatusChange) {
            absence_id = this.state.data.map((item, key) => {
                return {
                    absence_id: item["absence_id"],
                    staff_id: item["staff_id"],
                };
            });
        }

        this.setState({
            selectedRowKeys: selectedRowKeys,
            selectAllStatus: selectAllStatusChange,
            buttonPublishDisabledStatus: !selectAllStatusChange,
            formValue: {
                data: absence_id,
            },
        });
    };

    publish = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData(`/absence-publish`, this.state.formValue).then((result) => {
            const data = result.data;

            if (data.status === "success") {
                message.success(data.message);
                this.props.listRefreshRun(true);
                this.setState({
                    isLoading: false,
                    selectedRowKeys: [],
                    selectAllStatus: false,
                    buttonPublishDisabledStatus: true,
                });
            } else {
                message.error(data.message);
                let errors = data.errors;
                let formError = [];

                // Object.keys(errors).map(function (key) {
                //     formError[key] = {
                //         validateStatus: "error",
                //         help: errors[key][0],
                //     };
                // });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
        return false;
    };
}

export default RewardGuide;
