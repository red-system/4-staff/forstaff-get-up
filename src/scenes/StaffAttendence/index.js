import React, {Component} from "react";
import {Row, Col} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';
import {Link} from 'react-router-dom';

import '../../styles/ui/_card-icon.scss';
import {menuViewAccess} from "../../services/app/General";

let menuStaffAttendence = GENERALDATA.primaryMenu.staff_attendence;
let subStaffAttendence = GENERALDATA.primaryMenu.staff_attendence.sub_menu;
let gutter = GENERALDATA.gutter;

const menuGrid = {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 3
}

const breadcrumb = [
    {
        label: menuStaffAttendence.label,
        route: `${menuStaffAttendence.route}`
    },
];

class StaffAttendence extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        const menuList = Object.keys(subStaffAttendence).map(function (key) {

            let menuView = menuViewAccess(menuStaffAttendence.variable, subStaffAttendence[key].variable);

            if (menuView) {
                return (
                    <Col {...menuGrid} className="text-center" key={key}>
                        <Link to={subStaffAttendence[key].route}>
                            <img src={subStaffAttendence[key].img} alt={subStaffAttendence[key].label}/>
                            <h1>{subStaffAttendence[key].label}</h1>
                        </Link>
                    </Col>
                )
            }
        });


        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row gutter={gutter} className="menu-list" type="flex" align="top">
                    {menuList}
                </Row>
            </div>
        )

    }

}

export default StaffAttendence;