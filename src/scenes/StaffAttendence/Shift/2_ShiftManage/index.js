import React, { Component } from "react";

import Create from "../2_ShiftManageCreate";
import Data from "../2_ShiftManageData";
import { menuActionViewAccess } from "../../../../services/app/General";

class ShiftManage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listRefresh: true,
        };

        this.listRefreshRun = this.listRefreshRun.bind(this);
    }

    render() {
        const { listRefresh } = this.state;
        let menuList = menuActionViewAccess("staff_attendence", "shift", "list_shift");

        return (
            <span>
                <span className={menuList.class}>
                    <Create listRefreshRun={this.listRefreshRun} />
                </span>
                <Data listRefresh={listRefresh} listRefreshRun={this.listRefreshRun} />
            </span>
        );
    }

    listRefreshRun(value) {
        this.setState({
            listRefresh: value,
        });
    }
}

export default ShiftManage;
