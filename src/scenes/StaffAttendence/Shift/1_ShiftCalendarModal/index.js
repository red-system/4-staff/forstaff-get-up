import React, {Component} from 'react';
import {Button, Form, Icon, Modal, InputNumber, Input, TimePicker, Select, message} from "antd";
import moment from 'moment';
import {GetData, PostData} from "../../../../services/api";
import {dateFormatApp, dateFormatterApp} from "../../../../services/app/General";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: props.modalVisible,
            dateSelect: props.dateSelect,
            dateView: props.dateView,
            isLoading: false,
            shiftMaster: [],
            staff: [],
            shiftStaff: [],
            formValue: [],
            formError: []
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.shiftMaster();
        this.staffList();
        this.setState({
            modalVisible: nextProps.modalVisible,
            dateSelect: nextProps.dateSelect,
            dateView: nextProps.dateView,
            formValue: {
                ...this.state.formValue,
                shift_date: nextProps.dateSelect
            }
        })
    }

    render() {

        return (

            <Modal
                title={
                    <span>
                            <Icon type="calendar"/> {this.state.dateView} | Shift
                        </span>
                }
                visible={this.state.modalVisible}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Nama Shift"
                        {...this.state.formError.shift_master_id}>
                        <Select
                            showSearch
                            optionFilterProp="children"
                            onChange={this.onChangeShift("shift_master_id")}
                            value={this.state.formValue.shift_master_id}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {
                                this.state.shiftMaster.map((item, key) => {
                                    return <Select.Option value={item.shift_master_id}>{item.shiftm_name}</Select.Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Nama Staff"
                        {...this.state.formError.staff_id}>
                        <Select
                            showSearch
                            mode={"multiple"}
                            optionFilterProp="children"
                            value={this.state.formValue.staff_id}
                            onChange={this.onChangeStaff("staff_id")}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {
                                this.state.staff.map((item, key) => {
                                    return <Select.Option value={item.staff_id}>{item.staff_name}</Select.Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType="submit" icon={"check"}
                                loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        )
    }

    shiftMaster = () => {
        GetData('/shift-master').then((result) => {
            this.setState({
                shiftMaster: result.data.data,
            })
        })
    };

    staffList = () => {
        GetData('/shift-staff-list').then((result) => {
            this.setState({
                staff: result.data.data
            })
        });
    };

    staffShift = (shift_master_id) => {
        this.setState({
           isLoading: true
        });

        let data = {
            shift_master_id: shift_master_id,
            shift_date: this.state.dateSelect
        };

        PostData('/shift-staff-select-list', data).then((result) => {
            this.setState({
                isLoading: false,
                formValue: {
                    ...this.state.formValue,
                    staff_id: result.data.data.staff_id,
                }
            })
        });
    };

    onChangeShift = name => value => {
        this.staffShift(value);

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeStaff = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeTime = name => time => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: time.format('HH:mm:ss')
            }
        });
    };


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };


    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/shift-staff-insert', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        formError: [],
                        formValue: []
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;