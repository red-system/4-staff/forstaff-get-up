import React, { Component } from "react";
import { Button, Form, Icon, Modal, InputNumber, Input, TimePicker, message } from "antd";
import moment from "moment";
import { PostData, PutData } from "../../../../services/api";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formError: [],
            formValue: props.record,
        };
    }

    render() {
        return [
            <Button type="success" onClick={this.modalStatus(true)} icon={"edit"}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data Shift
                    </span>
                }
                visible={this.state.modalVisible}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Nama Shift" {...this.state.formError.shiftm_name}>
                        <Input defaultValue={this.state.formValue.shiftm_name} onChange={this.onChange("shiftm_name")} />
                    </Form.Item>
                    <Form.Item label="Jam Hadir" {...this.state.formError.shiftm_present_hour}>
                        <TimePicker
                            style={{ width: "100%" }}
                            defaultValue={moment(this.state.formValue.shiftm_present_hour, "HH:mm:ss")}
                            onChange={this.onChangeTime("shiftm_present_hour")}
                        />
                    </Form.Item>
                    <Form.Item label="Jam Pulang" {...this.state.formError.shiftm_return_hour}>
                        <TimePicker
                            style={{ width: "100%" }}
                            defaultValue={moment(this.state.formValue.shiftm_return_hour, "HH:mm:ss")}
                            onChange={this.onChangeTime("shiftm_return_hour")}
                        />
                    </Form.Item>
                    <Form.Item label="Toleransi" {...this.state.formError.shiftm_tolerance_minutes}>
                        <InputNumber
                            defaultValue={this.state.formValue.shiftm_tolerance_minutes}
                            min={0}
                            style={{ width: "100%" }}
                            formatter={(value) => `${value} menit`}
                            parser={(value) => value.replace(" menit", "")}
                            onChange={this.onChange("shiftm_tolerance_minutes")}
                        />
                    </Form.Item>
                    <Form.Item label="Keterangan" {...this.state.formError.shiftm_description}>
                        <Input.TextArea value={this.state.formValue.shiftm_description} onChange={this.onChange("shiftm_description")} />
                    </Form.Item>
                    <Form.Item label="Total Jam Kerja" {...this.state.formError.shiftm_total_work_hour}>
                        <InputNumber
                            defaultValue={this.state.formValue.shiftm_total_work_hour}
                            min={0}
                            style={{ width: "100%" }}
                            formatter={(value) => `${value} jam`}
                            parser={(value) => value.replace(" jam", "")}
                            onChange={this.onChange("shiftm_total_work_hour")}
                        />
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType="submit" icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    onChangeTime = (name) => (time) => {
        console.log(time);
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: time.format("HH:mm:ss"),
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData(`/shift-master/${this.state.formValue.shift_master_id}`, this.state.formValue).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;
