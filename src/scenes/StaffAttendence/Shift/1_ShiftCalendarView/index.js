import React, {Component} from 'react';
import {Calendar, Badge, Icon, Alert} from "antd";
import ShiftCalendarModal from "../1_ShiftCalendarModal";
import {GetData} from "../../../../services/api";

export default class AppComponent extends Component {

    constructor() {
        super();
        this.state = {
            modalVisible: false,
            dateSelect: '',
            dateView: '',
            listRefresh: true,
            calendarData: []
        }
    }

    componentWillMount() {
        this.listData();
    }

    componentDidUpdate(nextProps, nextState, nextContext) {

        if(this.state.listRefresh) {
            this.listData();
        }
    }


    render() {

        let {calendarData} = this.state;

        function getListData(value) {
            let listData = [];
            let year = value.format('YYYY');
            let month = value.format('MM');
            let day = value.format('DD');
            let date = `${year}-${month}-${day}`;

            Object.keys(calendarData).map((key) => {
                if (key == date) {
                    calendarData[key].map((item, key2) => {
                        listData[key2] = {
                            type: item['select'] === 'yes' ? 'error' : 'success',
                            shift_name: `${item['shiftm_name']} (${item['staff_total']})`,
                        }
                    });
                }
            });

            return listData || [];
        }

        function dateCellRender(value) {
            const listData = getListData(value);
            return (
                <ul className="events">
                    {listData.map(item => (
                        <li key={item.shift_name}>
                            <Badge status={item.type} text={item.shift_name}/>
                        </li>
                    ))}
                </ul>
            );
        }

        function getMonthData(value) {
            if (value.month() === 8) {
                return 1394;
            }
        }

        function monthCellRender(value) {
            const num = getMonthData(value);
            return num ? (
                <div className="notes-month">
                    <section>{num}</section>
                    <span>Backlog number</span>
                </div>
            ) : null;
        }

        return (
            <div>
                <Alert message={"Tanda bulat merah merupakan shift Anda"} type="info" />

                <Calendar
                    dateCellRender={dateCellRender}
                    monthCellRender={monthCellRender}
                    onSelect={this.onSelect}/>

                <ShiftCalendarModal
                    listRefreshRun={this.listRefreshRun}
                    dateSelect={this.state.dateSelect}
                    dateView={this.state.dateView}
                    modalVisible={this.state.modalVisible}/>
            </div>
        )
    }

    listData = e => {
        GetData('/shift-calendar').then((result) => {
            this.setState({
                calendarData: result.data.data,
                listRefresh: false
            });
        });
    };

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
            modalVisible: !value
        })
    };



    onSelect = value => {
        this.setState({
            dateSelect: value.format('YYYY-MM-DD'),
            dateView: value.format('DD-MM-YYYY'),
            modalVisible: true
        });
    };


}