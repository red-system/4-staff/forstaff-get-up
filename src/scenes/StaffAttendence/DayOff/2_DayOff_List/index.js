import React, {Component} from 'react';
import {Button, Table} from "antd";

import Delete from "../2_DayOff_Delete";
import Edit from "../2_DayOff_Edit";

import {GetData, PostData} from "../../../../services/api";
import {menuActionViewAccess, strRandom} from "../../../../services/app/General";


export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
        }
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        console.log('componentWillReceiveProps', nextProps);
        if (nextProps.listRefresh) {
            console.log('nextProps.listRefresh = TRUE', nextProps);
            this.list();
        }
    }


    render() {

        let menuEdit = menuActionViewAccess('staff_attendence', 'dayoff', 'edit_dayoff');
        let menuDelete = menuActionViewAccess('staff_attendence', 'dayoff', 'delete_dayoff');
        let menuList = menuActionViewAccess('staff_attendence', 'dayoff', 'list_dayoff');

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Judul',
                dataIndex: 'dayoff_title',
                key: 'dayoff_title'
            },
            {
                title: 'Keterangan',
                dataIndex: 'dayoff_description',
                key: 'dayoff_description',
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit
                                record={record}
                                listRefresh={this.props.listRefresh}
                                listRefreshRun={this.props.listRefreshRun}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete
                                record={record}
                                listRefresh={this.props.listRefresh}
                                listRefreshRun={this.props.listRefreshRun}/>
                        </span>
                    </Button.Group>

                )
            }
        ];

        const data = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            };
        });

        return (
            <div key={strRandom()}>
                <Table
                    className={menuList.class}
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    loading={this.state.isLoading}
                    size='small' rowKey={strRandom()}/>
            </div>
        )
    }

    list() {
        this.setState({
            isLoading: true
        });

        console.log('list run', this.props.date);

        PostData('/day-off-date', {date: this.props.date}).then(res => {
            const data = res.data.data;

            this.setState({
                data: data,
                isLoading: false
            });
        })
    }
}