import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import CalendarView from './1_Calendar';
import DayOffList from './2_DayOff_List';
import DayOffCreate from './2_DayOff_Create';

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {menuActionViewAccess} from "../../../services/app/General";

export default class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        let menuCalendar = menuActionViewAccess('staff_attendence', 'dayoff', 'calendar_dayoff');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <CalendarView
                                    listRefresh={this.state.listRefresh}
                                    listRefreshRun={this.listRefreshRun}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }
}


let pageNow = GENERALDATA.primaryMenu.staff_attendence.sub_menu["2"];
let pageParent = GENERALDATA.primaryMenu.staff_attendence;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];