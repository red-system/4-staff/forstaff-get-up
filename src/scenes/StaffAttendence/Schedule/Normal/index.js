import React, {Component} from 'react';
import {Checkbox, TimePicker, Button, Icon, InputNumber, Spin, message} from "antd";
import moment from "moment";
import {GetData, PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            workdays: {
                total_hour_workday: '',
                data: []
            },
            formValue: [],
            isLoading: true
        }
    }

    componentWillMount() {
        this.data();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.data();
        }
    }


    render() {
        const workdays = this.state.formValue;

        let menuList = menuActionViewAccess('staff_attendence', 'schedule', 'list');
        let menuEdit = menuActionViewAccess('staff_attendence', 'schedule', 'edit');

        return (
            <Spin tip={"Loading ..."} spinning={this.state.isLoading}>
                <table className={"table-bordered "+menuList.class}>
                    <thead className={"ant-table-thead"}>
                    <tr>
                        <th className="text-center" width={"100"}>
                            {/*<Checkbox><br/>Semua</Checkbox>*/}
                        </th>
                        <th className="text-center">Hari</th>
                        <th className="text-center">Kehadiran</th>
                        <th className="text-center">Toleransi Kehadiran</th>
                        <th className="text-center">Mulai Istirahat</th>
                        <th className="text-center">Selesai Istirahat</th>
                        <th className="text-center">Pulang</th>
                        <th className="text-center">Mulai Lembur</th>
                        <th className="text-center">Jam Kerja</th>
                    </tr>
                    {/*<tr>*/}
                    {/*    <th><TimePicker defaultValue={moment('08:00:00', 'HH:mm:ss')}/></th>*/}
                    {/*    <th><TimePicker defaultValue={moment('17:00:00', 'HH:mm:ss')}/></th>*/}
                    {/*    <th><InputNumber value={15}/></th>*/}
                    {/*    <th>9 Jam</th>*/}
                    {/*</tr>*/}
                    </thead>
                    <tbody>

                    {
                        Object.keys(workdays).map((workdays_id) => {
                            let status_workdays = workdays[workdays_id]['status_workdays'] === 'checked' ? true : false;
                            let row_disabled = status_workdays ? '' : 'row-disabled';
                            return (
                                <tr key={workdays_id} className={row_disabled}>
                                    <td className="text-center">
                                        <Checkbox key={workdays_id}
                                                  onChange={this.onChangeCheckbox(workdays_id)}
                                                  checked={status_workdays}>
                                        </Checkbox>
                                    </td>
                                    <td>{workdays[workdays_id]['days']}</td>
                                    <td align={"center"}>
                                        <TimePicker
                                            defaultValue={moment(workdays[workdays_id]['present_hour'], 'HH:mm:ss')}
                                            onChange={this.onChangeTime('present_hour', workdays_id)}/>
                                    </td>
                                    <td align={"center"}>
                                        <InputNumber
                                            value={workdays[workdays_id]['tolerance_minutes']}
                                            onChange={this.onChange('tolerance_minutes', workdays_id)}/>
                                    </td>
                                    <td align={"center"}>
                                        <TimePicker
                                            defaultValue={moment(workdays[workdays_id]['break_start'], 'HH:mm:ss')}
                                            onChange={this.onChangeTime('break_start', workdays_id)}/>
                                    </td>
                                    <td align={"center"}>
                                        <TimePicker
                                            defaultValue={moment(workdays[workdays_id]['break_finish'], 'HH:mm:ss')}
                                            onChange={this.onChangeTime('break_finish', workdays_id)}/>
                                    </td>
                                    <td align={"center"}>
                                        <TimePicker
                                            defaultValue={moment(workdays[workdays_id]['return_hour'], 'HH:mm:ss')}
                                            onChange={this.onChangeTime('return_hour',  workdays_id)}/>
                                    </td>
                                    <td align={"center"}>
                                        <TimePicker
                                            defaultValue={moment(workdays[workdays_id]['overtime_start'], 'HH:mm:ss')}
                                            onChange={this.onChangeTime('overtime_start',  workdays_id)}/>
                                    </td>
                                    <td align={"center"}>{workdays[workdays_id]['total_work_hour_view']}</td>
                                </tr>
                            )
                        })
                    }
                    <tr>
                        <td colSpan={8} align={"center"}><strong>TOTAL</strong></td>
                        <td align={"center"}>
                            <div id="resultwh"><strong>{this.state.workdays.total_hour_workday} / Minggu</strong></div>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colSpan={9} className={"text-center"}>
                            <Button
                                type={"success"}
                                size={"lg"}
                                icon={"check"}
                                className={menuEdit.class}
                                onClick={this.insert}> SIMPAN
                            </Button>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </Spin>
        )
    }

    onChange = (name, workdays_id) => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [workdays_id]: {
                    ...this.state.formValue[workdays_id],
                    [name]: value
                }
            }
        });
    };

    onChangeTime = (name, workdays_id) => value => {
        let value_row = 0;
        if(value !== null) {
            value_row = value.format('HH:mm:ss');
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [workdays_id]: {
                    ...this.state.formValue[workdays_id],
                    [name]: value_row
                }
            }
        });
    };

    onChangeCheckbox = (workdays_id) => e => {
        let status_workdays = e.target.checked ? 'checked' : 'unchecked';
        workdays_id = workdays_id.toString();

        this.setState({
            formValue: {
                ...this.state.formValue,
                [workdays_id]: {
                    ...this.state.formValue[workdays_id],
                    status_workdays: status_workdays
                }
            }
        });
    };

    data = () => {
        GetData('/work-days')
            .then((result) => {
                this.setState({
                    workdays: result.data.data,
                    isLoading: false,
                    formValue: result.data.data.data
                })
            });
    };

    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PostData('/work-days', {data: this.state.formValue})
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}_field`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}