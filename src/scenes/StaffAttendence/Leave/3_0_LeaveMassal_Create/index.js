import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Radio, InputNumber, message, DatePicker} from "antd";
import {PostData} from "../../../../services/api";
import moment from "moment";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";

const {TextArea}  = Input;

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formError: [],
            formValue: {
                leave_massive_title: '',
                leave_massive_date: dateToDay(),
                leave_massive_description: '',
            }
        };
    }

    render() {

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)} icon={"plus"}>
                    Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >

                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul Leave Massive"
                            {...this.state.formError.leave_massive_title}>
                            <Input
                                defaultValue={this.state.formValue.leave_massive_title}
                                onChange={this.onChange('leave_massive_title')}/>
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Leave Massive"
                            {...this.state.formError.leave_massive_date}>
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("leave_massive_date")}
                                value={moment(dateFormatterApp(this.state.formValue.leave_massive_date), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Keterangan Leave Massive"
                            {...this.state.formError.leave_massive_description}>
                            <TextArea
                                defaultValue={this.state.formValue.leave_massive_description}
                                onChange={this.onChange('leave_massive_description')}/>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                    loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };



    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };
    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PostData('/leave-massive', this.state.formValue)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const date_format = 'YYYY-MM-DD';

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 7},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 17},
    },
};


const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};