import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, Row, Tag} from "antd";
import {status_view} from "../../../../services/app/General";
import LeaveMasterDetail from "../4_0_LeaveManage_Detail";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            size: props.size
        };
    }

    render() {

        const {record} = this.props;

        return [
            <Button type={"primary"} size={this.state.size} onClick={this.modalStatus(true)}>
                <Icon type="eye"/> Detail
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="info-circle"/> Detil Leave
                        </span>
                }
                visible={this.state.modalVisible}
                width={600}
                onCancel={this.modalStatus(false)}
                onOk={this.modalStatus(false)}
                footer={[
                    <Row style={{textAlign: 'center'}} key="1">
                        <Col>
                            <Button key="back" type="warning" onClick={this.modalStatus(false)}>
                                <Icon type="close"/> Tutup
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                <Form {...formItemLayout} className={"form-info"}>
                    <Form.Item
                        label="Staf">
                        {record.staff_name}
                    </Form.Item>
                    <Form.Item
                        label="Jenis Cuti">
                        {record.leavem_name}
                    </Form.Item>
                    <Form.Item
                        label="Detail Jenis Cuti">
                        <LeaveMasterDetail record={record}/>
                    </Form.Item>
                    <Form.Item
                        label="Tanggal Rencana Cuti">
                        {record.leave_staff_date_format}
                    </Form.Item>
                    <Form.Item
                        label="Tanggal Pembuatan Cuti">
                        {record.leave_staff_publish_date_format}
                    </Form.Item>
                    {
                        record.leavem_need_replace === 'yes' ?
                            <div>
                                {/*<Form.Item*/}
                                {/*    label="Tanggal Pergantian">*/}
                                {/*    {record.substitution_date}*/}
                                {/*</Form.Item>*/}
                                <Form.Item
                                    label="Staf Pengganti">
                                    {record.substitution_staff_name}
                                </Form.Item>
                            </div> : ''
                    }
                    <Form.Item
                        label="Durasi (hari)">
                        {record.leave_staff_duration}
                    </Form.Item>
                    <Form.Item
                        label="Deskripsi">
                        {record.leave_staff_reason}
                    </Form.Item>
                    {
                        record.leavem_attachment === 'yes' ?
                            <Form.Item
                                label="File Pengantar">
                                <a href={record.leave_staff_attachment_url} target={"_blank"}>
                                    <Button type={"success"} icon={"file"}> Download File Pengantar</Button>
                                </a>
                            </Form.Item>
                            :
                            ''
                    }
                    {
                        record.leave_staff_status === 'reject' ?
                            <Form.Item
                                label="Alasan Ditolak">
                                {record.leave_staff_reject_reason}
                            </Form.Item>
                            :
                            ''
                    }
                </Form>

            </Modal>
        ]
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};