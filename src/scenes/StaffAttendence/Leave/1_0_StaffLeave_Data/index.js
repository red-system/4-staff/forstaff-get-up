import React, {Component} from "react";
import {Tabs, Icon} from 'antd';

import StaffLeaveApproved from "../1_2_StaffLeave_Approved";
import StaffLeaveConfirmed from "../1_3_StaffLeave_Confirmed";
import StaffLeaveWaiting from "../1_1_StaffLeave_Waiting";
import StaffLeaveCanceled from "../1_4_StaffLeave_Canceled";
import StaffLeaveRejected from "../1_5_StaffLeave_Rejected";



export default class AppComponent extends Component{

    constructor(props) {
        super(props);
        this.state = {
            listRefresh: true,
        }
    }

    render() {
        return(
            <Tabs type="card" defaultActiveKey="waiting-confirmed">
                <Tabs.TabPane tab={<span><Icon type={"bars"} /> Menunggu Persetujuan</span>} key="waiting-confirmed">
                    <StaffLeaveWaiting
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun} />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type={"check"} /> Disetujui</span>} key="approved">
                    <StaffLeaveApproved
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun} />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type={"check-circle"} /> Dikonfirmasi</span>} key="confirmed">
                    <StaffLeaveConfirmed
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun} />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type={"close"} /> Dibatalkan</span>} key="canceled">
                    <StaffLeaveCanceled
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun} />
                </Tabs.TabPane>
                <Tabs.TabPane tab={<span><Icon type={"close-circle"} /> Ditolak</span>} key="rejected">
                    <StaffLeaveRejected
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun} />
                </Tabs.TabPane>
            </Tabs>
        );
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}