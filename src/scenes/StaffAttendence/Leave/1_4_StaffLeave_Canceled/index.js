import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import StaffLeaveDetail from "../1_1_StaffLeave_Detail";
import StaffLeaveConfirm from "../1_1_StaffLeave_Confirm";
import StaffLeaveReject from "../1_1_StaffLeave_Reject";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var positionName = false;
            if (get(record, "structure_name") != null) {
                positionName = get(record, "structure_name").toString().match(reg);
            }
            var leaveMName = false;
            if (get(record, "leavem_name") != null) {
                leaveMName = get(record, "leavem_name").toString().match(reg);
            }
            var leaveDate = false;
            if (get(record, "leave_staff_date_format") != null) {
                leaveDate = get(record, "leave_staff_date_format").toString().match(reg);
            }
            var leaveDuration = false;
            if (get(record, "leave_staff_duration") != null) {
                leaveDuration = get(record, "leave_staff_duration").toString().match(reg);
            }
            var leaveReason = false;
            if (get(record, "leave_staff_reason") != null) {
                leaveReason = get(record, "leave_staff_reason").toString().match(reg);
            }
            if (!staffName && !positionName && !leaveMName && !leaveReason && !leaveDate && !leaveDuration) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "leave", "list_staff_canceled");
        let menuDetail = menuActionViewAccess("staff_attendence", "leave", "detail_staff_canceled");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "strcuture_name",
            },
            {
                title: "Jenis Cuti",
                dataIndex: "leavem_name",
                key: "leavem_name",
            },
            {
                title: "Tanggal Mulai",
                dataIndex: "leave_staff_date_format",
                key: "leave_staff_date_format",
            },
            {
                title: "Durasi/hari",
                dataIndex: "leave_staff_duration",
                key: "leave_staff_duration",
            },
            {
                title: "Keterangan",
                dataIndex: "leave_staff_reason",
                key: "leave_staff_reason",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <StaffLeaveDetail record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });

        GetData("/canceled-staff-leave").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}
