import { Button, Icon, Input, message, Popover } from "antd";
import React, { Component } from "react";
import { PostData } from "../../../../services/api";

const { TextArea } = Input;

class Delete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formValue: {
                leave_staff_reject_reason: "",
                leave_staff_id: props.record.leave_staff_id,
            },
        };
    }

    // componentWillMount() {
    //     this.props.listRefreshRun();
    // }
    render() {
        const content = (
            <div>
                <TextArea defaultValue={this.state.leave_staff_reject_reason} onChange={this.onChange("leave_staff_reject_reason")} />
                <div style={{ marginTop: "10px" }} className={"text-center"}>
                    <Button type={"success"} size={"small"} icon={"check"} onClick={this.reject()}>
                        Kirim
                    </Button>
                </div>
            </div>
        );

        return (
            <Popover content={content} title="Alasan Ditolak" trigger="click">
                <Button type="danger">
                    <Icon type="close" />
                    Tolak
                </Button>
            </Popover>
        );
    }

    onChange = (field) => (e) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [field]: e.target.value,
            },
        });
    };

    reject = () => (e) => {
        const props = this.props;
        const hide = message.loading("Action in progress..", 0);
        this.setState({
            isLoading: true,
        });
        PostData(`/reject-staff-leave`, this.state.formValue).then((result) => {
            props.listRefreshRun(true);
            message.success(result.data.message);
            this.setState({ isLoading: false });
            setTimeout(hide, 0);
        });
    };
}

export default Delete;
