import {Button, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {PostData} from "../../../../services/api";

export default class Delete extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }
    }

    render() {

        const text = 'Setujui cuti staff ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="success" icon={"check"} loading={this.state.isLoading}>
                    Setujui
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        PostData(`/approve-staff-leave/${props.record.leave_staff_id}`)
            .then((result) => {
                this.setState({isLoading: false});
                setTimeout(hide, 0);
                if(result.data.status === 'success') {
                    props.listRefreshRun(true);
                    message.success(result.data.message);
                } else {
                    message.warning(result.data.message);
                }
            });
    }
}