import React, {Component} from 'react';
import {Button, Table, Tag} from "antd";

import Create from '../4_0_LeaveManage_Create';
import Edit from '../4_0_LeaveManage_Edit';
import Delete from '../4_0_LeaveManage_Delete';
import Detail from '../4_0_LeaveManage_Detail';
import {GetData} from "../../../../services/api";
import {menuActionViewAccess, status_view} from "../../../../services/app/General";


class RewardGuide extends Component {

    constructor() {
        super();
        this.state = {
            listRefresh: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.listRefresh) {
            this.list();
        }
    }

    render() {

        let menuList = menuActionViewAccess('staff_attendence', 'leave', 'list_manage');
        let menuCreate = menuActionViewAccess('staff_attendence', 'leave', 'create_manage');
        let menuEdit = menuActionViewAccess('staff_attendence', 'leave', 'edit_manage');
        let menuDelete = menuActionViewAccess('staff_attendence', 'leave', 'delete_manage');
        let menuDetail = menuActionViewAccess('staff_attendence', 'leave', 'detail_manage');

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Nama Cuti/Ijin',
                dataIndex: 'leavem_name',
                key: 'leavem_name'
            },
            {
                title: 'Status Pembayaran Cuti/Ijin',
                dataIndex: 'leavem_regulation',
                key: 'leavem_regulation',
                render: (text, record) => {
                    return status_view(record.leavem_regulation);
                }
            },
            {
                title: 'Status Pengurangan Kuota Cuti/Ijin',
                dataIndex: 'leavem_reduce_quota_status',
                key: 'leavem_reduce_quota_status',
                render: (text, record) => {
                    return status_view(record.leavem_reduce_quota_status);
                }
            },
            {
                title: 'Dukungan File',
                dataIndex: 'leavem_attachment',
                key: 'leavem_attachment',
                render: (text, record) => {
                    return status_view(record.leavem_attachment);
                }
            },
            // {
            //     title: 'Rekomendasi Kuota',
            //     dataIndex: 'leavem_quota_recomendation',
            //     key: 'leavem_quota_recomendation'
            // },
            {
                title: 'Perlu Pengganti',
                dataIndex: 'leavem_need_replace',
                key: 'leavem_need_replace',
                render: (text, record) => {
                    return status_view(record.leavem_need_replace);
                }
            },
            // {
            //     title: 'Pengurangan Kuota Cuti/Ijin',
            //     dataIndex: 'leavem_reduce_quota',
            //     key: 'leavem_reduce_quota'
            // },
            // {
            //     title: 'Minimal Hari Pengajuan Cuti/Ijin',
            //     dataIndex: 'leavem_min_request_date',
            //     key: 'leavem_min_request_date'
            // },
            {
                title: 'Perlu Maksimal Durasi Cuti',
                dataIndex: 'leavem_need_max_duration',
                key: 'leavem_need_max_duration',
                render: (text, record) => {
                    return status_view(record.leavem_need_max_duration);
                }
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 230,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <Detail
                                record={record}/>
                        </span>
                        <span className={menuEdit.class}>
                            <Edit
                                record={record}
                                listRefreshRun={this.listRefreshRun}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete
                                record={record}
                                listRefreshRun={this.listRefreshRun}/>
                        </span>
                    </Button.Group>

                )
            }
        ];

        const data = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        return (
            <div>
                <span className={menuCreate.class}>
                    <Create
                        listRefresh={this.state.listRefresh}
                        listRefreshRun={this.listRefreshRun}/>
                </span>
                <Table
                    className={menuList.class}
                    columns={columns}
                    bordered={true}
                    dataSource={data}
                    size='small'/>
            </div>
        )
    }

    list = () => {
        GetData('/leave-master')
            .then((result) => {
                this.setState({
                    data: result.data.data,
                    listRefresh: false
                })
            })
    };

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default RewardGuide;