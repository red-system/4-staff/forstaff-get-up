import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Radio, InputNumber, message, Popover} from "antd";
import {PostData} from "../../../../services/api";

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formError: [],
            formValue: {
                leavem_name: '',
                leavem_regulation: 'unpaid',
                leavem_attachment: 'no',
                leavem_quota_recomendation: 0,
                leavem_need_replace: 'no',
                leavem_reduce_quota_status: 'no',
                leavem_reduce_quota_value: 0,
                leavem_min_request_date: 0,
                leavem_need_max_duration: 'no',
                leavem_max_duration: 0,
            }
        };
    }

    render() {

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)} icon={"plus"}>
                    Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >

                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Nama Cuti/Ijin"
                            {...this.state.formError.leavem_name}>
                            <Input
                                defaultValue={this.state.formValue.leavem_name}
                                onChange={this.onChange('leavem_name')}/>
                        </Form.Item>
                        <Form.Item
                            label="Status Pembayaran Cuti/Ijin"
                            {...this.state.formError.leavem_regulation}>
                            <Radio.Group
                                buttonStyle="solid"
                                defaultValue={this.state.formValue.leavem_regulation}
                                onChange={this.onChange('leavem_regulation')}>
                                <Radio.Button value="paid">Bayar</Radio.Button>
                                <Radio.Button value="unpaid">Tidak Bayar</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item
                            label="Dukungan File"
                            {...this.state.formError.leavem_attachment}>
                            <Radio.Group
                                defaultValue={this.state.formValue.leavem_attachment}
                                onChange={this.onChange('leavem_attachment')}
                                buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        {/*<Form.Item*/}
                        {/*    label="Rekomendasi Kuota"*/}
                        {/*    {...this.state.formError.leavem_quota_recomendation}>*/}
                        {/*    <InputNumber*/}
                        {/*        min={0}*/}
                        {/*        onChange={this.onChange('leavem_quota_recomendation')}*/}
                        {/*        defaultValue={this.state.formValue.leavem_quota_recomendation} />*/}
                        {/*</Form.Item>*/}
                        <Form.Item
                            label="Perlu Pengganti"
                            {...this.state.formError.leavem_need_replace}>
                            <Radio.Group
                                defaultValue={this.state.formValue.leavem_need_replace}
                                onChange={this.onChange('leavem_need_replace')}
                                buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item
                            label="Status Mengurangi Kuota Cuti"
                            {...this.state.formError.leavem_reduce_quota_status}>
                            <Radio.Group
                                defaultValue={this.state.formValue.leavem_reduce_quota_status}
                                onChange={this.onChange('leavem_reduce_quota_status')}
                                buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="custom">Custom Input</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                            &nbsp;&nbsp;
                            <Popover content={popPenguranganKuotaCutiDesc}
                                     title={popPenguranganKuotaCutiTitle}>
                                <Button type={"warning"} icon={"question-circle"}>Keterangan</Button>
                            </Popover>
                        </Form.Item>
                        {
                            this.state.formValue.leavem_reduce_quota_status === 'custom' ?
                                <Form.Item
                                    label="Jumlah Pengurangan Kuota Cuti"
                                    {...this.state.formError.leavem_reduce_quota_value}>
                                    <InputNumber
                                        min={0}
                                        onChange={this.onChange('leavem_reduce_quota_value')}
                                        defaultValue={this.state.formValue.leavem_reduce_quota_value}/>
                                </Form.Item> : ''
                        }
                        <Form.Item
                            label="Minimal Hari Pengajuan Cuti/Ijin"
                            {...this.state.formError.leavem_min_request_date}>
                            <InputNumber
                                min={0}
                                onChange={this.onChange('leavem_min_request_date')}
                                defaultValue={this.state.formValue.leavem_min_request_date}/>
                        </Form.Item>
                        <Form.Item
                            label="Perlu Maksimal Durasi Cuti/Ijin"
                            {...this.state.formError.leavem_need_replace}>
                            <Radio.Group
                                defaultValue={this.state.formValue.leavem_need_max_duration}
                                onChange={this.onChange('leavem_need_max_duration')}
                                buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        {
                            this.state.formValue.leavem_need_max_duration === 'no' ? '' :
                                <Form.Item
                                    label="Jumlah Maksimal Durasi Cuti/Ijin"
                                    {...this.state.formError.leavem_max_duration}>
                                    <InputNumber
                                        min={0}
                                        onChange={this.onChange('leavem_max_duration')}
                                        defaultValue={this.state.formValue.leavem_max_duration}/>
                                </Form.Item>
                        }
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                    loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PostData('/leave-master', this.state.formValue)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

const popPenguranganKuotaCutiTitle = (
    <div>
        <Icon type={"question-circle"} /> Informasi
    </div>
);
const popPenguranganKuotaCutiDesc = (
    <div>
        <strong>Ya</strong> : Pengurangan kuota cuti sesuai dengan durasi yang diinputkan oleh staff<br />
        <strong>Custom Input</strong> : Pengurangan kuota cuti sesuai dengan durasi yang diinputkan pada kolom dibawah<br />
        <strong>Tidak</strong> : Tidak ada pengurangan kuota cuti<br />
    </div>
);