import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import StaffLeave from "./1_0_StaffLeave_Data";
import LeaveKuota from "./2_0_LeaveKuota_Data";
import LeaveMassal from "./3_0_LeaveMassal_List";
import LeaveManage from "./4_0_LeaveManage_List";

export default class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>

                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="staff-leave">
                                <TabPane tab={<span><Icon type="container" /> Staf Leave</span>}
                                         key="staff-leave">
                                    <StaffLeave />
                                </TabPane>
                                <TabPane tab={<span><Icon type="swap" /> Kuota Leave</span>} key="leave-kuota">
                                    <LeaveKuota />
                                </TabPane>
                                <TabPane tab={<span><Icon type="usergroup-delete" /> Leave Masal</span>} key="leave-massal">
                                    <LeaveMassal />
                                </TabPane>
                                <TabPane tab={<span><Icon type="ordered-list" /> Manajemen Leave</span>} key="leave-management">
                                    <LeaveManage />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }


}


let pageNow = GENERALDATA.primaryMenu.staff_attendence.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.staff_attendence;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];