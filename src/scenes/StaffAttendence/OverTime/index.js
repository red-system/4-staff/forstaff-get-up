import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import StaffLeaveWaiting from "./Waiting";
import StaffLeaveApproved from "./Approved";
import StaffLeaveConfirmed from "./Confirmed";
import StaffLeaveCanceled from "./Canceled";
import StaffLeaveRejected from "./Rejected";
import {menuActionViewAccess} from "../../../services/app/General";


export default class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type={"card"} defaultActiveKey="waiting-confirmed">
                                <Tabs.TabPane tab={<span><Icon type={"bars"}/> Menunggu Persetujuan</span>}
                                              key="waiting-confirmed">
                                    <StaffLeaveWaiting
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={<span><Icon type={"check"}/> Disetujui</span>} key="approved">
                                    <StaffLeaveApproved
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={<span><Icon type={"check-circle"}/> Dikonfirmasi</span>} key="confirmed">
                                    <StaffLeaveConfirmed
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={<span><Icon type={"close"}/> Dibatalkan</span>} key="canceled">
                                    <StaffLeaveCanceled
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </Tabs.TabPane>
                                <Tabs.TabPane tab={<span><Icon type={"close-circle"}/> Ditolak</span>} key="rejected">
                                    <StaffLeaveRejected
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </Tabs.TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }
}


let pageNow = GENERALDATA.primaryMenu.staff_attendence.sub_menu["5"];
let pageParent = GENERALDATA.primaryMenu.staff_attendence;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];