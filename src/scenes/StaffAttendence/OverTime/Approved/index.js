import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Detail from "../Detail";
import Confirm from "../Confirm";
import Reject from "../Reject";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff.staff_name") != null) {
                staffName = get(record, "staff.staff_name").toString().match(reg);
            }
            var posittionName = false;
            if (get(record, "staff.position.structure_name") != null) {
                posittionName = get(record, "staff.position.structure_name").toString().match(reg);
            }
            var overtimeDateRequest = false;
            if (get(record, "overtime_date_request_format") != null) {
                overtimeDateRequest = get(record, "overtime_date_request_format").toString().match(reg);
            }
            var overtimeDate = false;
            if (get(record, "overtime_date_format") != null) {
                overtimeDate = get(record, "overtime_date_format").toString().match(reg);
            }
            var overtimeReason = false;
            if (get(record, "overtime_reason") != null) {
                overtimeReason = get(record, "overtime_reason").toString().match(reg);
            }
            if (!staffName && !posittionName && !overtimeDateRequest && !overtimeDate && !overtimeReason) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "overtime", "list_approve");
        let menuDetail = menuActionViewAccess("staff_attendence", "overtime", "detail_approve");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Waktu Pengajuan Lembur",
                dataIndex: "overtime_date_request_format",
                key: "overtime_date_request_format",
            },
            {
                title: "Tanggal Lembur",
                dataIndex: "overtime_date_format",
                key: "overtime_date_format",
            },
            {
                title: "Keterangan",
                dataIndex: "overtime_reason",
                key: "overtime_reason",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <Detail record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                staff_name: item.staff.staff_name,
                structure_name: item.staff.position.structure_name,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/approved-staff-overtime").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}
