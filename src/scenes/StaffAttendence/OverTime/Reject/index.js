import {Button, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {PutData} from "../../../../services/api";

export default class Delete extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }
    }

    render() {

        const text = 'Tolak lembur staff ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger" icon={"close"} loading={this.state.isLoading}>
                    Tolak
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        PutData(`/reject-staff-overtime/${props.record.overtime_id}`)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
                setTimeout(hide, 0);
            });
    }
}