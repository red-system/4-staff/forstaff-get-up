import {Button, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {PutData} from "../../../../services/api";

export default class Delete extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }
    }

    render() {

        const text = 'Setujui lembur staff ini ?';

        return (
            <Popconfirm
                placement="leftTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="success" icon={"check"} loading={this.state.isLoading}>
                    Setujui
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        PutData(`/approve-staff-overtime/${props.record.overtime_id}`)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
                setTimeout(hide, 0);
            });
    }
}