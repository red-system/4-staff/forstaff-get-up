import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import Wait from "./1_Wait";
import Confirm from "./2_Confirm";
import Reject from "./3_Reject";

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["9"];
let pageParent = GENERALDATA.primaryMenu.staff_management;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefreshRun: this.listRefreshRun
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>

                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="wait">
                                <TabPane tab={<span><Icon type="loading" /> Menunggu Konfirmasi</span>}
                                         key="wait">
                                    <Wait {...this.state}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="check" /> Sudah Dikonfirmasi</span>} key="confirm">
                                    <Confirm {...this.state}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="close" /> Sudah Ditolak</span>} key="reject">
                                    <Reject {...this.state}/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default RewardTabs;