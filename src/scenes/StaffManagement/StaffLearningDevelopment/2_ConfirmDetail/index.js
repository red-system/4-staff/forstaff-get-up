import React, {Component} from 'react';
import {Button, Form, Icon, Modal} from "antd";

const FormItem = Form.Item;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue: props.record
        };
    }


    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };

        return [
            <Button type="primary" onClick={this.showModal}>
                <Icon type="info-circle"/>
                Detil
            </Button>,
            <Modal
                visible={this.state.visible}
                onCancel={this.handleCancel}
                className="custom-modal-v1"
                width={1000}
                footer={null}
            >
                <section className="form-card row no-gutters">
                    <div className="form-card__img form-card__img--left col-lg-5"
                         style={{backgroundImage: `url('${this.state.formValue.staff_avatar_url}')`}}></div>
                    <div className="form-card__body col-lg-7 p-4">
                        <section className="form-v1-container">
                            <h4><Icon type="info-circle"/> Detil Claim</h4>
                            <hr/>
                            <Form>
                                <FormItem
                                    {...formItemLayout}
                                    label="Nama Staf">
                                    {this.state.formValue.staff_name}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Posisi">
                                    {this.state.formValue.structure_name}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Nominal">
                                    {this.state.formValue.learningd_nominal_view}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Status"
                                >
                                    {this.state.formValue.learningd_status}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Attachment"
                                >
                                    <Button type="success"
                                            onClick={this.openUrl(this.state.formValue.learningd_filename_url)}><Icon
                                        type="file"/> Lihat Attachment</Button>
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Keterangan"
                                >
                                    {this.state.formValue.learningd_reason}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Waktu Pengajuan"
                                >
                                    {this.state.formValue.created_at}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Dikonfirmasi Oleh"
                                >
                                    {this.state.formValue.confirm_staff_name}
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="Waktu konfirmasi"
                                >
                                    {this.state.formValue.learningd_confirm_date}
                                </FormItem>
                                <hr/>
                                <div className="text-center">
                                    <Button type="warning" onClick={this.handleCancel}>
                                        <Icon type="rollback"/> Tutup
                                    </Button>
                                </div>
                            </Form>

                        </section>
                    </div>
                </section>
            </Modal>
        ]
    }

    openUrl = url => e => {
        window.open(url, "_blank")
    }
}

export default Create;