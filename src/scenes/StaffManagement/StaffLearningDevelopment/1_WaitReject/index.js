import {Button, Icon, Input, Popover, message} from "antd";
import React, {Component} from "react";
import {PostData} from "../../../../services/api";

const {TextArea} = Input;

class Delete extends Component {
    constructor(props){
        super(props);
        this.state = {
            learningd_reject_reason:null,
        };
    }

    render() {
        const state = this.state
        const props = this.props
        function reject() {
            const formData = new FormData();
            console.log(state.learningd_reject_reason)
            formData.append('learningd_reject_reason',state.learningd_reject_reason)
            formData.append('learning_development_id',props.record.key)
            PostData('/learning-development/reject/do',formData).then((result)=>{
                if(result.data.status ==="success"){
                    props.listRefreshRun(true)
                    message.success(result.data.message)
                } else {
                    message.error(result.data.message)
                }
            })
        }
        const content = (
            <div>
                <TextArea onChange={this.onChange()}/>
                <div style={{marginTop: "10px"}} className={"text-center"}>
                    <Button type={"success"} size={"small"} onClick={reject}><Icon type={"check"}/> Kirim</Button>
                </div>
            </div>
        );

        return (

            <Popover content={content} title="Alasan Ditolak" trigger="click">
                <Button type="danger" >
                    <Icon type="close"/>
                    Tolak
                </Button>
            </Popover>
        )
    }
    onChange = () => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }

        this.setState({
            learningd_reject_reason:value
        })
    }
}

export default Delete;