import React, {Component} from 'react';
import {Button, Icon, message, Popconfirm} from "antd";
import {PostData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };

    }



    render() {

        const text = 'Yakin konfirmasi Claim ini?';
        const props = this.props
        function confirm() {
            const formData = new FormData();
            formData.append("learning_development_id",props.record.key)
           PostData('/learning-development/confirm/do',formData).then((result)=>{
               if(result.data.status === "success"){
                   props.listRefreshRun(true)
                   message.success(result.data.message)
               } else {
                   message.error(result.data.message)
               }
           })
        }

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Ya"
                cancelText="Tidak"
            >
                <Button type="success" onClick={this.modalEditShow}>
                    <Icon type="check"/>
                    Konfirmasi
                </Button>
            </Popconfirm>
        )
    }
}

export default Create;