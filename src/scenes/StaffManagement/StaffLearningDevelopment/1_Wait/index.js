import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Detail from "../1_WaitDetail";
import Confirm from "../1_WaitConfirm";
import Reject from "../1_WaitReject";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var larningDateMatch = false;
            if (get(record, "learningd_date_format")) {
                larningDateMatch = get(record, "learningd_date_format").match(reg);
            }
            var learningreasonmatch = false;
            if (get(record, "learningd_reason")) {
                learningreasonmatch = get(record, "learningd_reason").match(reg);
            }
            var staffNameMatch = false;
            if (get(record, "staff_name") != null) {
                staffNameMatch = get(record, "staff_name").match(reg);
            }
            var positionMatch = false;
            if (get(record, "structure_name") != null) {
                positionMatch = get(record, "structure_name").match(reg);
            }
            var learningNominalMatch = false;
            if (get(record, "learningd_nominal_view") != null) {
                learningNominalMatch = get(record, "learningd_nominal_view").match(reg);
            }
            if (!staffNameMatch && !positionMatch && !learningreasonmatch && !larningDateMatch && !learningreasonmatch && !learningNominalMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListWait = menuActionViewAccess("staff_management", "learning_development", "list_wait");
        let menuDetailWait = menuActionViewAccess("staff_management", "learning_development", "detail_wait");
        let menuConfirmWait = menuActionViewAccess("staff_management", "learning_development", "confirm_wait");
        let menuRejectWait = menuActionViewAccess("staff_management", "learning_development", "reject_wait");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Tanggal Pelatihan",
                dataIndex: "learningd_date_format",
                key: "learningd_date_format",
            },
            {
                title: "Keterangan",
                dataIndex: "learningd_reason",
                key: "learningd_reason",
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Nominal",
                dataIndex: "learningd_nominal_view",
                key: "learningd_nominal_view",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 260,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetailWait.class}>
                            <Detail record={record} listRefreshRun={this.props.listRefreshRun} formLayout={formLayout} />
                        </span>
                        <span className={menuConfirmWait.class}>
                            <Confirm record={record} listRefreshRun={this.props.listRefreshRun} formLayout={formLayout} />
                        </span>
                        <span className={menuRejectWait.class}>
                            <Reject record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["learning_development_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListWait.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        GetData("/learning-development/wait").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
