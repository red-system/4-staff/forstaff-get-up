import React, {Component} from 'react';
import {Col, Row} from "antd";

import Create from './Create';
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {GetData} from "../../../services/api";


class RewardGuide extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun,
            dataStaff: []
        }
    }

    componentWillMount() {
        GetData('/staff').then((result) => {
            this.setState({
                dataStaff: result.data.data
            })
        });

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Create {...this.state}/>
                                <List {...this.state} />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }
}


let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default RewardGuide;