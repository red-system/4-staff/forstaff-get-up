import React, { Component } from "react";
import { Button, Form, Icon, Modal, Select, message } from "antd";
import { PostData } from "../../../../services/api";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            listRefreshRun: props.listRefreshRun,
            dataStaff: [],

            department_title: props.record.department_title,
            structure_name: props.record.structure_name,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataStaff: nextProps.dataStaff,
        });
    }

    render() {
        const { dataStaff } = this.state;

        return [
            <Button type="success" onClick={this.modalStatus(true)} icon={"edit"}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data
                    </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Staf" {...this.state.formError.staff_id}>
                        <Select
                            defaultValue={`${this.state.formValue.staff.staff_name}`}
                            onChange={this.onChange("staff_id")}
                            showSearch
                            placeholder="Pilih Staf"
                            optionFilterProp="children"
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {Object.keys(dataStaff).map((key) => {
                                return (
                                    <Select.Option
                                        value={`${dataStaff[key]["staff_id"]}|${dataStaff[key]["department_title"]}|${dataStaff[key]["structure_name"]}`}
                                    >
                                        {dataStaff[key]["staff_name"]}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Departemen">{this.state.formValue.staff.position.department.department_title}</Form.Item>
                    <Form.Item label="Posisi">{this.state.formValue.staff.position.structure_name}</Form.Item>
                    <Form.Item label="Upload File Deksripsi Pekerjaan" {...this.state.formError.staff_filename}>
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File</button>
                            <input type="file" name="myfile" onChange={this.handleChange} />
                        </div>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_filename: event.target.files[0],
            },
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }

        if (name === "staff_id") {
            let valueArr = value.split("|");
            value = valueArr[0];
            let department_title = valueArr[1];
            let structure_name = valueArr[2];

            this.setState({
                department_title: department_title,
                structure_name: structure_name,
            });
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        const formData = new FormData();
        formData.append("staff_id", this.state.formValue.staff_id);
        formData.append("staff_filename", this.state.formValue.staff_filename);

        PostData(`/staff-job-description-update/${this.state.formValue.staff_job_desc_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 10,
        },
    },
};

export default Create;
