import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";

class Delete extends Component {

    render() {

        const text = 'Yakin hapus data ini ?';
        const props = this.props;

        function confirm() {
            DeleteData(`/staff-job-description/${props.record.staff_job_desc_id}`)
                .then((result) => {
                    props.listRefreshRun(true);
                    message.success(result.data.message);
                    this.setState({isLoading: false});
                });
        }

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger">
                    <Icon type="close"/>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }
}

export default Delete;