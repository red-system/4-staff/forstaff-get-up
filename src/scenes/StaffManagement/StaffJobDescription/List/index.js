import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Edit from "../Edit";
import Delete from "../Delete";
import ViewFile from "../ViewFile";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            listRefreshRun: props.listRefreshRun,
            dataStaff: props.dataStaff,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }

        this.setState({
            dataStaff: nextProps.dataStaff,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            const nameMatch = get(record, "staff.staff_name").match(reg);
            const departmentMath = get(record, "staff.position.department.department_title").match(reg);
            const positionMatch = get(record, "staff.position.structure_name").match(reg);
            if (!nameMatch && !departmentMath && !positionMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuViewFile = menuActionViewAccess("staff_management", "staff_job_description", "view_file");
        let menuEdit = menuActionViewAccess("staff_management", "staff_job_description", "edit");
        let menuDelete = menuActionViewAccess("staff_management", "staff_job_description", "delete");
        let menuList = menuActionViewAccess("staff_management", "staff_job_description", "list");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                key: "no",
                width: 20,
            },
            {
                title: "Staf",
                dataIndex: "staff.staff_name",
                key: "staff_name",
                className: "th-300",
            },
            {
                title: "Departemen",
                dataIndex: "staff.position.department.department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "staff.position.structure_name",
                key: "structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 250,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuViewFile.class}>
                            <ViewFile record={record} />
                        </span>
                        <span className={menuEdit.class}>
                            <Edit record={record} dataStaff={this.state.dataStaff} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_job_desc_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={data} bordered={true} size="small" loading={this.state.isLoading} />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/staff-job-description").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
