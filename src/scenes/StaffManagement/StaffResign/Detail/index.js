import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, Row, Popover, Input, message} from "antd";
import {PostData} from "../../../../services/api";
import Preview from "../Preview";
import {menuActionViewAccess} from "../../../../services/app/General";

const {TextArea} = Input;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue: props.record,
            profile: props.profile,
            reason: {
                resign_accept_reason: '',
                resign_reject_reason: '',
            }
        };

        this.modalClose = this.modalClose.bind(this);
        this.modalEditCancel = this.modalEditCancel.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            formlayout: nextProps.formLayout,
            formValue: nextProps.record,
        });
    }

    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel() {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalClose() {
        this.setState({
            modalEditVisible: false,
        });
    };

    render() {

        const {modalEditVisible, formLayout} = this.state;
        let menuAccept = menuActionViewAccess('staff_management', 'resign', 'accept');
        let menuReject = menuActionViewAccess('staff_management', 'resign', 'reject');

        var resign_accept_reason = '';
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const popoverContentApprove = (
            <span>
                <TextArea onChange={this.onChangeRason("resign_accept_reason")}/>
                <div className={"text-center"} style={{marginTop: "10px"}}>
                    <Button type={"success"} size={"small"} onClick={this.sendApprove()}><Icon
                        type={"check"}/> Kirim</Button>
                </div>
            </span>
        );

        const popoverContentReject = (
            <span>
                <TextArea onChange={this.onChangeRason("resign_reject_reason")}/>
                <div className={"text-center"} style={{marginTop: "10px"}}>
                    <Button type={"success"} size={"small"} onClick={this.sendReject()}><Icon
                        type={"check"}/> Kirim</Button>
                </div>
            </span>
        );

        return [
            <Button type="primary" onClick={this.modalEditShow}>
                <Icon type="poweroff"/>
                Detil Resign
            </Button>,
            <Modal
                visible={modalEditVisible}
                width={700}
                className="custom-modal-v1"
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={null}
            >
                <section className="form-card row no-gutters">
                    <div className="form-card__img form-card__img--left col-lg-5"
                         style={{backgroundImage: `url('${this.state.formValue.staff_avatar_url}')`}}/>
                    <div className="form-card__body col-lg-7 p-4">
                        <section className="form-v1-container">
                            <h4><Icon type="info-circle"/> Detil Resign</h4>
                            <hr/>
                            <Form {...formItemLayout}>
                                <Form.Item
                                    label="Nama Staf">
                                    {this.state.formValue.staff_name}
                                </Form.Item>
                                <Form.Item
                                    label="Departemen">
                                    {this.state.formValue.department_title}
                                </Form.Item>
                                <Form.Item
                                    label="Posisi">
                                    {this.state.formValue.structure_name}
                                </Form.Item>
                                <Form.Item
                                    label="Bergabung Sejak">
                                    {this.state.formValue.staff_work_start_view}
                                </Form.Item>
                                <Form.Item
                                    label="Total Waktu Bekerja">
                                    {this.state.formValue.staff_work_total_time}
                                </Form.Item>
                                <Form.Item
                                    label="KPI Staf">
                                    {this.state.formValue.kpi_rc_name}
                                </Form.Item>
                                <Form.Item
                                    label="KPI Staf Tahun">
                                    {this.state.formValue.kpis_year}
                                </Form.Item>
                                <Form.Item
                                    label="KPI Staf Bulan">
                                    {this.state.formValue.kpis_month}
                                </Form.Item>
                                <Form.Item
                                    label="Alasan Resign">
                                    {this.state.formValue.resign_reason}
                                </Form.Item>
                                {this.state.formValue.accept_staff != null &&
                                <Form.Item
                                    label="Disetujui oleh">
                                    {this.state.formValue.accept_staff.staff_name}
                                </Form.Item>
                                }
                                {this.state.formValue.accept_staff != null &&
                                <Form.Item
                                    label="Tanggal disetujui">
                                    {this.state.formValue.resign_accept_date}
                                </Form.Item>
                                }
                                {this.state.formValue.accept_staff != null &&
                                <Form.Item
                                    label="Alasan disetujui">
                                    {this.state.formValue.resign_accept_reason}
                                </Form.Item>
                                }
                                {this.state.formValue.reject_staff != null &&
                                <Form.Item
                                    label="Ditolak oleh">
                                    {this.state.formValue.reject_staff.staff_name}
                                </Form.Item>
                                }
                                {this.state.formValue.reject_staff != null &&
                                <Form.Item
                                    label="Tanggal ditolak">
                                    {this.state.formValue.resign_reject_date}
                                </Form.Item>
                                }
                                {this.state.formValue.reject_staff != null &&
                                <Form.Item
                                    label="Alasan ditolak">
                                    {this.state.formValue.resign_reject_reason}
                                </Form.Item>
                                }
                                <Form.Item
                                    label="File Surat Resign">
                                    <Preview
                                        resign_filename_url={this.state.formValue.resign_filename_url}
                                        resign_filename_type={this.state.formValue.resign_filename_type}/>
                                </Form.Item>
                            </Form>
                            <hr/>
                            <Row style={{textAlign: 'left'}}>
                                <Col {...formLayout}>
                                    {(this.state.formValue.resign_status === "process" && this.state.profile === false) &&
                                        <span>
                                            <Popover content={popoverContentApprove} title="Alasan Disetujui"
                                                     trigger="click">
                                                <Button className={menuAccept.class} key="submit" type="success" icon={"check"}>
                                                    Setujui
                                                </Button>
                                            </Popover>
                                            &nbsp;
                                            <Popover content={popoverContentReject} title="Alasan Ditolak"
                                                     trigger="click">
                                                <Button className={menuReject.class} key="submit" type="danger" icon={"close"}>
                                                    Tolak
                                                </Button>
                                            </Popover>
                                        </span>
                                    }
                                    &nbsp;
                                    <Button key="back" onClick={this.modalEditCancel}>
                                        <Icon type="rollback"/> Tutup
                                    </Button>
                                </Col>
                            </Row>
                        </section>
                    </div>
                </section>
            </Modal>
        ]
    }


    onChangeRason = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            reason: {
                ...this.state.reason,
                [name]: value
            }
        });
    };


    sendApprove = () => e => {
        const formData = new FormData();
        let resign_id = this.props.record.resign_id;

        formData.append('resign_accept_reason', this.state.reason.resign_accept_reason);
        formData.append('resign_id', resign_id);
        PostData(`/resign/accept`, formData)
            .then((result) => {
                this.props.listRefreshRun(true);
                message.success(result.data.message);
                this.modalClose();
            });
    }
    sendReject = () => e => {
        const formData = new FormData();
        let resign_id = this.props.record.resign_id;

        formData.append('resign_reject_reason', this.state.reason.resign_reject_reason);
        formData.append('resign_id', resign_id);
        PostData(`/resign/reject`, formData)
            .then((result) => {
                this.props.listRefreshRun(true);
                message.success(result.data.message);
                this.modalClose();
            });
    }
}