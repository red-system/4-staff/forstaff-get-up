import React, {Component} from 'react';
import {Col, Row} from "antd";
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

class Resign extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            popOver:false,
            listRefreshRun: this.listRefreshRun
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }
    render() {
        return (
            <div onClick={this.test}>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <List {...this.state}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["11"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];


export default Resign;