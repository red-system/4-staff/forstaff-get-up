import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Detail from "../Detail";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class Resign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataKpiResultCategory, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "department_title") != null) {
                departmentTitle = get(record, "department_title").toString().match(reg);
            }
            var positionName = false;
            if (get(record, "structure_name") != null) {
                positionName = get(record, "structure_name").toString().match(reg);
            }
            var resignProcessDate = false;
            if (get(record, "resign_process_date") != null) {
                resignProcessDate = get(record, "resign_process_date").toString().match(reg);
            }
            var resignReason = false;
            if (get(record, "resign_reason") != null) {
                resignReason = get(record, "resign_reason").toString().match(reg);
            }
            if (!staffName && !departmentTitle && !positionName && !resignProcessDate && !resignReason) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataKpiResultCategory,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataKpiResultCategory,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListResign = menuActionViewAccess("staff_management", "resign", "list");
        let menuDetailResign = menuActionViewAccess("staff_management", "resign", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Departement", dataIndex: "department_title", key: "department_title" },
            { title: "Posisi", dataIndex: "structure_name", key: "structure_name" },
            { title: "Tanggal", dataIndex: "resign_process_date", key: "resign_process_date" },
            { title: "Alasan", dataIndex: "resign_reason", key: "resign_reason" },
            {
                title: "Status",
                dataIndex: "resign_status",
                key: "resign_status",
                render: (status) => {
                    return status_view(status);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetailResign.class}>
                            <Detail profile={false} record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["resign_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListResign.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        GetData("/resign").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default Resign;
