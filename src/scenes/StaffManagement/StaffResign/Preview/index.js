import React, {Component} from 'react';
import {Button, Icon, Modal} from "antd";
import {renderFileType} from "../../../../services/app/General";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {

        console.log('preview', this.props);

        return (
            <span>
                <Button type="default" onClick={this.modalStatus(true)}>
                    <Icon type="eye"/>
                    Preview
                </Button>

               <Modal
                   title="File Resign Staff"
                   visible={this.state.modalVisible}
                   onCancel={this.modalStatus(false)}
                   width={900}
                   footer={[
                       <Button key="back" onClick={this.modalStatus(false)}>
                           Return
                       </Button>
                   ]}
               >
                   {
                       renderFileType(this.props.resign_filename_type, this.props.resign_filename_url)
                   }
                </Modal>
            </span>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}

export default Create;