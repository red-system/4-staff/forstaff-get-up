import React, { Component } from "react";
import { Button, Table } from "antd";

import Create from "../BenefitDataCreate";
import Edit from "../BenefitDataEdit";
import Delete from "../BenefitDataDelete";
import { menuActionViewAccess } from "../../../../services/app/General";
import SearchbarTable from "../../../../components/SearchbarTable";
import { Icon, Input } from "antd";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataBenefit: props.dataBenefit,
            listRefreshRun: props.listRefreshRun,
            isLoadingBenefit: props.isLoadingBenefit,
            dataSearchedBenefit: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            listRefreshRun: nextProps.listRefreshRun,
            dataBenefit: nextProps.dataBenefit,
            isLoadingBenefit: nextProps.isLoadingBenefit,
            dataSearchedBenefit: nextProps.dataBenefit,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataBenefit, (record) => {
            const benefitTitleMatch = get(record, "benefit_title").match(reg);
            const benefitDescMatch = get(record, "benefit_description").match(reg);
            if (!benefitTitleMatch && !benefitDescMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearchedBenefit: e.target.value ? filteredData : this.state.dataBenefit,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearchedBenefit: this.state.data,
            searchText: "",
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_benefit", "list_benefit");
        let menuCreate = menuActionViewAccess("staff_management", "staff_benefit", "create_benefit");
        let menuEdit = menuActionViewAccess("staff_management", "staff_benefit", "edit_benefit");
        let menuDelete = menuActionViewAccess("staff_management", "staff_benefit", "delete_benefit");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Judul Benefit",
                dataIndex: "benefit_title",
                key: "benefit_title",
            },
            {
                title: "Deskripsi Benefit",
                dataIndex: "benefit_description",
                key: "benefit_description",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearchedBenefit.map((item, key) => {
            return {
                key: item["benefit_id"],
                no: key + 1,
                benefit_title: item["benefit_title"],
                benefit_description: item["benefit_description"],
            };
        });

        return (
            <div>
                <span className={menuCreate.class}>
                    <Create listRefreshRun={this.state.listRefreshRun} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoadingBenefit} size="small" />
            </div>
        );
    }
}

export default RewardGuide;
