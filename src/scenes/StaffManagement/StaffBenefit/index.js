import React, { Component } from "react";
import { Col, Icon, Row, Tabs } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";

import BenefitList from "./BenefitDataList";
import Benefit from "./BenefitStaff";
import { GetData } from "../../../services/api";
import { menuActionViewAccess } from "../../../services/app/General";

class RewardTabs extends Component {
    constructor() {
        super();
        this.state = {
            isLoadingBenefit: true,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun,
            breadcrumbComponent: null,
            dataBenefit: [],
        };
    }

    async componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });

        await GetData("/benefit").then((result) => {
            this.listRefreshRun(true);
            this.setState({
                dataBenefit: result.data.data,
                isLoadingBenefit: false,
            });
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.listRefresh || this.state.isLoadingBenefit) {
            GetData("/benefit").then((result) => {
                this.setState({
                    dataBenefit: result.data.data,
                    listRefresh: false,
                    isLoadingBenefit: false,
                });
            });
        }
    }

    render() {
        let menuListBenefit = menuActionViewAccess("staff_management", "staff_benefit", "list_benefit");
        let menuListBenefitStaff = menuActionViewAccess("staff_management", "staff_benefit", "list_benefit_staff");

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="panduan-reward">
                                <TabPane
                                    className={menuListBenefit.class}
                                    tab={
                                        <span>
                                            <Icon type="ordered-list" /> Daftar Benefit
                                        </span>
                                    }
                                    key="panduan-reward"
                                >
                                    <BenefitList
                                        dataBenefit={this.state.dataBenefit}
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.state.listRefreshRun}
                                        isLoadingBenefit={this.state.isLoadingBenefit}
                                    />
                                </TabPane>
                                <TabPane
                                    className={menuListBenefitStaff.class}
                                    tab={
                                        <span>
                                            <Icon type="gift" /> Benefit Staff
                                        </span>
                                    }
                                    key="reward"
                                >
                                    <Benefit
                                        dataBenefit={this.state.dataBenefit}
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.state.listRefreshRun}
                                    />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });

        if (value) {
            this.setState({
                isLoadingBenefit: true,
            });
        }
    };
}

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["5"];
let pageParent = GENERALDATA.primaryMenu.staff_management;
const { TabPane } = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default RewardTabs;
