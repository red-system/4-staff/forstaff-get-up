import React, { Component } from "react";
import { Button, Col, Form, Icon, Modal, Row, Checkbox, message } from "antd";
import { PostData } from "../../../../services/api";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            record: props.record,
            formValue: {
                staff_id: props.record.key,
                benefit_id: props.record.benefit_id_json ? JSON.parse(props.record.benefit_id_json) : [],
            },
            formError: [],
            dataBenefit: props.dataBenefit,
        };
    }

    componentWillReceiveProps(nextProps, nextState, nextContext) {
        this.setState({
            dataBenefit: nextProps.dataBenefit,
        });
    }

    render() {
        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit" />
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data
                    </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Staff">{this.state.record.staff_name}</Form.Item>
                    <Form.Item label="Departemen">{this.state.record.department_title}</Form.Item>
                    <Form.Item label="Posisi">{this.state.record.structure_name}</Form.Item>
                    <Form.Item label="Masa Kerja">{this.state.record.staff_work_start}</Form.Item>
                    <Form.Item label="Daftar Manfaat" {...this.state.formError.benefit_id}>
                        <Checkbox.Group style={{ width: "100%" }} defaultValue={this.state.formValue.benefit_id}>
                            <Row>
                                {this.state.dataBenefit.map((item, key) => {
                                    return (
                                        <Col span={12} key={key}>
                                            <Checkbox onChange={this.onChangeBenefit} key={key} value={item["benefit_id"].toString()}>
                                                {item["benefit_title"]}
                                            </Checkbox>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Checkbox.Group>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    onChangeBenefit = (e) => {
        let value = e.target.value;
        let checked = e.target.checked;
        let benefit_id = this.state.formValue.benefit_id;

        if (checked) {
            benefit_id.push(value);
        } else {
            var index = benefit_id.indexOf(value);
            if (index !== -1) benefit_id.splice(index, 1);
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                benefit_id: benefit_id,
            },
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData("/benefit-staff", this.state.formValue).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                console.log(this.props.listRefreshRun);
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;
