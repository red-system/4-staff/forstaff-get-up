import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, message, Modal, Row} from "antd";
import {PostData} from "../../../../services/api";

const {TextArea} = Input;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            listRefreshRun: props.listRefreshRun
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            listRefreshRun: nextProps.listRefreshRun
        })
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul Benefit"
                        {...this.state.formError.benefit_title}>
                        <Input
                            defaultValue={this.state.formValue.benefit_title}
                            onChange={this.onChange('benefit_title')}/>
                    </Form.Item>
                    <Form.Item
                        label="Deskpripsi Benefit"
                        {...this.state.formError.benefit_description}>
                        <TextArea
                            defaultValue={this.state.formValue.benefit_description}
                            onChange={this.onChange('benefit_description')}/>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }


    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData(`/benefit/${this.state.formValue.key}`, this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 10,
        },
    },
};

export default Create;