import React, {Component} from 'react';
import {Button, Icon, Modal, Alert, Row, Col, Spin} from "antd";

import OrgChart from 'react-orgchart';
import 'react-orgchart/index.css';

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            overview: props.overview,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            overview: nextProps.overview
        })
    }

    render() {

        const {overview} = this.state;
        const MyNodeComponent = ({node}) => {
            return (
                <div className="initechNode" onClick={this.selectStaffIni(node.staff_id, node.staff_name)}>
                    <Row>
                        <Col span={24}>
                            <div
                                className={this.state.formValue.staff_id === node.staff_id ? "job-title select" : "job-title"}>
                                {node.structure_name}
                            </div>
                            <div
                                className={this.state.formValue.staff_id === node.staff_id ? "job-desc select" : "job-desc"}>
                                {node.staff_name}
                            </div>
                        </Col>
                    </Row>
                </div>
            );

        };

        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)}>
                    <Icon type="edit"/>
                    Ganti Personal
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="user"/> Pilih Personal
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={"900"}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Alert
                        message="Perhatian"
                        description="Untuk memilih personal yang akan dipilih, silahkan klik personal pada bagan."
                        type="info"
                        showIcon
                    />
                    <div className="overview-wrapper-footer">
                        <Spin spinning={this.state.isLoading} tip={"Loading ..."}>
                            <OrgChart tree={overview} NodeComponent={MyNodeComponent}/>
                        </Spin>
                    </div>

                </Modal>
            </span>
        )
    }

    selectStaffIni = (staff_id, staff_name) => e => {
        this.props.selectStaff(staff_id, staff_name);
        this.setState({
            modalVisible: false,
            formValue: {
                ...this.state.formValue,
                staff_id: staff_id,
                staff_name: staff_name
            }
        });
    };


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };
}