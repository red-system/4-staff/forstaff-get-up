import React, {Component} from 'react';
import {Button, Icon, Modal, Tabs} from "antd";

import DataIndicator from "../2_ConfigKPIManageList";
import Overview from "../2_ConfigKPIManageOverview";
import {menuActionViewAccess} from "../../../../services/app/General";

const {TabPane} = Tabs;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            tabActive: props.tabActive,
            structure: props.structure
        };
    }

    render() {

        let menuOverviewConfigKpi = menuActionViewAccess('staff_management', 'kpi', 'overview_config_kpi');
        let menuManagementConfigKpiCrud = menuActionViewAccess('staff_management', 'kpi', 'management_config_kpi_crud');

        return [
            <Button type="primary" onClick={this.showModal} icon={"info-circle"}>
                Manajemen Indikator
            </Button>,
            <Modal
                title={null}
                visible={this.state.modalVisible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={null}
                width="90%"
                className="modal-has-tabs"
            >

                <div className="card-container">
                    <Tabs type="card" defaultActiveKey={this.state.tabActive}>
                        <TabPane className={menuOverviewConfigKpi.class} tab={<span><Icon type="info-circle"/> Overview</span>} key="overview">
                            <Overview
                                structure={this.state.structure}
                                listRefresh={this.props.listRefresh}
                                listRefreshRun={this.props.listRefreshRun}/>
                        </TabPane>
                        <TabPane className={menuManagementConfigKpiCrud} tab={<span><Icon type="ordered-list"/> Manajemen Indikator</span>} key="manage">
                            <DataIndicator
                                structure={this.state.structure}
                                listRefresh={this.props.listRefresh}
                                listRefreshRun={this.props.listRefreshRun}/>
                        </TabPane>
                    </Tabs>
                </div>

            </Modal>
        ]
    }


    showModal = () => {
        this.setState({
            modalVisible: true,
        });
    };

    handleOk = (e) => {
        this.setState({
            modalVisible: false,
        });
    };
    handleCancel = (e) => {
        this.setState({
            modalVisible: false,
        });
    };
}