import React, {Component} from 'react';
import {Col, Icon, Row, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import DataKPITab from "./1_0_DataKPITab";
import CategoryKPI from "./3_CategoryKPIList";
import ConfigKPIList from "./2_ConfigKPIList";
import PeriodKPIList from "./4_PeriodKPIList";
import FooterKPIList from "./5_FooterKPIList";
import UserKPIPeriode from "../../PersonalMenu/KPI/UserKPIPeriode";

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["10"];
let pageParent = GENERALDATA.primaryMenu.staff_management;
const {TabPane} = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class RewardTabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>

                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="2">
                                <TabPane tab={<span><Icon type="ordered-list" /> Data KPI</span>}
                                         key="1">
                                    <DataKPITab
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="setting" /> Pengaturan KPI</span>} key="2">
                                    <ConfigKPIList
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="star" /> Kategori Hasil KPI</span>} key="3">
                                    <CategoryKPI
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="clock-circle" /> Periode KPI</span>} key="4">
                                    <PeriodKPIList
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type={"usergroup-delete"}/> KPI Footer</span>} key="5">
                                    <FooterKPIList />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default RewardTabs;