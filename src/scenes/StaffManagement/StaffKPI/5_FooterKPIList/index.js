import React, {Component} from 'react';
import {Button, Table, Tag} from "antd";

import Edit from '../5_FooterKPIEdit';
import {GetData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

class RewardGuide extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            breadcrumbComponent: null,
            data: [],
            staffData: [],
            overview: []
        };
    }

    componentWillMount() {
        this.list();
        this.overview();
    }

    render() {
        let menuListKpiFooter = menuActionViewAccess('staff_management', 'kpi', 'list_kpi_footer');
        let menuEditKpiFooter = menuActionViewAccess('staff_management', 'kpi', 'edit_kpi_footer');

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                key: "no",
                width: 20
            },
            {
                title: 'Judul Footer',
                dataIndex: 'kpi_footer_title',
                key: 'kpi_footer_title'
            },
            {
                title: 'Personal',
                dataIndex: 'staff_name',
                key: 'staff_name'
            },
            {
                title: 'Catatan',
                dataIndex: 'kpi_footer_description',
                key: 'kpi_footer_description',
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 70,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEditKpiFooter.class}>
                            <Edit record={record} overview={this.state.overview} list={this.list}  />
                        </span>
                    </Button.Group>

                )
            }
        ];

        const data = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            }

        });

        return (
            <div>
                <Table
                    className={menuListKpiFooter.class}
                    columns={columns}
                       dataSource={data}
                       bordered={true}
                       loading={this.state.isLoading}
                       size='small'/>
            </div>
        )
    }

    list = () => {
        this.setState({
           isLoading: true
        });
        GetData('/kpi-footer-list')
            .then((result) => {
                this.setState({
                    data: result.data.data,
                    isLoading: false,
                })
            })
    };

    overview() {
        GetData('/structure-staff-overview').then((result) => {
            const json = result.data;

            this.setState({
                overview: json.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;