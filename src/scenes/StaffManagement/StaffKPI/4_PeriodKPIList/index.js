import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Create from "../4_PeriodKPICreate";
import Edit from "../4_PeriodKPIEdit";
import Delete from "../4_PeriodKPIDelete";
import Staff from "../4_PeriodKPIStaff";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            breadcrumbComponent: null,
            dataKpiResultCategory: [],
            staffData: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextState.listRefresh) {
            this.list();
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataKpiResultCategory, (record) => {
            var kpiPeriodeTitle = false;
            if (get(record, "kpi_periode_title") != null) {
                kpiPeriodeTitle = get(record, "kpi_periode_title").toString().match(reg);
            }
            var kpiPeriodeYear = false;
            if (get(record, "kpi_periode_year") != null) {
                kpiPeriodeYear = get(record, "kpi_periode_year").toString().match(reg);
            }
            var kpiPeriodeMonth = false;
            if (get(record, "kpi_periode_month_view") != null) {
                kpiPeriodeMonth = get(record, "kpi_periode_month_view").toString().match(reg);
            }
            var kpiPeriodeDescription = false;
            if (get(record, "kpi_periode_description") != null) {
                kpiPeriodeDescription = get(record, "kpi_periode_description").toString().match(reg);
            }
            if (!kpiPeriodeTitle && !kpiPeriodeYear && !kpiPeriodeMonth && !kpiPeriodeDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataKpiResultCategory,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataKpiResultCategory,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListPeriodeKpi = menuActionViewAccess("staff_management", "kpi", "list_periode_kpi");
        let menuCreatePeriodeKpi = menuActionViewAccess("staff_management", "kpi", "create_periode_kpi");
        let menuEditPeriodeKpi = menuActionViewAccess("staff_management", "kpi", "edit_periode_kpi");
        let menuDeletePeriodeKpi = menuActionViewAccess("staff_management", "kpi", "delete_periode_kpi");
        let menuStaffPeriodeKpi = menuActionViewAccess("staff_management", "kpi", "staff_periode_kpi");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                key: "no",
                width: 20,
            },
            {
                title: "Nama Periode KPI",
                dataIndex: "kpi_periode_title",
                key: "kpi_periode_title",
            },
            {
                title: "Tahun Periode",
                dataIndex: "kpi_periode_year",
                key: "kpi_periode_year",
            },
            {
                title: "Bulan Periode",
                dataIndex: "kpi_periode_month_view",
                key: "kpi_periode_month_view",
                render: (text, record) => {
                    return record.kpi_periode_month_view.map((result) => {
                        return result + ", ";
                    });
                },
            },
            {
                title: "Publish",
                dataIndex: "kpi_periode_publish",
                key: "kpi_periode_publish",
                render: (text, record) => {
                    return status_view(record.kpi_periode_publish);
                },
            },
            {
                title: "Deskripsi",
                dataIndex: "kpi_periode_description",
                key: "kpi_periode_description",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 270,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuStaffPeriodeKpi.class}>
                            <Staff record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuEditPeriodeKpi.class}>
                            <Edit record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDeletePeriodeKpi.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
                kpi_periode_month: JSON.parse(item.kpi_periode_month),
            };
        });

        return (
            <div>
                <span className={menuCreatePeriodeKpi.class}>
                    <Create listRefreshRun={this.props.listRefreshRun} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListPeriodeKpi.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/kpi-period-list").then((result) => {
            this.setState({
                dataKpiResultCategory: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
