import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Row, Col, Input, Slider, message, Select} from "antd";
import {PostData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: [],
            listRefreshRun: props.listRefreshRun,
        };
    }

    render() {

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Nama Kategori"
                            {...this.state.formError.kpi_rc_name}>
                            <Input
                                defaultValue={this.state.formValue.kpi_rc_name}
                                onChange={this.onChangeName()}/>
                        </Form.Item>
                        <Form.Item
                            label="Rentang Persentase"
                            {...this.state.formError.kpi_rc_range_start}
                            {...this.state.formError.kpi_rc_range_end}>
                            <Slider
                                range
                                defaultValue={[this.state.formValue.kpi_rc_range_start, this.state.formValue.kpi_rc_range_end]}
                                onChange={this.onChangeRange()}/>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    onChangeName = () => e => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                kpi_rc_name: e.target.value
            }
        });
    };

    onChangeRange = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                kpi_rc_range_start: value[0],
                kpi_rc_range_end: value[1],
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };



    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/kpi-result-category', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 9},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 15},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 9,
        },
    },
};

export default Create;