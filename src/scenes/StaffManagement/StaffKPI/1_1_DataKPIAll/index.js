import React, { Component } from "react";
import { Button, Table, Tag, Dropdown, Menu, Icon, Input } from "antd";

import { GetData } from "../../../../services/api";
import DataKPIDetail from "../1_1_DataKPIDetail";
import Publish from "../1_1_DataKPIAllPublish";
import Edit from "../1_1_DataKPIAllEdit";
import Delete from "../1_1_DataKPIAllDelete";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: true,
            listStaff: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }

        this.setState({
            listStaff: nextProps.listStaff,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffNameMatch = false;
            if (get(record, "staff_name")) {
                staffNameMatch = get(record, "staff_name").toString().match(reg);
            }
            var kpisYearMatch = false;
            if (get(record, "kpis_year")) {
                kpisYearMatch = get(record, "kpis_year".toString()).toString().match(reg);
            }
            var kpisMonthMatch = false;
            if (get(record, "kpis_month_label") != null) {
                kpisMonthMatch = get(record, "kpis_month_label").toString().match(reg);
            }
            var positionMatch = false;
            if (get(record, "structure_name") != null) {
                positionMatch = get(record, "structure_name").toString().match(reg);
            }
            var kpiResultMatch = false;
            if (get(record, "kpi_result_value") != null) {
                kpiResultMatch = get(record, "kpi_result_value").toString().match(reg);
            }
            var kpiCategoryMatch = false;
            if (get(record, "kpi_rc_name") != null) {
                kpiCategoryMatch = get(record, "kpi_rc_name").toString().match(reg);
            }
            var kpiStatusMatch = false;
            if (get(record, "kpis_status") != null) {
                kpiStatusMatch = get(record, "kpis_status").toString().match(reg);
            }
            var kpiPublishMatch = false;
            if (get(record, "learningd_nominal_view") != null) {
                kpiPublishMatch = get(record, "learningd_nominal_view".toString()).match(reg);
            }
            if (
                !kpisMonthMatch &&
                !positionMatch &&
                !kpisYearMatch &&
                !staffNameMatch &&
                !kpiResultMatch &&
                !kpiStatusMatch &&
                !kpiCategoryMatch &&
                !kpiPublishMatch
            ) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListAllDataKpi = menuActionViewAccess("staff_management", "kpi", "list_all_data_kpi");
        let menuPreviewDataKpi = menuActionViewAccess("staff_management", "kpi", "preview_data_kpi");
        let menuPublishDataKpi = menuActionViewAccess("staff_management", "kpi", "publish_data_kpi");
        let menuEditDataKpi = menuActionViewAccess("staff_management", "kpi", "edit_data_kpi");
        let menuDeleteDataKpi = menuActionViewAccess("staff_management", "kpi", "delete_data_kpi");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Tahun",
                dataIndex: "kpis_year",
                key: "kpis_year",
            },
            {
                title: "Bulan",
                dataIndex: "kpis_month_label",
                key: "kpis_month_label",
            },
            {
                title: "Hasil",
                dataIndex: "kpi_result_value",
                key: "kpi_result_value",
            },
            {
                title: "Kategori",
                dataIndex: "kpi_rc_name",
                key: "kpi_rc_name",
            },
            {
                title: "Status",
                dataIndex: "kpis_status",
                key: "kpis_status",
                render: (text, record) => {
                    return status_view(record.kpis_status);
                },
            },
            {
                title: "Publish",
                dataIndex: "kpis_publish",
                key: "kpis_publish",
                render: (text, record) => {
                    return status_view(record.kpis_publish);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => {
                    let show_menu = record.kpis_publish === "no" && record.kpis_status === "process" ? true : false;

                    const menu = (
                        <Menu size={"small"}>
                            <Menu.Item key="1" className={menuPreviewDataKpi.class}>
                                <DataKPIDetail listRefresh={this.props.listRefresh} record={record} />
                            </Menu.Item>
                            {show_menu
                                ? [
                                      <Menu.Item key="2" className={menuPublishDataKpi.class}>
                                          <Publish record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                                      </Menu.Item>,
                                      <Menu.Item key="3" className={menuEditDataKpi.class}>
                                          <Edit
                                              record={record}
                                              listRefresh={this.props.listRefresh}
                                              listRefreshRun={this.props.listRefreshRun}
                                              listStaff={this.state.listStaff}
                                          />
                                      </Menu.Item>,
                                      <Menu.Item key="4" className={menuDeleteDataKpi.class}>
                                          <Delete record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                                      </Menu.Item>,
                                  ]
                                : ""}
                        </Menu>
                    );

                    return (
                        <div id="components-dropdown-demo-dropdown-button">
                            <Dropdown overlay={menu}>
                                <Button type={"primary"} icon={"down"}>
                                    Menu
                                </Button>
                            </Dropdown>
                        </div>
                    );
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListAllDataKpi.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/kpi-data/summary").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
