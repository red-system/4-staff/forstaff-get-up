import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import KPIsDetail from "../1_1_DataKPIDetail";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            listRefresh: props.listRefresh,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }

        this.setState({
            listRefresh: nextProps.listRefresh,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffNameMatch = false;
            if (get(record, "staff_name") != null) {
                staffNameMatch = get(record, "staff_name").toString().match(reg);
            }
            var positionMatch = false;
            if (get(record, "structure_name") != null) {
                positionMatch = get(record, "structure_name").toString().match(reg);
            }
            var kpisYearMatch = false;
            if (get(record, "kpis_year")) {
                kpisYearMatch = get(record, "kpis_year").toString().match(reg);
            }
            var kpiMonthMatch = false;
            if (get(record, "kpis_month_label")) {
                kpiMonthMatch = get(record, "kpis_month_label").toString().match(reg);
            }
            var kpiResultMatch = false;
            if (get(record, "kpi_result_value") != null) {
                kpiResultMatch = get(record, "kpi_result_value").toString().match(reg);
            }
            var kpiCategoryMatch = false;
            if (get(record, "kpi_rc_name") != null) {
                kpiCategoryMatch = get(record, "kpi_rc_name").toString().match(reg);
            }
            if (!staffNameMatch && !positionMatch && !kpisYearMatch && !kpiMonthMatch && !kpiResultMatch && !kpiCategoryMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListAcceptDataKpi = menuActionViewAccess("staff_management", "kpi", "list_accept_data_kpi");
        let menuPreviewAcceptDataKpi = menuActionViewAccess("staff_management", "kpi", "preview_accept_data_kpi");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Tahun",
                dataIndex: "kpis_year",
                key: "kpis_year",
            },
            {
                title: "Bulan",
                dataIndex: "kpis_month_label",
                key: "kpis_month_label",
            },
            {
                title: "Hasil",
                dataIndex: "kpi_result_value",
                key: "kpi_result_value",
            },
            {
                title: "Kategori",
                dataIndex: "kpi_rc_name",
                key: "kpi_rc_name",
            },
            {
                title: "Status",
                dataIndex: "kpis_status",
                key: "kpis_status",
                render: (text, record) => {
                    return status_view(record.kpis_status);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 80,
                render: (text, record) => {
                    return (
                        <Button.Group size={"small"}>
                            <span className={menuPreviewAcceptDataKpi.class}>
                                <KPIsDetail listRefresh={this.state.listRefresh} record={record} />
                            </span>
                        </Button.Group>
                    );
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuListAcceptDataKpi.class}
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/kpi-data/accept/list").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
