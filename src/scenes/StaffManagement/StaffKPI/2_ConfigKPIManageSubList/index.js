import React, {Component} from 'react';
import {Button, Table} from "antd";

import SubIndicatorEdit from "../2_ConfigKPIManageSubEdit";
import SubIndicatorDelete from "../2_ConfigKPIManageSubDelete";
import SubIndicatorCreate from "../2_ConfigKPIManageSubCreate";
import {PostData} from "../../../../services/api";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: true,
            structure: props.structure,
            dataIndicatorSub: [],
            listRefreshSub: true
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if(nextState.listRefreshSub) {
            this.list();
        }
    }

    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Nama Indikator',
                dataIndex: 'indicator_name',
                key: 'indicator_name',
                render: (text, record) => this.props.indicator_name
            },
            {
                title: 'Nama Sub Indikator',
                dataIndex: 'indicator_name',
                key: 'indicator_name'
            },
            {
                title: 'Bobot',
                dataIndex: 'indicator_weight_label',
                key: 'indicator_weight_label',
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 200,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <SubIndicatorEdit
                            record={record}
                            listRefreshSubRun={this.listRefreshSubRun}
                            listRefreshRun={this.props.listRefreshRun}/>
                        <SubIndicatorDelete
                            record={record}
                            listRefreshSubRun={this.listRefreshSubRun}
                            listRefreshRun={this.props.listRefreshRun}/>
                    </Button.Group>

                )
            }
        ];

        const data = this.state.dataIndicatorSub.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                kpi_indicator_id: item['kpi_indicator_id'],
                indicator_name: item['indicator_name'],
                indicator_weight_label: `${item['indicator_weight']}%`,
                indicator_weight: item['indicator_weight'],
            }
        });

        return (
            <div>
                <SubIndicatorCreate
                    listRefreshSubRun={this.listRefreshSubRun}
                    listRefreshRun={this.props.listRefreshRun}
                    kpi_indicator_parent_id={this.props.kpi_indicator_parent_id}
                    structure_id={this.props.structure_id}/>
                <Table columns={columns}
                       dataSource={data}
                       bordered={true}
                       isLoading={this.state.isLoading}
                       size='small'/>
            </div>
        )
    }

    list = () => {
        let data = {
            'kpi_indicator_id': this.props.kpi_indicator_parent_id
        };
        PostData('/kpi-indicator/structure/sub', data)
            .then((result) => {
                this.setState({
                    dataIndicatorSub: result.data.data,
                    isLoading: false,
                    listRefreshSub: false
                })
            });
    };

    listRefreshSubRun = (value) => {
        this.setState({
            listRefreshSub: value
        });
    };

    showModal = () => {
        this.setState({
            modalVisible: true,
        });
    };

    handleOk = (e) => {
        console.log(e);
        this.setState({
            modalVisible: false,
        });
    };
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            modalVisible: false,
        });
    };
}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 17,
        offset: 7
    },
    'lg': {
        span: 17,
        offset: 7
    }
};