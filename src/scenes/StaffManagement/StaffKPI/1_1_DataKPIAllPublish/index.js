import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData, PostData} from "../../../../services/api";

export default class AppComponent extends Component {

    render() {

        const text = 'Publish KPI Staff ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="primary">
                    <Icon type="check"/>
                    Publish
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        let data = {
            kpi_staff_id: props.record.kpi_staff_id
        };
        PostData(`/kpi-data/staff/process/publish`, data)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
                setTimeout(hide, 0);
            });
    }
}