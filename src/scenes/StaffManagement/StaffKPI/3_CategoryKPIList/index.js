import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Create from "../3_CategoryKPICreate";
import Edit from "../3_CategoryKPIEdit";
import Delete from "../3_CategoryKPIDelete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            breadcrumbComponent: null,
            dataKpiResultCategory: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };

        console.log("category kpi list", this.props);
    }

    componentWillMount() {
        this.list();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextState.listRefresh) {
            this.list();
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataKpiResultCategory, (record) => {
            var kpiCategoryMatch = false;
            if (get(record, "kpi_rc_name") != null) {
                kpiCategoryMatch = get(record, "kpi_rc_name").toString().match(reg);
            }
            var rentangmatch = false;
            if (get(record, "rentang") != null) {
                rentangmatch = get(record, "rentang").toString().match(reg);
            }
            if (!kpiCategoryMatch && !rentangmatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataKpiResultCategory,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataKpiResultCategory,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListResultKpi = menuActionViewAccess("staff_management", "kpi", "list_result_kpi");
        let menuCreateResultKpi = menuActionViewAccess("staff_management", "kpi", "create_result_kpi");
        let menuEditResultKpi = menuActionViewAccess("staff_management", "kpi", "edit_result_kpi");
        let menuDeleteResultKpi = menuActionViewAccess("staff_management", "kpi", "delete_result_kpi");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                className: "th-20",
            },
            {
                title: "Nama Kategori",
                dataIndex: "kpi_rc_name",
                key: "kpi_rc_name",
            },
            {
                title: "Rentang Persentase",
                dataIndex: "rentang",
                key: "rentang",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEditResultKpi.class}>
                            <Edit record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDeleteResultKpi.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                kpi_rc_range_start: item["kpi_rc_range_start"],
                kpi_rc_range_end: item["kpi_rc_range_end"],
                key: item["kpi_result_category_id"],
                no: key + 1,
                kpi_rc_name: item["kpi_rc_name"],
                rentang: `${item["kpi_rc_range_start"]}% - ${item["kpi_rc_range_end"]}%`,
            };
        });

        return (
            <div>
                <span className={menuCreateResultKpi.class}>
                    <Create listRefreshRun={this.props.listRefreshRun} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListResultKpi.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/kpi-result-category").then((result) => {
            this.setState({
                dataKpiResultCategory: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
