import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Input, message, Select, Radio} from "antd";
import {PostData} from "../../../../services/api";

import GENERALDATA from "../../../../constants/generalData";
import {yearList} from "../../../../services/app/General";

let monthList = GENERALDATA.monthList;

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            listRefreshRun: props.listRefreshRun,
        };
    }

    render() {


        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul Periode KPI"
                        {...this.state.formError.kpi_periode_title}>
                        <Input
                            defaultValue={this.state.formValue.kpi_periode_title}
                            onChange={this.onChange('kpi_periode_title')}/>
                    </Form.Item>
                    <Form.Item
                        label="Tahun Periode KPI"
                        {...this.state.formError.kpi_periode_year}>
                        <Select
                            showSearch
                            value={this.state.formValue.kpi_periode_year}
                            style={{width: "100%"}}
                            onChange={this.onChangeTime('kpi_periode_year')}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }>
                            {
                                Object.keys(yearList()).map((key) => {
                                    return <Select.Option value={key}>{key}</Select.Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Bulan Periode KPI"
                        {...this.state.formError.kpi_periode_month}>
                        <Select
                            showSearch
                            mode="multiple"
                            value={this.state.formValue.kpi_periode_month}
                            style={{width: "100%"}}
                            onChange={this.onChangeTime('kpi_periode_month')}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }>
                            {
                                Object.keys(monthList).map((key) => {
                                    return <Select.Option value={key}>{monthList[key]}</Select.Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label="Publish Periode KPI"
                        {...this.state.formError.kpi_periode_month}>

                        <Radio.Group defaultValue={this.state.formValue.kpi_periode_publish}
                                     onChange={this.onChange('kpi_periode_publish')} buttonStyle="solid">
                            <Radio.Button value="yes">Ya</Radio.Button>
                            <Radio.Button value="no">Tidak</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        label="Keterangan Periode KPI"
                        {...this.state.formError.kpi_periode_description}>
                        <Input.TextArea
                            defaultValue={this.state.formValue.kpi_periode_description}
                            onChange={this.onChange('kpi_periode_description')}/>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Perbarui
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeTime = (name) => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };


    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/kpi-period-update', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 9},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 15},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 9,
        },
    },
};