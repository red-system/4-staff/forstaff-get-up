import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Input, message, Select, Radio, Row, Col, Spin} from "antd";
import {PostData} from "../../../../services/api";
import FooterKPIStaff from "../5_FooterKPIStaff";

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            list: props.list,
            overview: props.overview,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            overview: nextProps.overview
        })
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul Footer KPI"
                        {...this.state.formError.kpi_periode_title}>
                        <Input
                            defaultValue={this.state.formValue.kpi_footer_title}
                            onChange={this.onChange('kpi_footer_title')}/>
                    </Form.Item>
                    <Form.Item
                        label="Catatan Footer KPI"
                        {...this.state.formError.kpi_periode_description}>
                        <Input.TextArea
                            defaultValue={this.state.formValue.kpi_footer_description}
                            onChange={this.onChange('kpi_footer_description')}/>
                    </Form.Item>
                    <Form.Item
                        label="Personal"
                        {...this.state.formError.staff_id}>
                        <h4 style={{margin: 0, padding: 0}}>{this.state.formValue.staff_name}</h4>
                        <FooterKPIStaff record={this.state.formValue} overview={this.state.overview}
                                        selectStaff={this.selectStaff}/>
                        {/*<div className="overview-wrapper-footer">*/}
                        {/*    <Spin spinning={this.state.isLoading} tip={"Loading ..."}>*/}
                        {/*        <OrgChart tree={overview} NodeComponent={MyNodeComponent}/>*/}
                        {/*    </Spin>*/}
                        {/*</div>*/}
                    </Form.Item>
                    <div className={"wrapper-button-float"}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Perbarui
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </div>
                </Form>

            </Modal>
        ]
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    selectStaff = (staff_id, staff_name) => {
        console.log(staff_id, staff_name);
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_id: staff_id,
                staff_name: staff_name
            }
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/kpi-footer-update', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.list();
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 9},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 15},
    },
};