import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Row, Col, Input, InputNumber, message} from "antd";
import {PostData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,

            formError: [],
            formValue: {
                kpi_indicator_parent_id: props.kpi_indicator_parent_id,
                structure_id: props.structure_id,
                indicator_name: '',
                indicator_weight: ''
            }
        };
    }

    render() {

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Sub Indikator Baru
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Sub Indikator Baru
                        </span>
                    }
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul Indikator"
                            {...this.state.formError.indicator_name}>
                            <Input onChange={this.onChange('indicator_name')}
                                   defaultValue={this.state.formValue.indicator_name}/>
                        </Form.Item>
                        <Form.Item
                            label="Bobot"
                            {...this.state.formError.indicator_weight}>
                            <InputNumber onChange={this.onChange('indicator_weight')}
                                         defaultValue={this.state.formValue.indicator_weight} />
                            <Button>%</Button>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/kpi-indicator', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    this.props.listRefreshSubRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }

}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 7},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 17},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;