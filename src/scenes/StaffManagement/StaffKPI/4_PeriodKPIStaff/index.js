import React, { Component } from "react";
import { Button, Icon, Modal, Table, Tag, Input } from "antd";
import GENERALDATA from "../../../../constants/generalData";
import { GenerateLink, PostData } from "../../../../services/api";
import { map, get } from "lodash";
const Search = Input.Search;

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            staffData: [],
            recordKpiPeriode: props.record,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            recordKpiPeriode: nextProps.recordKpiPeriode,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.staffData, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "department_title") != null) {
                departmentTitle = get(record, "department_title").toString().match(reg);
            }
            var positionName = false;
            if (get(record, "structure_name") != null) {
                positionName = get(record, "structure_name").toString().match(reg);
            }
            if (!staffName && !departmentTitle && !positionName) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.staffData,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.staffData,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                key: "no",
                width: 20,
            },
            {
                title: "Nama",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Departemen",
                dataIndex: "department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 70,
                render: (text, record) => {
                    if (record.kpi_periode_exist) {
                        return (
                            <a href={GenerateLink("/kpi-period-staff/" + this.state.recordKpiPeriode.kpi_periode_id + "/" + record.staff_id)} target={"_blank"}>
                                <Button type={"success"} icon={"file-pdf"} size={"small"}>
                                    View Periode KPI
                                </Button>
                            </a>
                        );
                    } else {
                        return <Tag color={"green"}>Belum ada KPI</Tag>;
                    }
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return [
            <Button type="primary" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete" />
                KPI Staff
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="usergroup-delete" /> Daftar Periode KPI Staff
                    </span>
                }
                visible={this.state.modalVisible}
                width={1000}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </Modal>,
        ];
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        } else {
            this.list();
        }
    };

    list = () => {
        this.setState({
            isLoading: true,
        });
        let data = {
            kpi_periode_id: this.state.recordKpiPeriode.kpi_periode_id,
        };
        PostData("/kpi-period-staff-list", data).then((result) => {
            this.setState({
                staffData: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}
