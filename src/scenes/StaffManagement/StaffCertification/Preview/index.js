import React, {Component} from 'react';
import {Button, Form, Icon, Modal} from "antd";
import moment from "moment";
import {renderFileType} from "../../../../services/app/General";

const FormItem = Form.Item;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {
        return (
            <span>
                <Button type="default" onClick={this.modalStatus(true)}>
                    <Icon type="eye"/>
                    Preview
                </Button>

               <Modal
                   title="File Sertifikasi Staf"
                   visible={this.state.modalVisible}
                   onCancel={this.modalStatus(false)}
                   width={900}
                   footer={[
                       <Button type="warning" icon={"rollback"} onClick={this.modalStatus(false)}>
                           Kembali
                       </Button>
                   ]}
               >
                   {
                       renderFileType(this.props.record.staff_certificate_filename_type, this.props.record.staff_certificate_filename_url)
                   }
                </Modal>
            </span>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}

export default Create;