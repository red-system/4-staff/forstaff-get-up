import React, {Component} from 'react';
import {Button, Icon, Modal, Row, Col, Table} from "antd";
import {GetData} from "../../../../services/api";
import Preview from "../Preview";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            data: [],
            isLoading: true,
        };
    }

    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Keterampilan/Kursus',
                dataIndex: 'staff_certificate_skill',
                key: 'staff_certificate_skill'
            },
            {
                title: 'Tanggal Mulai',
                dataIndex: 'staff_certificate_date_from',
                key: 'staff_certificate_date_from',
            },
            {
                title: 'Tanggal Selesai',
                dataIndex: 'staff_certificate_date_to',
                key: 'staff_certificate_date_to',
            },
            {
                title: 'Level',
                dataIndex: 'staff_certificate_level',
                key: 'staff_certificate_level',
            },
            {
                title: 'Keterangan',
                dataIndex: 'staff_certificate_description',
                key: 'staff_certificate_description',
            },
            {
                title: 'Menu',
                render: (text, record) => {
                    return <Preview record={record}/>
                }
            }

        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete"/>
                Daftar Sertifikasi
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="usergroup-delete"/> Daftar Sertifikasi - {this.state.record.staff_name}
                        </span>
                }
                visible={this.state.modalVisible}
                width={1000}
                onOk={this.modalStatus(true)}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row>
                        <Col>
                            <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Kembali
                            </Button>
                        </Col>
                    </Row>
                ]}
            >

                <Table columns={columns}
                       loading={this.state.isLoading}
                       dataSource={dataList}
                       size='small'/>

            </Modal>
        ]
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    list = () => {
        GetData(`/staff-sertifikasi/${this.state.record.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        })
    }
}

export default Create;