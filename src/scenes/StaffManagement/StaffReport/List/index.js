import React, { Component } from "react";
import { Button, Icon, Table, Input } from "antd";

import { GetData } from "../../../../services/api";
import { Link } from "react-router-dom";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class StaffSallary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var labelMatch = false;
            if (get(record, "staff_id_label")) {
                labelMatch = get(record, "staff_id_label").match(reg);
            }
            const nameMatch = get(record, "staff_name").match(reg);
            const nicknameMatch = get(record, "staff_nickname").match(reg);
            var departmentMatch = false;
            if (get(record, "position.department.department_title") != null) {
                departmentMatch = get(record, "position.department.department_title").match(reg);
            }
            const positionMatch = get(record, "position.structure_name").match(reg);
            if (!labelMatch && !nameMatch && !nicknameMatch && !positionMatch && !departmentMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "report", "list");
        let menuDetail = menuActionViewAccess("staff_management", "report", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "ID Staff",
                dataIndex: "staff_id_label",
                key: "staff_id_label",
            },
            {
                title: "Nama Lengkap",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Nama Panggilan",
                dataIndex: "staff_nickname",
                key: "staff_nickname",
            },
            {
                title: "Departemen",
                dataIndex: "position.department.department_title",
                key: "position.department.department_title",
            },
            {
                title: "Posisi",
                dataIndex: "position.structure_name",
                key: "position.structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (test, record) => (
                    <Link
                        className={menuDetail.class}
                        to={{
                            pathname: `${this.props.match.url}/staff`,
                            state: {
                                record: record,
                            },
                        }}
                    >
                        <Button type={"success"} size={"small"} icon={"profile"}>
                            Detil Laporan
                        </Button>
                    </Link>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });

        GetData("/staff").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}
export default StaffSallary;
