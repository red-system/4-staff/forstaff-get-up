import React, {Component} from 'react';
import {Table} from "antd";
import {PostData} from "../../../../services/api";



export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            staff: props.staff,
            data: [],
        };
    }

    componentWillMount() {
        this.list();
    }

    render() {

        let {data} = this.state;

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Tanggal Kegiatan',
                dataIndex: 'date_format',
                key: 'date_format'
            },
            {
                title: 'Keterangan',
                dataIndex: 'description',
                key: 'description',
            },
            {
                title: 'Waktu pengisian',
                dataIndex: 'created_at',
                key: 'created_at',
            },
        ];

        const dataList = Object.keys(data).map((item, key) => {
            // console.log('data', data[item][key]['description']);

            return {
                key: key,
                no: key + 1,
                ...data[item],
            }
        });

        return <Table columns={columns}
                      dataSource={dataList}
                      bordered={true}
                      loading={this.state.isLoading}
                      size='small'/>
    }

    list = () => {
        this.setState({
            isLoading: true
        });

        let data = {
            staff_id: this.state.staff.staff_id,
        };

        PostData('/report-list-date-table', data).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}
