import React, {Component} from 'react';
import {Alert, Badge, Calendar, Form, Icon, Modal} from "antd";

import {PostData} from "../../../../services/api";
import {dateFormatterApp} from "../../../../services/app/General";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            staff: props.staff,
            dataAll: [],
            dataOne: [],
        };
    }

    componentWillMount() {
        this.reportListDateAll();

    }

    render() {

        let {dataAll} = this.state;

        function getListData(value) {
            let listData = [];
            let year = value.format('YYYY');
            let month = value.format('MM');
            let day = value.format('DD');
            let date = `${year}-${month}-${day}`;

            Object.keys(dataAll).map((key) => {
                if (key == date) {
                    dataAll[key].map((item, key2) => {
                        listData[key2] = {
                            type: 'success',
                            content: `${item['description']}`
                        }
                    });
                }
            });

            return listData || [];
        }

        function dateCellRender(value) {
            const listData = getListData(value);
            return (
                <ul className="events">
                    {listData.map(item => (
                        <li key={item.content}>
                            <Badge status={item.type} text={item.content}/>
                        </li>
                    ))}
                </ul>
            );
        }

        function getMonthData(value) {
            if (value.month() === 8) {
                return 1394;
            }
        }

        function monthCellRender(value) {
            const num = getMonthData(value);
            return num ? (
                <div className="notes-month">
                    <section>{num}</section>
                    <span>Backlog number</span>
                </div>
            ) : null;
        }


        return (
            <div>
                <Alert
                    message="Tips Penggunaan"
                    description="Laporan pekerjaan bisa dilihat dengan klik tanggal pada kalender."
                    type="info"
                    showIcon
                />
                <Calendar dateCellRender={dateCellRender}
                          onSelect={this.onSelect}
                          monthCellRender={monthCellRender}/>

                <Modal
                    title={
                        <span>
                                            <Icon type="calendar"/> Laporan {dateFormatterApp(this.state.dateSelect)}
                                        </span>
                    }
                    onCancel={this.modalStatus(false)}
                    visible={this.state.modalVisible}
                    width={700}
                    footer={null}>
                    <Form {...formItemLayout} className={"form-detail"}>
                        {
                            Object.keys(this.state.dataOne).map((item, key) => {
                                return (
                                    <Form.Item
                                        label="Item Pekerjaan"
                                    >
                                        {this.state.dataOne[item]["description"]}
                                    </Form.Item>
                                )
                            })
                        }
                    </Form>
                </Modal>
            </div>
        )
    }

    onSelect = value => {
        this.list(value.format('YYYY-MM-DD'));
        this.setState({
            dateSelect: value.format('YYYY-MM-DD'),
            modalVisible: true
        });
    };

    list = (date) => {
        let data = {
            staff_id: this.state.staff.staff_id,
            date: date
        };
        PostData('/report-list-date-one', data).then((result) => {
            this.setState({
                dataOne: result.data.data
            })
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };

    reportListDateAll = () => {
        let data = {
            staff_id: this.state.staff.staff_id,
        };
        PostData('/report-list-date-all', data).then((result) => {
            this.setState({
                dataAll: result.data.data
            })
        });
    };


}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};