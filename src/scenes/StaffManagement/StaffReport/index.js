import React, {Component} from 'react';
import {Button, Col, Row, Table} from "antd";

import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {GetData} from "../../../services/api";

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["6"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class StaffSallary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            match: props.match,
            staff:[],
            listRefreshRun: this.listRefreshRun
        };
    }

    componentWillMount() {
        GetData('/staff').then((result) => {
            this.setState({
                staff: result.data.data,
            });
        });

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })

    }

    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <List {...this.state}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

export default StaffSallary;