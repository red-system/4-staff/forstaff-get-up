import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Radio, message} from "antd";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General"; // ES6

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: {
                news_title: '',
                news_description: '',
                news_attachment: '',
                news_publish: 'no'
            }
        };
    }
    render() {

        let menuList = menuActionViewAccess('staff_management', 'news', 'list');

        const modules = {
            toolbar: [
                [{ 'header': [1, 2, false] }],
                ['bold', 'italic', 'underline','strike', 'blockquote'],
                [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                ['link', 'image'],
                ['clean']
            ],
        };
        const formats = [
            'header',
            'bold', 'italic', 'underline', 'strike', 'blockquote',
            'list', 'bullet', 'indent',
            'link', 'image'
        ];

        return (
            <div className={menuList.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul"
                            {...this.state.formError.news_title}>
                            <Input defaultValue={this.state.formValue.news_title} onChange={this.onChange("news_title")}/>
                        </Form.Item>
                        <Form.Item
                            label="Isi Berita"
                            {...this.state.formError.news_description}>
                            <ReactQuill theme="snow"
                                        modules={modules}
                                        formats={formats}
                                        onChange={this.onChange("news_description")} />
                        </Form.Item>
                        <Form.Item
                            label="Publikasi"
                            {...this.state.formError.news_publish}
                        >
                            <Radio.Group defaultValue={"no"}
                                         onChange={this.onChange("news_publish")}
                                         buttonStyle="solid"
                                         key={this.state.formValue.news_publish}
                                         name={"news_publish"}>
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item
                            label="File Berita"
                            {...this.state.formError.news_attachment}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )

    }
    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                news_attachment: event.target.files[0]
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };
    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };
    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        formData.append('news_title', this.state.formValue.news_title);
        formData.append('news_description', this.state.formValue.news_description);
        formData.append('news_attachment', this.state.formValue.news_attachment);
        formData.append('news_publish', this.state.formValue.news_publish);

        PostData('/news', formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}
const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};
export default Create;