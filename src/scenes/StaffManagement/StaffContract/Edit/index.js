import React, { Component } from "react";
import { Button, Form, Icon, Modal, Select, DatePicker, message } from "antd";
import { GetData, PostData } from "../../../../services/api";
import moment from "moment";
import { dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay } from "../../../../services/app/General";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
            listRefreshRun: props.listRefreshRun,
            dataStaff: props.dataStaff,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataStaff: nextProps.dataStaff,
            formValue: nextProps.record,
        });
    }

    render() {
        const { dataStaff } = this.state;

        return [
            <Button type="success" onClick={this.modalStatus(true)} icon={"edit"}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="plus" /> Edit Data
                    </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Staf" {...this.state.formError.staff_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Staf"
                            value={this.state.formValue.staff_id}
                            onChange={this.onChange("staff_id")}
                            optionFilterProp="children"
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {Object.keys(dataStaff).map((key, no) => {
                                return (
                                    <Select.Option key={no} value={dataStaff[key]["staff_id"]}>
                                        {dataStaff[key]["staff_name"]}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Tanggal Kontrak" {...this.state.formError.staff_contract_date_from}>
                        <DatePicker
                            style={{ width: "100%" }}
                            onChange={this.onChangeDate("staff_contract_date_from")}
                            value={moment(dateFormatterApp(this.state.formValue.staff_contract_date_from), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item label="Tanggal Berakhir" {...this.state.formError.staff_contract_date_to}>
                        <DatePicker
                            style={{ width: "100%" }}
                            onChange={this.onChangeDate("staff_contract_date_to")}
                            value={moment(dateFormatterApp(this.state.formValue.staff_contract_date_to), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item label="Upload File Kontrak" {...this.state.formError.staff_contract_filename}>
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File Kontrak</button>
                            <input type="file" name="myfile" onChange={this.handleChange} />
                        </div>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_contract_filename: event.target.files[0],
            },
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    handleDatePickerChange = (date, dateString, fieldName) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [fieldName]: dateString,
            },
        });
    };

    onChangeDate = (name) => (value) => {
        value = value != null ? value.format(dateFormatApi()) : "";

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        }
    };

    getDataEdit = () => {
        let staff_contract_id = this.props.record.staff_contract_id;
        GetData(`/staff-contract/${staff_contract_id}`).then((result) => {
            this.setState({
                formValue: result.data.data,
            });
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        const formData = new FormData();
        formData.append("staff_id", this.state.formValue.staff_id);
        formData.append("staff_contract_date_from", this.state.formValue.staff_contract_date_from);
        formData.append("staff_contract_date_to", this.state.formValue.staff_contract_date_to);
        formData.append("staff_contract_filename", this.state.formValue.staff_contract_filename);

        PostData(`/staff-contract/${this.state.formValue.staff_contract_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;
