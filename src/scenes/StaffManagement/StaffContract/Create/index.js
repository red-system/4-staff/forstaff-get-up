import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Select, DatePicker,  message} from "antd";
import {PostData} from "../../../../services/api";
import moment from "moment";
import {
    dateFormatApi,
    dateFormatApp,
    dateFormatterApp,
    dateToDay,
    menuActionViewAccess
} from "../../../../services/app/General";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: {
                staff_id: '',
                staff_contract_filename: '',
                staff_contract_date_from: dateToDay(),
                staff_contract_date_to: dateToDay()
            },
            listRefreshRun: props.listRefreshRun,
            dataStaff: []
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataStaff: nextProps.dataStaff
        })
    }

    render() {

        const {dataStaff} = this.state;
        let menuCreate = menuActionViewAccess('staff_management', 'staff_contract', 'create');

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Staf"
                            {...this.state.formError.staff_id}>
                            <Select
                                showSearch
                                placeholder="Pilih Staf"
                                defaultValue={this.state.formValue.staff_id}
                                onChange={this.onChange('staff_id')}
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    Object.keys(dataStaff).map((key, no) => {
                                        return (
                                            <Select.Option key={no} value={dataStaff[key]['staff_id']}>{dataStaff[key]['staff_name']}</Select.Option>
                                        );
                                    })
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Kontrak"
                            {...this.state.formError.staff_contract_date_from}>
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("staff_contract_date_from")}
                                value={moment(dateFormatterApp(this.state.formValue.staff_contract_date_from), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Berakhir"
                            {...this.state.formError.staff_contract_date_to}>
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("staff_contract_date_to")}
                                value={moment(dateFormatterApp(this.state.formValue.staff_contract_date_to), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Upload File Kontrak"
                            {...this.state.formError.staff_contract_filename}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File Kontrak</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_contract_filename: event.target.files[0]
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    handleDatePickerChange = (date, dateString, fieldName) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [fieldName]: dateString
            }
        });
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        formData.append('staff_id', this.state.formValue.staff_id);
        formData.append('staff_contract_date_from', this.state.formValue.staff_contract_date_from);
        formData.append('staff_contract_date_to', this.state.formValue.staff_contract_date_to);
        formData.append('staff_contract_filename', this.state.formValue.staff_contract_filename);

        PostData('/staff-contract', formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;