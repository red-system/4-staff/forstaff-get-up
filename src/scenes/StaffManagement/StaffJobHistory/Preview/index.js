import React, { Component } from "react";
import { Button, Icon, Modal } from "antd";
import { renderFileType } from "../../../../services/app/General";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {
        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)}>
                    <Icon type="eye" />
                    Preview
                </Button>

                <Modal
                    title="File Surat Keterangan Kerja Staff"
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    width={900}
                    footer={[
                        <Button key="back" onClick={this.modalStatus(false)}>
                            Kembali
                        </Button>,
                    ]}
                >
                    {renderFileType(this.props.record.staff_jh_employment_certificate_type, this.props.record.staff_jh_employment_certificate_url)}
                </Modal>
            </span>
        );
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };
}

export default Create;
