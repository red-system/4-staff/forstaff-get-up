import React, {Component} from 'react';
import {Button, Icon, Modal, Row, Col, Form} from "antd";
import Preview from "../Preview";


export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            staff: props.staff,
            data: [],
            isLoading: true,
        };
    }

    render() {

        let {record} = this.state;

        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)}>
                    <Icon type="search"/>
                    Detail
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="usergroup-delete"/> Detail Pekerjaan - {this.state.staff.staff_name}
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onOk={this.modalStatus(true)}
                    onCancel={this.modalStatus(false)}
                    footer={[
                        <Row>
                            <Col>
                                <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                    Kembali
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >

                    <Form {...formItemLayout} className={"form-detail"}>
                        <Form.Item
                            label="Nama Perusahaan"
                        >
                            {record.staff_jh_company}
                        </Form.Item>
                        <Form.Item
                            label="Dari Tanggal"
                        >
                            {record.staff_jh_date_from}
                        </Form.Item>
                        <Form.Item
                            label="Sampai Tanggal"
                        >
                            {record.staff_jh_date_to}
                        </Form.Item>
                        <Form.Item
                            label="Alamat"
                        >
                            {record.staff_jh_address}
                        </Form.Item>
                        <Form.Item
                            label="Kontak"
                        >
                            {record.staff_jh_telp}
                        </Form.Item>
                        <Form.Item
                            label="Bidang Pekerjaan"
                        >
                            {record.staff_jh_bussines_field}
                        </Form.Item>
                        <Form.Item
                            label="Bos"
                        >
                            {record.staff_jh_boss}
                        </Form.Item>
                        <Form.Item
                            label="Bos"
                        >
                            {record.staff_jh_job_description}
                        </Form.Item>
                        <Form.Item
                            label="Jabatan Pekerjaan"
                        >
                            {record.staff_jh_position}
                        </Form.Item>
                        <Form.Item
                            label="Nonimal Gaji"
                        >
                            {record.staff_jh_salary}
                        </Form.Item>
                        <Form.Item
                            label="Alasan Berhenti"
                        >
                            {record.staff_jh_reason_quit}
                        </Form.Item>
                        <Form.Item
                            label="Surat Keterangan Kerja"
                        >
                            <Preview record={record} />
                        </Form.Item>
                    </Form>

                </Modal>
            </span>
        )
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};