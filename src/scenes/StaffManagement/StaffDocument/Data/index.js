import React, {Component} from 'react';
import {Button, Icon, Modal, Row, Col, Table} from "antd";
import {GetData} from "../../../../services/api";
import Preview from "../Preview";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            data: [],
            isLoading: true,
        };
    }

    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Judul Dokumen',
                dataIndex: 'staff_document_title',
                key: 'staff_document_title'
            },
            {
                title: 'Jenis Dokumen',
                dataIndex: 'staff_document_type',
                key: 'staff_document_type',
            },
            {
                title: 'Tanggal Input Data',
                dataIndex: 'created_at',
                key: 'created_at',
            },
            {
                title: 'Tanggal Perbaruan Data',
                dataIndex: 'updated_at',
                key: 'updated_at',
            },
            {
                title: 'Menu',
                render: (text, record) => {
                    return <Preview record={record}/>
                }
            }

        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete"/>
                Daftar Dokumen
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="usergroup-delete"/> Daftar Dokumen - {this.state.record.staff_name}
                        </span>
                }
                visible={this.state.modalVisible}
                width={1000}
                onOk={this.modalStatus(true)}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row>
                        <Col>
                            <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Kembali
                            </Button>
                        </Col>
                    </Row>
                ]}
            >

                <Table columns={columns}
                       bordered={true}
                       loading={this.state.isLoading}
                       dataSource={dataList}
                       size='small'/>

            </Modal>
        ]
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    list = () => {
        GetData(`/staff-document/${this.state.record.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        })
    }
}

export default Create;