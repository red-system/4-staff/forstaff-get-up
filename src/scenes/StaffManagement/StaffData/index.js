import React, {Component} from 'react';
import {Button, Col, Icon, Row} from "antd";
import {Link} from 'react-router-dom';

import Print from './Print';
import Import from './Import';
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {menuActionViewAccess} from "../../../services/app/General";

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 18,
        offset: 6
    },
    'lg': {
        span: 18,
        offset: 6
    }
};

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class JobDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            match: props.match,
            listRefreshRun:this.listRefreshRun,
            isLoading: true,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {
        const {match} = this.state;

        let menuCreate = menuActionViewAccess('staff_management', 'staff_data', 'create');
        let menuImport = menuActionViewAccess('staff_management', 'staff_data', 'import');
        let menuPrint = menuActionViewAccess('staff_management', 'staff_data', 'print');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Link className={menuCreate.class} to={`${match.url}/create`}>
                                    <Button type="primary">
                                        <Icon type="plus"/> Tambah Data
                                    </Button>
                                </Link>
                                &nbsp;
                                <span className={menuImport.class}>
                                    <Import formLayout={formLayout}/>
                                </span>
                                &nbsp;
                                <span className={menuPrint.class}>
                                <Print formLayout={formLayout}/>
                                </span>

                                <br/><br/>

                                <List {...this.state} />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
            isLoading: value
        })
    }

}

export default JobDescription;