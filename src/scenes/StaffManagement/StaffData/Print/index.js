import React, {Component} from 'react';
import {Button, Icon} from "antd";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout
        };
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        const {modalCreateVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };

        return (
            <span>
                <Button type="warning" onClick={this.modalCreateShow}>
                    <Icon type="printer" /> Print
                </Button>
            </span>
        )
    }
}

export default Create;