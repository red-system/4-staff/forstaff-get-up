import React, { Component } from "react";
import { Button, Icon, Table, Tag, Input } from "antd";
import { Link } from "react-router-dom";
import Delete from "../Delete";
import { GetData } from "../../../../services/api";
import { map, get } from "lodash";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
const Search = Input.Search;

class JobDescription extends Component {
    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoading: props.isLoading,
            match: props.match,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            const nameMatch = get(record, "staff_name").match(reg);
            const nicknameMatch = get(record, "staff_nickname").match(reg);
            const departmentMath = get(record, "position.department.department_title").match(reg);
            const positionMatch = get(record, "position.structure_name").match(reg);
            const roleMatch = get(record, "role_master.role_master_title").match(reg);
            const statusMatch = get(record, "staff_status").match(reg);
            if (!nameMatch && !departmentMath && !nicknameMatch && !positionMatch && !roleMatch && !statusMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_data", "list");
        let menuEdit = menuActionViewAccess("staff_management", "staff_data", "edit");
        let menuDelete = menuActionViewAccess("staff_management", "staff_data", "delete");

        const { match } = this.state;

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "ID Staff",
                dataIndex: "staff_id_label",
                key: "staff_id_label",
            },
            {
                title: "Nama Lengkap",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Nama Panggilan",
                dataIndex: "staff_nickname",
                key: "staff_nickname",
            },
            {
                title: "Departemen",
                dataIndex: "position.department.department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "position.structure_name",
                key: "structure_name",
            },
            {
                title: "Role Permission",
                dataIndex: "role_master.role_master_title",
                key: "role_master_title",
                render: (text, record) => {
                    if (record.role_master.role_master_title !== null) {
                        return <Tag color="blue">{record.role_master.role_master_title}</Tag>;
                    }
                },
            },
            {
                title: "Status",
                dataIndex: "staff_status",
                key: "staff_status",
                render: (text, record) => {
                    return status_view(text);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (test, record) => (
                    <Button.Group size={"small"}>
                        {/*<ViewData*/}
                        {/*    staff_id={record.staff_id}/>*/}
                        <Link
                            className={menuEdit.class}
                            to={{
                                pathname: `${match.url}/edit`,
                                state: {
                                    record: record,
                                },
                            }}
                        >
                            <Button type="success" size={"small"}>
                                <Icon type="edit" /> Edit
                            </Button>
                        </Link>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>

                <Table
                    className={menuList.class}
                    columns={columns}
                    dataSource={dataList}
                    onChange={this.handleChange}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });

        GetData("/staff").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default JobDescription;
