import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, Row, Spin} from "antd";
import OrgChart from "react-orgchart";
import {GetData} from "../../../../services/api";

import 'react-orgchart/index.css';

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: true,
            data: [],
            listDepartment: [],
            structure_id: props.structure_id
        };
    }

    componentWillMount() {
        this.data();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            structure_id: nextProps.structure_id
        })
    }

    render() {

        const {data, structure_id} = this.state;
        const MyNodeComponent = ({node}) => {

            let posisi_select_class = node.structure_id === structure_id ? 'job-title select': 'job-title';

            return (
                <div className="initechNode-posisi" onClick={this.selectStructure(node.structure_id, node.structure_name)}>
                    <Row>
                        <Col span={24}>
                            <div className={posisi_select_class}>
                                {node.structure_name}
                            </div>
                        </Col>
                    </Row>
                </div>
            );
        };

        return (
            <div>
                <Button type={'success'} icon={"user"} onClick={this.modalStatus(true)}>Pilih Posisi Staff</Button>

                <Modal
                    title={
                        <span>
                            <Icon type="user"/> Pilih Posisi Staff
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={"80%"}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >

                    <div className="overview-wrapper">
                        <Spin spinning={this.state.isLoading} tip={"Loading ..."}>
                            <OrgChart tree={data} NodeComponent={MyNodeComponent}/>
                        </Spin>
                    </div>

                </Modal>
            </div>
        )
    }

    data() {
        GetData('/structure-overview').then((result) => {
            const json = result.data;

            this.setState({
                data: json.data,
                isLoading: false,
            });
        });
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    selectStructure = (structure_id, structure_name) => e => {
        this.setState({
            modalVisible: false
        });
        this.props.changeStructure(structure_id, structure_name);
    }

}