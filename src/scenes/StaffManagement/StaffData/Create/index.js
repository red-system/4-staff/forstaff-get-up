import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, InputNumber, Radio, Row, Select, Table, DatePicker, message} from "antd";
import {Link, Redirect} from 'react-router-dom';

import GENERALDATA from "../../../../constants/generalData";
import Breadcrumb from '../../../../components/Breadcrumb';
import {GetData, PostData} from "../../../../services/api";
import {
    dateFormatterApi,
    strRandom,
    dateFormatApi,
    dateFormatApp,
    dateFormatterApp,
    dateToDay, staff_avatar_rules_upload_image, staff_avatar_config_upload_image
} from "../../../../services/app/General";
import PosisiSelect from "../PosisiSelect";

import ImageEditorRc from 'react-cropper-image-editor';
import 'cropperjs/dist/cropper.css';
import moment from "moment";

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.staff_management;
const FormItem = Form.Item;
const {Option} = Select;

export default class JobDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirectPage: false,
            breadcrumbComponent: null,
            province: [],
            primaryDistrict: [],
            secondaryDistrict: [],
            match: props.match,
            isLoading: true,
            formError: [],
            formValue: {
                structure_id: null,
                structure_name: '-',
                staff_quota_leave: 0,
                role_master_id: [],
                staff_status: 'not_active',
                staff_nationality: 'wni',
                staff_gender: 'male',
                staff_blood_type: 'A',
                staff_identity_type: 'ktp',
                staff_religion: 'hindu',
                staff_marital_status: 'yes',
                staff_primary_district_id: null,
                staff_second_district_id: null,
                staff_avatar: '',
                staff_avatar_url: '',
                staff_work_start: dateToDay(),
                staff_birth_date: dateToDay()
            },
            roleMaster: [],
        };
        this.loadDistrict = this.loadDistrict.bind(this)
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
        GetData('/province').then((result) => {
            this.setState({
                province: result.data.data,
            });
        });
        GetData('/role-permission').then((result) => {
            this.setState({
                roleMaster: result.data.data
            });
        });
        this.setState({
            isLoading: false
        })

    }

    render() {

        if (this.state.redirectPage) {
            return <Redirect to={`../../${pageNow.route}`}/>
        }

        return (
            <div loading="true">
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Form>
                                    <div className="form-divider">
                                        DATA UMUM
                                    </div>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Nama Lengkap"
                                        {...this.state.formError.staff_name}>

                                        <Input onChange={this.onChange("staff_name")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Nama Panggilan"
                                        required={true}
                                        {...this.state.formError.staff_nickname}>

                                        <Input onChange={this.onChange("staff_nickname")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Posisi"
                                        {...this.state.formError.structure_id}
                                        required={true}
                                    >
                                        {this.state.formValue.structure_name}
                                        <PosisiSelect
                                            changeStructure={this.changeStructure}
                                            structure_id={this.state.formValue.structure_id}/>


                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Awal Bekerja"
                                        {...this.state.formError.staff_work_start}
                                        required={true}
                                    >
                                        <DatePicker
                                            style={{width: "100%"}}
                                            value={moment(dateFormatterApp(this.state.formValue.staff_work_start), dateFormatApp())}
                                            format={dateFormatApp()}
                                            onChange={this.onChangeDate("staff_work_start")}/>
                                    </FormItem>
                                    {/*<FormItem*/}
                                    {/*    {...formItemLayout}*/}
                                    {/*    label="Kuota Cuti Tahunan"*/}
                                    {/*    {...this.state.formError.staff_quota_leave}*/}
                                    {/*>*/}
                                    {/*    <InputNumber value={this.state.formValue.staff_quota_leave}*/}
                                    {/*                 onChange={this.onChange("staff_quota_leave")}/>*/}
                                    {/*    <Button style={{marginTop: "-4px"}}>Hari</Button>*/}
                                    {/*</FormItem>*/}

                                    <div className="form-divider">
                                        APLIKASI
                                    </div>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Hak Akses"
                                        {...this.state.formError.role_master_id}
                                        required={true}
                                    >
                                        <Select
                                            // mode="multiple"
                                            placeholder={"Hak Akses"}
                                            onSelect={this.onSelect("role_master_id")}
                                            onDeselect={this.onDeselect("role_master_id")}
                                            filterOption={(input, option) =>
                                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }

                                        >
                                            {
                                                Object.keys(this.state.roleMaster).map((key) => {
                                                    return (
                                                        <Option key={key}
                                                                value={this.state.roleMaster[key].role_master_id}>{this.state.roleMaster[key].role_master_title}</Option>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Username"
                                        {...this.state.formError.staff_username}
                                        required={true}
                                    >
                                        <Input
                                            autoComplete={"new-username"}
                                            onChange={this.onChange("staff_username")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_radius}
                                        label="Password"
                                        {...this.state.formError.staff_password}
                                        required={true}
                                    >
                                        <Input.Password
                                            autoComplete={"new-password"}
                                            onChange={this.onChange("staff_password")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout_radius}
                                        label="Konfirmasi Password"
                                        {...this.state.formError.staff_password_confirmation}
                                        required={true}
                                    >
                                        <Input.Password
                                            id={strRandom()}
                                            onChange={this.onChange("staff_password_confirmation")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Status Staf"
                                        {...this.state.formError.staff_status}
                                        extra={"Jika status staf tidak aktf, maka staf tidak bisa login pada sistem"}
                                    >
                                        <Radio.Group defaultValue={this.state.formValue.staff_status} buttonStyle="solid"
                                                     onChange={this.onChange("staff_status")}>
                                            <Radio.Button value="active">Aktif</Radio.Button>
                                            <Radio.Button value="not_active">Tidak Aktif</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>


                                    <div className="form-divider">
                                        DATA PRIBADI
                                    </div>
                                    <FormItem
                                        {...this.state.formError.staff_avatar}
                                        {...formItemLayout}
                                        label="Foto Profil"
                                    >
                                        <ImageEditorRc
                                            {...staff_avatar_config_upload_image()}
                                            saveImage={this.cropper} // it has to catch the returned data and do it whatever you want
                                            src={this.state.formValue.staff_avatar_url}
                                            responseType='blob/base64'/>
                                        <div className="upload-btn-wrapper-register">
                                            <button className="btn-file">Browse Foto</button>
                                            <input type="file" name="myfile" onChange={this.browseFotoProfile}/>
                                        </div>
                                        {staff_avatar_rules_upload_image()}
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Kewarganegaraan"
                                        {...this.state.formError.staff_nationality}
                                    >
                                        <Radio.Group defaultValue="wni" buttonStyle="solid"
                                                     onChange={this.onChange("staff_nationality")}>
                                            <Radio.Button value="wni">Warga Negara Indonesia</Radio.Button>
                                            <Radio.Button value="wna">Warga Negara Asing</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Jenis Kelamin"
                                        {...this.state.formError.staff_gender}
                                    >
                                        <Radio.Group defaultValue="male" buttonStyle="solid"
                                                     onChange={this.onChange("staff_gender")}>
                                            <Radio.Button value="male">Pria</Radio.Button>
                                            <Radio.Button value="female">Wanita</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>

                                    <Form.Item
                                        {...formItemLayout}
                                        label="Tempat Lahir"
                                        {...this.state.formError.staff_birth_place}>
                                        <Row>
                                            <Col {...divide21}>
                                                <Input onChange={this.onChange("staff_birth_place")}/>
                                            </Col>
                                            <Col {...divide22}>
                                                <Form.Item
                                                    label="Tanggal Lahir" {...formItemLayout2} {...this.state.formError.staff_birth_date}>
                                                    <DatePicker
                                                        style={{width: "100%"}}
                                                        value={
                                                            this.state.formValue.staff_birth_date === ''
                                                                ?
                                                                '' :
                                                                moment(dateFormatterApp(this.state.formValue.staff_birth_date), dateFormatApp())
                                                        }
                                                        format={dateFormatApp()}
                                                        onChange={this.onChangeDate("staff_birth_date")}/>

                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Golongan Darah"
                                        {...this.state.formError.staff_blood_type}
                                    >
                                        <Radio.Group defaultValue={ this.state.formValue.staff_blood_type}
                                                     buttonStyle="solid"
                                                     onChange={this.onChange("staff_blood_type")}>
                                            <Radio.Button value="A">A</Radio.Button>
                                            <Radio.Button value="B">B</Radio.Button>
                                            <Radio.Button value="O">O</Radio.Button>
                                            <Radio.Button value="AB">AB</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Jenis Identitas"
                                        {...this.state.formError.staff_identity_type}
                                    >
                                        <Radio.Group defaultValue="ktp" buttonStyle="solid"
                                                     onChange={this.onChange("staff_identity_type")}>
                                            <Radio.Button value="ktp">KTP</Radio.Button>
                                            <Radio.Button value="sim">SIM</Radio.Button>
                                            <Radio.Button value="paspor">PASPOR</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="No Identitas"
                                        {...this.state.formError.staff_identity_number}
                                    >
                                        <Input onChange={this.onChange("staff_identity_number")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Agama"
                                        {...this.state.formError.staff_religion}
                                    >
                                        <Select defaultValue="hindu" onChange={this.onChange("staff_religion")}>
                                            <Option value="hindu">Hindu</Option>
                                            <Option value="budha">Budha</Option>
                                            <Option value="islam">Islam</Option>
                                            <Option value="konghuchu">Khonghuchu</Option>
                                            <Option value="katolik">Kristen Katolik</Option>
                                            <Option value="kristen_protestan">Kristen Protestan</Option>
                                        </Select>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Status Perkawinan"
                                        {...this.state.formError.staff_marital_status}
                                    >
                                        <Radio.Group defaultValue="yes" buttonStyle="solid"
                                                     onChange={this.onChange("staff_marital_status")}>
                                            <Radio.Button value="yes">Menikah</Radio.Button>
                                            <Radio.Button value="no">Belum Menikah</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>

                                    <div className="form-divider">
                                        KONTAK
                                    </div>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Alamat Tetap"
                                        {...this.state.formError.staff_primary_address}
                                    >
                                        <Input onChange={this.onChange("staff_primary_address")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Provinsi"
                                        {...this.state.formError.staff_primary_province_id}
                                    >
                                        <Row>
                                            <Col {...divide31}>
                                                <Select
                                                    showSearch
                                                    style={{width: 200}}
                                                    placeholder="Pilih Provinsi"
                                                    optionFilterProp="children"
                                                    onChange={this.onChangePrimaryProvince()}
                                                    filterOption={(input, option) =>
                                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    {
                                                        Object.keys(this.state.province).map((key) => {
                                                            return (
                                                                <Option key={key}
                                                                        value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </Col>
                                            <Col {...divide32}>
                                                <Form.Item
                                                    label="Kabupaten/Kota" {...formItemLayout3} {...this.state.formError.staff_primary_district_id}>
                                                    <Select
                                                        showSearch
                                                        style={{width: 200}}
                                                        placeholder="Pilih Kabupaten"
                                                        optionFilterProp="children"
                                                        value={this.state.formValue.staff_primary_district_id}
                                                        onChange={this.onChange("staff_primary_district_id")}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {
                                                            Object.keys(this.state.primaryDistrict).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.primaryDistrict[key].district_id}>{this.state.primaryDistrict[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                            </Col>
                                            <Col {...divide33}>
                                                <Form.Item
                                                    label="Kode Pos" {...formItemLayout3} {...this.state.formError.staff_primary_post_code}>
                                                    <Input onChange={this.onChange("staff_primary_post_code")}/>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Alamat Sekarang"
                                    >
                                        <Input onChange={this.onChange("staff_second_address")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Provinsi"
                                    >
                                        <Row>
                                            <Col {...divide31}>
                                                <Select
                                                    showSearch
                                                    style={{width: 200}}
                                                    placeholder="Pilih Provinsi"
                                                    optionFilterProp="children"
                                                    onChange={this.onChangeSecondProvince("staff_second_district_id")}
                                                    filterOption={(input, option) =>
                                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    {
                                                        Object.keys(this.state.province).map((key) => {
                                                            return (
                                                                <Option key={key}
                                                                        value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </Col>
                                            <Col {...divide32}>
                                                <Form.Item label="Kabupaten/Kota" {...formItemLayout3}>
                                                    <Select
                                                        showSearch
                                                        style={{width: 200}}
                                                        placeholder="Pilih Kabupaten"
                                                        optionFilterProp="children"
                                                        value={this.state.formValue.staff_second_district_id}
                                                        onChange={this.onChange("staff_second_district_id")}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {
                                                            Object.keys(this.state.secondaryDistrict).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.secondaryDistrict[key].district_id}>{this.state.secondaryDistrict[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                            </Col>
                                            <Col {...divide33}>
                                                <Form.Item label="Kode Pos" {...formItemLayout3}>
                                                    <Input onChange={this.onChange("staff_second_post_code")}/>
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="No HP"
                                        {...this.state.formError.staff_phone_number}
                                    >
                                        <Row>
                                            <Col {...divide31}>
                                                <Input onChange={this.onChange("staff_phone_number")}/>
                                            </Col>
                                        </Row>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Kontak Darurat"
                                        {...this.state.formError.staff_phone_emergency}
                                    >
                                        <Input onChange={this.onChange("staff_phone_emergency")}/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Email"
                                        {...this.state.formError.staff_email}
                                    >
                                        <Input onChange={this.onChange("staff_email")}/>
                                    </FormItem>


                                    <div className={"wrapper-button-float"}>
                                        <Button type="primary" size={"large"} htmlType="submit" onClick={this.insert}>
                                            <Icon type="check"/> Simpan Data Staf
                                        </Button>
                                        &nbsp;
                                        <Link to={`../../${pageNow.route}`}>
                                            <Button type="warning" size={"large"} htmlType="submit">
                                                <Icon type="rollback"/> Kembali
                                            </Button>
                                        </Link>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }


    cropper = (data) => {
        console.log('cropper', data);
        let staff_avatar = this.dataURLtoFile(data, 'staff.jpg');
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_avatar: staff_avatar,
                staff_avatar_url: URL.createObjectURL(staff_avatar),
            }
        })
    };

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    browseFotoProfile = (event) => {
        if (event.target.files[0]) {
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    staff_avatar_url: URL.createObjectURL(event.target.files[0]),
                    staff_avatar: event.target.files[0]
                }
            })
        }
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onSelect = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                // [name]: this.state.formValue[name].concat(value) untuk multiple select
                [name]: value
            }
        });
        console.log(this.state.formValue)
    };
    onDeselect = name => value => {
        var array = [...this.state.formValue.role_id_json]; // make a separate copy of the array
        var index = array.indexOf(value)
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    ["role_id_json"]: array
                }
            });
        }
        console.log(this.state.formValue)
    };

    onChangePrimaryProvince = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_primary_province_id: value,
                staff_primary_district_id: null
            }
        });
        this.loadDistrict("primaryDistrict", value)
    };
    onChangeSecondProvince = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_second_province_id: value,
                staff_second_district_id: null
            }
        });
        this.loadDistrict("secondaryDistrict", value)
    };

    loadDistrict(state, province_id) {
        const formData = new FormData();
        formData.append("province_id", province_id)
        PostData('/district', formData).then((result) => {
            console.log(result)
            this.setState({
                [state]: result.data.data
            })
        });
        console.log(this.state.formValue)
    }

    changeStructure = (structure_id, structure_name) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                structure_id: structure_id,
                structure_name: structure_name
            }
        })
    };

    insert = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != null) {
                formData.append(key, this.state.formValue[key])
            }
        });
        PostData('/staff', formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    redirectPage: true,
                });
            } else {
                message.warning(data.message);
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayout_radius = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 5},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 12,
            offset: 6,
        },
    },
};

const breadcrumb = [
    {
        label: pageParent.label,
        route: `../${pageParent.route}`
    },
    {
        label: pageNow.label,
        route: `../${pageNow.route}`
    },
    {
        label: 'Tambah Data',
        route: `../${pageNow.route}/create`
    }
];

const formItemLayout2 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayout3 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};

const divide21 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 8,
    xl: 8
};

const divide22 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 16,
    xl: 16
};


const divide31 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 6,
    xl: 6
};

const divide32 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 9,
    xl: 9
};

const divide33 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 9,
    xl: 9
};