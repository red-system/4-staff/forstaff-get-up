import React, {Component} from 'react';
import {Button, Icon} from "antd";
import {Link} from 'react-router-dom';


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
        };
    }


    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {
        return (
            <Link to={{
                pathname: `profil/detil`,
                state: {
                    staff_id: this.props.staff_id
                }
            }}>
                <Button type="file" onClick={this.modalEditShow}>
                    <Icon type="eye"/> Staf Detil
                </Button>
            </Link>
        )
    }
}

export default ViewFile;