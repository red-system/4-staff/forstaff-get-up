import React, {Component} from 'react';
import {Button, Form, Icon, message, Modal, Select} from "antd";
import {GetData, PostData} from "../../../../services/api";
import PosisiSelect from "../../StaffData/PosisiSelect";

class Create extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            isLoadingPosition: false,
            dataPosition: [],

            formValue: props.record,
            formError: []
        };
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Nama Staf"
                        {...this.state.formError.staff_id}>
                        {this.state.formValue.staff_name}
                    </Form.Item>
                    <Form.Item
                        label="Posisi Staf"
                        {...this.state.formError.structure_id}>
                        {this.state.formValue.structure_name}
                        <PosisiSelect
                            changeStructure={this.changeStructure}
                            structure_id={this.state.formValue.structure_id}/>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });

        if (name === 'department_id') {
            this.listDataPosition();
        }

    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });

        if (!value) {
            // this.listDataStaff();
            this.setState({
                isLoading: false,
                formError: []
            })
        } else {
            // this.listDataPosition();
        }


    };

    changeStructure = (structure_id, structure_name) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                structure_id: structure_id,
                structure_name: structure_name
            }
        })
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData('/staff-position/update', this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        // formValue: [],
                        dataPosition: [],
                        dataStaff: []
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    };

}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;