import React, { Component } from "react";
import { Tag, Table, Button, Input, Icon } from "antd";

import { GetData } from "../../../../services/api";
import SalaryDataView from "../SalaryDataView";
import SalaryDataDownload from "../SalaryDataDownload";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class StaffSallary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffNameMatch = false;
            if (get(record, "staff_name")) {
                staffNameMatch = get(record, "staff_name").match(reg);
            }
            var positionMatch = false;
            if (get(record, "structure_name")) {
                positionMatch = get(record, "structure_name").match(reg);
            }
            var salaryMonth = false;
            if (get(record, "salary_month") != null) {
                salaryMonth = get(record, "salary_month").toString().match(reg);
            }
            var salaryYearMatch = false;
            if (get(record, "salary_year") != null) {
                salaryYearMatch = get(record, "salary_year").match(reg);
            }
            var salaryBallanceViewMatch = false;
            if (get(record, "salary_ballance_view") != null) {
                salaryBallanceViewMatch = get(record, "salary_ballance_view").match(reg);
            }
            if (!salaryMonth && !salaryYearMatch && !staffNameMatch && !positionMatch && !salaryBallanceViewMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListHistoryStaff = menuActionViewAccess("staff_management", "salary", "list_history_salary_staff");
        let menuViewHistoryStaff = menuActionViewAccess("staff_management", "salary", "view_history_salary_staff");
        let menuDownloadHistoryStaff = menuActionViewAccess("staff_management", "salary", "download_history_salary_staff");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Nama Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Posisi Staff", dataIndex: "structure_name", key: "structure_name" },
            { title: "Bulan", dataIndex: "salary_month", key: "salary_month" },
            { title: "Tahun", dataIndex: "salary_year", key: "salary_year" },
            { title: "Total Gaji", dataIndex: "salary_ballance_view", key: "salary_ballance_view" },
            {
                title: "Publish",
                dataIndex: "salary_publish",
                key: "salary_publish",
                render: (text, record) => {
                    return status_view(record.salary_publish);
                },
            },
            { title: "Last Update", dataIndex: "updated_at", key: "updated_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 190,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuViewHistoryStaff.class}>
                            <SalaryDataView record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDownloadHistoryStaff.class}>
                            <SalaryDataDownload record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
                staff_name: item.staff.staff_name,
                structure_name: item.staff.position.structure_name,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuListHistoryStaff.class}
                    columns={columns}
                    dataSource={dataList}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list = (e) => {
        this.setState({
            isLoading: true,
        });

        GetData("/salary-history").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}

export default StaffSallary;
