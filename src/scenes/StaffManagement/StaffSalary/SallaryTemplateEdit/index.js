import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, InputNumber, Input, Row, Select, DatePicker, Upload, message} from "antd";
import moment from 'moment';
import {PostData} from "../../../../services/api";
import {Link} from "react-router-dom";
import Spin from "antd/es/spin";
import {formatMoney} from "../../../../services/app/General";


export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            staff: props.record,
            isLoading: false,
            formError: [],
            earnings: [],
            expenditure: [],
        };
    }

    render() {

        let total_gaji = 0;
        let total_earnings = 0;
        let total_expenditure = 0;
        this.state.earnings.map((item, key) => {
            total_earnings += item.amount;
        });
        this.state.expenditure.map((item, key) => {
            total_expenditure += item.amount;
        });

        total_gaji = total_earnings - total_expenditure;


        return [
            <Button type="success" size={"small"} onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Template Gaji Staff - {this.props.record.staff_name}
                        </span>
                }
                visible={this.state.modalVisible}
                style={{top: 20}}
                width={900}
                onOk={this.modalStatus(false)}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Spin spinning={this.state.isLoading}>
                    <div className={"form-title-plus"}>Penerimaan (+)</div>
                    <div className={"row-divider-group"}>
                        {
                            this.state.earnings.map((item, key) => {
                                return (
                                    <Row>
                                        <Col span={10}>
                                            <Input value={item.template_name}
                                                   onChange={this.changeTemplateNameEarnings(key)}/>
                                        </Col>
                                        <Col span={10} offset={4}>
                                            <InputNumber
                                                defaultValue={item.amount}
                                                formatter={value => `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\Rp.\s?|(,*)/g, '')}
                                                style={{width: "70%"}}
                                                onChange={this.changeAmountEarnings(key)}
                                            />
                                            <Button type="danger" icon={"close"}
                                                    onClick={this.deletePenerimaan(key)}>Hapus</Button>
                                        </Col>
                                    </Row>
                                )
                            })
                        }
                    </div>
                    <Button type="primary" icon={"plus"} onClick={this.addPenerimaan()}>Tambah Penerimaan</Button>
                    <br/>
                    <br/>

                    <div className={"form-title-minus"}>Potongan (-)</div>
                    <div className={"row-divider-group"}>
                        {
                            this.state.expenditure.map((item, key) => {
                                return (
                                    <Row>
                                        <Col span={10}>
                                            <Input value={item.template_name}
                                                   onChange={this.changeTemplateNameExpenditure(key)}/>
                                        </Col>
                                        <Col span={10} offset={4}>
                                            <InputNumber
                                                defaultValue={item.amount}
                                                formatter={value => `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\Rp.\s?|(,*)/g, '')}
                                                style={{width: "70%"}}
                                                onChange={this.changeAmountExpenditure(key)}
                                            />
                                            <Button type="danger" icon={"close"}
                                                    onClick={this.deletePotongan(key)}>Hapus</Button>
                                        </Col>
                                    </Row>
                                )
                            })
                        }
                    </div>
                    <Button type="primary" icon={"plus"} onClick={this.addPotongan()}>Tambah Potongan</Button>
                </Spin>
                <br/>
                <br/>

                <div className={"wrapper-button-float"}>
                    <h5>Total : </h5><h3 align={"center"}>Rp. {formatMoney(total_gaji)}</h3>
                    <Button type="primary" size={"large"} htmlType="submit" onClick={this.insert()}
                            loading={this.state.isLoading}>
                        <Icon type="check"/> Perbarui Template Gaji
                    </Button>
                    &nbsp;
                    <Button type="warning" size={"large"} onClick={this.modalStatus(false)}>
                        <Icon type="rollback"/> Batal
                    </Button>
                </div>

            </Modal>
        ]
    }

    list() {
        this.setState({
            isLoading: true
        });


        let data = {
            staff_id: this.state.staff.staff_id
        };

        PostData('/salary-template-staff', data).then((result) => {
            this.setState({
                earnings: result.data.data.earnings,
                expenditure: result.data.data.expenditure,
                isLoading: false
            })
        })
    }

    insert = () => e => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        let earnings = this.state.earnings;
        let expenditure = this.state.expenditure;
        let template = earnings.concat(expenditure);
        let data = {
            staff_id: this.state.staff.staff_id,
            template: template
        };

        PostData('/salary-template-insert', data).then((result) => {
            this.setState({
                isLoading: false,
                modalVisible: false
            });
            this.props.list();
        });

        return false;
    };

    modalStatus = (status) => e => {
        this.setState({
            modalVisible: status,
        });

        if (status) {
            this.list();
        }
    };

    addPenerimaan = () => e => {
        let dataAdd = {
            amount: 0,
            template_name: "",
            template_type: "earnings",

        };
        let earnings = this.state.earnings;

        earnings.push(dataAdd);

        this.setState({
            earnings: earnings
        });
    };

    addPotongan = () => e => {
        let dataAdd = {
            amount: 0,
            template_name: "",
            template_type: "expenditure",

        };
        let expenditure = this.state.expenditure;

        expenditure.push(dataAdd);

        this.setState({
            expenditure: expenditure
        });
    };

    deletePenerimaan = (key) => e => {
        let earnings = this.state.earnings;

        delete earnings[key];
        this.setState({
            earnings: earnings
        })
    };

    deletePotongan = (key) => e => {
        let expenditure = this.state.expenditure;

        delete expenditure[key];
        this.setState({
            expenditure: expenditure
        })
    };

    changeTemplateNameEarnings = (key) => e => {
        let earnings = this.state.earnings;
        let value = e.target.value;

        earnings[key]['template_name'] = value;

        this.setState({
            earnings: earnings
        });
    };

    changeTemplateNameExpenditure = (key) => e => {
        let expenditure = this.state.expenditure;
        let value = e.target.value;

        expenditure[key]['template_name'] = value;

        this.setState({
            expenditure: expenditure
        });
    };

    changeAmountEarnings = (key) => value => {
        let earnings = this.state.earnings;

        earnings[key]['amount'] = value;

        this.setState({
            earnings: earnings
        });
    };

    changeAmountExpenditure = (key) => value => {
        let expenditure = this.state.expenditure;

        expenditure[key]['amount'] = value;

        this.setState({
            expenditure: expenditure
        });
    }

}
