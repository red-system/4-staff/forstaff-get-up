import React, { Component } from "react";
import { Button, Form, Icon, Modal, Row, Col, Input, Select, DatePicker, Radio, Upload, message, InputNumber } from "antd";
import { GetData, PostData } from "../../../../services/api";
import { formatMoney, menuActionViewAccess, monthToDay, yearToDay } from "../../../../services/app/General";
import moment from "moment";
import AbsenceSummaryDetail from "../../../StaffAttendence/Absence/1_AbsenceSummaryDetail";

const { MonthPicker } = DatePicker;
const { Option } = Select;

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            staff: [],
            formLayout: props.formLayout,
            formError: [],
            earnings: [],
            datePickerDisabled: true,
            expenditure: [],
            formValue: {
                staff_id: 0,
                slip_salary_number: "-",
                slip_salary_view: "-",
                salary_year: yearToDay(),
                salary_month: monthToDay(),
                salary_payment_method: "transfer",
                salary_publish: "no",
            },
        };
    }

    componentWillMount() {
        this.listStaff();
        this.slipNumberSalary("", "");
    }

    render() {
        let total_gaji = 0;
        let total_earnings = 0;
        let total_expenditure = 0;
        this.state.earnings.map((item, key) => {
            total_earnings += item.amount;
        });
        this.state.expenditure.map((item, key) => {
            total_expenditure += item.amount;
        });

        total_gaji = total_earnings - total_expenditure;

        let year_month = `${this.state.formValue.salary_year}-${this.state.formValue.salary_month}`;
        let menuCreateSalaryStaff = menuActionViewAccess("staff_management", "salary", "list_salary_staff");

        return (
            <div className={menuCreateSalaryStaff.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus" /> Buat Gaji
                </Button>
                <br />
                <br />

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> Buat Gaji Baru
                        </span>
                    }
                    visible={this.state.modalVisible}
                    style={{ top: 20 }}
                    width={900}
                    onOk={this.modalStatus(false)}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Nomer Slip Gaji">{this.state.formValue.slip_salary_view}</Form.Item>

                        <Form.Item label="Nama Staf" {...this.state.formError.staff_id}>
                            <Select
                                showSearch
                                style={{ width: "100%" }}
                                placeholder="Pilih Staf"
                                optionFilterProp="children"
                                onChange={this.onChange("staff_id")}
                                value={this.state.formValue.staff_id}
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            >
                                {this.state.staff.map((item, key) => {
                                    return (
                                        <Option key={key} value={item.staff_id}>
                                            {item.staff_name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Bulan Tahun Gajian" {...this.state.formError.salary_month} {...this.state.formError.salary_year}>
                            <MonthPicker
                                placeholder="Pilih Tahun & Bulan Gaji"
                                style={{ width: "100%" }}
                                value={moment(year_month, "YYYY-MM")}
                                disabled={this.state.datePickerDisabled}
                                onChange={this.onChangeDate()}
                            />
                        </Form.Item>
                        <Form.Item label="Metode Pembayaran" {...this.state.formError.salary_payment_method}>
                            <Radio.Group
                                defaultValue={this.state.formValue.salary_payment_method}
                                onChange={this.onChange("salary_payment_method")}
                                buttonStyle="solid"
                            >
                                <Radio.Button value="transfer">Transfer</Radio.Button>
                                <Radio.Button value="cash">Cash</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item label="Publish Gaji" {...this.state.formError.sallary_publish}>
                            <Radio.Group defaultValue={this.state.formValue.salary_publish} onChange={this.onChange("salary_publish")} buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item label="Rangkuman Absensi">
                            <AbsenceSummaryDetail
                                record={{
                                    staff_id: this.state.formValue.staff_id,
                                }}
                            />
                        </Form.Item>

                        <div className={"form-title-plus"}>Penerimaan (+)</div>
                        <div className={"row-divider-group"}>
                            {this.state.earnings.map((item, key) => {
                                return (
                                    <Row key={key}>
                                        <Col span={10}>
                                            <Input value={item.template_name} onChange={this.changeTemplateNameEarnings(key)} />
                                        </Col>
                                        <Col span={10} offset={4}>
                                            <InputNumber
                                                value={item.amount}
                                                formatter={(value) => `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                                parser={(value) => value.replace(/\Rp.\s?|(,*)/g, "")}
                                                style={{ width: "70%" }}
                                                onChange={this.changeAmountEarnings(key)}
                                            />
                                            <Button type="danger" icon={"close"} onClick={this.deletePenerimaan(key)}>
                                                Hapus
                                            </Button>
                                        </Col>
                                    </Row>
                                );
                            })}
                        </div>
                        <Button type="primary" icon={"plus"} onClick={this.addPenerimaan()}>
                            Tambah Penerimaan
                        </Button>
                        <br />
                        <br />

                        <div className={"form-title-minus"}>Potongan (-)</div>
                        <div className={"row-divider-group"}>
                            {this.state.expenditure.map((item, key) => {
                                return (
                                    <Row key={key}>
                                        <Col span={10}>
                                            <Input value={item.template_name} onChange={this.changeTemplateNameExpenditure(key)} />
                                        </Col>
                                        <Col span={10} offset={4}>
                                            <InputNumber
                                                value={item.amount}
                                                formatter={(value) => `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                                parser={(value) => value.replace(/\Rp.\s?|(,*)/g, "")}
                                                style={{ width: "70%" }}
                                                onChange={this.changeAmountExpenditure(key)}
                                            />
                                            <Button type="danger" icon={"close"} onClick={this.deletePotongan(key)}>
                                                Hapus
                                            </Button>
                                        </Col>
                                    </Row>
                                );
                            })}
                        </div>
                        <Button type="primary" icon={"plus"} onClick={this.addPotongan()}>
                            Tambah Potongan
                        </Button>
                        <br />
                        <br />

                        <div className={"wrapper-button-float"}>
                            <h5>Total : </h5>
                            <h3 align={"center"}>Rp. {formatMoney(total_gaji)}</h3>
                            <Button type="primary" size={"large"} htmlType="submit" icon={"check"} onClick={this.insert()} loading={this.state.isLoading}>
                                Simpan Gaji
                            </Button>
                            &nbsp;
                            <Button type="warning" size={"large"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </div>
                    </Form>
                </Modal>
            </div>
        );
    }

    listStaff = (e) => {
        GetData("/staff").then((result) => {
            this.setState({
                staff: result.data.data,
            });
        });
    };

    slipNumberSalary = (salary_year = "", salary_month = "") => {
        salary_year = salary_year === "" ? yearToDay() : salary_year;
        salary_month = salary_month === "" ? monthToDay() : salary_month;

        let data = {
            salary_year: salary_year,
            salary_month: salary_month,
            type: "create",
        };
        PostData("/salary-slip-number", data).then((result) => {
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    slip_salary_number: result.data.data.salary_slip_number,
                    slip_salary_view: result.data.data.salary_slip_view,
                },
            });
        });
    };

    insert = () => (e) => {
        let data = {
            ...this.state.formValue,
            template: this.state.earnings.concat(this.state.expenditure),
        };

        PostData("/salary", data).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                    earnings: [],
                    expenditure: [],
                    formValue: {
                        ...this.state.formValue,
                        staff_id: "",
                    },
                });
            } else {
                let errors = data.errors;
                let formError = [];
                message.warning(data.message);

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });
                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
        return false;
    };

    salary_template = (staff_id, salary_year, salary_month) => {
        let data = {
            staff_id: staff_id,
            salary_year: salary_year,
            salary_month: salary_month,
        };

        PostData("/salary-create", data).then((result) => {
            this.setState({
                earnings: result.data.data.earnings,
                expenditure: result.data.data.expenditure,
            });
        });
    };

    modalStatus = (status) => (e) => {
        this.setState({
            modalVisible: status,
        });
    };

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                sallary_filename: event.target.files[0],
            },
        });
    };
    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }

        if (name === "staff_id") {
            this.salary_template(value, yearToDay(), monthToDay());
            this.slipNumberSalary(yearToDay(), monthToDay());
            this.setState({
                datePickerDisabled: false,
                formValue: {
                    ...this.state.formValue,
                    salary_year: yearToDay(),
                    salary_month: monthToDay(),
                    [name]: value,
                },
            });
        } else {
            console.log(2);
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    [name]: value,
                },
            });
        }
    };
    onChangeDate = () => (value) => {
        let salary_year = value.format("YYYY");
        let salary_month = value.format("M");

        this.setState({
            formValue: {
                ...this.state.formValue,
                salary_year: salary_year,
                salary_month: salary_month,
            },
        });

        this.salary_template(this.state.formValue.staff_id, salary_year, salary_month);
        this.slipNumberSalary(salary_year, salary_month);
    };

    addPenerimaan = () => (e) => {
        let dataAdd = {
            amount: 0,
            template_name: "",
            template_variable: "",
            template_type: "earnings",
        };
        let earnings = this.state.earnings;

        earnings.push(dataAdd);

        this.setState({
            earnings: earnings,
        });
    };

    addPotongan = () => (e) => {
        let dataAdd = {
            amount: 0,
            template_name: "",
            template_variable: "",
            template_type: "expenditure",
        };
        let expenditure = this.state.expenditure;

        expenditure.push(dataAdd);

        this.setState({
            expenditure: expenditure,
        });
    };

    deletePenerimaan = (key) => (e) => {
        let earnings = this.state.earnings;

        delete earnings[key];
        this.setState({
            earnings: earnings,
        });
    };

    deletePotongan = (key) => (e) => {
        let expenditure = this.state.expenditure;

        delete expenditure[key];
        this.setState({
            expenditure: expenditure,
        });
    };

    changeTemplateNameEarnings = (key) => (e) => {
        let earnings = this.state.earnings;
        let value = e.target.value;

        earnings[key]["template_name"] = value;

        this.setState({
            earnings: earnings,
        });
    };

    changeTemplateNameExpenditure = (key) => (e) => {
        let expenditure = this.state.expenditure;
        let value = e.target.value;

        expenditure[key]["template_name"] = value;

        this.setState({
            expenditure: expenditure,
        });
    };

    changeAmountEarnings = (key) => (value) => {
        let earnings = this.state.earnings;

        earnings[key]["amount"] = value;

        this.setState({
            earnings: earnings,
        });
    };

    changeAmountExpenditure = (key) => (value) => {
        let expenditure = this.state.expenditure;

        expenditure[key]["amount"] = value;

        this.setState({
            expenditure: expenditure,
        });
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};
