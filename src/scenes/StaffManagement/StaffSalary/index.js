import React, {Component} from 'react';
import {Button, Col, Icon, Row, Table, Tabs} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import SallaryTemplateList from "./SallaryTemplateList";
import SalaryDataCreate from "./SalaryDataCreate";
import SalaryDataList from "./SalaryDataList";
import SalaryHistoryList from "./SalaryHistoryList";
import {menuActionViewAccess} from "../../../services/app/General";

const {TabPane} = Tabs;

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["7"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class StaffSallary extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun
        };
    }

    async componentWillMount() {

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })

    }

    render() {
        let menuListSalaryStaff = menuActionViewAccess('staff_management', 'salary', 'list_salary_staff');
        let menuListHistorySalaryStaff = menuActionViewAccess('staff_management', 'salary', 'list_history_salary_staff');
        let menuListSalaryTemplateStaff = menuActionViewAccess('staff_management', 'salary', 'list_salary_template_staff');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="list">
                                <TabPane className={menuListSalaryStaff.class} tab={<span><Icon type="ordered-list"/> Daftar Gaji Staff</span>}
                                         key="list">
                                    <SalaryDataCreate
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.state.listRefreshRun}/>
                                    <SalaryDataList
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.state.listRefreshRun}/>
                                </TabPane>
                                <TabPane className={menuListHistorySalaryStaff.class} tab={<span><Icon type="border-left"/> History Gaji Staff</span>} key="history">
                                    <SalaryHistoryList
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.state.listRefreshRun} />
                                </TabPane>
                                <TabPane className={menuListSalaryTemplateStaff.class} tab={<span><Icon type="align-left"/> Template Gaji Staff</span>}
                                         key="template">
                                    <SallaryTemplateList/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default StaffSallary;