import React, {Component} from 'react';
import {Button, Icon, Modal} from "antd";
import {GenerateLink, PostData} from "../../../../services/api";
import Spin from "antd/es/spin";


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            isLoading: true
        };
    }

    componentWillMount() {
        this.trigger();
    }

    render() {


        return (
            <span>
                <a href={GenerateLink('/salary-pdf-download/' + this.state.record.salary_id)} target={"_blank"}>
                    <Button type="success" size={"small"}>
                        <Icon type="download"/> Download
                    </Button>
                </a>

                <Modal
                    title={
                        <span>
                            <Icon type="eye"/> View File
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={900}
                    style={{top: 20}}
                    onOk={this.modalStatus(false)}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Spin spinning={this.state.isLoading}>
                       <object width="100%"
                               height="700"
                               data={GenerateLink('/salary-pdf-preview/' + this.state.record.salary_id)}
                               type="application/pdf"></object>
                    </Spin>
                </Modal>
            </span>
        )
    }

    trigger() {
        setTimeout(
            function () {
                this.setState({
                    isLoading: false
                });
            }.bind(this),
            3000
        );
    };


    modalStatus = (status) => e => {
        this.setState({
            modalVisible: status,
        });
    };
}

export default ViewFile;