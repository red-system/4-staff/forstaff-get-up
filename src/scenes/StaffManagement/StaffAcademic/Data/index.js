import React, {Component} from 'react';
import {Button, Icon, Modal, Row, Col, Table} from "antd";
import {GetData} from "../../../../services/api";
import Preview from "../Preview";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            data: [],
            isLoading: true,
        };
    }

    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Dari Tanggal',
                dataIndex: 'staff_education_date_from',
                key: 'staff_education_date_from'
            },
            {
                title: 'Sampai Tanggal',
                dataIndex: 'staff_education_date_to',
                key: 'staff_education_date_to',
            },
            {
                title: 'Tempat Pendidikan',
                dataIndex: 'staff_education_place',
                key: 'staff_education_place',
            },
            {
                title: 'Jurusan',
                dataIndex: 'staff_education_department',
                key: 'staff_education_department',
            },
            {
                title: 'Kualifikasi',
                dataIndex: 'staff_education_qualified',
                key: 'staff_education_qualified',
            },
            {
                title: 'Menu',
                render: (text, record) => {
                    return <Preview record={record}/>
                }
            }

        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete"/>
                Daftar Pendidikan
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="usergroup-delete"/> Daftar Pendidikan - {this.state.record.staff_name}
                        </span>
                }
                visible={this.state.modalVisible}
                width={1000}
                onOk={this.modalStatus(true)}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row>
                        <Col>
                            <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Kembali
                            </Button>
                        </Col>
                    </Row>
                ]}
            >

                <Table columns={columns}
                       bordered={true}
                       loading={this.state.isLoading}
                       dataSource={dataList}
                       size='small'/>

            </Modal>
        ]
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    list = () => {
        GetData(`/staff-pendidikan/${this.state.record.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        })
    }
}

export default Create;