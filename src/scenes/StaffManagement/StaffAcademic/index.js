import React, { Component } from "react";
import Breadcrumb from "../../../components/Breadcrumb";
import { Row, Col, Button, Table, Input, Icon } from "antd";

import GENERALDATA from "../../../constants/generalData";
import { GetData } from "../../../services/api";
import Data from "./Data";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "position.department.department_title") != null) {
                departmentTitle = get(record, "position.department.department_title").toString().match(reg);
            }
            var positionName = false;
            if (get(record, "position.structure_name") != null) {
                positionName = get(record, "position.structure_name").toString().match(reg);
            }
            if (!staffName && !departmentTitle && !positionName) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_education_history", "list");
        let menuDetail = menuActionViewAccess("staff_management", "staff_education_history", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Nama Staff",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Departemen",
                dataIndex: "position.department.department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "position.structure_name",
                key: "structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <Data record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];
        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <div className="row mb-1">
                                        <div className="col-9"></div>
                                        <div className="col-3 pull-right">
                                            <Search
                                                size="default"
                                                ref={(ele) => (this.searchText = ele)}
                                                suffix={suffix}
                                                onChange={this.onSearch}
                                                placeholder="Search Records"
                                                value={this.state.searchText}
                                                onPressEnter={this.onSearch}
                                            />
                                        </div>
                                    </div>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        bordered={true}
                                        loading={this.state.isLoading}
                                        dataSource={dataList}
                                        size="small"
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        this.setState({
            isLoading: true,
        });

        GetData(`/staff`).then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["15"];
let pageParent = GENERALDATA.primaryMenu.staff_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];
