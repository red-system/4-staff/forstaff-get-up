import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Detail from "./components/Detail";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};

class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }
    componentWillMount() {
        this.list();
    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var claimDateMatch = false;
            if (get(record, "claim_date_format")) {
                claimDateMatch = get(record, "claim_date_format").match(reg);
            }
            var claimReasonMatch = false;
            if (get(record, "claim_reason")) {
                claimReasonMatch = get(record, "claim_reason").match(reg);
            }
            var staffNameMatch = false;
            if (get(record, "staff.staff_name") != null) {
                staffNameMatch = get(record, "staff.staff_name").match(reg);
            }
            var positionMatch = false;
            if (get(record, "staff.position.structure_name") != null) {
                positionMatch = get(record, "staff.position.structure_name").match(reg);
            }
            if (!claimDateMatch && !claimReasonMatch && !staffNameMatch && !positionMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListConfirm = menuActionViewAccess("staff_management", "reimburse", "list_confirm");
        let menuDetailConfirm = menuActionViewAccess("staff_management", "reimburse", "detail_confirm");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                minWidth: 40,
            },
            {
                title: "Tanggal Claim",
                dataIndex: "claim_date_format",
                key: "claim_date_format",
            },
            {
                title: "Keterangan",
                dataIndex: "claim_reason",
                key: "claim_reason",
            },
            {
                title: "Staf",
                dataIndex: "staff.staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi",
                dataIndex: "staff.position.structure_name",
                key: "structure_name",
            },
            {
                title: "Nominal",
                dataIndex: "claim_nominal",
                key: "claim_nominal",
                render: (claim_nominal) =>
                    new Intl.NumberFormat("en-US", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                    }).format(claim_nominal),
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 80,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetailConfirm.class}>
                            <Detail formLayout={formLayout} record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["claim_id"],
                no: key + 1,
                ...item,
            };
        });
        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListConfirm.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }
    list() {
        GetData("/claim/confirm").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
