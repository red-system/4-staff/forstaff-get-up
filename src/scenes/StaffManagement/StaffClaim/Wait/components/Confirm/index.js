import React, {Component} from 'react';
import {Button, Icon, message, Popconfirm} from "antd";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }



    render() {

        const text = 'Yakin konfirmasi Claim ini?';
        const props = this.props
        function confirm() {
            props.confirm(props.record.claim_id)
        }
        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Ya"
                cancelText="Tidak"
            >
                <Button type="success">
                    <Icon type="check"/>
                    Konfirmasi
                </Button>
            </Popconfirm>
        )
    }
}

export default Create;