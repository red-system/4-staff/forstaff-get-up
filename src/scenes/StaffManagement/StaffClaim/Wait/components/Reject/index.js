import {Button, Icon, Input, Popover} from "antd";
import React, {Component} from "react";

const {TextArea} = Input;

class Delete extends Component {
    constructor(props){
        super(props);
        this.state = {
            claim_reject_reason:''
        };
    }

    render() {

        const content = (
            <div>
                <TextArea onChange={this.onChange("claim_reject_reason")}/>
                <div style={{marginTop: "10px"}} className={"text-center"}>
                    <Button type={"success"} size={"small"} onClick={this.sendReject()}><Icon type={"check"}/> Kirim</Button>
                </div>
            </div>
        );

        return (

            <Popover content={content} title="Alasan Ditolak" trigger="click">
                <Button type="danger">
                    <Icon type="close"/>
                    Tolak
                </Button>
            </Popover>
        )
    }
    onChange = name => value => {
        this.setState({
            claim_reject_reason:value.target.value
        })
    };
    sendReject = () => e =>{
        this.props.reject(this.props.record.claim_id,this.state.claim_reject_reason)
    }

}

export default Delete;