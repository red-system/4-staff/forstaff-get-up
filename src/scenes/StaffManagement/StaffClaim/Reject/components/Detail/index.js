import React, { Component } from "react";
import { Button, Form, Icon, Modal } from "antd";

const FormItem = Form.Item;

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue: props.record,
        };
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    at = (url) => (e) => {
        window.open(url, "_blank");
    };
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };
    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    render() {
        const { modalEditVisible, formLayout } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };

        function numberFormat(value) {
            return new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "IDR",
                minimumFractionDigits: 0,
            }).format(value);
        }

        return [
            <Button type="primary" onClick={this.showModal}>
                <Icon type="info-circle" />
                Detil
            </Button>,
            <Modal visible={this.state.visible} onCancel={this.handleCancel} className="custom-modal-v1" width={1000} footer={null}>
                <section className="form-card row no-gutters">
                    <div
                        className="form-card__img form-card__img--left col-lg-5"
                        style={{ backgroundImage: `url('${this.state.formValue.staff_avatar_url}')` }}
                    ></div>
                    <div className="form-card__body col-lg-7 p-4">
                        <section className="form-v1-container">
                            <h4>
                                <Icon type="info-circle" /> Detil Claim
                            </h4>
                            <hr />
                            <Form>
                                <FormItem {...formItemLayout} label="Nama Staf">
                                    {this.state.formValue.staff.staff_name}
                                </FormItem>
                                <FormItem {...formItemLayout} label="Posisi">
                                    {this.state.formValue.staff.position.structure_name}
                                </FormItem>
                                <FormItem {...formItemLayout} label="Nominal">
                                    {numberFormat(this.state.formValue.claim_nominal)}
                                </FormItem>
                                <FormItem {...formItemLayout} label="Status">
                                    {this.state.formValue.claim_status}
                                </FormItem>
                                <FormItem {...formItemLayout} label="Attachment">
                                    <Button type="success" onClick={this.at(this.state.formValue.claim_filename_url)}>
                                        <Icon type="file" /> Lihat Attachment
                                    </Button>
                                </FormItem>
                                <FormItem {...formItemLayout} label="Keterangan">
                                    {this.state.formValue.claim_reason}
                                </FormItem>
                                <FormItem {...formItemLayout} label="Alasan ditolak">
                                    {this.state.formValue.claim_reject_reason}
                                </FormItem>
                            </Form>
                        </section>
                    </div>
                </section>
            </Modal>,
        ];
    }
}

export default Create;
