import React, {Component} from "react";
import {Row, Col, Tabs, Table, Icon, Button, Spin, Input} from "antd";
import generalData from "../../../constants/generalData";
import {Link} from 'react-router-dom';
import Highlighter from 'react-highlight-words';
import {menuActionViewAccess} from "../../../services/app/General";

const {TabPane} = Tabs;
const gutter = generalData.gutter;
let LOG_LIST_PERSONAL = generalData.primaryMenu.logListPersonal;
let LOG_LIST_ALL = generalData.primaryMenu.logListAll;


class Logs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            loadingStatus: true,
            log_all: [],
            log_self: []
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            log_all: newProps.log_activity_all,
            log_self: newProps.log_activity_self,
            loadingStatus: false
        })
    }

    render() {

        let {match, loadingStatus, log_all, log_self} = this.state;
        const data_log_all = Object.keys(log_all).map((item, key) => {
            return {
                key: key,
                no: key + 1,
                menu: log_all[item]['menu_label'],
                activity_title: log_all[item]['log_activity_title'],
                activity_description: log_all[item]['log_activity_description'],
                name: log_all[item]['staff_name'],
                waktu: log_all[item]['created_at'],
            };
        });
        const data_log_self = Object.keys(log_self).map((item, key) => {
            return {
                key: key,
                no: key + 1,
                menu: log_self[item]['menu_label'],
                activity_title: log_self[item]['log_activity_title'],
                activity_description: log_self[item]['log_activity_description'],
                name: log_self[item]['staff_name'],
                waktu: log_self[item]['created_at'],
            };
        });
        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                key: 'no',
                defaultSortOrder: 'ascend',
                sorter: (a, b) => a.no - b.no,
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                ...this.getColumnSearchProps('menu'),
            },
            {
                title: 'Judul Aktivitas',
                dataIndex: 'activity_title',
                key: 'activity_title',
                ...this.getColumnSearchProps('activity_title'),
            },
            {
                title: 'Keterangan Aktivitas',
                dataIndex: 'activity_description',
                key: 'activity_description',
                ...this.getColumnSearchProps('activity_description'),
            },
            {
                title: 'Staff Name',
                dataIndex: 'name',
                key: 'name',
                ...this.getColumnSearchProps('name'),
            },
            {
                title: 'Waktu Aktifitas',
                dataIndex: 'waktu',
                key: 'waktu',
                ...this.getColumnSearchProps('waktu')
            },
        ];

        let menuActionLogPersonal = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'log_personal');
        let menuActionLogAll = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'log_all');

        // console.log('log index', `${match.url}/${LOG_LIST_PERSONAL.route}`);


        return (
            <Row gutter={gutter}>
                <Col lg={24}>
                    <Spin spinning={loadingStatus} tip={"Loading ..."}>
                        <div className="card-container">
                            <Tabs type="card">
                                <TabPane className={menuActionLogPersonal} tab={<span><Icon type="info-circle"/> Jejak Aktivitas Personal</span>} key="1">
                                    <Link to={`${LOG_LIST_PERSONAL.route}`}>
                                        <Button type="primary" size="small">
                                            <Icon type={LOG_LIST_PERSONAL.icon}/> {LOG_LIST_PERSONAL.label}
                                        </Button>
                                    </Link>
                                    <br/><br/>
                                    <Table columns={columns}
                                           bordered={true}
                                           dataSource={data_log_self}
                                           size='small'/>
                                </TabPane>
                                <TabPane className={menuActionLogAll} tab={<span><Icon type="build"/> Jejak Aktivitas Semua Staf</span>} key="2">
                                    <Link to={`${LOG_LIST_ALL.route}`}>
                                        <Button type="primary" size="small">
                                            <Icon type={LOG_LIST_ALL.icon}/> {LOG_LIST_ALL.label}
                                        </Button>
                                    </Link>
                                    <br/><br/>
                                    <Table columns={columns}
                                           bordered={true}
                                           dataSource={data_log_all}
                                           size='small'/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Spin>
                </Col>
            </Row>
        )

    }



    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

}

export default Logs;