import React, {Component} from 'react';
import {Col, Row} from "antd";
import ReactEcharts from "echarts-for-react";

import generalData from "../../../constants/generalData";
import moment from "moment";
import Summary from "../Summary";
import {Link} from "react-router-dom";
import GENERALDATA from "../../../constants/generalData";
import {menuActionViewAccess} from "../../../services/app/General";

const gutter = generalData.gutter;
const COLOR = {
    success: 'rgba(139,195,74,.7)',
    info: 'rgba(1,188,212,.7)',
    text: '#3D4051',
    gray: '#EDF0F1'
};

const chartResponsive = {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 12,
    xl: 12
};

class Chart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rewardPunishment: {
                Data: [],
                reward: [],
                punishment: []
            },
            kehadiran: {
                Data: [],
                kehadiranData: [],
            }
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            rewardPunishment: nextProps.rewardPunishment,
            kehadiran: nextProps.kehadiran,
        })
    }


    render() {
        let combo = {};
        let combo2 = {};

        combo.option = {
            legend: {
                show: true,
                x: 'right',
                y: 'top',
                data: ['Kehadiran']
            },
            grid: {
                x: 40,
                y: 60,
                x2: 40,
                y2: 30,
                borderWidth: 0
            },
            tooltip: {
                show: true,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        color: COLOR.gray
                    }
                }
            },
            xAxis: [
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#607685'
                        }
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: '#f3f3f3'
                        }
                    },
                    data: this.state.kehadiran.Data
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#607685'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#f3f3f3'
                        }
                    }
                }
            ],
            series: [
                {
                    name: 'Kehadiran',
                    type: 'line',
                    animation: false,
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: COLOR.success,
                            areaStyle: {
                                color: COLOR.success,
                                type: 'default'
                            }
                        }
                    },
                    data: this.state.kehadiran.kehadiranData,
                    symbol: 'none',
                    legendHoverLink: false,
                    z: 3
                }
            ]
        };
        combo2.option = {
            legend: {
                show: true,
                x: 'right',
                y: 'top',
                data: ['Reward', 'Punishment']
            },
            grid: {
                x: 40,
                y: 60,
                x2: 40,
                y2: 30,
                borderWidth: 0
            },
            tooltip: {
                show: true,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        color: COLOR.gray
                    }
                }
            },
            xAxis: [
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#607685'
                        }
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: '#f3f3f3'
                        }
                    },
                    data: this.state.rewardPunishment.Data
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#607685'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#f3f3f3'
                        }
                    }
                }
            ],
            series: [
                {
                    name: 'Reward',
                    type: 'line',
                    animation: false,
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: COLOR.success,
                            areaStyle: {
                                color: COLOR.success,
                                type: 'default'
                            }
                        }
                    },
                    data: this.state.rewardPunishment.reward,
                    symbol: 'none',
                    legendHoverLink: false,
                    z: 3
                },
                {
                    name: 'Punishment',
                    type: 'line',
                    animation: false,
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: COLOR.info,
                            areaStyle: {
                                color: COLOR.info,
                                type: 'default'
                            }
                        }
                    },
                    data: this.state.rewardPunishment.punishment,
                    symbol: 'none',
                    legendHoverLink: false,
                    z: 4
                }
            ]
        };


        let menuAbsence = GENERALDATA.primaryMenu.staff_attendence.sub_menu["3"];
        let menuReward = GENERALDATA.primaryMenu.sop_management.sub_menu["6"];
        let menuActionStaffAttendence = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'staff_attendence_chart');
        let menuActionSanksiPunishment = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'staff_sanksi_punishment_chart');

        return (
            <Row gutter={gutter}>
                <Col {...chartResponsive} className={menuActionStaffAttendence.class}>
                    <Link to={menuAbsence.route}>
                        <div className="app-content">
                            <div className="app-content-title">Grafik Kehadiran Staf perBulan
                                tahun {moment().format("YYYY")}</div>
                            <div className="app-content-body">
                                <ReactEcharts option={combo.option} theme={"macarons"} style={{height: '450px'}}/>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col {...chartResponsive} className={menuActionSanksiPunishment.class}>
                    <Link to={menuReward.route}>
                        <div className="app-content">
                            <div className="app-content-title">Grafik Staf Penghargaan and Sanksi
                                Tahun {moment().format("YYYY")}</div>
                            <div className="app-content-body">
                                <ReactEcharts option={combo2.option} theme={"macarons"} style={{height: '450px'}}/>
                            </div>
                        </div>
                    </Link>
                </Col>
            </Row>
        )
    }
}

export default Chart;