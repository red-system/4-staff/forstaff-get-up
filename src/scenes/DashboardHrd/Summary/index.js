import React, {Component} from 'react';
import {Col, Icon, Row, Spin} from "antd";
import generalData from "../../../constants/generalData";
import {Link} from "react-router-dom";
import '../../../styles/ui/_card-number.scss';
import {menuActionViewAccess} from "../../../services/app/General";

const dataStaff = generalData.primaryMenu.staff_management.sub_menu["1"];
const absence = generalData.primaryMenu.staff_attendence.sub_menu["3"];
const reward = generalData.primaryMenu.sop_management.sub_menu["6"];
const punishment = generalData.primaryMenu.sop_management.sub_menu["7"];

const cardResponsive = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 6,
    xl: 6
};

const gutter = generalData.gutter;

class Summary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            summary: [],
            loadingStatus: true
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            summary: newProps.summary,
            loadingStatus: false
        });
    }

    render() {

        const {summary, loadingStatus} = this.state;
        let menuActionTotalStaff = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'total_staff');
        let menuActionTotalAbsence = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'total_absence');
        let menuActionTotalReward = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'total_reward');
        let menuActionTotalPunishment = menuActionViewAccess('dashboard_hrd', 'dashboard_hrd_view', 'total_punishment');
        return (
            <Row gutter={gutter} className="card-wrapper">
                <Col {...cardResponsive} className={menuActionTotalStaff.class}>
                    <Link to={`${dataStaff.route}`}>
                        <Spin tip="Loading..." spinning={loadingStatus}>
                            <div className="number-card-v1 card-staff">
                                <div className="card-top">
                                    <Icon type="usergroup-delete"/>
                                </div>
                                <div className="card-info">
                                    <span>TOTAL STAF</span>
                                </div>
                                <div className="card-bottom">
                                    <span>{summary.total_staff}</span>
                                </div>
                            </div>
                        </Spin>
                    </Link>
                </Col>
                <Col {...cardResponsive} className={menuActionTotalAbsence.class}>
                    <Link to={`${absence.route}`}>
                        <Spin tip="Loading..." spinning={loadingStatus}>
                            <div className="number-card-v1 card-absence">
                                <div className="card-top">
                                    <Icon type="issues-close"/>
                                </div>
                                <div className="card-info">
                                    <span>TOTAL TIDAK HADIR</span>
                                </div>
                                <div className="card-bottom">
                                    <span>{summary.total_absence}</span>
                                </div>
                            </div>
                        </Spin>
                    </Link>
                </Col>
                <Col {...cardResponsive} className={menuActionTotalReward}>
                    <Link to={`${reward.route}`}>
                        <Spin tip="Loading..." spinning={loadingStatus}>
                            <div className="number-card-v1 card-reward">
                                <div className="card-top">
                                    <Icon type="gift"/>
                                </div>
                                <div className="card-info">
                                    <span>TOTAL PENGHARGAAN</span>
                                </div>
                                <div className="card-bottom">
                                    <span>{summary.total_reward}</span>
                                </div>
                            </div>
                        </Spin>
                    </Link>
                </Col>
                <Col {...cardResponsive} className={menuActionTotalPunishment}>
                    <Link to={`${punishment.route}`}>
                        <Spin tip="Loading..." spinning={loadingStatus}>
                            <div className="number-card-v1 card-punishment">
                                <div className="card-top">
                                    <Icon type="warning"/>
                                </div>
                                <div className="card-info">
                                    <span>TOTAL SANKSI</span>
                                </div>
                                <div className="card-bottom">
                                    <span>{summary.total_punishment}</span>
                                </div>
                            </div>
                        </Spin>
                    </Link>
                </Col>
            </Row>
        )
    }

}

export default Summary;