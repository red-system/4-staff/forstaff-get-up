import React, {Component} from "react";

import Breadcrumb from '../../components/Breadcrumb';
import Summary from './Summary';
import Chart from './Chart';
import Logs from './Logs';
import './styles.scss';
import {GetData} from "../../services/api";

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            match: props.match,
            summary: '',
            chart: '',
            log_activity: [],
            log_activity_all: [],
            log_activity_self: [],
            rewardPunishment: [],
            kehadiran: [],
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb key="0"/>)]
        });

        GetData('/dashboard-hrd').then((result) => {
            let data = result.data;
            this.setState({
                summary: data.data.summary,
                chart: data.data.chart,
                log_activity_all: data.data.log_activity.all,
                log_activity_self: data.data.log_activity.self,
                kehadiran: data.data.absence,
                rewardPunishment: data.data.reward_punishment,
            });
        });
    }

    render() {
        let {match, summary, chart, log_activity_all, log_activity_self} = this.state;

        return (
            <div className="content-gutter-vertical">
                {this.state.breadcrumbComponent}
                <Summary summary={summary}/>
                <Chart rewardPunishment={this.state.rewardPunishment}
                       kehadiran={this.state.kehadiran}
                       chart={chart}/>
                <Logs match={match}
                      log_activity_all={log_activity_all}
                      log_activity_self={log_activity_self}/>
            </div>
        )

    }

}

export default Dashboard;