import React, { Component } from "react";
import { Row, Col } from "antd";
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from "../../components/Breadcrumb";
import { Link } from "react-router-dom";

import "../../styles/ui/_card-icon.scss";
import { menuViewAccess } from "../../services/app/General";

let personalMenu = GENERALDATA.primaryMenu.personal_menu;
let subPersonalMenu = GENERALDATA.primaryMenu.personal_menu.sub_menu;
let gutter = GENERALDATA.gutter;

const menuGrid = {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 3,
};

const breadcrumb = [
    {
        label: personalMenu.label,
        route: `${personalMenu.route}`,
    },
];

export default class AppComponent extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="-1" />],
        });
    }

    render() {
        let tabActive = "informasi-umum";
        //delete subPersonalMenu[0];
        const menuList = Object.keys(subPersonalMenu).map(function (key) {
            if (key == 1) {
                tabActive = "informasi-umum";
            } else if (key == 2) {
                tabActive = "kpi";
            } else if (key == 3) {
                tabActive = "salary";
            }

            if (key > 0) {
                let menuView = menuViewAccess(personalMenu.variable, subPersonalMenu[key].variable);

                if (menuView) {
                    return (
                        <Col {...menuGrid} className="text-center" key={key}>
                            <Link
                                to={{
                                    pathname: subPersonalMenu[key].route,
                                    state: {
                                        tabActive: tabActive,
                                    },
                                }}
                            >
                                <img src={subPersonalMenu[key].img} alt={subPersonalMenu[key].label} />
                                <h1>{subPersonalMenu[key].label} </h1>
                            </Link>
                        </Col>
                    );
                }
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row gutter={gutter} className="menu-list" type="flex" align="top">
                    {menuList}
                </Row>
            </div>
        );
    }
}
