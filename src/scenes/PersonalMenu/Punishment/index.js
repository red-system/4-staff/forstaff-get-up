import React, { Component } from "react";
import { Button, Col, Icon, Row, Table, Input } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { GetData } from "../../../services/api";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["15"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        GetData("/punishment-staff").then((result) => {
            this.setState({
                data: result.data.data,
                breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
                isLoading: false,
            });
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var punishmentDate = false;
            if (get(record, "punishment_date_format") != null) {
                punishmentDate = get(record, "punishment_date_format").toString().match(reg);
            }
            var punishmentTitle = false;
            if (get(record, "punishmentg_title") != null) {
                punishmentTitle = get(record, "punishmentg_title").toString().match(reg);
            }
            var punishmentDescription = false;
            if (get(record, "punishmentg_description") != null) {
                punishmentDescription = get(record, "punishmentg_description").toString().match(reg);
            }
            if (!punishmentDate && !punishmentTitle && !punishmentDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("personal_menu", "punishment", "list");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Tanggal",
                dataIndex: "punishment_date_format",
                key: "punishment_date_format",
            },
            {
                title: "Pelanggaran",
                dataIndex: "punishmentg_title",
                key: "punishmentg_title",
            },
            {
                title: "Deskripsi Pelanggaran",
                dataIndex: "punishmentg_description",
                key: "punishmentg_description",
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    dataSource={data}
                                    bordered={true}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RewardGuide;
