import React, { Component } from 'react';
import AbsenceSummaryDetailTable from "../../StaffAttendence/Absence/1_AbsenceSummaryDetailTable";
import {Col, Row} from "antd";
import Breadcrumb from "../../../components/Breadcrumb";
import GENERALDATA from "../../../constants/generalData";

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor() {
        super();
        const loginData = JSON.parse(localStorage.getItem("loginData"))
        this.state = {
            breadcrumbComponent: null,
            staff_id:loginData.staff_id
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }


    render() {
        return(
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <AbsenceSummaryDetailTable staff_id={this.state.staff_id}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}