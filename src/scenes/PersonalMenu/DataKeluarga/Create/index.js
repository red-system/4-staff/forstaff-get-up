import React, {Component} from 'react';
import {Button, DatePicker, Form, Icon, Input, Modal, Row, Col, message} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            formValue: {
                staff_family_birthday: dateToDay()
            },
            formError: [],
        };


    }

    render() {

        const {formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 5},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 19},
            },
        };

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onOk={this.modalStatus(true)}
                    onCancel={this.modalStatus(false)}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert()}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalStatus(false)}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Nama Lengkap"
                            required={true}
                            {...this.state.formError.staff_family_name}
                        >
                            <Input
                                value={this.state.formValue.staff_family_name}
                                onChange={this.onChange("staff_family_name")}/>
                        </Form.Item>
                        <Form.Item
                            label="Hubungan"
                            required={true}
                            {...this.state.formError.staff_family_relation}
                        >
                            <Input
                                value={this.state.formValue.staff_family_relation}
                                onChange={this.onChange("staff_family_relation")}/>
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Lahir"
                            required={true}
                            {...this.state.formError.staff_family_birthday}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                value={moment(dateFormatterApp(this.state.formValue.staff_family_birthday), dateFormatApp())}
                                format={dateFormatApp()}
                                onChange={this.onChangeDate("staff_family_birthday")}
                            />
                        </Form.Item>
                        <Form.Item
                            label="No Telepon"
                            required={true}
                            {...this.state.formError.staff_family_contact}
                        >
                            <Input
                                value={this.state.formValue.staff_family_contact}
                                onChange={this.onChange("staff_family_contact")}/>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    insert = () => e => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            formData.append(key, this.state.formValue[key])
        });
        PostData(`/staff-keluarga/${this.props.staff_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formValue: {
                        staff_family_birthday: dateToDay()
                    },
                    formError: [],
                });
                this.props.list()
            } else {
                message.warning(data.message);

                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

export default Create;