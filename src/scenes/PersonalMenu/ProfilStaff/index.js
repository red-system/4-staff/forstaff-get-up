import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import ImageEditorRc from "react-cropper-image-editor";
import {Link} from "react-router-dom";
import {
    dateFormatApi,
    dateFormatApp,
    dateFormatterApp,
    menuActionViewAccess, staff_avatar_config_upload_image,
    staff_avatar_rules_upload_image
} from "../../../services/app/General";
import {
    Icon,
    Row,
    Col,
    Form,
    Input,
    Select,
    DatePicker,
    message,
    Button,
    Spin
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import moment from "moment";
import {GetData, PostData} from "../../../services/api";
import 'cropperjs/dist/cropper.css';


const gutter = GENERALDATA.gutter;
const {Option} = Select;

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        const loginData = JSON.parse(localStorage.getItem("loginData"));
        this.state = {
            breadcrumbComponent: null,
            staff_id: loginData.staff_id,
            staff: [],
            province: [],
            firstLoad: true,
            primaryDistrict: [],
            secondaryDistrict: [],
            department: [],
            position: [],
            formError: [],
        }
    }

    componentWillMount() {
        this.loadStaffDetail();
        this.loadProvince();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });

    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    handleConfirmBlur = e => {
        const {value} = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };


    render() {
        let menuList = menuActionViewAccess('personal_menu', 'profile_staff', 'list');
        let menuEdit = menuActionViewAccess('personal_menu', 'profile_staff', 'edit');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <Row gutter={gutter} className={menuList.class}>
                                        <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                            <h2><Icon type="user"/> DATA PRIBADI</h2>
                                            <Form {...formItemLayout}>
                                                <Form.Item
                                                    {...this.state.formError.staff_avatar}
                                                    {...formItemLayout}
                                                    label="Foto Profil"
                                                >
                                                    <ImageEditorRc
                                                        {...staff_avatar_config_upload_image()}
                                                        src={this.state.staff.staff_avatar_url}
                                                        saveImage={this.cropper} // it has to catch the returned data and do it whatever you want
                                                        responseType='blob/base64'/>
                                                    <div className="upload-btn-wrapper-register">
                                                        <button className="btn-file">Browse Foto</button>
                                                        <input type="file" name="myfile"
                                                               onChange={this.browseFotoProfile}/>
                                                    </div>
                                                    {staff_avatar_rules_upload_image()}
                                                </Form.Item>
                                                <Form.Item
                                                    label="Nama Lengkap"
                                                    {...this.state.formError.staff_name}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_name}
                                                        onChange={this.onChange("staff_name")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Nama Panggilan" {...this.state.formError.staff_nickname}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_nickname}
                                                        onChange={this.onChange("staff_nickname")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Kewarganegaraan"
                                                    {...this.state.formError.staff_nationality}
                                                >
                                                    <Select value={this.state.staff.staff_nationality}
                                                            onChange={this.onChange("staff_nationality")}
                                                            style={{width: "100%"}}>
                                                        <Option value="wni">Warna Negara Indonesia</Option>
                                                        <Option value="wna">Warga Negara Asing</Option>
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item label="Jenis Kelamin" {...this.state.formError.staff_gender}
                                                >
                                                    <Select value={this.state.staff.staff_gender}
                                                            onChange={this.onChange("staff_gender")}
                                                            style={{width: "100%"}}>
                                                        <Option value="male">Pria</Option>
                                                        <Option value="female">Wanita</Option>
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Tempat Lahir" {...this.state.formError.staff_birth_place}

                                                >
                                                    <Row>
                                                        <Col {...divide21}>
                                                            <Input value={this.state.staff.staff_birth_place}
                                                                   onChange={this.onChange("staff_birth_place")}/>
                                                        </Col>
                                                        <Col {...divide22} style={{paddingLeft: "20px"}}>
                                                            <Form.Item
                                                                label="Tanggal Lahir" {...formItemLayout2} {...this.state.formError.staff_birth_date} >
                                                                <DatePicker
                                                                    style={{width: '100%'}}
                                                                    onChange={this.onChangeDate("staff_birth_date")}
                                                                    value={moment(dateFormatterApp(this.state.staff.staff_birth_date), dateFormatApp())}
                                                                    format={dateFormatApp()}
                                                                />
                                                            </Form.Item>
                                                        </Col>
                                                    </Row>
                                                </Form.Item>
                                                <Form.Item label="Jenis Identitas"
                                                           {...this.state.formError.staff_identity_type}
                                                >
                                                    <Select
                                                        value={this.state.staff.staff_identity_type}
                                                        onChange={this.onChange("staff_identity_type")}
                                                        style={{width: "100%"}}
                                                    >
                                                        <Option value="sim">SIM</Option>
                                                        <Option value="ktp">KTP</Option>
                                                        <Option value="kk">KK</Option>
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item label="Nomer Identitas"
                                                           {...this.state.formError.staff_identity_number}
                                                >
                                                    <Input value={this.state.staff.staff_identity_number}
                                                           onChange={this.onChange("staff_identity_number")}/>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Agama"
                                                    {...this.state.formError.staff_religion}
                                                >
                                                    <Select
                                                        value={this.state.staff.staff_religion}
                                                        onChange={this.onChange("staff_religion")}
                                                        style={{width: "100%"}}
                                                    >
                                                        <Option value="hindu">Hindu</Option>
                                                        <Option value="budha">Budha</Option>
                                                        <Option value="islam">Islam</Option>
                                                        <Option value="konghuchu">Khonghuchu</Option>
                                                        <Option value="katolik">Kristen Katolik</Option>
                                                        <Option value="kristen_protestan">Kristen Protestan</Option>
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Status Perkawinan"
                                                    {...this.state.formError.staff_marital_status}
                                                >
                                                    <Select value={this.state.staff.staff_marital_status}
                                                            onChange={this.onChange("staff_marital_status")}
                                                            style={{width: "100%"}}
                                                    >
                                                        <Option value="yes">Menikah</Option>
                                                        <Option value="no">Belum Menikah</Option>
                                                    </Select>
                                                </Form.Item>

                                                <Form.Item
                                                    label="Golongan Darah"
                                                    {...this.state.formError.staff_blood_type}
                                                >
                                                    <Select value={this.state.staff.staff_blood_type}
                                                            onChange={this.onChange("staff_blood_type")}
                                                            style={{width: "100%"}}
                                                    >
                                                        <Option value="A">A</Option>
                                                        <Option value="B">B</Option>
                                                        <Option value="AB">AB</Option>
                                                        <Option value="O">O</Option>
                                                    </Select>
                                                </Form.Item>
                                            </Form>
                                        </Col>
                                        <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                                            <h2><Icon type="database"/> DATA APLIKASI</h2>
                                            <Form.Item
                                                label="Username"
                                                {...this.state.formError.staff_username}
                                                required={true}
                                            >
                                                <Input
                                                    autocomplete="new-username"
                                                    onChange={this.onChange("staff_username")}
                                                    value={this.state.staff.staff_username}
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                label="Password"
                                                required={true}
                                                {...this.state.formError.staff_password}
                                            >
                                                <Input.Password
                                                    autocomplete="new-password"
                                                    onChange={this.onChange("staff_password")}/>
                                            </Form.Item>
                                            <Form.Item
                                                label="Konfirmasi Password"
                                                required={true}
                                                {...this.state.formError.staff_password_confirmation}
                                            >
                                                <Input.Password
                                                    autocomplete="new-password"
                                                    onChange={this.onChange("staff_password_confirmation")}/>
                                            </Form.Item>

                                            <br />
                                            <h2><Icon type="contacts"/> KONTAK</h2>
                                            <Form {...formItemLayout}>
                                                <strong><Icon type="check"/> ALAMAT ASAL</strong>
                                                <hr/>
                                                <Form.Item
                                                    label="Alamat Tetap"
                                                    {...this.state.formError.staff_primary_address}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_primary_address}
                                                        onChange={this.onChange("staff_primary_address")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Provinsi"
                                                    {...this.state.formError.staff_primary_province_id}
                                                >
                                                    <Select
                                                        showSearch
                                                        style={{width: "100%"}}
                                                        placeholder="Pilih Provinsi"
                                                        optionFilterProp="children"
                                                        value={this.state.staff.staff_primary_province_id}
                                                        onChange={this.onChangePrimaryProvince()}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {this.state.province == undefined ? "" :
                                                            Object.keys(this.state.province).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Kabupaten/Kodya"
                                                    {...this.state.formError.staff_primary_district_id}
                                                >
                                                    <Select
                                                        showSearch
                                                        style={{width: "100%"}}
                                                        placeholder="Pilih Kabupaten"
                                                        optionFilterProp="children"
                                                        value={this.state.staff.staff_primary_district_id}
                                                        onChange={this.onChange("staff_primary_district_id")}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {
                                                            Object.keys(this.state.primaryDistrict).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.primaryDistrict[key].district_id}>{this.state.primaryDistrict[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Kode Pos"
                                                    {...this.state.formError.staff_primary_post_code}
                                                >
                                                    <Input value={this.state.staff.staff_primary_post_code}
                                                           onChange={this.onChange("staff_primary_post_code")}
                                                    />
                                                </Form.Item>

                                                <strong><Icon type="check"/> ALAMAT TINGGAL</strong>
                                                <hr/>

                                                <Form.Item
                                                    label="Alamat Tetap"
                                                    {...this.state.formError.staff_second_address}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_second_address}
                                                        onChange={this.onChange("staff_second_address")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Provinsi"
                                                    {...this.state.formError.staff_second_province_id}
                                                >
                                                    <Select
                                                        showSearch
                                                        style={{width: "100%"}}
                                                        placeholder="Pilih Provinsi"
                                                        optionFilterProp="children"
                                                        value={this.state.staff.staff_second_province_id}
                                                        onChange={this.onChangeSecondProvince()}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {this.state.province == undefined ? "" :
                                                            Object.keys(this.state.province).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Kabupaten/Kodya"
                                                    {...this.state.formError.staff_second_district_id}
                                                >
                                                    <Select
                                                        showSearch
                                                        style={{width: "100%"}}
                                                        placeholder="Pilih Kabupaten"
                                                        optionFilterProp="children"
                                                        value={this.state.staff.staff_second_district_id}
                                                        onChange={this.onChange("staff_second_district_id")}
                                                        filterOption={(input, option) =>
                                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                        }
                                                    >
                                                        {
                                                            Object.keys(this.state.secondaryDistrict).map((key) => {
                                                                return (
                                                                    <Option key={key}
                                                                            value={this.state.secondaryDistrict[key].district_id}>{this.state.secondaryDistrict[key].name}</Option>
                                                                )
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item
                                                    label="Kode Pos"
                                                    {...this.state.formError.staff_second_post_code}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_second_post_code}
                                                        onChange={this.onChange("staff_second_post_code")}
                                                    />
                                                </Form.Item>


                                                <strong><Icon type="check"/> DATA LAINNYA</strong>
                                                <hr/>
                                                <Form.Item
                                                    label="Nomer HP"
                                                    {...this.state.formError.staff_phone_number}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_phone_number}
                                                        onChange={this.onChange("staff_phone_number")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Nomer Kontak Darurat"
                                                    {...this.state.formError.staff_phone_emergency}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_phone_emergency}
                                                        onChange={this.onChange("staff_phone_emergency")}
                                                    />
                                                </Form.Item>
                                                <Form.Item
                                                    label="Email"
                                                    {...this.state.formError.staff_email}
                                                >
                                                    <Input
                                                        value={this.state.staff.staff_email}
                                                        onChange={this.onChange("staff_email")}
                                                    />
                                                </Form.Item>
                                                <div className={"wrapper-button-float"}>
                                                    <Button
                                                        className={menuEdit.class}
                                                        type="primary"
                                                        onClick={this.insert()}
                                                        icon={"check"}
                                                        size={"large"}>
                                                        Perbarui Data
                                                    </Button>
                                                    &nbsp;
                                                    <Link to={`../${pageParent.route}`}>
                                                        <Button
                                                            type="warning"
                                                            icon={"rollback"}
                                                            size={"large"}>
                                                            Kembali
                                                        </Button>
                                                    </Link>
                                                </div>
                                            </Form>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }


    cropper = (data) => {
        console.log('cropper', data);
        let staff_avatar = this.dataURLtoFile(data, 'staff.jpg');
        this.setState({
            staff: {
                ...this.state.staff,
                staff_avatar: staff_avatar,
                staff_avatar_url: URL.createObjectURL(staff_avatar),
            }
        })
    };

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    browseFotoProfile = (event) => {
        if (event.target.files[0]) {
            this.setState({
                staff: {
                    ...this.state.staff,
                    staff_avatar_url: URL.createObjectURL(event.target.files[0]),
                    staff_avatar: event.target.files[0]
                }
            })
        }
    };

    onChangePrimaryProvince = () => value => {
        this.setState({
            staff: {
                ...this.state.staff,
                staff_primary_province_id: value,
                staff_primary_district_id: null
            }
        });
        this.loadDistrict("primaryDistrict", value)
    };

    loadDistrict(state, province_id) {
        const formData = new FormData();
        formData.append("province_id", province_id);
        PostData('/district', formData).then((result) => {
            this.setState({
                [state]: result.data.data
            })
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            staff: {
                ...this.state.staff,
                [name]: value
            }
        });
    };

    onChangeSecondProvince = () => value => {
        this.setState({
            staff: {
                ...this.state.staff,
                staff_second_province_id: value,
                staff_second_district_id: null
            }
        });
        this.loadDistrict("secondaryDistrict", value)
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            staff: {
                ...this.state.staff,
                [name]: value
            }
        });
    };

    loadStaffDetail = () => {
        let staff_id = this.state.staff_id;
        GetData(`/staff/${staff_id}`).then((result) => {
            const staff = result.data.data;

            this.loadDistrict("primaryDistrict", staff.staff_primary_province_id);
            this.loadDistrict("secondaryDistrict", staff.staff_second_province_id);
            // staff.roles_id_json = JSON.parse(staff.role_master_id);


            this.setState({
                staff: staff
            });
        });
    };

    loadProvince = () => {
        GetData('/province').then((result) => {
            this.setState({
                province: result.data.data
            })
        });
    };

    insert = () => e => {
        const formData = new FormData();
        Object.keys(this.state.staff).map((key) => {
            if (this.state.staff[key] != null) {
                formData.append(key, this.state.staff[key]);
            }
        });
        PostData(`/staff/${this.state.staff.staff_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    formError: []
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 5},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 19},
    },
};

const formItemLayout2 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};

const divide21 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 8,
    xl: 8
};

const divide22 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 16,
    xl: 16
};
