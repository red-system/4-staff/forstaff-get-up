import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Row,
    Col, Button, Table,
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import {PostData} from "../../../services/api";
import Preview from "./Preview";
import {menuActionViewAccess} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["6"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        let loginData = localStorage.getItem('loginData');
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            data: []
        }

    }

    componentWillMount() {
        this.data();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }

    render() {
        let menuList = menuActionViewAccess('personal_menu', 'job_contract', 'list');
        let menuPreview = menuActionViewAccess('personal_menu', 'job_contract', 'preview');

        const columns = [
            {title: 'No', dataIndex: 'no', width: 30},
            {
                title: 'Tanggal Mulai Kontrak',
                dataIndex: 'staff_contract_date_from_format',
                key: 'staff_contract_date_from_format'
            },
            {
                title: 'Tanggal Berakhir Kontrak',
                dataIndex: 'staff_contract_date_to_format',
                key: 'staff_contract_date_to_format'
            },
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuPreview.class}>
                            <Preview record={record}/>
                        </span>
                    </Button.Group>
                )
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">

                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        bordered={true}
                                        dataSource={dataList}
                                        size='small'/>

                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    data() {
        PostData('/profile-contract-list', {staff_id: this.state.staff_id})
            .then((result) => {
                this.setState({
                    data: result.data.data
                })
            });
    }

}
