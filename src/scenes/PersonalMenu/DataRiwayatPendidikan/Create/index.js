import React, {Component} from 'react';
import {Button, Col, DatePicker, Form, Icon, Input, message, Modal, Row} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            formValue: {
                staff_education_date_from: dateToDay(),
                staff_education_date_to: dateToDay()
            },
            formError: [],
        };
    }

    render() {

        const {formLayout} = this.state;

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)} icon={"check"}>
                    Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalStatus(false)}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Dari Tanggal"
                            required={true}
                            {...this.state.formError.staff_education_date_from}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("staff_education_date_from")}
                                value={moment(dateFormatterApp(this.state.formValue.staff_education_date_from), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Sampai Tanggal"
                            required={true}
                            {...this.state.formError.staff_education_date_to}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("staff_education_date_to")}
                                value={moment(dateFormatterApp(this.state.formValue.staff_education_date_to), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Sekolah/Institusi/Universitas"
                            required={true}
                            {...this.state.formError.staff_education_place} >
                            <Input
                                onChange={this.onChange("staff_education_place")}
                                value={this.state.formValue.staff_education_place}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Jurusan"
                            required={true}
                            {...this.state.formError.staff_education_department} >
                            <Input
                                onChange={this.onChange("staff_education_department")}
                                value={this.state.formValue.staff_education_department}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Kualifikasi"
                            required={true}
                            {...this.state.formError.staff_education_qualified} >
                            <Input
                                onChange={this.onChange("staff_education_qualified")}
                                value={this.state.formValue.staff_education_qualified}
                            />
                        </Form.Item>
                        <Form.Item
                            label="File Scan Ijazah"
                            required={true}
                            {...this.state.formError.staff_education_filename} >
                            <img src={this.state.formValue.staff_education_filename_url} className={"image-preview"}/>
                            <br/>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File Scan Ijazah</button>
                                <input type="file" name="myfile" onChange={this.onChangeFileImage}/>
                            </div>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        )
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeFileImage = (e) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_education_filename_url: URL.createObjectURL(e.target.files[0]),
                staff_education_filename: e.target.files[0]
            }
        });
    };

    insert = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != null) {
                formData.append(key, this.state.formValue[key])
            }
        });
        PostData(`/staff-pendidikan/${this.props.staff_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formValue: {
                        staff_education_date_from: dateToDay(),
                        staff_education_date_to: dateToDay()
                    },
                    formError: []
                });
                this.props.list()
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        });

        return false;
    }
}

export default Create;


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};