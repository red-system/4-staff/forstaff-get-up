import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";

class Delete extends Component {
    constructor(props){
        super(props)
    }

    render() {

        const text = 'Yakin hapus data ini ?';
        const props = this.props;
        function confirm() {
            DeleteData(`/staff-pendidikan/${props.record.staff_education_id}`).then((result)=>{
                if(result.data.status === "success"){
                    message.success(result.data.message)
                    props.list()
                } else {
                    message.error(result.data.message)
                }
            })
        }

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger">
                    <Icon type="close"/>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }
}

export default Delete;