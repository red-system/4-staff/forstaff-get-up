import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Row,
    Col,
    Button,
    Table,
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import {GetData} from "../../../services/api";
import Edit from "./Edit";
import Delete from "./Delete";
import Create from "./Create";
import Preview from "./Preview";
import {menuActionViewAccess} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["8"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem('loginData');
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }

    render() {

        let menuList = menuActionViewAccess('personal_menu', 'education_history', 'list');
        let menuCreate = menuActionViewAccess('personal_menu', 'education_history', 'create');
        let menuEdit = menuActionViewAccess('personal_menu', 'education_history', 'edit');
        let menuDelete = menuActionViewAccess('personal_menu', 'education_history', 'delete');
        let menuPreview = menuActionViewAccess('personal_menu', 'education_history', 'preview');

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Dari Tanggal',
                dataIndex: 'staff_education_date_from_format',
                key: 'staff_education_date_from_format'
            },
            {
                title: 'Sampai Tanggal',
                dataIndex: 'staff_education_date_to_format',
                key: 'staff_education_date_to_format'
            },
            {
                title: 'Sekolah/Institusi/Universitas',
                dataIndex: 'staff_education_place',
                key: 'staff_education_place',
            },
            {
                title: 'Jurusan',
                dataIndex: 'staff_education_department',
                key: 'staff_education_department'
            },
            {
                title: 'Kualifikasi',
                dataIndex: 'staff_education_qualified',
                key: 'staff_education_qualified'
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 250,
                render: (test, record) => (

                    <Button.Group size={"small"}>
                        <span className={menuPreview.class}>
                            <Preview record={record}/>
                        </span>
                        <span className={menuEdit.class}>
                            <Edit
                                record={record}
                                list={this.list}
                                staff_id={this.state.staff_id}
                                formLayout={formLayout}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete
                                record={record}
                                staff_id={this.state.staff_id}
                                list={this.list}/>
                        </span>
                    </Button.Group>

                )
            }
        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['staff_family_id'],
                no: key + 1,
                ...item
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuCreate.class}>
                                        <Create
                                            list={this.list}
                                            staff_id={this.state.staff_id}
                                            formLayout={formLayout}/>
                                    </span>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        bordered={true}
                                        loading={this.state.isLoading}
                                        dataSource={dataList}
                                        size='small'/>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        this.setState({
            isLoading: true
        });

        GetData(`/staff-pendidikan/${this.state.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 16,
        offset: 8
    },
    'lg': {
        span: 16,
        offset: 8
    }
};
