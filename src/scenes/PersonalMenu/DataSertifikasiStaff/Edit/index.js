import React, {Component} from 'react';
import {Button, DatePicker, Form, Icon, Input, Modal, Row, Col, message} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";

const {TextArea} = Input;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue: props.record,
            formError: [],
        };
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        const {modalCreateVisible, formLayout} = this.state;

        return [
            <Button
                type="success"
                onClick={this.modalCreateShow}
                icon={"check"}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                }
                visible={modalCreateVisible}
                width={700}
                onOk={this.modalCreateSubmit}
                onCancel={this.modalCreateCancel}
                footer={[
                    <Row style={{textAlign: 'left'}}>
                        <Col {...formLayout}>
                            <Button key="submit" type="primary" onClick={this.insert()}>
                                Simpan
                            </Button>
                            <Button key="back" onClick={this.modalCreateCancel}>
                                Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Keterampilan/Kursus"
                        {...this.state.formError.staff_certificate_skill}
                    >
                        <Input value={this.state.formValue.staff_certificate_skill}
                               onChange={this.onChange("staff_certificate_skill")}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Tanggal Mulai"
                        {...this.state.formError.staff_certificate_date_from}
                    >
                        <DatePicker
                            style={{width: '100%'}}
                            onChange={this.onChangeDate("staff_certificate_date_from")}
                            value={moment(dateFormatterApp(this.state.formValue.staff_certificate_date_from), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Tanggal Selesai"
                        {...this.state.formError.staff_certificate_date_to}
                    >

                        <DatePicker
                            style={{width: '100%'}}
                            onChange={this.onChangeDate("staff_certificate_date_to")}
                            value={moment(dateFormatterApp(this.state.formValue.staff_certificate_date_to), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Level"
                        {...this.state.formError.staff_certificate_level}
                    >
                        <Input
                            value={this.state.formValue.staff_certificate_level}
                            onChange={this.onChange("staff_certificate_level")}/>
                    </Form.Item>
                    <Form.Item
                        label="Keterangan"
                        {...this.state.formError.staff_certificate_description}
                    >
                        <TextArea
                            value={this.state.formValue.staff_certificate_description}
                            onChange={this.onChange("staff_certificate_description")}/>
                    </Form.Item>
                    <Form.Item
                        label="File Scan Certificate"
                        {...this.state.formError.staff_certificate_filename} >
                        <img src={this.state.formValue.staff_certificate_filename_url} className={"image-preview"}/>
                        <br/>
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File Scan Sertifikat</button>
                            <input type="file" name="myfile" onChange={this.onChangeFileImage}/>
                        </div>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };


    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeFileImage = (e) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_certificate_filename_url: URL.createObjectURL(e.target.files[0]),
                staff_certificate_filename: e.target.files[0]
            }
        });
    };

    insert = () => e => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != null) {
                formData.append(key, this.state.formValue[key]);
            }
        });
        PostData(`/staff-sertifikasi/${this.state.formValue.staff_id}/${this.state.formValue.staff_certificate_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalCreateVisible: false,
                    formError: [],
                });
                this.props.list()
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

export default Create;


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};