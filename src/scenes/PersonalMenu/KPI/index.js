import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Icon,
    Row,
    Col,
    Tabs, Form, Input, Modal,
} from "antd";

import GENERALDATA from '../../../constants/generalData'
import UserKPIStatistic from "./UserKPIStatistic";
import UserKPIApprovalList from "./UserKPIApprovalList";
import UserKPIHistory from "./UserKPIHistory";
import UserKPIPeriode from "./UserKPIPeriode";
import KPIManageOverview from "../../StaffManagement/StaffKPI/2_ConfigKPIManageOverview";
import {menuActionViewAccess} from "../../../services/app/General";

const {TabPane} = Tabs;


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["2"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            breadcrumbComponent: null,
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }


    render() {
        let loginData = JSON.parse(localStorage.getItem('loginData'));
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 5},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 19},
            },
        };

        let structure = {
                structure_id: loginData.structure_id
        };

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey={"1"}>
                                <TabPane tab={<span><Icon type={"check"}/> Indikator KPI Anda</span>} key="1">
                                    <Form {...formItemLayout} className={"form-info"}>
                                        <Form.Item
                                            label="Nama Departemen"
                                        >
                                            {loginData.department_title}
                                        </Form.Item>
                                        <Form.Item
                                            label="Nama Posisi"
                                        >
                                            {loginData.structure_name}
                                        </Form.Item>
                                    </Form>
                                    <br />
                                    <KPIManageOverview structure={structure}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type={"check"}/> Persetujuan KPI</span>} key="6">
                                    <UserKPIApprovalList/>
                                </TabPane>
                                <TabPane tab={<span><Icon type={"check-circle"}/> KPI Feedback</span>} key="2">

                                </TabPane>
                                <TabPane tab={<span><Icon type={"area-chart"}/> Statistik</span>} key="3">
                                    <UserKPIStatistic/>
                                </TabPane>
                                <TabPane tab={<span><Icon type={"history"}/> History</span>} key="4">
                                    <UserKPIHistory/>
                                </TabPane>
                                <TabPane tab={<span><Icon type={"fullscreen"}/> Periode</span>} key="5">
                                    <UserKPIPeriode />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}