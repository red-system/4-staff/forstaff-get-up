import React, {Component} from 'react';
import {Button, Table} from "antd";
import {GetData} from "../../../../services/api";
import UserKPIApprovalDetail from "../../../StaffManagement/StaffKPI/1_1_DataKPIDetail";
import UserKPIApprovalApprove from "../UserKPIApprovalApprove";
import UserKPIApprovalRevise from "../UserKPIApprovalRevise";
import {menuActionViewAccess} from "../../../../services/app/General";


const data = [];
for (let i = 1; i <= 3; i++) {
    data.push({
        key: i,
        periode: 2019,
        posisi: `Posisi ${i}`,
        hasil: `${i}%`,
        kategori: `Kategori ${i}`
    });
}

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    render() {
        let menuList = menuActionViewAccess('personal_menu', 'kpi', 'list_approve_kpi');
        let menuPreview = menuActionViewAccess('personal_menu', 'kpi', 'preview_approve_kpi');
        let menuApprove = menuActionViewAccess('personal_menu', 'kpi', 'approve_approve_kpi');
        let menuRevision = menuActionViewAccess('personal_menu', 'kpi', 'revision_approve_kpi');

        const columns = [
            {
                title: 'Tahun',
                dataIndex: 'kpis_year',
                key: 'kpis_year',
            },
            {
                title: 'Bulan',
                dataIndex: 'kpis_month_label',
                key: 'kpis_month_label',
            },
            {
                title: 'Hasil',
                dataIndex: 'kpi_result_value',
                key: 'kpi_result_value',
            },
            {
                title: 'Kategori',
                dataIndex: 'kpi_rc_name',
                key: 'kpi_rc_name'
            },
            {
                title: 'Menu',
                width: 260,
                render: (text, record) => {
                    return (
                        <Button.Group size={"small"}>
                            <span className={menuPreview.class}>
                                <UserKPIApprovalDetail record={record} listRefresh={true}/>
                            </span>
                            <span className={menuApprove.class}>
                                <UserKPIApprovalApprove record={record} list={this.list}/>
                            </span>
                            <span className={menuRevision.class}>
                                <UserKPIApprovalRevise record={record} list={this.list}/>
                            </span>
                        </Button.Group>
                    )
                }
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });
        return <Table
            className={menuList.class}
            columns={columns}
            bordered={true}
            dataSource={dataList}
            size='small'/>
    }

    list = () => {
        GetData('/kpi-approval-list').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}