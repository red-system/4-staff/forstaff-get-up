import {Button, Icon, Popconfirm, Popover, Input, message} from "antd";
import React, {Component} from "react";
import {PostData} from "../../../../services/api";

class Delete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formValue: {
                kpi_staff_id: props.record.kpi_staff_id
            }
        }
    }

    render() {

        return (
            <Popconfirm
                placement="rightTop"
                title={"Menyetujui KPI ini ?"}
                onConfirm={this.submit()}
                okText="Yes"
                cancelText="No"
            >
                    <Button type="success">
                        <Icon type="check"/>
                        Setuju
                    </Button>
            </Popconfirm>
        )
    }

    submit = () => e => {
        PostData(`/kpi-approval-accept`, this.state.formValue).then((result)=>{
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    reasonVisible:false,
                });
                this.props.list();
            } else {
                let errors = data.errors;
                let formError = [];
                message.warning(data.message);

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }


}

export default Delete;