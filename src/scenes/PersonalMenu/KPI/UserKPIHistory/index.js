import React, {Component} from 'react';
import {Table} from "antd";
import {GetData} from "../../../../services/api";
import KPIDetail from "../../../StaffManagement/StaffKPI/1_1_DataKPIDetail";
import {menuActionViewAccess} from "../../../../services/app/General";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    render() {

        let menuList = menuActionViewAccess('personal_menu', 'kpi', 'history_kpi_list');
        let menuPreview = menuActionViewAccess('personal_menu', 'kpi', 'preview_kpi_list');

        const columns = [
            {
                title: 'Tahun',
                dataIndex: 'kpis_year',
                key: 'kpis_year',
            },
            {
                title: 'Bulan',
                dataIndex: 'kpis_month_label',
                key: 'kpis_month_label',
            },
            {
                title: 'Hasil',
                dataIndex: 'kpi_result_value',
                key: 'kpi_result_value',
            },
            {
                title: 'Kategori',
                dataIndex: 'kpi_rc_name',
                key: 'kpi_rc_name'
            },
            {
                title: 'Menu',
                width: 100,
                render: (text, record) => {
                    return (
                        <span className={menuPreview.class}>
                            <KPIDetail record={record} listRefresh={true}/>
                        </span>
                    )
                }
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['announcement_id'],
                no: key + 1,
                ...item
            }
        });
        return <Table
            className={menuList.class}
            columns={columns}
            dataSource={dataList}
            size='small'/>
    }

    list() {
        GetData('/kpi-history').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}