import {Button, Icon, Popconfirm, Popover, Input, message} from "antd";
import React, {Component} from "react";
import {PostData} from "../../../../services/api";

class Delete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reasonVisible: false,
            formValue: {
                kpis_revise_reason: '',
                kpi_staff_id: props.record.kpi_staff_id
            }
        }
    }

    render() {
        const content = (
            <div className={"text-center"}>
                <Input.TextArea defaultValue={this.state.formValue.reason} onChange={this.onChange("kpis_revise_reason")} />
                <br/><br/>
                <Button type={"success"} icon={"check"} onClick={this.submit()}>Kirim</Button>
            </div>
        );

        return (
            <Popconfirm
                placement="rightTop"
                title={"Yakin mengajukan revisi ?"}
                onConfirm={this.confirm}
                okText="Yes"
                cancelText="No"
            >
                <Popover content={content} title="Alasan mengajukan Revisi ?" visible={this.state.reasonVisible}>
                    <Button type="warning">
                        <Icon type="close"/>
                        Revisi
                    </Button>
                </Popover>
            </Popconfirm>
        )
    }

    confirm = () => {
        this.setState({
            reasonVisible: true
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    submit = () => e => {
        PostData(`/kpi-approval-revise`, this.state.formValue).then((result)=>{
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    reasonVisible:false,
                });
                this.props.list();
            } else {
                let errors = data.errors;
                let formError = [];
                message.warning(data.message);

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }


}

export default Delete;