import React, {Component} from 'react';
import {Button, Form, Icon, Radio, Input, Modal, Tag} from "antd";
import {PostData} from "../../../../services/api";
import {status_view} from "../../../../services/app/General";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            detail: null
        };
    }

    componentWillMount() {
        this.detail();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.detail();
        }
    }

    render() {

        const {detail} = this.state;
        let staff_name = detail == null ? '-' : detail.staff.staff_name;
        let kpis_year = detail == null ? '-' : detail.kpis_year;
        let kpis_month = detail == null ? '-' : detail.kpis_month_label;
        let category_name = detail == null ? '-' : detail.category_name;
        let structure_name = detail == null ? '-' : detail.staff.position.structure_name;
        let kpis_status = detail == null ? '-' : detail.kpis_status;
        let kpis_status_label = detail == null ? '-' : this.status_label(kpis_status, detail.kpis_status_label);
        const data_indicator = detail == null ? [] : detail.data_indicator;

        return (
            <span>
                <Button onClick={this.modalStatus(true)}>
                    <Icon type="eye"/> Preview
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="eye"/> Preview KPI Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width="80%"
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout} className={"form-detail"}>
                        <Form.Item
                            label="Staf">
                            {staff_name}
                        </Form.Item>
                        <Form.Item
                            label="Posisi">
                            {structure_name}
                        </Form.Item>
                        <Form.Item
                            label="Tahun">
                            {kpis_year}
                        </Form.Item>
                        <Form.Item
                            label="Bulan">
                            {kpis_month}
                        </Form.Item>
                        <Form.Item
                            label="Kategori">
                            {category_name}
                        </Form.Item>
                        <Form.Item
                            label="Status">
                            {kpis_status_label}
                        </Form.Item>
                        <Form.Item
                            label="Persentase Indikator">
                            <br/>
                            <table className="table-bordered">
                                <thead className={"ant-table-thead"}>
                                <tr>
                                    <th>Item</th>
                                    <th className="text-center">Weight</th>
                                    <th>Indicator</th>
                                    <th className="text-center">Weight</th>
                                    <th className="text-center">Result</th>
                                </tr>
                                </thead>
                                {
                                    data_indicator.map((item, key) => {

                                        const sub_indicator_first = item['sub_indicator_first'];
                                        let indicator_name = '-';
                                        let indicator_weight = 0;
                                        let kpisr_percent = '0';
                                        if (typeof sub_indicator_first[0] !== 'undefined') {
                                            indicator_name = sub_indicator_first[0]['indicator_name'];
                                            indicator_weight = sub_indicator_first[0]['indicator_weight'];
                                            kpisr_percent = sub_indicator_first[0]['kpisr_percent'];
                                        }


                                        return (
                                            <tbody key={key}>
                                            <tr key={key}>
                                                <td rowSpan={item['sub_indicator_count']}>{item['indicator_name']}</td>
                                                <td
                                                    rowSpan={item['sub_indicator_count']}
                                                    className="text-center">{item['indicator_weight']}%
                                                </td>
                                                <td>{indicator_name}</td>
                                                <td
                                                    className="text-center">{indicator_weight}%
                                                </td>
                                                <td className="text-center">{kpisr_percent}%</td>
                                            </tr>
                                            {
                                                item['sub_indicator_others'].map((item2, key2) => {
                                                    return (
                                                        <tr key={key2}>
                                                            <td>{item2['indicator_name']}</td>
                                                            <td
                                                                className="text-center">{item2['indicator_weight']}%
                                                            </td>
                                                            <td className="text-center">{item2['kpisr_percent']}%</td>
                                                        </tr>
                                                    );
                                                })
                                            }
                                            </tbody>
                                        )
                                    })
                                }
                            </table>
                        </Form.Item>
                    </Form>

                </Modal>
            </span>
        )
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };

    async detail() {
        let data = {
            kpi_staff_id: this.props.record.kpi_staff_id
        };
        await PostData('/kpi-data/staff/process/row', data)
            .then((result) => {
                this.setState({
                    detail: result.data.data
                })
            })
    }

    status_label(kpis_status, kpis_status_label) {
        return status_view(kpis_status, kpis_status_label);
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 4},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 20},
    },
};