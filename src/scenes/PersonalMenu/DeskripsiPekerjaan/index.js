import React, { Component } from "react";
import Breadcrumb from "../../../components/Breadcrumb";
import { Row, Col } from "antd";

import GENERALDATA from "../../../constants/generalData";
import { PostData } from "../../../services/api";
import { menuActionViewAccess, renderFileType } from "../../../services/app/General";

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["5"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem("loginData");
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            job_desc: {},
        };
    }

    componentWillMount() {
        this.data();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    render() {
        let menuList = menuActionViewAccess("personal_menu", "job_description", "list");

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row className={menuList.class}>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    {isNaN(this.state.job_desc) ? (
                                        renderFileType(this.state.job_desc.staff_filename_type, this.state.job_desc.staff_filename_url)
                                    ) : (
                                        <div className="center-text">Belum ada data yang diinputkan.</div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    data() {
        PostData("/staff-job-description", { staff_id: this.state.staff_id }).then((result) => {
            this.setState({
                job_desc: result.data.data,
            });
        });
    }
}
