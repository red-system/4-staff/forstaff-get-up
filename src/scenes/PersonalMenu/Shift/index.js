import React, { Component } from "react";
import { Col, Icon, Row, Tabs } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import ShiftCalendar from "./ShiftCalendar";
import { menuActionViewAccess } from "../../../services/app/General";

export default class RewardTabs extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    render() {
        let menuList = menuActionViewAccess("personal_menu", "shift", "preview_shift");

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuList.class}>
                                        <ShiftCalendar />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };
}

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["13"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;
const { TabPane } = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];
