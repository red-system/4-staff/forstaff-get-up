import React, {Component} from 'react';
import {Icon, Modal, Tabs} from "antd";
import { PostData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: props.modalVisible,
            dateSelect: props.dateSelect,
            dateView: props.dateView,
            isLoading: false,
            data: [],
            modalStatus: props.modalStatus
        };

    }

    componentWillReceiveProps(nextProps, nextContext) {

        console.log('receive', nextProps);

        this.shiftStaffDetail(nextProps.dateSelect);
        this.setState({
            modalVisible: nextProps.modalVisible,
            dateSelect: nextProps.dateSelect,
            dateView: nextProps.dateView,
            modalStatus: nextProps.modalStatus
        })
    }

    render() {

        let {data} = this.state;

        return (

            <Modal
                title={
                    <span>
                            <Icon type="calendar"/> {this.state.dateView} | Daftar Shift
                        </span>
                }
                width={700}
                visible={this.state.modalVisible}
                onCancel={this.state.modalStatus(false)}
                onOk={this.state.modalStatus(false)}
            >
                <Tabs type="card">
                    {
                        Object.keys(data).map((item, key) => {
                            let staff_list = data[item].staff_list;
                            return (
                                <Tabs.TabPane tab={<span><Icon type="down-circle"/> {data[item].shiftm_name}</span>} key={key}>
                                    <ol>
                                        {
                                            Object.keys(staff_list).map((item2, key2) => {
                                                return <li>{staff_list[item2].staff_name}</li>
                                            })
                                        }
                                    </ol>
                                </Tabs.TabPane>
                            )
                        })
                    }
                </Tabs>

            </Modal>
        )
    }

    shiftStaffDetail = (dateSelect) => {
        let data = {
            shift_date: dateSelect
        };
        PostData('/shift-staff-detail', data).then((result) => {
            this.setState({
                data: result.data.data
            })
        });
    };

}


export default Create;