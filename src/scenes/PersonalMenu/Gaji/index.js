import React, { Component } from "react";
import Breadcrumb from "../../../components/Breadcrumb";
import { Icon, Row, Col, Button, Table, Input } from "antd";

import GENERALDATA from "../../../constants/generalData";
import { GenerateLink, GetData, PostData } from "../../../services/api";
import "cropperjs/dist/cropper.css";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["3"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem("loginData");
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            staff_id: staff.staff_id,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataSearched: nextProps.dataSearched,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var salaryMonth = false;
            if (get(record, "salary_month_view") != null) {
                salaryMonth = get(record, "salary_month_view").toString().match(reg);
            }
            var salaryYear = false;
            if (get(record, "salary_year") != null) {
                salaryYear = get(record, "salary_year").toString().match(reg);
            }
            var salaryBallance = false;
            if (get(record, "salary_ballance_view") != null) {
                salaryBallance = get(record, "salary_ballance_view").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!salaryMonth && !salaryYear && !salaryBallance && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("personal_menu", "salary", "list");
        let menuDownload = menuActionViewAccess("personal_menu", "salary", "download");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "Bulan",
                dataIndex: "salary_month_view",
                key: "salary_month_view",
            },
            {
                title: "Tahun",
                dataIndex: "salary_year",
                key: "salary_year",
            },
            {
                title: "Nominal",
                dataIndex: "salary_ballance_view",
                key: "salary_ballance_view",
            },
            {
                title: "Tanggal diterbitkan",
                dataIndex: "updated_at",
                key: "updated_at",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (test, record) => (
                    <a className={menuDownload.class} href={GenerateLink("/salary-pdf-download/" + record.salary_id)} target={"_blank"}>
                        <Button type="primary">
                            <Icon type="download" />
                            Download
                        </Button>
                    </a>
                ),
            },
        ];

        let dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["sallary_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <div className="row mb-1">
                                        <div className="col-9"></div>
                                        <div className="col-3 pull-right">
                                            <Search
                                                size="default"
                                                ref={(ele) => (this.searchText = ele)}
                                                suffix={suffix}
                                                onChange={this.onSearch}
                                                placeholder="Search Records"
                                                value={this.state.searchText}
                                                onPressEnter={this.onSearch}
                                            />
                                        </div>
                                    </div>
                                    <Table className={menuList.class} columns={columns} dataSource={dataList} size="small" />
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list() {
        GetData("/salary-staff").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }

    openUrl = (url) => (e) => {
        window.open(url, "_blank");
    };
}
