import React, { Component } from "react";
import { Button, Form, Icon, Modal, TimePicker, DatePicker, Input, InputNumber, message } from "antd";
import moment from "moment";
import { PostData } from "../../../../services/api";
import { dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay } from "../../../../services/app/General";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            isLoading: false,

            formError: [],
            formValue: {
                learningd_reason: "",
                learningd_date: dateToDay(),
                learningd_time: "00:00:00",
                learningd_nominal: 0,
                learningd_filename: "",
            },
        };
    }

    render() {
        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)} icon={"plus"}>
                    Pengajuan Pelatihan
                </Button>
                <br />
                <br />

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> Pengajuan Pelatihan
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Tanggal Pelatihan" {...this.state.formError.learningd_date}>
                            <DatePicker
                                style={{ width: "100%" }}
                                onChange={this.onChangeDate("learningd_date")}
                                value={moment(dateFormatterApp(this.state.formValue.learningd_date), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item label="Jam Pelatihan" {...this.state.formError.learningd_time}>
                            <TimePicker
                                style={{ width: "100%" }}
                                defaultValue={moment(this.state.formValue.learningd_time, "HH:mm:ss")}
                                onChange={this.onChangeTime("learningd_time")}
                            />
                        </Form.Item>
                        <Form.Item label="Judul Pelatihan" {...this.state.formError.learningd_reason}>
                            <Input onChange={this.onChange("learningd_reason")} defaultValue={this.state.formValue.learningd_reason} />
                        </Form.Item>
                        <Form.Item label="Nominal Pelatihan" {...this.state.formError.learningd_nominal}>
                            <InputNumber
                                defaultValue={this.state.formValue.learningd_nominal}
                                style={{ width: "100%" }}
                                onChange={this.onChange("learningd_nominal")}
                                formatter={(value) => `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                parser={(value) => value.replace(/\Rp.\s?|(,*)/g, "")}
                            />
                        </Form.Item>
                        <Form.Item label="File Brosur Pelatihan" {...this.state.formError.learningd_filename}>
                            <img src={this.state.formValue.learningd_filename_url} className={"image-preview"} />
                            <br />
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange} />
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        );
    }

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    onChangeTime = (name) => (time) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: time.format("HH:mm:ss"),
            },
        });
    };

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                learningd_filename_url: URL.createObjectURL(event.target.files[0]),
                learningd_filename: event.target.files[0],
            },
        });
    };

    onChangeDate = (name) => (value) => {
        value = value != null ? value.format(dateFormatApi()) : "";

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        const formData = new FormData();
        formData.append("learningd_reason", this.state.formValue.learningd_reason);
        formData.append("learningd_nominal", this.state.formValue.learningd_nominal);
        formData.append("learningd_date", this.state.formValue.learningd_date + " " + this.state.formValue.learningd_time);
        formData.append("learningd_filename", this.state.formValue.learningd_filename);

        PostData("/learning-development", formData).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formValue: [],
                    formError: [],
                });
                this.props.listRefreshRun(true);
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const date_format = "YYYY-MM-DD";
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};

export default Create;
