import React, { Component } from "react";
import { Button, Col, Row, Table, Input, Icon } from "antd";

import Create from "./Create";
import Edit from "./Edit";
import Delete from "./Delete";
import ViewFile from "./ViewFile";
import Publish from "./Publish";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { GetData } from "../../../services/api";
import { menuActionViewAccess, status_view } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            data: [],
            listRefresh: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
        this.list();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var learningDate = false;
            if (get(record, "learningd_date_format") != null) {
                learningDate = get(record, "learningd_date_format").toString().match(reg);
            }
            var learningReason = false;
            if (get(record, "learningd_reason") != null) {
                learningReason = get(record, "learningd_reason").toString().match(reg);
            }
            var learningNominal = false;
            if (get(record, "learningd_nominal_view") != null) {
                learningNominal = get(record, "learningd_nominal_view").toString().match(reg);
            }
            var learningStatus = false;
            if (get(record, "learningd_status") != null) {
                learningStatus = get(record, "learningd_status").toString().match(reg);
            }
            var learningPublish = false;
            if (get(record, "learningd_publish") != null) {
                learningPublish = get(record, "learningd_publish").toString().match(reg);
            }
            if (!learningDate && !learningReason && !learningNominal && !learningStatus && !learningPublish) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("personal_menu", "learning_development", "list");
        let menuCreate = menuActionViewAccess("personal_menu", "learning_development", "create");
        let menuEdit = menuActionViewAccess("personal_menu", "learning_development", "edit");
        let menuDelete = menuActionViewAccess("personal_menu", "learning_development", "delete");
        let menuPreview = menuActionViewAccess("personal_menu", "learning_development", "preview");
        let menuPublish = menuActionViewAccess("personal_menu", "learning_development", "publish");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Tanggal Pelatihan",
                dataIndex: "learningd_date_format",
                key: "learningd_date_format",
            },
            {
                title: "Pelatihan",
                dataIndex: "learningd_reason",
                key: "learningd_reason",
            },
            {
                title: "Harga Pelatihan",
                dataIndex: "learningd_nominal_view",
                key: "learningd_nominal_view",
            },
            {
                title: "Status",
                dataIndex: "learningd_status",
                key: "learningd_status",
            },
            {
                title: "Publish",
                dataIndex: "learningd_publish",
                key: "learningd_publish",
                render: (learningd_publish) => {
                    return status_view(learningd_publish);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 340,
                render: (text, record) => {
                    let button_action = "";
                    if (record.learningd_publish === "no") {
                        button_action = [
                            <span className={menuPublish.class}>
                                <Publish record={record} listRefreshRun={this.listRefreshRun} />
                            </span>,
                            <span className={menuEdit.class}>
                                <Edit record={record} listRefreshRun={this.listRefreshRun} />
                            </span>,
                            <span className={menuDelete.class}>
                                <Delete record={record} listRefreshRun={this.listRefreshRun} />
                            </span>,
                        ];
                    }

                    return (
                        <Button.Group size={"small"}>
                            <span className={menuPreview.class}>
                                <ViewFile record={record} />
                            </span>
                            {button_action}
                        </Button.Group>
                    );
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <span className={menuCreate.class}>
                                    <Create listRefreshRun={this.listRefreshRun} key="creatLearningDevelopment" />
                                </span>
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    bordered={true}
                                    dataSource={data}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };

    list() {
        GetData("/learning-development").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
                listRefresh: false,
            });
        });
    }
}

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["16"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default RewardGuide;
