import React, { Component } from "react";
import { Col, Row, Table, Input, Icon } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { GetData } from "../../../services/api";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["17"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class AppComponent extends Component {
    constructor() {
        super();
        let loginData = localStorage.getItem("loginData");
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        GetData(`/benefit-staff-personal/${this.state.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
            });
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var BenefitTitle = false;
            if (get(record, "benefit_title") != null) {
                BenefitTitle = get(record, "benefit_title").toString().match(reg);
            }
            var benefitDescription = false;
            if (get(record, "benefit_description") != null) {
                benefitDescription = get(record, "benefit_description").toString().match(reg);
            }
            if (!BenefitTitle && !benefitDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("personal_menu", "benefit_staff", "list");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Judul Benefit",
                dataIndex: "benefit_title",
                key: "benefit_title",
            },
            {
                title: "Keterangan",
                dataIndex: "benefit_description",
                key: "benefit_description",
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table className={menuList.class} columns={columns} bordered={true} dataSource={data} size="small" />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
