import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, message, Modal, Row, Upload} from "antd";
import {PostData} from "../../../../services/api";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            formValue: props.record,
            formError: []
        };
    }

    componentWillMount() {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_document_filename_url: undefined,
            }
        })
    }

    modalEditShow = () => {
        this.setState({
            modalVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalVisible: false,
        });
    };

    render() {

        const {modalVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };

        return [
            <Button type="success" onClick={this.modalEditShow}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={modalVisible}
                width={700}
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={[

                    <Row style={{textAlign: 'left'}}>
                        <Col {...formLayout}>
                            <Button key="submit" type="primary" onClick={this.insert}>
                                Simpan
                            </Button>
                            <Button key="back" onClick={this.modalEditCancel}>
                                Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul Dokumen"
                        {...this.state.formError.staff_document_title}

                    >
                        <Input defaultValue={this.state.formValue.staff_document_title}
                               onChange={this.onChange("staff_document_title")}/>
                    </Form.Item>
                    <Form.Item
                        label="Jenis Dokumen"
                        {...this.state.formError.staff_document_type}
                    >
                        <Input defaultValue={this.state.formValue.staff_document_type}
                               onChange={this.onChange("staff_document_type")}/>
                    </Form.Item>
                    <Form.Item
                        label="File Dokumen"
                        {...this.state.formError.staff_document_filename}
                    >
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File</button>
                            <input type="file" name="myfile" onChange={this.handleChange}/>
                        </div>
                    </Form.Item>
                </Form>
            </Modal>
        ]
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_document_filename: event.target.files[0]
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };
    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != undefined) {
                formData.append(key, this.state.formValue[key])
            }
        });

        PostData(`/staff-document/${this.props.record.staff_id}/${this.props.record.staff_document_id}`, formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.list();
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

export default Create;