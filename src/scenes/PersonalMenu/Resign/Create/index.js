import React, { Component } from 'react';
import {Button, DatePicker, Form, Icon, Input, Modal, InputNumber, Row, Col, message, Popconfirm} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";
const { MonthPicker } = DatePicker;
const { TextArea } = Input;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue:{
                resign_date: dateToDay()
            },
            formError:[],
            process:props.process
        };

    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            process:nextProps.process
        })
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        const {modalCreateVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };
        const text = "Apakah anda yakin mengejukan resign";
        return(
            <div>
                {this.state.process == 0 &&
                    <Button type="warning" onClick={this.modalCreateShow}>
                        <Icon type="close"/> Ajukan Resign

                    </Button>
                }
                <br/><br/>

                <Modal
                    title={
                        <span>
                            Pengajuan Resign
                        </span>
                    }
                    visible={modalCreateVisible}
                    width={700}
                    onOk={this.modalCreateSubmit}
                    onCancel={this.modalCreateCancel}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Popconfirm
                                    placement="top"
                                    title={text}
                                    onConfirm={this.insert()}
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <Button key="submit" type="primary">
                                        Simpan
                                    </Button>
                                </Popconfirm>

                                <Button key="back" onClick={this.modalCreateCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Resign"
                            {...this.state.formError.resign_date}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("resign_date")}
                                value={moment(dateFormatterApp(this.state.formValue.resign_date), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Alasan Resign"
                            {...this.state.formError.resign_reason}

                        >
                            <TextArea
                                value={this.state.formValue.resign_reason}
                                onChange={this.onChange("resign_reason")}
                            />
                        </Form.Item>

                        <Form.Item
                            label="File Surat Resign"
                            {...this.state.formError.resign_filename} >
                            <img src={this.state.formValue.resign_filename_url} className={"image-preview"}/>
                            <br/>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.onChangeFileImage}/>
                            </div>
                        </Form.Item>

                    </Form>

                </Modal>
            </div>
        )
    }
    onChange = name => value => {
        if(typeof value == 'object'){
            value = value.target.value
        }
        this.setState({
            formValue:{
                ...this.state.formValue,
                [name]: value
            }
        })
    };

    onChangeFileImage = (e) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                resign_filename_url: URL.createObjectURL(e.target.files[0]),
                resign_filename: e.target.files[0]
            }
        });
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    insert = () => e => {
        const  formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            formData.append(key,this.state.formValue[key])
        });
        PostData(`/staff-resign/${this.props.staff_id}`,formData).then((result)=>{
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalCreateVisible:false,
                    formValue:{
                        resign_date: moment().format("YYYY-MM-DD"),
                    },
                });
                this.props.list()
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

export default Create;