import React, {Component} from "react";
import {Row, Col, Timeline, Icon} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';

import '../../styles/ui/_card-icon.scss';

let ReleaseNote = GENERALDATA.primaryMenu.releaseNote;
let GUTTER = GENERALDATA.gutter;

const breadcrumb = [
    {
        label: ReleaseNote.label,
        route: `${ReleaseNote.route}`
    },
]

class GeneralData extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {


        return (
            <div className="content-gutter-vertical">
                {this.state.breadcrumbComponent}
                <Row gutter={GUTTER}>
                    <Col xs={24} sm={24} md={12} lg={12} x={12}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Timeline>
                                    <Timeline.Item dot={<Icon type="check-circle" theme="filled"
                                                              style={{fontSize: '24px'}}/>} color="green">
                                                <span style={{
                                                    fontSize: '20px',
                                                    paddingLeft: '14px',
                                                    paddingTop: '-2px'
                                                }}>IMPROVEMENTS</span>
                                    </Timeline.Item>
                                    <Timeline.Item color="green">
                                        <strong>[IMP-005]</strong>
                                        <p>Perbaikan alur pendaftaran staff</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="green">
                                        <strong>[IMP-004]</strong>
                                        <p>Perbaikan alur pendaftaran hrd</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="green">
                                        <strong>[IMP-003]</strong>
                                        <p>Perbaikan tampilan profil</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="green">
                                        <strong>[IMP-002]</strong>
                                        <p>Tambahan data dashboard</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="green">
                                        <strong>[IMP-001]</strong>
                                        <p>Tambahan grafik absen di dashboard</p>
                                    </Timeline.Item>
                                </Timeline>
                            </div>
                        </div>
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={12} x={12}>

                        <div className="app-content">
                            <div className="app-content-body">
                                <Timeline>
                                    <Timeline.Item
                                        dot={<Icon type="bug" theme="filled" style={{fontSize: '24px'}}/>}
                                        color="red">
                                                <span style={{
                                                    fontSize: '20px',
                                                    paddingLeft: '14px',
                                                    paddingTop: '-2px'
                                                }}>BUGS</span>
                                    </Timeline.Item>
                                    <Timeline.Item color="red">
                                        <strong>[BGS-003]</strong>
                                        <p>Tidak bisa update profil</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="red">
                                        <strong>[BGS-002]</strong>
                                        <p>Tidak bisa tambah bank</p>
                                    </Timeline.Item>
                                    <Timeline.Item color="red">
                                        <strong>[BGS-001]</strong>
                                        <p>Tampilan profil tidak rapi</p>
                                    </Timeline.Item>
                                </Timeline>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )

    }

}

export default GeneralData;