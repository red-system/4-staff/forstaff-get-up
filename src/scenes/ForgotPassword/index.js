import React from 'react';
import QueueAnim from 'rc-queue-anim';
import {Form, Icon, Input, Button} from 'antd';
import {Redirect} from "react-router-dom";
import forgotPasswordBackgroundForStaff from '../../images/forgot-password-background-forStaff.jpg';
import appLogo from '../../images/app-logo.png';


// 3rd
import '../../styles/antd.less';
import '../../styles/bootstrap/bootstrap.scss'
// custom
import "../../styles/layout.scss"
import "../../styles/theme.scss"
import "../../styles/ui.scss"

const FormItem = Form.Item;

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            redirectLogin: false
        }
    }

    redirectLogin = (e) => {
        e.preventDefault();
        this.setState({
            redirectLogin: true
        })
    }

    render() {

        let { redirectLogin } = this.state;

        if(redirectLogin) {
            return <Redirect to={``} />
        }

        return (

            <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <section className="form-card-page form-card row no-gutters" style={{height: '100vh'}}>
                        <div className="form-card__img form-card__img--left col-lg-6"
                             style={{backgroundImage: `url('${forgotPasswordBackgroundForStaff}')`}}></div>
                        <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center">
                            <section className="form-v1-container">
                                <div className={"text-center"}>
                                    <img src={appLogo} style={{width: 'auto'}} />
                                    <br /><br />
                                </div>
                                <h2>Reset Password</h2>
                                <p className="additional-info col-lg-10 mx-lg-auto mb-3">Enter the email address you
                                    used when you joined and we’ll send you instructions to reset your password.</p>
                                <Form onSubmit={this.redirectLogin} className="form-v1">
                                    <FormItem className="mb-3">
                                        <Input size="large" prefix={<Icon type="mail" style={{fontSize: 13}}/>}
                                               placeholder="Email"/>
                                    </FormItem>
                                    <FormItem>
                                        <Button type="primary" htmlType="submit" className="btn-cta btn-block">
                                            Send Reset Instructions
                                        </Button>
                                    </FormItem>
                                </Form>
                            </section>
                        </div>
                    </section>
                </div>
            </QueueAnim>
        );
    }
}

export default Signup;
