import React, {Component} from 'react';
import {
    Button,
    Col,
    Form,
    Icon,
    Input,
    InputNumber,
    Divider,
    Radio,
    Row,
    Select,
    Table,
    DatePicker,
    message
} from "antd";
import {Link, Redirect} from "react-router-dom";
import {GetData, PostData} from "../../../services/api";
import {renderFileType} from "../../../services/app/General";

const FormItem = Form.Item;
const {Option} = Select;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            redirect: false
        }
    }

    componentWillMount() {
        let loginData = localStorage.getItem('loginData');
        let staff_id = JSON.parse(loginData).staff_id;
        let data = {
            staff_id: staff_id
        };
        PostData('/staff-job-description-register', data)
            .then((result) => {
                this.setState({
                    data: result.data.data
                })
            })
    }


    render() {

        if (this.state.redirect) {
            return <Redirect to={"/register/step-4"}/>
        }

        return (
            <div>

                <div className={"btn-register-action"}>
                    <Button.Group size={"large"} style={{padding: "0"}}>
                        <Link to={"/register/step-2"} style={{padding: "0", margin: "0"}}>
                            <Button type="default" size={"large"}>
                                <Icon type="left"/>
                                Kembali
                            </Button>
                        </Link>
                        <Button type="primary" onClick={this.insert}>
                            Saya Mengerti dan Lanjutkan
                            <Icon type="right"/>
                        </Button>
                    </Button.Group>
                </div>
                {renderFileType(this.state.data == null ? '':this.state.data.staff_filename_type, this.state.data == null ? '': this.state.data.staff_filename_url)}
            </div>
        );
    }

    insert = (e) => {

        let loginData = localStorage.getItem('loginData');
        let staff_id = JSON.parse(loginData).staff_id;

        let data = {
            'staff_id': staff_id,
            'staff_register_step': 4
        };

        PostData('/staff-step', data)
            .then((result) => {
                this.setState({
                    redirect: true
                });
                this.props.changeStep(4);
            })
    }


}
