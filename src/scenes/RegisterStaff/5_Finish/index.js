import React, {Component} from 'react';
import {
    Button,
    Col,
    Form,
    Icon,
    Input,
    InputNumber,
    Divider,
    Radio,
    Row,
    Select,
    Table,
    DatePicker,
    message
} from "antd";
import {Link, Redirect} from "react-router-dom";
import {GetData, PostData} from "../../../services/api";

const FormItem = Form.Item;
const {Option} = Select;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            redirect: false,
            loginData: []
        }
    }

    componentWillMount() {
        let loginData = localStorage.getItem('loginData');
        loginData = JSON.parse(loginData);

        GetData(`/${loginData.staff_id}`)
            .then((result) => {
                this.setState({
                    loginData: loginData,
                    data: result.data.data
                })
            });

    }

    render() {

        const {loginData} = this.state;
        let staff_id_label = localStorage.getItem('staff_id_label');
        let level_list = localStorage.getItem('level_list');
        let level_total = localStorage.getItem('level_total');
        let redirectTo = '';

        if (this.state.redirect) {
            return <Redirect to={"/register/step-4"}/>
        }

        return (
            <div className={"text-center"}>
                <h1 align={"center"} style={{color: "green", fontSize: "60px"}}><Icon type={"check-circle"}/></h1>
                <h1>FINISH</h1>
                <h4 align={"center"}>Selamat Datang - {loginData.staff_name}</h4>
                <h4 align={"center"}>Kode Staff : <strong>{staff_id_label}</strong></h4>
                <br />
                <img src={this.state.data.staff_avatar_url} className={"register-finish-avatar"} />
                <br /><br />
                <Link to={"/logout"}>
                    <Button type={"primary"} icon={"login"} size={"large"}>Silahkan mencoba login kembali</Button>
                </Link>
            </div>
        );
    }



}
