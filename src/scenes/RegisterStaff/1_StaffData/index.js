import React, {Component} from 'react';
import {
    Button,
    Col,
    Form,
    Icon,
    Input,
    Divider,
    Radio,
    Row,
    Select,
    DatePicker,
    message
} from "antd";
import {Link} from "react-router-dom";
import {GetData, PostData} from "../../../services/api";
import {Redirect} from 'react-router-dom';
import moment from 'moment';
import {
    dateFormatterApi,
    dateFormatterApp,
    dateFormatApp,
    dateToDay,
    dateToDay2,
    staff_avatar_rules_upload_image, staff_avatar_config_upload_image
} from '../../../services/app/General';

import ImageEditorRc from 'react-cropper-image-editor';
import 'cropperjs/dist/cropper.css';

const FormItem = Form.Item;
const {Option} = Select;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            province: [],
            primaryDistrict: [],
            secondaryDistrict: [],
            department: [],
            position: [],
            match: props.match,
            isLoading: true,
            formError: [],
            formValue: {
                staff_birth_date: dateToDay2()
            },
            roleMaster: [],

            redirect: '',
        };

        this.loadPosition = this.loadPosition.bind(this);
        this.loadDistrict = this.loadDistrict.bind(this);
    }

    componentWillMount() {
        let loginData = localStorage.getItem('loginData');
        loginData = JSON.parse(loginData);
        let staff_id = loginData.staff_id;

        GetData('/province').then((result) => {
            this.setState({
                province: result.data.data,
            });
        });

        GetData(`/staff/${staff_id}`)
            .then((result) => {
                console.log(result.data.data);
                this.setState({
                    formValue: {
                        ...result.data.data,
                        staff_birth_date: dateFormatterApp(result.data.data.staff_birth_date)
                    }
                })
            });

        this.setState({
            isLoading: false
        });
        this.loadDistrict("primaryDistrict", this.state.formValue.staff_primary_province_id);
        this.loadDistrict("secondaryDistrict", this.state.formValue.staff_second_province_id);
        this.insert = this.insert.bind(this);
        this.browseFotoProfile = this.browseFotoProfile.bind(this);
    }


    render() {

        if (this.state.redirect) {
            return <Redirect to="/register/step-2"/>
        }

        return (
            <div>

                <div className={"btn-register-action"}>
                    <Button.Group size={"large"} style={{padding: "0"}}>
                        <Link to={"/logout"} style={{padding: "0", margin: "0"}}>
                            <Button type="default" size={"large"}>
                                <Icon type="left"/>
                                Logout
                            </Button>
                        </Link>
                        <Button type="primary" onClick={this.insert}>
                            Simpan dan Lanjutkan
                            <Icon type="right"/>
                        </Button>
                    </Button.Group>
                </div>


                <Form>
                    <Divider orientation="left">
                        <Icon type={"info-circle"}/> DATA UMUM
                    </Divider>
                    <FormItem
                        {...formItemLayout}
                        label="Nama Lengkap"
                        {...this.state.formError.staff_name}
                        required={true}>

                        <Input
                            onChange={this.onChange("staff_name")}
                            value={this.state.formValue.staff_name}/>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Nama Panggilan"
                        {...this.state.formError.staff_nickname}
                        required={true}>
                        <Input
                            onChange={this.onChange("staff_nickname")}
                            value={this.state.formValue.staff_nickname}/>
                    </FormItem>
                    <br/><br/>

                    <Divider orientation="left">
                        <Icon type={"user"}/> DATA PRIBADI
                    </Divider>
                    <FormItem
                        {...this.state.formError.staff_avatar}
                        {...formItemLayout}
                        label="Foto Profil"
                        required={true}
                    >
                        <ImageEditorRc
                            {...staff_avatar_config_upload_image()}
                            saveImage={this.cropper}
                            src={this.state.formValue.staff_avatar_url} // it has to catch the returned data and do it whatever you want
                            responseType='blob/base64'/>
                        <div className="upload-btn-wrapper-register">
                            <button className="btn-file">Browse Foto</button>
                            <input type="file" name="myfile" onChange={this.browseFotoProfile}/>
                        </div>
                        {staff_avatar_rules_upload_image()}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Kewarganegaraan"
                        {...this.state.formError.staff_nationality}
                        required={true}
                    >
                        <Radio.Group value={this.state.formValue.staff_nationality} buttonStyle="solid"
                                     onChange={this.onChange("staff_nationality")}>
                            <Radio.Button value="wni">Warga Negara Indonesia</Radio.Button>
                            <Radio.Button value="wna">Warga Negara Asing</Radio.Button>
                        </Radio.Group>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Jenis Kelamin"
                        {...this.state.formError.staff_gender}
                        required={true}
                    >
                        <Radio.Group value={this.state.formValue.staff_gender} buttonStyle="solid"
                                     onChange={this.onChange("staff_gender")}>
                            <Radio.Button value="male">Pria</Radio.Button>
                            <Radio.Button value="female">Wanita</Radio.Button>
                        </Radio.Group>
                    </FormItem>

                    <Form.Item
                        {...formItemLayout}
                        label="Tempat Lahir"
                        {...this.state.formError.staff_birth_place}
                        required={true}>
                        <Row>
                            <Col {...divide21}>
                                <Input
                                    onChange={this.onChange("staff_birth_place")}
                                    value={this.state.formValue.staff_birth_place}/>
                            </Col>
                            <Col {...divide22}>
                                <Form.Item label={"Tanggal Lahir "}
                                           style={{paddingLeft: "20px"}}
                                           {...formItemLayout2}
                                           required={true}
                                           {...this.state.formError.staff_birth_date}>
                                    <DatePicker
                                        style={{width: '100%'}}
                                        value={moment(this.state.formValue.staff_birth_date, dateFormatApp())}
                                        format={dateFormatApp()}
                                        onChange={this.onChangeDate("staff_birth_date")}/>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form.Item>
                    <FormItem
                        {...formItemLayout}
                        label="Golongan Darah"
                        {...this.state.formError.staff_blood_type}
                        required={true}
                    >
                        <Radio.Group value={this.state.formValue.staff_blood_type} buttonStyle="solid"
                                     onChange={this.onChange("staff_blood_type")}>
                            <Radio.Button value="A">A</Radio.Button>
                            <Radio.Button value="B">B</Radio.Button>
                            <Radio.Button value="O">O</Radio.Button>
                            <Radio.Button value="AB">AB</Radio.Button>
                        </Radio.Group>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Jenis Identitas"
                        {...this.state.formError.staff_identity_type}
                        required={true}
                    >
                        <Radio.Group value={this.state.formValue.staff_identity_type} buttonStyle="solid"
                                     onChange={this.onChange("staff_identity_type")}>
                            <Radio.Button value="ktp">KTP</Radio.Button>
                            <Radio.Button value="sim">SIM</Radio.Button>
                            <Radio.Button value="paspor">PASPOR</Radio.Button>
                        </Radio.Group>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="No Identitas"
                        {...this.state.formError.staff_identity_number}
                        required={true}
                    >
                        <Input
                            value={this.state.formValue.staff_identity_number}
                            onChange={this.onChange("staff_identity_number")}/>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Agama"
                        {...this.state.formError.staff_religion}
                        required={true}
                    >
                        <Select value={this.state.formValue.staff_religion} onChange={this.onChange("staff_religion")}>
                            <Option value="hindu">Hindu</Option>
                            <Option value="budha">Budha</Option>
                            <Option value="islam">Islam</Option>
                            <Option value="konghuchu">Khonghuchu</Option>
                            <Option value="katolik">Kristen Katolik</Option>
                            <Option value="kristen_protestan">Kristen Protestan</Option>
                        </Select>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Status Perkawinan"
                        {...this.state.formError.staff_marital_status}
                        required={true}
                    >
                        <Radio.Group value={this.state.formValue.staff_marital_status} buttonStyle="solid"
                                     onChange={this.onChange("staff_marital_status")}>
                            <Radio.Button value="yes">Menikah</Radio.Button>
                            <Radio.Button value="no">Belum Menikah</Radio.Button>
                        </Radio.Group>
                    </FormItem>

                    <br/><br/>

                    <Divider orientation="left">
                        <Icon type={"phone"}/> KONTAK STAFF
                    </Divider>
                    <FormItem
                        {...formItemLayout}
                        label="Alamat Tetap"
                        {...this.state.formError.staff_primary_address}
                        required={true}
                    >
                        <Input
                            value={this.state.formValue.staff_primary_address}
                            onChange={this.onChange("staff_primary_address")}/>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Provinsi"
                        {...this.state.formError.staff_primary_province_id}
                        required={true}
                    >
                        <Row>
                            <Col {...divide31}>
                                <Select
                                    showSearch
                                    style={{width: 200}}
                                    placeholder="Pilih Provinsi"
                                    optionFilterProp="children"
                                    value={this.state.formValue.staff_primary_province_id}
                                    onChange={this.onChangePrimaryProvince()}
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {
                                        Object.keys(this.state.province).map((key) => {
                                            return (
                                                <Option key={key}
                                                        value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                            )
                                        })
                                    }
                                </Select>
                            </Col>
                            <Col {...divide32}>
                                <Form.Item
                                    label="Kabupaten/Kota" {...formItemLayout3} {...this.state.formError.staff_primary_district_id}
                                    required={true}>
                                    <Select
                                        showSearch
                                        style={{width: 200}}
                                        placeholder="Pilih Kabupaten"
                                        optionFilterProp="children"
                                        value={this.state.formValue.staff_primary_district_id}
                                        onChange={this.onChange("staff_primary_district_id")}
                                        filterOption={(input, option) =>
                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                        }
                                    >
                                        {
                                            Object.keys(this.state.primaryDistrict).map((key) => {
                                                return (
                                                    <Option key={key}
                                                            value={this.state.primaryDistrict[key].district_id}>{this.state.primaryDistrict[key].name}</Option>
                                                )
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col {...divide33}>
                                <Form.Item
                                    label="Kode Pos" {...formItemLayout3}
                                    {...this.state.formError.staff_primary_post_code}
                                    required={true}>
                                    <Input
                                        value={this.state.formValue.staff_primary_post_code}
                                        onChange={this.onChange("staff_primary_post_code")}/>
                                </Form.Item>
                            </Col>
                        </Row>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Alamat Sekarang"
                        {...this.state.formError.staff_second_address}
                        required={true}
                    >
                        <Input
                            value={this.state.formValue.staff_second_address}
                            onChange={this.onChange("staff_second_address")}/>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Provinsi"
                        {...this.state.formError.staff_second_province_id}
                        required={true}
                    >
                        <Row>
                            <Col {...divide31}>
                                <Select
                                    showSearch
                                    style={{width: 200}}
                                    placeholder="Pilih Provinsi"
                                    optionFilterProp="children"
                                    value={this.state.formValue.staff_second_province_id}
                                    onChange={this.onChangeSecondProvince("staff_second_province_id")}
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {
                                        Object.keys(this.state.province).map((key) => {
                                            return (
                                                <Option key={key}
                                                        value={this.state.province[key].province_id}>{this.state.province[key].name}</Option>
                                            )
                                        })
                                    }
                                </Select>
                            </Col>
                            <Col {...divide32}>
                                <Form.Item
                                    label="Kabupaten/Kota"
                                    {...formItemLayout3}
                                    {...this.state.formError.staff_second_district_id}
                                    required={true}>
                                    <Select
                                        showSearch
                                        style={{width: 200}}
                                        placeholder="Pilih Kabupaten"
                                        optionFilterProp="children"
                                        value={this.state.formValue.staff_second_district_id}
                                        onChange={this.onChange("staff_second_district_id")}
                                        filterOption={(input, option) =>
                                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                        }
                                    >
                                        {
                                            Object.keys(this.state.secondaryDistrict).map((key) => {
                                                return (
                                                    <Option key={key}
                                                            value={this.state.secondaryDistrict[key].district_id}>{this.state.secondaryDistrict[key].name}</Option>
                                                )
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col {...divide33}>
                                <Form.Item
                                    label="Kode Pos"
                                    {...formItemLayout3}
                                    {...this.state.formError.staff_second_post_code}
                                    required={true}>
                                    <Input
                                        value={this.state.formValue.staff_second_post_code}
                                        onChange={this.onChange("staff_second_post_code")}/>
                                </Form.Item>
                            </Col>
                        </Row>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="No HP"
                        {...this.state.formError.staff_phone_number}
                        required={true}
                    >
                        <Row>
                            <Col {...divide31}>
                                <Input
                                    value={this.state.formValue.staff_phone_number}
                                    onChange={this.onChange("staff_phone_number")}/>
                            </Col>
                        </Row>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Kontak Darurat"
                        {...this.state.formError.staff_phone_emergency}
                        required={true}
                    >
                        <Input
                            value={this.state.formValue.staff_phone_emergency}
                            onChange={this.onChange("staff_phone_emergency")}/>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Email"
                        {...this.state.formError.staff_email}
                        required={true}
                    >
                        <Input
                            value={this.state.formValue.staff_email}
                            onChange={this.onChange("staff_email")}/>
                    </FormItem>
                    <Form.Item {...tailFormItemLayout} className={"hide"}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Simpan
                        </Button>
                    </Form.Item>

                </Form>
            </div>
        );
    }

    cropper = (data) => {
        console.log('cropper', data);
        let staff_avatar = this.dataURLtoFile(data, 'staff.jpg');
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_avatar: staff_avatar,
                staff_avatar_url: URL.createObjectURL(staff_avatar),
            }
        })
    };

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    browseFotoProfile(event) {
        if (event.target.files[0]) {

            this.setState({
                formValue: {
                    ...this.state.formValue,
                    staff_avatar_url: URL.createObjectURL(event.target.files[0]),
                    staff_avatar: event.target.files[0]
                }
            })
        }
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
        // console.log(this.state.formValue)
    };
    onChangeDate = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value !== null ? value.format(dateFormatApp()) : ''
            }
        });
        //console.log(this.state.formValue)
    };
    onSelect = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: this.state.formValue[name].concat(value)
            }
        });
        // console.log(this.state.formValue)
    };
    onDeselect = name => value => {
        var array = [...this.state.formValue.role_id_json]; // make a separate copy of the array
        var index = array.indexOf(value)
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    ["role_id_json"]: array
                }
            });
        }
        // console.log(this.state.formValue)
    };
    onChangeDepartment = () => value => {
        this.loadPosition(value)
    };
    onChangePrimaryProvince = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_primary_province_id: value,
                staff_primary_district_id: null
            }
        });
        this.loadDistrict("primaryDistrict", value)

    };
    onChangeSecondProvince = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_second_province_id: value,
                staff_second_district_id: null
            }
        });
        this.loadDistrict("secondaryDistrict", value)
    };

    loadDistrict(state, province_id) {
        const formData = new FormData();
        formData.append("province_id", province_id);
        PostData('/district', formData).then((result) => {
            // console.log(result)
            this.setState({
                [state]: result.data.data
            })
        });
        // console.log(this.state.formValue)
    }

    loadPosition(id) {
        const formData = new FormData();
        if (id != null) {
            formData.append('department_id', id)
        }
        PostData('/structure/department', formData).then((result) => {
            this.setState({
                position: result.data.data[0].structure,
                formValue: {
                    ...this.state.formValue,
                    structure_id: null
                }
            })
        })

    }

    insert = (e) => {

        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != null) {
                let value = this.state.formValue[key];
                if (key == 'staff_birth_date') {
                    value = dateFormatterApi(value);
                }
                formData.append(key, value);
            }
        });
        console.log(this.state.formValue);
        PostData(`/staff/register/${this.state.formValue.staff_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    redirect: true
                });
                this.props.changeStep(2);

                this.setState({
                    isLoading: false,
                    redirect: true
                });

            } else {
                message.warning(data.message);
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                // console.log('form error', formError);

                this.setState({
                    formError: formError,
                    isLoading: false,
                    redirect: false
                });
            }
        });
        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 12,
            offset: 6,
        },
    },
};

const formItemLayout2 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayout3 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};

const divide21 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 8,
    xl: 8
};

const divide22 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 16,
    xl: 16
};


const divide31 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 6,
    xl: 6
};

const divide32 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 9,
    xl: 9
};

const divide33 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 9,
    xl: 9
};