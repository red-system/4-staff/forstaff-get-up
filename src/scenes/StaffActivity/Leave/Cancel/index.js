import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import Menu from "antd/lib/menu";

export default class AppComponent extends Component {

    render() {

        const text = 'Yakin hapus data ini ?';

        function confirm() {
            message.info('Clicked on Yes.');
        }

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={confirm}
                okText="Yes"
                cancelText="No"
            >
                <Menu.Item type="warning">
                    <Icon type="rollback"/>
                     Batal
                </Menu.Item>
            </Popconfirm>
        )
    }
}