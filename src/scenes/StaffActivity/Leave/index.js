import React, { Component } from "react";
import { Button, Col, Row, Table, Icon, message, Menu, Dropdown, Input, Popconfirm } from "antd";

import Create from "./Create";
import Cancel from "./Cancel";
import Detail from "./Detail";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { DeleteData, GetData, PostData } from "../../../services/api";
import moment from "moment";
import Tag from "antd/lib/tag";
import { forEach } from "async";
import Edit from "./Edit";
import DetailLeave from "../../StaffAttendence/Leave/1_1_StaffLeave_Detail";
import { dateFormatterApp, menuActionViewAccess, status_view } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 16,
        offset: 8,
    },
    lg: {
        span: 16,
        offset: 8,
    },
};

let pageNow = GENERALDATA.primaryMenu.key_activity.sub_menu["3"];
let pageParent = GENERALDATA.primaryMenu.key_activity;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class JobDescription extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: [],
            list: this.list,
            attachment: [],
            regulation: [],
            master: [],
            detail: [],
            base: [],
            edit: [],
            replace: [],
            staff: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
        this.list();
        this.loadMaster();
        GetData("/staff").then((result) => {
            this.setState({
                staff: result.data.data,
            });
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var leaveName = false;
            if (get(record, "leavem_name") != null) {
                leaveName = get(record, "leavem_name").toString().match(reg);
            }
            var createdAt = false;
            if (get(record, "created_at") != null) {
                createdAt = get(record, "created_at").toString().match(reg);
            }
            var leavePublishDate = false;
            if (get(record, "leave_staff_publish_date_format") != null) {
                leavePublishDate = get(record, "leave_staff_publish_date_format").toString().match(reg);
            }
            var leaveDate = false;
            if (get(record, "leave_staff_date_format") != null) {
                leaveDate = get(record, "leave_staff_date_format").toString().match(reg);
            }
            var tanggalSelesai = false;
            if (get(record, "tanggal_selesai") != null) {
                tanggalSelesai = get(record, "tanggal_selesai").toString().match(reg);
            }
            var leaveDuration = false;
            if (get(record, "leave_staff_duration") != null) {
                leaveDuration = get(record, "leave_staff_duration").toString().match(reg);
            }
            var leaveStatus = false;
            if (get(record, "leave_staff_status") != null) {
                leaveStatus = get(record, "leave_staff_status").toString().match(reg);
            }
            if (!leaveName && !createdAt && !leavePublishDate && !leaveDate && !tanggalSelesai && !leaveDuration && !leaveStatus) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("key_activity", "leave", "list");
        let menuCreate = menuActionViewAccess("key_activity", "leave", "create");
        let menuEdit = menuActionViewAccess("key_activity", "leave", "edit");
        let menuDelete = menuActionViewAccess("key_activity", "leave", "delete");
        let menuCancel = menuActionViewAccess("key_activity", "leave", "cancel");
        let menuPublish = menuActionViewAccess("key_activity", "leave", "publish");
        let menuDetail = menuActionViewAccess("key_activity", "leave", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const text = "Yakin hapus data ini ?";
        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Jenis Cuti",
                dataIndex: "leavem_name",
                key: "leavem_name",
            },
            {
                title: "Tanggal Pembuatan",
                dataIndex: "created_at",
                key: "created_at",
            },
            {
                title: "Tanggal Publish",
                dataIndex: "leave_staff_publish_date_format",
                key: "leave_staff_publish_date_format",
                render: (text, record) => {
                    return record.leave_staff_publish_date ? record.leave_staff_publish_date_format : "-";
                },
            },
            {
                title: "Tanggal Cuti",
                dataIndex: "leave_staff_date_format",
                key: "leave_staff_date_format",
            },
            {
                title: "Tanggal Selesai",
                dataIndex: "tanggal_selesai",
                key: "tanggal_selesai",
                render: (test, record) => {
                    let date = moment(record.leave_staff_date, "YYYY-MM-DD").add(record.leave_staff_duration, "days").format("YYYY-MM-DD");
                    return dateFormatterApp(date);
                },
            },
            {
                title: "Durasi",
                dataIndex: "leave_staff_duration",
                key: "leave_staff_duration",
            },
            {
                title: "Status",
                dataIndex: "leave_staff_status",
                key: "leave_staff_status",
                render: (status) => {
                    return status_view(status);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 70,
                render: (test, record) => {
                    const menu = (
                        <Menu>
                            {/*<Menu.Item key="1" onClick={this.showDetail(record.key)}>*/}
                            {/*    <Icon type="eye"/>*/}
                            {/*    Detail*/}
                            {/*</Menu.Item>*/}
                            {record.leave_staff_status === "unpublish" && (
                                <Menu.Item key="2" className={menuEdit.class} onClick={this.showEdit(record.key)}>
                                    <Icon type="edit" />
                                    Edit
                                </Menu.Item>
                            )}
                            {record.leave_staff_status === "unpublish" && (
                                <Menu.Item key="3" className={menuDelete.class}>
                                    <Popconfirm placement="rightTop" title={text} onConfirm={this.confirm(record.key)} okText="Yes" cancelText="No">
                                        <Icon type="delete" />
                                        &nbsp;&nbsp;Delete
                                    </Popconfirm>
                                </Menu.Item>
                            )}
                            {record.leave_staff_status === "unpublish" && (
                                <Menu.Item className={menuPublish.class} key="4" onClick={this.publish(record.key)}>
                                    <Icon type="check" />
                                    Publish
                                </Menu.Item>
                            )}
                            {record.leave_staff_status === "publish" && (
                                <Menu.Item className={menuCancel.class} key="5" onClick={this.cancel(record.key)}>
                                    <Icon type="rollback" />
                                    Cancel
                                </Menu.Item>
                            )}
                        </Menu>
                    );
                    return [
                        <Dropdown overlay={menu} trigger={["click"]}>
                            <Button type={"success"} size={"small"}>
                                Menu <Icon type="down" />
                            </Button>
                        </Dropdown>,
                        <span className={menuDetail.class}>
                            <DetailLeave record={record} size={"small"} />
                        </span>,
                        <span className={menuEdit.class}>
                            <Edit
                                replace={this.state.replace}
                                staff={this.state.staff}
                                list={this.state.list}
                                master={this.state.master}
                                attachment={this.state.attachment}
                                regulation={this.state.regulation}
                                record={record}
                                modalView={this.state.edit[record.key]}
                            />
                        </span>,
                    ];
                },
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["leave_staff_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <span className={menuCreate.class}>
                                    <Create staff={this.state.staff} list={this.state.list} formLayout={formLayout} />
                                </span>
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    bordered={true}
                                    loading={this.state.isLoading}
                                    dataSource={dataList}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    loadMaster() {
        GetData("/leave-master").then((result) => {
            const data = result.data.data;
            this.setState({
                master: data,
            });
            data.map((item, key) => {
                this.setState({
                    attachment: {
                        ...this.state.attachment,
                        [item["leave_master_id"]]: item["leavem_attachment"],
                    },
                    regulation: {
                        ...this.state.regulation,
                        [item["leave_master_id"]]: item["leavem_regulation"],
                    },
                    replace: {
                        ...this.state.replace,
                        [item["leave_master_id"]]: item["leavem_need_replace"],
                    },
                });
            });
        });
    }

    showDetail = (overtime_id) => (e) => {
        this.setState({
            detail: {
                ...this.state.base,
                [overtime_id]: true,
            },
            edit: this.state.base,
        });
    };

    showEdit = (overtime_id) => (e) => {
        this.setState({
            edit: {
                ...this.state.base,
                [overtime_id]: true,
            },
            detail: this.state.base,
        });
    };
    confirm = (leave_id) => (e) => {
        DeleteData("/leave/" + leave_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    publish = (leave_id) => (e) => {
        GetData("/leave-publish/" + leave_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    cancel = (leave_id) => (e) => {
        GetData("/leave-cancel/" + leave_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    list = (e) => {
        this.setState({
            isLoading: true,
        });
        GetData("/leave").then((result) => {
            let response = result.data.data;
            this.setState({
                data: response,
                dataSearched: response,
                isLoading: false,
            });
            const detail = [];
            response.map((item) => {
                detail[item["leave_id"]] = false;
            });
            this.setState({
                detail: detail,
                base: detail,
                edit: detail,
            });
        });
    };
}
