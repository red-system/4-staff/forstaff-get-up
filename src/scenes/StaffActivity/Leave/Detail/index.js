import React, { Component } from "react";
import { Button, Col, Icon, Modal, Row, Form } from "antd";
import moment from "moment";
import { status_view } from "../../../../services/app/General";

class ViewFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: props.modalView,
            numPages: null,
            pageNumber: 1,
            formValue: props.record,
            formLayout: props.formLayout,
        };
    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            modalEditVisible: nextProps.modalView,
        });
    }

    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({ numPages }) => {
        this.setState({ numPages });
    };

    render() {
        const { modalEditVisible, formLayout } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        return (
            <Modal
                visible={modalEditVisible}
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={[
                    <Row style={{ textAlign: "center" }} key="1">
                        <Col {...formLayout}>
                            <Button key="back" onClick={this.modalEditCancel}>
                                <Icon type="rollback" /> Tutup
                            </Button>
                        </Col>
                    </Row>,
                ]}
            >
                <Form {...formItemLayout} className={"form-info"}>
                    <Form.Item label="Tanggal Pengajuan">{moment(this.state.formValue.leave_staff_publish_date).format("YYYY-MM-DD")}</Form.Item>
                    <Form.Item label="Tanggal Cuti">{moment(this.state.formValue.leave_staff_date).format("YYYY-MM-DD")}</Form.Item>
                    <Form.Item label="Tanggal Selesai">
                        {moment(this.state.formValue.leave_staff_date).add(this.state.formValue.leave_staff_duration, "days").format("YYYY-MM-DD")}
                    </Form.Item>
                    <Form.Item label="Durasi">{this.state.formValue.leave_staff_duration}</Form.Item>
                    <Form.Item label="Regulasi">{status_view(this.state.formValue.leavem_regulation)}</Form.Item>
                    <Form.Item label="Status">{status_view(this.state.formValue.leave_staff_status)}</Form.Item>
                    {this.state.formValue.leavem_attachment === "yes" && (
                        <Form.Item label="Lampiran Cuti">
                            <Button type="primary" onClick={this.openUrl(this.state.formValue.leave_staff_attachment_url)}>
                                <Icon type="download" />
                            </Button>
                        </Form.Item>
                    )}
                </Form>
            </Modal>
        );
    }
    openUrl = (url) => (e) => {
        window.open(url, "_blank");
    };
}

export default ViewFile;
