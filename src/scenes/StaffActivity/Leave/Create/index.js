import React, {Component} from 'react';
import {Button, Form, Icon, Input, DatePicker, Modal, Row, Col, InputNumber, Select, message} from "antd";
import 'react-quill/dist/quill.snow.css';
import {GetData, PostData} from "../../../../services/api";
import moment from "moment";
import LeaveManageDetil from "../../../StaffAttendence/Leave/4_0_LeaveManage_Detail";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay, status_view} from "../../../../services/app/General";

let {Option} = Select;
const {TextArea} = Input;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            pengumuman: '',
            master: [],
            formValue: {
                leave_staff_date: dateToDay(),
                leave_master_id: ''
            },
            attachment: [],
            replace: [],
            regulation_label: "-",
            attachment_field: "no",
            replace_field: "no",
            formError: [],
            staff: [],
            row_leave_master: {
                '': {}
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            staff: nextProps.staff
        })
    }

    componentWillMount() {
        this.loadMaster()
    }

    render() {
        const {formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };

        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Pengajuan Cuti Baru
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Pengajuan Baru
                        </span>
                    }
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalStatus(false)}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Cuti"
                            {...this.state.formError.leave_staff_date}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("leave_staff_date")}
                                value={moment(dateFormatterApp(this.state.formValue.leave_staff_date), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Jenis Cuti"
                            {...this.state.formError.leave_master_id}
                        >

                            <Select
                                showSearch
                                style={{width: "54%"}}
                                placeholder="Pilih Jenis Cuti"
                                optionFilterProp="children"
                                onChange={this.onChangeJenis()}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }>
                                {
                                    Object.keys(this.state.master).map((key) => {
                                        return (
                                            <Option key={key}
                                                    value={this.state.master[key].leave_master_id}>{this.state.master[key].leavem_name}</Option>
                                        )
                                    })
                                }

                            </Select>
                            &nbsp;
                            <LeaveManageDetil
                                record={this.state.row_leave_master[this.state.formValue.leave_master_id]}/>
                        </Form.Item>
                        <Form.Item
                            label="Durasi"
                            {...this.state.formError.leave_staff_duration}
                        >
                            <InputNumber
                                style={{"width": "100%"}}
                                value={this.state.formValue.leave_staff_duration}
                                onChange={this.onChange("leave_staff_duration")}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Alasan Cuti"
                            {...this.state.formError.leave_staff_reason}
                        >
                            <TextArea
                                value={this.state.formValue.leave_staff_reason}
                                onChange={this.onChange("leave_staff_reason")}
                            />
                        </Form.Item>
                        {this.state.replace_field === "yes" &&
                        <Form.Item
                            label="Staf Pengganti"
                            {...this.state.formError.substitution_staff_id}>

                            <Select
                                showSearch
                                style={{width: "100%"}}
                                placeholder="Pilih Staff"
                                optionFilterProp="children"
                                defaultValue={this.state.formValue.substitution_staff_id}
                                onChange={this.onChange("substitution_staff_id")}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {
                                    Object.keys(this.state.staff).map((key) => {

                                        return (
                                            <Option key={key}
                                                    value={this.state.staff[key]['staff_id']}>{this.state.staff[key]['staff_name']}</Option>
                                        )
                                    })
                                }
                            </Select>

                        </Form.Item>
                        }
                        {this.state.attachment_field === "yes" &&
                        <Form.Item
                            label="Lampiran Cuti"
                            {...this.state.formError.leave_staff_attachment}
                        >
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        }
                    </Form>
                </Modal>
            </div>
        )
    }

    loadMaster() {
        GetData("/leave-master").then((result) => {
            const data = result.data.data;
            this.setState({
                master: data
            });
            data.map((item, key) => {
                this.setState({
                    attachment: {
                        ...this.state.attachment,
                        [item["leave_master_id"]]: item["leavem_attachment"]
                    },
                    replace: {
                        ...this.state.replace,
                        [item["leave_master_id"]]: item["leavem_need_replace"]
                    },
                    row_leave_master: {
                        ...this.state.row_leave_master,
                        [item["leave_master_id"]]: {
                            ...item
                        }
                    }
                })
            })
        })
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                leave_staff_attachment: event.target.files[0]
            }
        })
    };
    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };
    onChange = name => value => {
        if (typeof value == "object") {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };
    onChangeJenis = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                leave_master_id: value
            },
            attachment_field: this.state.attachment[value],
            replace_field: this.state.replace[value],
        })

    };
    insert = (e) => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] !== undefined) {
                formData.append(key, this.state.formValue[key])
            }
        });
        PostData("/leave", formData).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.setState({
                    formValue: {
                        leave_staff_date: dateToDay(),
                        leave_master_id: ''
                    },
                    modalVisible: false,
                });
                this.props.list()
            } else {
                let errors = response.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

export default Create;