import React, { Component } from "react";
import { Button, Col, Menu, Row, Table, Icon, Dropdown, Tag, Popconfirm, message, Input } from "antd";

import Create from "./Create";
import Cancel from "./Cancel";
import Edit from "./Edit";
import Detail from "./Detail";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { DeleteData, GetData } from "../../../services/api";
import { menuActionViewAccess, status_view } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            list: this.list,
            data: [],
            detail: [],
            base: [],
            edit: [],
            refreshRun: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });

        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var overtimeDate = false;
            if (get(record, "overtime_date_format") != null) {
                overtimeDate = get(record, "overtime_date_format").toString().match(reg);
            }
            var overtimeReason = false;
            if (get(record, "overtime_reason") != null) {
                overtimeReason = get(record, "overtime_reason").toString().match(reg);
            }
            var publishDate = false;
            if (get(record, "publish_date_format") != null) {
                publishDate = get(record, "publish_date_format").toString().match(reg);
            }
            if (!overtimeDate && !overtimeReason && !publishDate) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("key_activity", "overtime", "list");
        let menuCreate = menuActionViewAccess("key_activity", "overtime", "create");
        let menuEdit = menuActionViewAccess("key_activity", "overtime", "edit");
        let menuDelete = menuActionViewAccess("key_activity", "overtime", "delete");
        let menuCancel = menuActionViewAccess("key_activity", "overtime", "cancel");
        let menuPublish = menuActionViewAccess("key_activity", "overtime", "publish");
        let menuDetail = menuActionViewAccess("key_activity", "overtime", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const text = "Yakin hapus data ini ?";
        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Tanggal Lembur",
                dataIndex: "overtime_date_format",
                key: "overtime_date_format",
            },
            {
                title: "Keterangan",
                dataIndex: "overtime_reason",
                key: "overtime_reason",
            },
            {
                title: "Tanggal Pengajuan",
                dataIndex: "publish_date_format",
                key: "publish_date_format",
                render: (text, record) => {
                    return record.publish_date ? record.publish_date_format : "-";
                },
            },
            {
                title: "Status",
                dataIndex: "overtime_status",
                key: "overtime_status",
                render: (status) => {
                    return status_view(status);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (test, record) => {
                    const menu = (
                        <Menu>
                            <Menu.Item className={menuDetail.class} onClick={this.showDetail(record.key)}>
                                <Icon type={"eye"} />
                                Detail
                            </Menu.Item>
                            {record.overtime_status === "unpublish" && (
                                <Menu.Item className={menuEdit.class} onClick={this.showEdit(record.key)}>
                                    <Icon type={"edit"} />
                                    Edit
                                </Menu.Item>
                            )}
                            {record.overtime_status === "unpublish" && (
                                <Menu.Item className={menuDelete.class}>
                                    <Popconfirm placement="rightTop" title={text} onConfirm={this.confirm(record.key)} okText="Yes" cancelText="No">
                                        <Icon type="delete" />
                                        &nbsp; Delete
                                    </Popconfirm>
                                </Menu.Item>
                            )}
                            {record.overtime_status === "unpublish" && (
                                <Menu.Item className={menuPublish.class} key="4" onClick={this.publish(record.key)}>
                                    <Icon type="check" />
                                    Publish
                                </Menu.Item>
                            )}
                            {record.overtime_status === "publish" && (
                                <Menu.Item className={menuCancel.class} key="5" onClick={this.cancel(record.key)}>
                                    <Icon type="rollback" />
                                    Cancel
                                </Menu.Item>
                            )}
                        </Menu>
                    );
                    return [
                        <Dropdown overlay={menu} trigger={["click"]}>
                            <Button size={"small"} type={"primary"}>
                                Menu <Icon type="down" />
                            </Button>
                        </Dropdown>,
                        <span className={menuDetail.class}>
                            <Detail record={record} view={this.state.detail[record.key]} />
                        </span>,
                        <span className={menuEdit.class}>
                            <Edit record={record} view={this.state.edit[record.key]} list={this.state.list} />
                        </span>,
                    ];
                },
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["overtime_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <span className={menuCreate.class}>
                                    <Create list={this.state.list} formLayout={formLayout} />
                                </span>
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    dataSource={dataList}
                                    bordered={true}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    showDetail = (overtime_id) => (e) => {
        this.setState({
            detail: {
                ...this.state.base,
                [overtime_id]: true,
            },
            edit: this.state.base,
        });
    };
    showEdit = (overtime_id) => (e) => {
        this.setState({
            edit: {
                ...this.state.base,
                [overtime_id]: true,
            },
            detail: this.state.base,
        });
    };
    confirm = (overtime_id) => (e) => {
        DeleteData("/overtime/" + overtime_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    publish = (overtime_id) => (e) => {
        GetData("/overtime-publish/" + overtime_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    cancel = (overtime_id) => (e) => {
        GetData("/overtime-cancel/" + overtime_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    list = (e) => {
        GetData("/overtime").then((result) => {
            let response = result.data.data;
            this.setState({
                data: response,
                dataSearched: response,
                isLoading: false,
            });
            const detail = [];
            response.map((item) => {
                detail[item["overtime_id"]] = false;
            });
            this.setState({
                detail: detail,
                base: detail,
                edit: detail,
            });
        });
    };
}

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 16,
        offset: 8,
    },
    lg: {
        span: 16,
        offset: 8,
    },
};

let pageNow = GENERALDATA.primaryMenu.key_activity.sub_menu["5"];
let pageParent = GENERALDATA.primaryMenu.key_activity;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];
