import React, { Component } from "react";
import { Button, Col, Icon, Modal, Row, Form } from "antd";
import moment from "moment";
import { status_view } from "../../../../services/app/General";

class ViewFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formValue: props.record,
            formLayout: props.formLayout,
        };
    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            modalEditVisible: nextProps.view,
        });
    }

    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({ numPages }) => {
        this.setState({ numPages });
    };

    render() {
        const { modalEditVisible, formLayout } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };

        return (
            <Modal
                visible={modalEditVisible}
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={[
                    <Row style={{ textAlign: "center" }} key="1">
                        <Col {...formLayout}>
                            <Button key="back" onClick={this.modalEditCancel}>
                                <Icon type="rollback" /> Tutup
                            </Button>
                        </Col>
                    </Row>,
                ]}
            >
                <Form {...formItemLayout} className={"form-info"}>
                    <Form.Item label="Tanggal Lembur">{this.state.formValue.overtime_date_format}</Form.Item>
                    <Form.Item label="Keterangan">{this.state.formValue.overtime_reason}</Form.Item>
                    <Form.Item label="Tanggal Pengajuan">{this.state.formValue.publish_date ? this.state.formValue.publish_date_format : "-"}</Form.Item>
                    <Form.Item label="Status">{status_view(this.state.formValue.overtime_status)}</Form.Item>
                    {this.state.formValue.overtime_status === "approved" && (
                        <Form.Item label="Disetujui oleh">
                            {this.state.formValue.approve_staff == null ? "" : this.state.formValue.approve_staff.staff_name}
                        </Form.Item>
                    )}
                    {this.state.formValue.overtime_status === "approved" && (
                        <Form.Item label="Tanggal disetujui">
                            {this.state.formValue.approved_date == null ? "" : moment(this.state.formValue.approved_date).format("YYYY-MM-DD")}
                        </Form.Item>
                    )}
                    {this.state.formValue.overtime_status === "rejected" && (
                        <Form.Item label="Ditolak oleh">
                            {this.state.formValue.rejected_staff == null ? "" : this.state.formValue.rejected_staff.staff_name}
                        </Form.Item>
                    )}
                    {this.state.formValue.overtime_status == "rejected" && (
                        <Form.Item label="Tanggal ditolak">
                            {this.state.formValue.rejected_staff == null ? "" : moment(this.state.formValue.rejected_date).format("YYYY-MM-DD")}
                        </Form.Item>
                    )}
                </Form>
            </Modal>
        );
    }
}

export default ViewFile;
