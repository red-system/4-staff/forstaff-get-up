import React, { Component } from "react";
import { Button, Form, Icon, Input, DatePicker, Modal, Row, Col, TimePicker, message } from "antd";
import "react-quill/dist/quill.snow.css"; // ES6
import moment from "moment";
import { PostData } from "../../../../services/api";
import {
    dateFormatApi,
    dateFormatApp,
    dateFormatterApp,
    dateTimeFormatApi,
    dateTimeFormatApp,
    dateTimeFormatterApp,
    dateTimeToDay,
    dateToDay,
} from "../../../../services/app/General";
import { now } from "moment";

const { TextArea } = Input;

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formError: [],
            pengumuman: "",
            formValue: {
                overtime_date: dateToDay(),
            },
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.setState({ pengumuman: value });
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {
        const { modalCreateVisible, formLayout } = this.state;

        return (
            <div>
                <Button type="primary" onClick={this.modalCreateShow}>
                    <Icon type="plus" /> Pengajuan Lembur
                </Button>
                <br />
                <br />

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> Pengajuan Lembur
                        </span>
                    }
                    visible={modalCreateVisible}
                    onOk={this.modalCreateSubmit}
                    onCancel={this.modalCreateCancel}
                    footer={[
                        <Row style={{ textAlign: "left" }}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert()}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalCreateCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>,
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Tanggal" {...this.state.formError.overtime_date}>
                            <DatePicker
                                showTime
                                style={{ width: "100%" }}
                                onChange={this.onChangeDate("overtime_date")}
                                defaultValue={moment(dateFormatterApp(now()), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item label="Keterangan" {...this.state.formError.overtime_reason}>
                            <TextArea defaultValue="" placeholder="Masukkan keterangan lembur anda disini" onChange={this.onChange("overtime_reason")} />
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        );
    }

    onChangeDate = (name) => (value) => {
        console.log("change", dateFormatApi(), value.format(dateFormatApi()));

        value = value != null ? value.format(dateFormatApi()) : "";

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };
    insert = () => (e) => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] !== undefined) {
                formData.append(key, this.state.formValue[key]);
            }
        });
        PostData("/overtime", formData).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.setState({
                    modalCreateVisible: false,
                });
                this.props.list();
            } else {
                let errors = response.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
