import React, {Component} from 'react';
import {Button, Col, Row, Table} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import Calendar from './Calendar';
import {GetData} from "../../../services/api";

let pageNow = GENERALDATA.primaryMenu.key_activity.sub_menu["7"];
let pageParent = GENERALDATA.primaryMenu.key_activity;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class StaffSallary extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            staff:[],
            listRefreshRun: this.listRefreshRun
        };
    }

    async componentWillMount() {
        await GetData('/staff').then((result) => {
            this.setState({
                staff: result.data.data,

            });
        });

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })

    }

    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Calendar />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

export default StaffSallary;