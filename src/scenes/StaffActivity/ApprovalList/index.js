import React, { Component } from 'react';
import {Col, Row, Table, Button} from "antd";
import Breadcrumb from "../../../components/Breadcrumb";
import GENERALDATA from "../../../constants/generalData";
import ApprovalDetial from "../ApprovalDetail";
import ApprovalLeave from  "../ApprovalLeave"
import {GetData, PostData} from "../../../services/api";
import moment from "moment";

let pageNow = GENERALDATA.primaryMenu.key_activity.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.key_activity;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data:[],
            list:this.list
        }
        // this.list=this.list.bind(this)
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
        this.list()
    }


    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Jenis Approval',
                dataIndex: 'approval_type',
                key: 'approval_type',
                render:type =>{
                    const label = type == "kpi_staff" ? "KPI" : "Subtitusi Cuti"
                    return label
                }
            },
            {
                title: 'Tanggal Publikasi',
                dataIndex: 'tanggal',
                key: 'tanggal',
                render:(test,record) =>{
                    const label = record.approval_type == "kpi_staff" ? (record.kpis_process_date == null ? "" : moment(record.kpis_process_date).format("YYYY-MM-DD")) : (record.leave_staff_publish_date == null ? "" : moment(record.leave_staff_publish_date).format("YYYY-MM-DD"))
                    return label
                }
            },
            {
                title: 'Keterangan',
                dataIndex: 'keterangan',
                key: 'keterangan',
                render:(test,record)=>{
                    const kpiPrefix = "KPI Staff Periode "
                    const leavePrefix = "Subtitusi cuti "
                    const periodeKpi = record.kpis_date_start + " s/d " + record.kpis_date_end
                    const totalKpi = record.kpi_result_value

                    const label = record.approval_type == "kpi_staff" ? (kpiPrefix+periodeKpi+" total nilai "+totalKpi+"%") : (leavePrefix+record.staff_name+" dari tanggal "+record.leave_staff_date+" selama "+record.leave_staff_duration+" hari")
                    return label
                }
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 100,
                render: (test,record) => (

                    <Button.Group size={"small"}>
                        {record.approval_type == "kpi_staff" &&
                            <ApprovalDetial list={this.state.list} kpi_staff_id={record.kpi_staff_id} />
                        }
                        {record.approval_type == "leave_staff" &&
                        <ApprovalLeave  list={this.state.list} record={record} />
                        }
                    </Button.Group>

                )
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['kpi_staff_id'],
                no: key + 1,
                ...item
            }
        });

        return(
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Table columns={columns}
                                       bordered={true}
                                       dataSource={dataList}
                                       size='small'/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
    list = e => {
        const loginData = JSON.parse(localStorage.getItem("loginData"))
        const staff_id = loginData.staff_id
        const formData = new FormData();
        formData.append("staff_id",staff_id)
        PostData("/approval-per-staff",formData).then((result)=>{
            const temp = result.data.data
            this.setState({
                    data:temp
                }
            )
        })

    }
}