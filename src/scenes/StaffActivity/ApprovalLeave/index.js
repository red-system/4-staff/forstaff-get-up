import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, Modal, Row, Radio, Spin, message, Popover} from "antd";
import {PostData, PutData} from "../../../services/api";
import moment from "moment";

const {TextArea} = Input;

class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
            isLoading: true,
            dataPeriods: [],
            dataStaffIndicator: [],
            kpis_revise_reason:'',
            formValue:props.record,
        };
    }

    componentWillMount() {
        this.load()
    }


    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {

        const {modalEditVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };
        const content = (
            <div>
                <TextArea onChange={this.onChange()}/>
                <div style={{marginTop: "10px"}} className={"text-center"}>
                    <Button type={"success"} size={"small"} onClick={this.sendReject()}><Icon type={"check"}/> Kirim</Button>
                </div>
            </div>
        );

        return (
            <span>
                <Button type="primary" icon="info-circle" onClick={this.modalEditShow}>
                    Detil Approval
                </Button>


                <Modal
                    title={"Detail Approval"}
                    visible={modalEditVisible}
                    onOk={this.modalEditSubmit}
                    onCancel={this.modalEditCancel}
                    width={600}
                    footer={[

                        <Row style={{textAlign: 'center'}} key="1">
                            <Col {...formLayout}>
                                <Button key="back" type={"primary"} onClick={this.approve()}>
                                    <Icon type="check" /> Approve
                                </Button>
                                <Popover content={content} title="Alasan Ditolak" trigger="click">
                                    <Button key="back" type={"warning"}>
                                        <Icon type="rollback" /> Revise
                                    </Button>
                                </Popover>

                                <Button key="back" onClick={this.modalEditCancel}>
                                    <Icon type="close" /> Tutup
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                     <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Pengajuan">
                            {moment(this.state.formValue.leave_staff_publish_date).format("YYYY-MM-DD")}
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Cuti">
                            {moment(this.state.formValue.leave_staff_date).format("YYYY-MM-DD")}
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Selesai">
                            {moment(this.state.formValue.leave_staff_date).add(this.state.formValue.leave_staff_duration,"days").format("YYYY-MM-DD")}
                        </Form.Item>
                        <Form.Item
                            label="Durasi">
                            {this.state.formValue.leave_staff_duration}
                        </Form.Item>
                       <Form.Item
                           label="Alasan Cuti">
                            {this.state.formValue.leave_staff_reason}
                        </Form.Item>

                    </Form>
                </Modal>

            </span>
        )
    }
    approve=()=>e=>{
        let leave_staff_id = this.props.record.leave_staff_id;
        const formData = new FormData();
        formData.append("leave_staff_id",leave_staff_id)
        PostData("/approval-confirm",formData).then((result)=>{
            let data = result.data
            if(data.status="success"){
                this.props.list()
                this.setState({
                    modalEditVisible:false
                })
                message.success(data.message)
            } else {
                message.error(data.message)
            }

        })
    }
    sendReject=()=>e=>{
        let leave_staff_id = this.props.record.leave_staff_id;
        const formData = new FormData();
        formData.append("leave_staff_reject_reason",this.state.kpis_revise_reason)
        formData.append("leave_staff_id",leave_staff_id)
        PostData("reject-staff-leave",formData).then((result)=>{
            let data = result.data
            if(data.status="success"){
                this.props.list()
                this.setState({
                    modalEditVisible:false
                })
                message.success(data.message)
            } else {
                message.error(data.message)
            }

        })
    }
    load(){
        let kpi_staff_id = this.props.kpi_staff_id;
        let data = {
            kpi_staff_id: kpi_staff_id
        };
        this.setState({
            isLoading: true
        });
        PostData('kpi-indicator/overview/kpi-result/edit', data)
            .then((result) => {
                this.setState({
                    dataStaffIndicator: result.data.data,
                    isLoading: false
                })
            });
    }
    onChange=()=>value=>{
        if(typeof value == "object"){
            value = value.target.value
        }
        this.setState({
            kpis_revise_reason:value
        })
    }
}

export default ViewFile;