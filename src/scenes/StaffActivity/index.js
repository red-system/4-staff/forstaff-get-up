import React, {Component} from "react";
import {Row, Col} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';
import {Link} from 'react-router-dom';

import '../../styles/ui/_card-icon.scss';

let menuStaffActivity = GENERALDATA.primaryMenu.key_activity;
let subStaffActivity = GENERALDATA.primaryMenu.key_activity.sub_menu;
let gutter = GENERALDATA.gutter;

const menuGrid = {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 3
};

const breadcrumb = [
    {
        label: menuStaffActivity.label,
        route: `${menuStaffActivity.route}`
    },
];

class StaffActivity extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="-1"/>)]
        })
    }

    render() {

        let tabActive = 'informasi-umum';
        const menuList = Object.keys(subStaffActivity).map(function (key) {
            if(key == 1) {
                tabActive = 'informasi-umum';
            }else if(key == 2) {
                tabActive = 'kpi';
            } else if(key == 3) {
                tabActive = 'salary';
            }
            return (
                <Col {...menuGrid} className="text-center" key={key}>
                    <Link to={{
                            pathname: subStaffActivity[key].route,
                            state: {
                                tabActive: tabActive
                            }
                        }}>
                        <img src={subStaffActivity[key].img} alt={subStaffActivity[key].label}/>
                        <h1>{subStaffActivity[key].label}</h1>
                    </Link>
                </Col>
            )
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row gutter={gutter} className="menu-list" type="flex" align="top">
                    {menuList}
                </Row>
            </div>
        )

    }

}

export default StaffActivity;