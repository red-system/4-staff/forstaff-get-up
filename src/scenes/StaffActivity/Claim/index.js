import React, { Component } from "react";
import { Button, Col, Dropdown, Icon, Menu, message, Popconfirm, Row, Table, Tag, Input } from "antd";

import Create from "./Create";
import Cancel from "./Cancel";
import Detail from "./Detail";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { DeleteData, GetData } from "../../../services/api";
import moment from "moment";
import { color } from "echarts/src/export";
import Edit from "./Edit";
import { menuActionViewAccess, status_view } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 16,
        offset: 8,
    },
    lg: {
        span: 16,
        offset: 8,
    },
};

let pageNow = GENERALDATA.primaryMenu.key_activity.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.key_activity;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class AppComponent extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: [],
            list: this.list,
            detail: [],
            base: [],
            edit: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var calimDate = false;
            if (get(record, "claim_date_format") != null) {
                calimDate = get(record, "claim_date_format").toString().match(reg);
            }
            var claimReason = false;
            if (get(record, "claim_reason") != null) {
                claimReason = get(record, "claim_reason").toString().match(reg);
            }
            var claimNominal = false;
            if (get(record, "claim_nominal") != null) {
                claimNominal = get(record, "claim_nominal").toString().match(reg);
            }
            var salarySlip = false;
            if (get(record, "salary_slip_number") != null) {
                salarySlip = get(record, "salary_slip_number").toString().match(reg);
            }
            if (!calimDate && !claimReason && !claimNominal && !salarySlip) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("key_activity", "claim", "list");
        let menuCreate = menuActionViewAccess("key_activity", "claim", "create");
        let menuEdit = menuActionViewAccess("key_activity", "claim", "edit");
        let menuDelete = menuActionViewAccess("key_activity", "claim", "delete");
        let menuCancel = menuActionViewAccess("key_activity", "claim", "cancel");
        let menuPublish = menuActionViewAccess("key_activity", "claim", "publish");
        let menuDetail = menuActionViewAccess("key_activity", "claim", "detail");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const text = "Yakin hapus data ini ?";
        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Tanggal Claim",
                dataIndex: "claim_date_format",
                key: "claim_date_format",
            },
            {
                title: "Keterangan",
                dataIndex: "claim_reason",
                key: "claim_reason",
            },
            {
                title: "Nominal",
                dataIndex: "claim_nominal",
                key: "claim_nominal",
                render: (nominal) => {
                    return new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                    }).format(nominal);
                },
            },
            {
                title: "Status",
                dataIndex: "claim_status",
                key: "claim_status",
                render: (status) => {
                    return status_view(status);
                },
            },
            {
                title: "Nomer Slip Gaji",
                dataIndex: "salary_slip_number",
                key: "salary_slip_number",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (test, record) => {
                    const menu = (
                        <Menu>
                            <Menu.Item className={menuDetail.class} key="1" onClick={this.showDetail(record.key)}>
                                <Icon type="eye" />
                                Detail
                            </Menu.Item>
                            {record.claim_status === "wait" && (
                                <Menu.Item className={menuEdit.class} key="2" onClick={this.showEdit(record.key)}>
                                    <Icon type="edit" />
                                    Edit
                                </Menu.Item>
                            )}
                            {record.claim_status === "wait" && (
                                <Menu.Item key="3" className={menuDelete.class}>
                                    <Popconfirm placement="rightTop" title={text} onConfirm={this.confirm(record.key)} okText="Yes" cancelText="No">
                                        <Icon type="delete" />
                                        &nbsp; Delete
                                    </Popconfirm>
                                </Menu.Item>
                            )}
                            {record.claim_status === "wait" && (
                                <Menu.Item className={menuPublish.class} key="4" onClick={this.publish(record.key)}>
                                    <Icon type="check" />
                                    Publish
                                </Menu.Item>
                            )}
                            {record.claim_status === "publish" && (
                                <Menu.Item className={menuCancel.class} key="5" onClick={this.cancel(record.key)}>
                                    <Icon type="rollback" />
                                    Cancel
                                </Menu.Item>
                            )}
                        </Menu>
                    );
                    return [
                        <Dropdown overlay={menu} trigger={["click"]}>
                            <Button>
                                Menu <Icon type="down" />
                            </Button>
                        </Dropdown>,
                        <span className={menuDetail.class}>
                            <Detail record={record} modalView={this.state.detail[record.key]} />
                        </span>,
                        <span className={menuEdit.class}>
                            <Edit list={this.list} record={record} modalView={this.state.edit[record.key]} />
                        </span>,
                    ];
                },
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["claim_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <span className={menuCreate.class}>
                                    <Create list={this.list} formLayout={formLayout} />
                                </span>
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    bordered={true}
                                    dataSource={dataList}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    showDetail = (claim_id) => (e) => {
        this.setState({
            detail: {
                ...this.state.base,
                [claim_id]: true,
            },
            edit: this.state.base,
        });
    };
    showEdit = (claim_id) => (e) => {
        this.setState({
            edit: {
                ...this.state.base,
                [claim_id]: true,
            },
            detail: this.state.base,
        });
    };
    confirm = (claim_id) => (e) => {
        DeleteData("/claim/" + claim_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    publish = (claim_id) => (e) => {
        GetData("/claim/publish/" + claim_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    cancel = (claim_id) => (e) => {
        GetData("/claim/cancel/" + claim_id).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.list();
            } else {
                message.error(response.message);
            }
        });
    };
    list = (e) => {
        this.setState({
            isLoading: true,
        });
        GetData("/claim").then((result) => {
            let response = result.data.data;
            this.setState({
                data: response,
                dataSearched: response,
                isLoading: false,
            });
            const detail = [];
            response.map((item) => {
                detail[item["claim_id"]] = false;
            });
            this.setState({
                detail: detail,
                base: detail,
                edit: detail,
            });
        });
    };
}
