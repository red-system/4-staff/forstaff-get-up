import React, {Component} from 'react';
import {Button, Form, Icon, Input, InputNumber, DatePicker, Upload, Modal, Row, Col, message} from "antd";
import 'react-quill/dist/quill.snow.css';
import moment from "moment";
import {PostData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General"; // ES6

const {TextArea} = Input;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            pengumuman: '',
            formValue: {
                claim_date: dateToDay(),
                claim_nominal: 0,
                claim_filename: null,
                claim_reason: ''
            },
            formError: []
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.setState({pengumuman: value})
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        const {modalCreateVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };

        const modules = {
            toolbar: [
                [{'header': [1, 2, false]}],
                ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                ['link', 'image'],
                ['clean']
            ],
        };

        const formats = [
            'header',
            'bold', 'italic', 'underline', 'strike', 'blockquote',
            'list', 'bullet', 'indent',
            'link', 'image'
        ];

        return (
            <div>
                <Button type="primary" onClick={this.modalCreateShow}>
                    <Icon type="plus"/> Claim Baru
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Claim Baru
                        </span>
                    }
                    visible={modalCreateVisible}
                    onOk={this.modalCreateSubmit}
                    onCancel={this.modalCreateCancel}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert()}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalCreateCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Claim"
                            {...this.state.formError.claim_date}
                        >
                            <DatePicker
                                style={{width: '100%'}}
                                onChange={this.onChangeDate("claim_date")}
                                value={moment(dateFormatterApp(this.state.formValue.claim_date), dateFormatApp())}
                                format={dateFormatApp()}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Nominal"
                            {...this.state.formError.claim_nominal}
                        >
                            <InputNumber
                                style={{width: '100%'}}
                                defaultValue={1000}
                                value={this.state.formValue.claim_nominal}
                                formatter={value => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\Rp\s?|(,*)/g, '')}
                                onChange={this.onChange("claim_nominal")}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Attachment"
                            {...this.state.formError.claim_filename}
                        >
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                        <Form.Item
                            label="Keterangan"
                            {...this.state.formError.claim_reason}
                        >
                            <TextArea
                                value={this.state.formValue.claim_reason}
                                onChange={this.onChange("claim_reason")}
                            />
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        )
    }

    // onChangeDate = name => value =>{
    //     this.setState({
    //         formValue:{
    //             ...this.state.formValue,
    //             [name]:value
    //         }
    //     })
    // }

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChange = name => value => {
        if (typeof value == "object") {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };
    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                claim_filename: event.target.files[0]
            }
        })
    };
    insert = () => e => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != undefined) {
                formData.append(key, this.state.formValue[key])
            }
        });
        PostData("/claim", formData).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message);
                this.setState({
                    // formValue: [],
                    modalCreateVisible: false,
                });
                this.props.list()
            } else {
                let errors = response.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }

}


export default Create;