import React, {Component} from 'react';
import {Button, Col, DatePicker, Icon, Input, Modal, Row, Form} from "antd";


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
        };
    }


    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {

        const {modalEditVisible, formLayout, pageNumber, numPages} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };


        return (
            <span>
                <Button type="primary" icon={"eye"} onClick={this.modalEditShow}>
                    Detail
                </Button>


                <Modal
                    visible={modalEditVisible}
                    onOk={this.modalEditSubmit}
                    onCancel={this.modalEditCancel}
                    footer={[

                        <Row style={{textAlign: 'center'}} key="1">
                            <Col {...formLayout}>
                                <Button key="back" onClick={this.modalEditCancel}>
                                    <Icon type="rollback" /> Tutup
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Pengajuan">
                            10 Agustus 2019
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Cuti">
                            11 Agustus 2019
                        </Form.Item>
                        <Form.Item
                            label="Tanggal Selesai">
                            11 Oktober 2019
                        </Form.Item>
                        <Form.Item
                            label="Durasi">
                            60 Hari
                        </Form.Item>
                        <Form.Item
                            label="Regulasi">
                            Dengan Pengganti
                        </Form.Item>
                        <Form.Item
                            label="Status">
                            Pending
                        </Form.Item>
                    </Form>
                </Modal>

            </span>
        )
    }
}

export default ViewFile;