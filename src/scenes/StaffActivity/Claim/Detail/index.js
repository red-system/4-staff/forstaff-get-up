import React, {Component} from 'react';
import {Button, Col, Icon, Modal, Row, Form} from "antd";
import moment from "moment";
import {status_view} from "../../../../services/app/General";


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
            formValue:props.record
        };
    }
    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            modalEditVisible:nextProps.modalView
        })
    }

    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {

        const {modalEditVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };


        return (
                <Modal
                    visible={modalEditVisible}
                    onOk={this.modalEditSubmit}
                    onCancel={this.modalEditCancel}
                    footer={[

                        <Row style={{textAlign: 'center'}} key="1">
                            <Col {...formLayout}>
                                <Button key="back" onClick={this.modalEditCancel}>
                                    <Icon type="rollback" /> Tutup
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Tanggal Claim">
                            {moment(this.state.formValue.claim_date).format("YYYY-MM-DD")}
                        </Form.Item>
                        <Form.Item
                            label="Keterangan">
                            {this.state.formValue.claim_reason}
                        </Form.Item>
                        <Form.Item
                            label="Nominal">
                            {new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR',
                                minimumFractionDigits: 0
                            }).format(this.state.formValue.claim_nominal)}
                        </Form.Item>
                        <Form.Item
                            label="Attachment">
                            <Button type={"primary"} icon={"eye"} onClick={this.openUrl(this.state.formValue.claim_filename_url)}>Lihat File</Button>
                        </Form.Item>
                        <Form.Item
                            label="Status">
                            {status_view(this.state.formValue.claim_status)}
                        </Form.Item>
                    </Form>
                </Modal>
        )
    }
    openUrl = url =>e =>{
        window.open(url,"_blank")
    }
}

export default ViewFile;