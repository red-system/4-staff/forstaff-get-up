import React from 'react';
import {Form, Icon, Input, Button, Modal} from 'antd';
import {Redirect} from 'react-router-dom';
import loginBackgroundForStaff from '../../images/login-background-forStaff.jpg';
import appLogoLogin from '../../images/app-logo-login.png';
import {PostData} from "../../services/api";
import GENERALDATA from "../../constants/generalData";

import "./index.scss";

// 3rd
import '../../styles/bootstrap/bootstrap.scss'
// custom
import "../../styles/layout.scss"
import "../../styles/theme.scss"
import "../../styles/ui.scss"

let prefixUrl = GENERALDATA.appUriUniq;

const FormItem = Form.Item;

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            redirectLogin: false,
            redirectTo: '',
            isLoading: false,

            username: '',
            password: '',
            form_error: {},
            modalLevel: false,

        };

        this.onChange = this.onChange.bind(this);
        this.login = this.login.bind(this);
    };

    componentWillMount() {

        const loginStatus = localStorage.getItem('loginStatus');
        const staff_register_complete = localStorage.getItem('staff_register_complete');
        const level_total = localStorage.getItem('level_total');
        const level_list = localStorage.getItem('level_list');
        const roles = localStorage.getItem('roles');
        const redirectTo = localStorage.getItem('redirectTo');

        if (loginStatus && staff_register_complete == "yes") {

            let state = {};

            if (redirectTo) {
                state = {
                    redirectTo: redirectTo,
                    redirectLogin: true,
                };
            } else {
                let menuFirst = this.menuFirst(roles);

                    state = {
                        redirectLogin: true,
                        redirectTo: `${prefixUrl}/${menuFirst[0].route}`
                    }
            }


            this.setState(state);

        } else if (loginStatus && staff_register_complete == 'no') {
            const staff_register_step = localStorage.getItem('staff_register_step');
            let redirectTo = 'register';
            switch (staff_register_step) {
                case "1":
                    redirectTo = 'register';
                    break;
                case "2":
                    redirectTo = 'register/step-2';
                    break;
                case "3":
                    redirectTo = 'register/step-3';
                    break;
                case "4":
                    redirectTo = 'register/step-4';
                    break;
            }

            this.setState({
                redirectLogin: true,
                redirectTo: redirectTo
            });
        }
    }

    render() {

        let {match, redirectLogin, redirectTo, isLoading} = this.state;

        let {username_field, password_field} = this.state.form_error;

        if (redirectLogin) {
            return <Redirect to={`${match.url}${redirectTo}`}/>
        }


        return (

            <div key="1">

                <Modal
                    title="Pilih Level Login"
                    visible={this.state.modalLevel}
                    onCancel={null}
                    onOk={null}
                    footer={null}
                >
                    <div className={"app-level row"}>
                        <div className={"col-lg-6"}>
                            <div onClick={this.levelDirect('hrd')}>
                                <img src={require('../../images/level-hrd.png')} width={120}/>
                                <br/>
                                HRD
                            </div>
                        </div>
                        <div className={"col-lg-6"}>
                            <div onClick={this.levelDirect('staff')}>
                                <img src={require('../../images/level-staff.png')} width={120}/>
                                <br/>
                                STAFF
                            </div>
                        </div>

                    </div>

                </Modal>

                <section className="form-card-page form-card row no-gutters" style={{height: '100vh'}}>
                    <div className="form-card__img form-card__img--left col-lg-6"
                         style={{backgroundImage: `url('${loginBackgroundForStaff}')`}}></div>
                    <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center">
                        <section className="form-v1-container">
                            <div className={"text-center"}>
                                <img src={appLogoLogin} style={{width: 'auto'}}/>
                                <br/><br/>
                            </div>
                            <p className="lead">Selamat Datang, silahkan login akun forstaff anda.</p>
                            <Form autoComplete="off" className="form-v1" onSubmit={this.login}>
                                <FormItem {...username_field}>
                                    <Input size="large"
                                           prefix={
                                               <Icon type="user"
                                                     style={{fontSize: 13}}/>
                                           }
                                           placeholder="Username"
                                           name={"username"}
                                           autoComplete="false"
                                           onChange={this.onChange}/>
                                </FormItem>
                                <FormItem {...password_field}>
                                    <Input size="large"
                                           prefix={
                                               <Icon type="lock"
                                                     style={{fontSize: 13}}/>
                                           }
                                           type="password"
                                           name={"password"}
                                           placeholder="Password"
                                           autoComplete="false"
                                           onChange={this.onChange}/>
                                </FormItem>
                                <FormItem>
                                    <Button type="primary" loading={isLoading} icon={"login"} htmlType="submit"
                                            className="btn-cta btn-block"
                                            onClick={this.login}>
                                        Log in
                                    </Button>
                                </FormItem>
                            </Form>
                            <p className="additional-info">Development By <a href={"http://redsystem.id"}
                                                                             target={"_blank"}>Red System</a> - PT. Guna
                                Teknologi Nusantara</p>
                            {/*<p className="additional-info">Anda belum mempunyai akun? <Link*/}
                            {/*    to={`${match.url}sign-up`}> Daftar Sekarang</Link></p>*/}
                            {/*<p className="additional-info">Anda lupa username atau password ? <Link*/}
                            {/*    to={`${match.url}forgot-password`}>Reset Password</Link>*/}
                            {/*</p>*/}
                        </section>
                    </div>
                </section>
            </div>

        );
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    levelDirect = (level) => e => {
        this.setState({
            redirectLogin: true,
            redirectTo: level
        });

        localStorage.setItem('redirectTo', level);
    };

    login(e) {
        e.preventDefault();
        const {username, password} = this.state;
        this.setState({
            isLoading: true
        });
        let data = {
            username: username,
            password: password
        };

        PostData('/login', data).then((result) => {
            const data = result.data;
            if (data.status === 'success') {

                localStorage.setItem('loginData', JSON.stringify(data.data.staff));
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('loginStatus', true);
                localStorage.setItem('staff_register_complete', data.data.staff.staff_register_complete);
                localStorage.setItem('staff_register_step', data.data.staff.staff_register_step);
                localStorage.setItem('staff_status', data.data.staff.staff_status);
                localStorage.setItem('roles', data.data.roles);
                let state = {};
                let menuFirst = this.menuFirst(data.data.roles);

                if (data.data.staff.staff_register_complete === 'yes') {
                    state = {
                        redirectLogin: true,
                        isLoading: false,
                        redirectTo: `${prefixUrl}/${menuFirst[0].route}`
                    }
                } else {
                    state = {
                        redirectLogin: true,
                        redirectTo: 'register'
                    }
                }

                this.setState(state);
            } else {
                let errors = data.errors;
                let form_error = [];

                Object.keys(errors).map(function (key) {
                    form_error[`${key}_field`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    form_error: form_error,
                    isLoading: false
                });
            }
        });

        return false;
    }

    menuFirst = (roles) => {
        roles = JSON.parse(roles);
        let menu = [];
        let menuGlobal = GENERALDATA.primaryMenu;
        let menuView = [];

        Object.keys(roles).map((item, key) => {
            Object.keys(roles[item]).map((item2, key2) => {
                Object.keys(roles[item][item2]).map((item3, key3) => {
                    Object.keys(roles[item][item2][item3]).map((item4, key4) => {
                        Object.keys(roles[item][item2][item3][item4]).map((item5, key5) => {
                            let status = roles[item][item2][item3][item4][item5];
                            if (status) {
                                menu[key] = item;
                            }
                        })
                    })
                });
            })
        });

        Object.keys(menu).map((item, key) => {
            let menuVariable = menu[item];
            menuView[key] = GENERALDATA.primaryMenu[menuVariable];
        });

        return menuView;

    }

}

export default Login;
