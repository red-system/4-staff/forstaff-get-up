import React, {Component} from 'react';
import './styles.scss';
import {GetData} from "../../../services/api";
import {Link} from "react-router-dom";
import {Button, Tag} from "antd";
import GENERALDATA from "../../../constants/generalData";
import {menuActionViewAccess} from "../../../services/app/General";

class AnnouncementNews extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            data: props.absence_summary
        });

        console.log('use info', props);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            data: nextProps.absence_summary
        })
    }

    render() {
        let menuAbsen = GENERALDATA.primaryMenu.personal_menu.sub_menu[4];
        let menuAction = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'absence_summary');

        return (
            <Link to={menuAbsen.route} className={menuAction.class}>
                <div className="app-content">
                    <div className="app-content-title">
                        Rangkuman Absen : <strong>{`${this.state.data.month_view} ${this.state.data.year}`}</strong>
                        <Button type={"success"} size={"small"} className={"float-right"} icon={"arrow-right"}> Detail Absen</Button>
                    </div>
                    <div className="app-content-body">
                        <table className="table-bordered no-link-style">
                            <tbody>
                                <tr>
                                    <td width={"40%"}>Jumlah Kehadiran</td>
                                    <td width={"60%"} className={"text-right"}>{this.state.data.total_work} Hari</td>
                                </tr>
                                <tr>
                                    <td width={"40%"}>Libur Kerja</td>
                                    <td width={"60%"} className={"text-right"}>{this.state.data.total_dayoff} Hari</td>
                                </tr>
                                <tr>
                                    <td width={"40%"}>Tidak Hadir</td>
                                    <td width={"60%"} className={"text-right"}>{this.state.data.total_not_work} Hari</td>
                                </tr>
                                <tr>
                                    <td width={"40%"}>Lembur</td>
                                    <td width={"60%"} className={"text-right"}>{this.state.data.total_overtime} Menit</td>
                                </tr>
                                <tr>
                                    <td width={"40%"}>Total Cuti Bulan Ini</td>
                                    <td width={"60%"} className={"text-right"}>{`${this.state.data.total_leave_month}`} Hari</td>
                                </tr>
                                <tr>
                                    <td width={"40%"}>Total Cuti Tahun Ini</td>
                                    <td width={"60%"} className={"text-right"}>{`${this.state.data.total_leave_year}/${this.state.data.total_quota_leave}`} Hari</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Link>
        )
    }

}

export default AnnouncementNews;