import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import GENERAL_DATA from '../../../constants/generalData';
import {menuActionViewAccess} from "../../../services/app/General";
import {Button} from "antd";

export default class AnnouncementNews extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        let menuAction = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'staff_info');
        let menuProfilStaff = GENERAL_DATA.primaryMenu.personal_menu.sub_menu[1];
        let loginData = localStorage.getItem('loginData');
        loginData = JSON.parse(loginData);

        return (
            <Link to={menuProfilStaff.route} className={menuAction.class}>
                <section className="app-content form-card row no-gutters">
                    <div className="form-card__img form-card__img--left col-lg-4"
                         style={{backgroundImage: `url('${loginData.staff_avatar_url}')`}}/>
                    <div className="form-card__body col-lg-8 p-4">
                        <section className="form-v1-container left-text">
                            <h3>{loginData.staff_name}</h3>
                            <div className="form-text">
                                <div>
                                    <span>Posisi</span> {loginData.structure_name}
                                </div>
                                <div>
                                    <span>Departemen</span> {loginData.department_title}
                                </div>
                                <div>
                                    <span>Bergabung ke Perusahaan</span> {loginData.staff_work_start_view}
                                </div>
                                <div>
                                    <span>Kontrak sampai</span> {loginData.staff_contract_date_to_view}
                                </div>
                            </div>
                            <br />
                            <br />
                            <Button type={"primary"} size={"small"} icon={"edit"}> Edit Data Personal</Button>
                        </section>
                    </div>
                </section>
            </Link>

        )
    }

}