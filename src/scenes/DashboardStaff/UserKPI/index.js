import React, {Component} from 'react';
import ReactEcharts from "echarts-for-react";
import {Button} from "antd";
import {PostData} from "../../../services/api";
import moment from "moment";
import {Link} from "react-router-dom";
import GENERALDATA from "../../../constants/generalData";
import UserKPIStatistic from "../../PersonalMenu/KPI/UserKPIStatistic";
import {menuActionViewAccess} from "../../../services/app/General";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staff: null,
            month: [],
            value: [],
            year: moment(Date()).format("Y")
        }
    }

    render() {
        let menuKPI = GENERALDATA.primaryMenu.personal_menu.sub_menu[2];
        let menuAction = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'kpi_chart');
        let loginData = localStorage.getItem('loginData');
        loginData = JSON.parse(loginData);

        return (
                <div className={"app-content "+menuAction.class}>
                    <div className="app-content-title">
                        KPI - {loginData.staff_name}
                        <Link to={menuKPI.route} className={"float-right"}>
                            <Button type={"success"} icon={"arrow-right"} size={"small"}> Detail KPI</Button>
                        </Link>
                    </div>
                    <div className="app-content-body">
                        <UserKPIStatistic />
                    </div>
                </div>
        )
    }

}