import React, {Component} from 'react';
import {Button, Col, Icon, Row, Table, Tabs} from "antd";
import GENERALDATA from "../../../constants/generalData";
import ViewFileAnnouncement from "../../GeneralData/Announcement/ViewFile"
import ViewFile from "../../GeneralData/News/ViewFile"
import {menuActionViewAccess} from "../../../services/app/General";

const {TabPane} = Tabs;

const gutter = GENERALDATA.gutter;

export default class AnnouncementNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            announcement_data: props.announcement_data,
            news_data: props.news_data,
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            announcement_data: nextProps.announcement_data,
            news_data: nextProps.news_data
        })
    }

    render() {
        let menuActionAnnouncement = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'announcement');
        let menuActionNews = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'news');

        const columnsAnnoucement = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Judul', dataIndex: 'announcement_title', key: 'announcement_title'},
            {title: 'Deskripsi', dataIndex: 'announcement_description_view', key: 'announcement_description_view'},
            {title: 'Waktu Pengumuman', dataIndex: 'updated_at', key: 'updated_at'},
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <ViewFileAnnouncement record={record}/>
                    </Button.Group>

                )
            }
        ];

        const columnsNews = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Judul', dataIndex: 'news_title', key: 'news_title'},
            {title: 'Deskripsi', dataIndex: 'news_description_view', key: 'news_description_view'},
            {title: 'Waktu Berita', dataIndex: 'updated_at', key: 'updated_at'},
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <ViewFile record={record}/>
                    </Button.Group>

                )
            }
        ];

        const dataListAnnouncement = this.state.announcement_data.map((item, key) => {
            return {
                key: item['announcement_id'],
                no: key + 1,
                ...item,
            }
        });

        const dataListNews = this.state.news_data.map((item, key) => {
            return {
                key: item['news_id'],
                no: key + 1,
                ...item,
            }
        });

        return (
            <Row gutter={gutter}>
                <Col span={24}>
                    <div className="card-container table-body-image">
                        <Tabs type="card">
                            <TabPane className={menuActionAnnouncement.class} tab={<span><Icon type="alert"/> Pengumuman</span>} key="1">
                                <Table columns={columnsAnnoucement}
                                       dataSource={dataListAnnouncement}
                                       size='small'/>
                            </TabPane>
                            <TabPane className={menuActionNews.class} tab={<span><Icon type="file-done"/> Berita</span>} key="2">
                                <Table columns={columnsNews}
                                       dataSource={dataListNews}
                                       size='small'/>
                            </TabPane>
                        </Tabs>
                    </div>
                </Col>
            </Row>
        )
    }
}