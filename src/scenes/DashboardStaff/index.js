import React, {Component} from "react";
import {Row, Col, Spin} from 'antd';
import Breadcrumb from '../../components/Breadcrumb';
import GENERALDATA from "../../constants/generalData";

import UserCard from './UserCard';
import UserInfo from './UserInfo';
import UserKPI from './UserKPI';
import UserShift from './UserShift';
import AnnouncementNews from './AnnouncementNews';
import {GetData, PostData} from "../../services/api";


let menuProfile = GENERALDATA.primaryMenu.profile;
const gutter = GENERALDATA.gutter;


const breadcrumb = [
    {
        label: menuProfile.label,
        route: `${menuProfile.route}`
    },
];

class Profile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            breadcrumbComponent: null,
            absence_summary: [],
            shift_year: '',
            shift_month: '',
            shift_data: [],
            news_data: [],
            announcement_data: [],
            is_loading: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });

        PostData('/dashboard-staff').then((result) => {
            let data = result.data.data;
            this.setState({
                is_loading: false,

                absence_summary: data.absence_summary,
                shift_year: data.shift.year,
                shift_month: data.shift.month,
                shift_data: data.shift.shift,
                news_data: data.news,
                announcement_data: data.announcement
            });
        });
    }

    render() {

        const {breadcrumbComponent} = this.state;

        return (

            <div className="profile-wrapper content-gutter-vertical">
                {breadcrumbComponent}
                <Row gutter={gutter}>
                    <Col xs={24} sm={24} md={24} lg={15}>
                        <UserCard/>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={9}>
                        <Spin spinning={this.state.is_loading}>
                            <UserInfo
                                absence_summary={this.state.absence_summary}/>
                        </Spin>
                    </Col>
                </Row>
                <Row gutter={gutter}>
                    <Col xs={24} sm={24} md={24} lg={15}>
                        <UserKPI/>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={9}>
                        <Spin spinning={this.state.is_loading}>
                            <UserShift
                                shift_year={this.state.shift_year}
                                shift_month={this.state.shift_month}
                                shift_data={this.state.shift_data}
                            />
                        </Spin>
                    </Col>
                </Row>

                <Spin spinning={this.state.is_loading}>
                    <AnnouncementNews
                        announcement_data={this.state.announcement_data}
                        news_data={this.state.news_data}/>
                </Spin>
            </div>

        )

    }

}

export default Profile;