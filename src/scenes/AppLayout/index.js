import React, {Component} from 'react';
import Loadable from 'react-loadable';
import {Redirect, Route} from 'react-router-dom';
import GENERALDATA from "../../constants/generalData";

import {Layout} from "antd";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import Loading from "../../components/Loading";
import Profile from "../DashboardStaff";


import '../styles.scss';

// 3rd
import '../../styles/bootstrap/bootstrap.scss'
// custom
import "../../styles/layout.scss"
import "../../styles/theme.scss"
import "../../styles/ui.scss"
import "../../styles/vendors.scss"

const {Content} = Layout;
let userLevel = "hrd";


// PRIMARY MENU //
let PROFILE = GENERALDATA.primaryMenu.profile;
let PROFILE_DETAIL = GENERALDATA.primaryMenu.profileDetil;
let SETTINGS = GENERALDATA.primaryMenu.settings;
let RELEASE_NOTE = GENERALDATA.primaryMenu.releaseNote;
let LOG_LIST_PERSONAL = GENERALDATA.primaryMenu.logListPersonal;
let LOG_LIST_ALL = GENERALDATA.primaryMenu.logListAll;

// DASHBOARD //
let DASHBOARD_HRD = GENERALDATA.primaryMenu.dashboard_hrd;
let DASHBOARD_STAFF = GENERALDATA.primaryMenu.dashboard_staff;


// SOP MANAGEMENT //
let SOP_MANAGEMENT = GENERALDATA.primaryMenu.sop_management;
let HRD_CONFIG = GENERALDATA.primaryMenu.sop_management.sub_menu["1"];
let COMPANY_PROFILE = GENERALDATA.primaryMenu.sop_management.sub_menu["2"];
let ORGANIZATION_STRUCTURE = GENERALDATA.primaryMenu.sop_management.sub_menu["3"];
let JOB_DESCRIPTION = GENERALDATA.primaryMenu.sop_management.sub_menu["4"];
let WORK_FLOW = GENERALDATA.primaryMenu.sop_management.sub_menu["5"];
let REWARD = GENERALDATA.primaryMenu.sop_management.sub_menu["6"];
let PUNISHMENT = GENERALDATA.primaryMenu.sop_management.sub_menu["7"];
let RULES_REGULATION = GENERALDATA.primaryMenu.sop_management.sub_menu["8"];

// STAFF MANAGEMENT //
let STAFF_MANAGEMENT = GENERALDATA.primaryMenu.staff_management;
let STAFF_DATA = GENERALDATA.primaryMenu.staff_management.sub_menu["1"];
let STAFF_POSITION = GENERALDATA.primaryMenu.staff_management.sub_menu["2"];
let STAFF_CONTRACT = GENERALDATA.primaryMenu.staff_management.sub_menu["3"];
let STAFF_JOB_DESCRIPTION = GENERALDATA.primaryMenu.staff_management.sub_menu["4"];
let STAFF_BENEFIT = GENERALDATA.primaryMenu.staff_management.sub_menu["5"];
let STAFF_REPORT = GENERALDATA.primaryMenu.staff_management.sub_menu["6"];
let STAFF_SALARY = GENERALDATA.primaryMenu.staff_management.sub_menu["7"];
let STAFF_CLAIM = GENERALDATA.primaryMenu.staff_management.sub_menu["8"];
let STAFF_LEARNING_DEVELOPMENT = GENERALDATA.primaryMenu.staff_management.sub_menu["9"];
let STAFF_KPI = GENERALDATA.primaryMenu.staff_management.sub_menu["10"];
let STAFF_RESIGN = GENERALDATA.primaryMenu.staff_management.sub_menu["11"];
let STAFF_ANNOUNCEMENT = GENERALDATA.primaryMenu.staff_management.sub_menu["12"];
let STAFF_NEWS = GENERALDATA.primaryMenu.staff_management.sub_menu["13"];
let STAFF_FAMILY = GENERALDATA.primaryMenu.staff_management.sub_menu["14"];
let STAFF_ACADEMIC = GENERALDATA.primaryMenu.staff_management.sub_menu["15"];
let STAFF_JOB_HISTORY = GENERALDATA.primaryMenu.staff_management.sub_menu["16"];
let STAFF_CERTIFICATION = GENERALDATA.primaryMenu.staff_management.sub_menu["17"];
let STAFF_DOCUMENT = GENERALDATA.primaryMenu.staff_management.sub_menu["18"];


// STAFF ATTENDENCE //
let STAFF_ATTENDENCE = GENERALDATA.primaryMenu.staff_attendence;
let SCHEDULE = GENERALDATA.primaryMenu.staff_attendence.sub_menu["1"];
let DAY_OFF = GENERALDATA.primaryMenu.staff_attendence.sub_menu["2"];
let ABSENCE = GENERALDATA.primaryMenu.staff_attendence.sub_menu["3"];
let LEAVE = GENERALDATA.primaryMenu.staff_attendence.sub_menu["4"];
let OVER_TIME = GENERALDATA.primaryMenu.staff_attendence.sub_menu["5"];
let SHIFT_ATTANDENCE = GENERALDATA.primaryMenu.staff_attendence.sub_menu["6"];


// GENERAL DATA //
let GENERAL_DATA = GENERALDATA.primaryMenu.general_data;
// let ANNOUNCEMENT = GENERALDATA.primaryMenu.general_data.sub_menu["1"];
// let NEWS = GENERALDATA.primaryMenu.general_data.sub_menu["2"];
let ROLE_PERMISSION = GENERALDATA.primaryMenu.general_data.sub_menu["3"];
let ROLE_USER = GENERALDATA.primaryMenu.general_data.sub_menu["4"];
let BANNER = GENERALDATA.primaryMenu.general_data.sub_menu["7"];


// MENU PERSONAL //
let PERSONAL_MENU = GENERALDATA.primaryMenu.personal_menu;
let PROFIL_STAFF = GENERALDATA.primaryMenu.personal_menu.sub_menu['1'];
let KPI = GENERALDATA.primaryMenu.personal_menu.sub_menu['2'];
let GAJI = GENERALDATA.primaryMenu.personal_menu.sub_menu['3'];
let ABSEN = GENERALDATA.primaryMenu.personal_menu.sub_menu['4'];
let DESKRIPSI_PEKERJAAN = GENERALDATA.primaryMenu.personal_menu.sub_menu['5'];
let KONTRAK_PEKERJAAN = GENERALDATA.primaryMenu.personal_menu.sub_menu['6'];
let DATA_KELUARGA = GENERALDATA.primaryMenu.personal_menu.sub_menu['7'];
let DATA_RIWAYAT_PENDIDIKAN = GENERALDATA.primaryMenu.personal_menu.sub_menu['8'];
let DATA_RIWAYAT_PEKERJAAN = GENERALDATA.primaryMenu.personal_menu.sub_menu['9'];
let DATA_SERTIFIKASI_STAFF = GENERALDATA.primaryMenu.personal_menu.sub_menu['10'];
let DOKUMEN_STAFF = GENERALDATA.primaryMenu.personal_menu.sub_menu['11'];
let RESIGN = GENERALDATA.primaryMenu.personal_menu.sub_menu['12'];
let SHIFT = GENERALDATA.primaryMenu.personal_menu.sub_menu['13'];
let PERSONAL_REWARD = GENERALDATA.primaryMenu.personal_menu.sub_menu['14'];
let PERSONAL_PUNISHMENT = GENERALDATA.primaryMenu.personal_menu.sub_menu['15'];
let PERSONAL_LEARNING_DEVELOPMENT = GENERALDATA.primaryMenu.personal_menu.sub_menu['16'];
let PERSONAL_BENEFIT = GENERALDATA.primaryMenu.personal_menu.sub_menu['17'];


// STAFF ACTIVITY //
let STAFF_ACTIVITY = GENERALDATA.primaryMenu.key_activity;
// let APPROVAL_LIST = GENERALDATA.primaryMenu.key_activity.sub_menu['1'];
let LEAVE_STAFF = GENERALDATA.primaryMenu.key_activity.sub_menu['3'];
let CLAIM = GENERALDATA.primaryMenu.key_activity.sub_menu['4'];
let OVERTIME = GENERALDATA.primaryMenu.key_activity.sub_menu['5'];
let STAFF_REPORT_ACTIVITY = GENERALDATA.primaryMenu.key_activity.sub_menu['7'];

// COMPANY MENU //
let COMPANY_MENU = GENERALDATA.primaryMenu.my_company;
let COMPANY_MENU_PROFILE = GENERALDATA.primaryMenu.my_company.sub_menu['1'];
let COMPANY_WORKFLOW = GENERALDATA.primaryMenu.my_company.sub_menu['2'];
let COMPANY_RULES_REGULATION = GENERALDATA.primaryMenu.my_company.sub_menu['3'];
let COMPANY_REWARD = GENERALDATA.primaryMenu.my_company.sub_menu['4'];
let COMPANY_PUNISHMENT = GENERALDATA.primaryMenu.my_company.sub_menu['5'];
let COMPANY_DAYOFF = GENERALDATA.primaryMenu.my_company.sub_menu['7'];
let COMPANY_ORGANIZATION_STRUCTURE = GENERALDATA.primaryMenu.my_company.sub_menu['8'];
let COMPANY_ANNOUNCEMENT = GENERALDATA.primaryMenu.my_company.sub_menu["9"];
let COMPANY_NEWS = GENERALDATA.primaryMenu.my_company.sub_menu["10"];


// PRIMARY MENU //
const AsyncProfile = Loadable({
    loader: () => import('../DashboardStaff'),
    loading: Loading
});
// const AsyncProfileDetail = Loadable({
//     loader: () => import('../ProfileDetail'),
//     loading: Loading
// });
const AsyncSettings = Loadable({
    loader: () => import('../Settings'),
    loading: Loading
});
const AsyncReleaseNote = Loadable({
    loader: () => import('../ReleaseNote'),
    loading: Loading
});
const AsyncLogListPersonal = Loadable({
    loader: () => import('../LogListPersonal'),
    loading: Loading
});
const AsyncLogListAll = Loadable({
    loader: () => import('../LogListAll'),
    loading: Loading
});


// DASHBOARD //
const AsyncDashboardHrd = Loadable({
    loader: () => import('../DashboardHrd'),
    loading: Loading
});
const AsyncDashboardStaff = Loadable({
    loader: () => import('../DashboardStaff'),
    loading: Loading
});

// PERSONAL MENU//
const AsyncPersonalMenu = Loadable({
    loader: () => import('../PersonalMenu'),
    loading: Loading
});
const AsyncProfilStaff = Loadable({
    loader: () => import('../PersonalMenu/ProfilStaff'),
    loading: Loading
});
const AsyncKPI = Loadable({
    loader: () => import('../PersonalMenu/KPI'),
    loading: Loading
});
const AsyncGaji = Loadable({
    loader: () => import('../PersonalMenu/Gaji'),
    loading: Loading
});
const AsyncAbsen = Loadable({
    loader: () => import('../PersonalMenu/Absen'),
    loading: Loading
});
const AsyncDeskripsiPekerjaan = Loadable({
    loader: () => import('../PersonalMenu/DeskripsiPekerjaan'),
    loading: Loading
});
const AsyncKontrakPekerjaan = Loadable({
    loader: () => import('../PersonalMenu/KontrakPekerjaan'),
    loading: Loading
});
const AsyncDataKeluarga = Loadable({
    loader: () => import('../PersonalMenu/DataKeluarga'),
    loading: Loading
});
const AsyncDataRiwayatPendidikan = Loadable({
    loader: () => import('../PersonalMenu/DataRiwayatPendidikan'),
    loading: Loading
});
const AsyncDataRiwayatPekerjaan = Loadable({
    loader: () => import('../PersonalMenu/DataRiwayatPekerjaan'),
    loading: Loading
});
const AsyncDataSertifikasiStaff = Loadable({
    loader: () => import('../PersonalMenu/DataSertifikasiStaff'),
    loading: Loading
});
const AsyncDokumenStaff = Loadable({
    loader: () => import('../PersonalMenu/DokumenStaff'),
    loading: Loading
});
const AsyncResign = Loadable({
    loader: () => import('../PersonalMenu/Resign'),
    loading: Loading
});
const AsyncShift = Loadable({
    loader: () => import('../PersonalMenu/Shift'),
    loading: Loading
});
const AsyncPersonalReward = Loadable({
    loader: () => import('../PersonalMenu/Reward'),
    loading: Loading
});
const AsyncPersonalPunishment = Loadable({
    loader: () => import('../PersonalMenu/Punishment'),
    loading: Loading
});
const AsyncPersonalLearningDevelopment = Loadable({
    loader: () => import('../PersonalMenu/LearningDevelopment'),
    loading: Loading
});
const AsyncPersonalBenefit= Loadable({
    loader: () => import('../PersonalMenu/Benefit'),
    loading: Loading
});

// SOP MANAGEMENT //
const AsyncSopManagement = Loadable({
    loader: () => import('../SopManagement'),
    loading: Loading
});
const AsyncConfigHrd = Loadable({
    loader: () => import('../SopManagement/ConfigHrd'),
    loading: Loading
});
const AsyncCompanyProfile = Loadable({
    loader: () => import('../SopManagement/CompanyProfile'),
    loading: Loading
});
const AsyncOrganizationStructure = Loadable({
    loader: () => import('../SopManagement/OrganizationStructure'),
    loading: Loading
});
const AsyncJobDescription = Loadable({
    loader: () => import('../SopManagement/JobDescription'),
    loading: Loading
});
const AsyncWorkFlow = Loadable({
    loader: () => import('../SopManagement/WorkFlow'),
    loading: Loading
});
const AsyncReward = Loadable({
    loader: () => import('../SopManagement/Reward'),
    loading: Loading
});
const AsyncPunishment = Loadable({
    loader: () => import('../SopManagement/Punishment'),
    loading: Loading
});
const AsyncRulesRegulation = Loadable({
    loader: () => import('../SopManagement/RulesRegulation'),
    loading: Loading
});


// STAFF MANAGEMENT //
const AsyncStaffManagement = Loadable({
    loader: () => import('../StaffManagement'),
    loading: Loading
});
const AsyncStaffData = Loadable({
    loader: () => import('../StaffManagement/StaffData'),
    loading: Loading
});
const AsyncStaffDataCreate = Loadable({
    loader: () => import('../StaffManagement/StaffData/Create'),
    loading: Loading
});
const AsyncStaffDataEdit = Loadable({
    loader: () => import('../StaffManagement/StaffData/Edit'),
    loading: Loading
});
const AsyncStaffPosition = Loadable({
    loader: () => import('../StaffManagement/StaffPosition'),
    loading: Loading
});
const AsyncStaffContract = Loadable({
    loader: () => import('../StaffManagement/StaffContract'),
    loading: Loading
});
const AsyncStaffJobDescription = Loadable({
    loader: () => import('../StaffManagement/StaffJobDescription'),
    loading: Loading
});
const AsyncStaffBenefit = Loadable({
    loader: () => import('../StaffManagement/StaffBenefit'),
    loading: Loading
});
const AsyncStaffReport = Loadable({
    loader: () => import('../StaffManagement/StaffReport'),
    loading: Loading
});
const AsyncStaffReportDetil = Loadable({
    loader: () => import('../StaffManagement/StaffReport/Detail'),
    loading: Loading
});
const AsyncStaffSalary = Loadable({
    loader: () => import('../StaffManagement/StaffSalary'),
    loading: Loading
});
const AsyncStaffClaim = Loadable({
    loader: () => import('../StaffManagement/StaffClaim'),
    loading: Loading
});
const AsyncStaffLearningDevelopment = Loadable({
    loader: () => import('../StaffManagement/StaffLearningDevelopment'),
    loading: Loading
});
const AsyncStaffKPI = Loadable({
    loader: () => import('../StaffManagement/StaffKPI'),
    loading: Loading
});
const AsyncStaffResign = Loadable({
    loader: () => import('../StaffManagement/StaffResign'),
    loading: Loading
});

const AsyncStaffAnnouncement = Loadable({
    loader: () => import('../StaffManagement/StaffAnnouncement'),
    loading: Loading
});
const AsyncStaffNews = Loadable({
    loader: () => import('../StaffManagement/StaffNews'),
    loading: Loading
});
const AsyncStaffFamily = Loadable({
    loader: () => import('../StaffManagement/StaffFamily'),
    loading: Loading
});
const AsyncStaffAcademic = Loadable({
    loader: () => import('../StaffManagement/StaffAcademic'),
    loading: Loading
});
const AsyncStaffJobHistory = Loadable({
    loader: () => import('../StaffManagement/StaffJobHistory'),
    loading: Loading
});
const AsyncStaffCertification = Loadable({
    loader: () => import('../StaffManagement/StaffCertification'),
    loading: Loading
});
const AsyncStaffDocument = Loadable({
    loader: () => import('../StaffManagement/StaffDocument'),
    loading: Loading
});


// STAFF ATTENDENCE //
const AsyncStaffAttendence = Loadable({
    loader: () => import('../StaffAttendence'),
    loading: Loading
});
const AsyncSchedule = Loadable({
    loader: () => import('../StaffAttendence/Schedule'),
    loading: Loading
});
const AsyncDayOff = Loadable({
    loader: () => import('../StaffAttendence/DayOff'),
    loading: Loading
});
const AsyncAbsence = Loadable({
    loader: () => import('../StaffAttendence/Absence'),
    loading: Loading
});
const AsyncLeave = Loadable({
    loader: () => import('../StaffAttendence/Leave'),
    loading: Loading
});
const AsyncOverTime = Loadable({
    loader: () => import('../StaffAttendence/OverTime'),
    loading: Loading
});
const AsyncShiftAttendence = Loadable({
    loader: () => import('../StaffAttendence/Shift'),
    loading: Loading
});


// GENERAL DATA //
const AsyncGeneralData = Loadable({
    loader: () => import('../GeneralData'),
    loading: Loading
});
// const AsyncAnnouncement = Loadable({
//     loader: () => import('../GeneralData/Announcement'),
//     loading: Loading
// });
// const AsyncNews = Loadable({
//     loader: () => import('../GeneralData/News'),
//     loading: Loading
// });
const AsyncRolePermission = Loadable({
    loader: () => import('../GeneralData/RolePermission'),
    loading: Loading
});
const AsyncRoleUser = Loadable({
    loader: () => import('../GeneralData/RoleUser'),
    loading: Loading
});
const AsyncBanner = Loadable({
    loader: () => import('../GeneralData/Banner'),
    loading: Loading
});

// STAFF ACTIVITY//
const AsyncStaffActivity = Loadable({
    loader: () => import('../StaffActivity'),
    loading: Loading
});
// const AsyncApprovalList = Loadable({
//     loader: () => import('../StaffActivity/ApprovalList'),
//     loading: Loading
// });
const AsyncLeaveStaff = Loadable({
    loader: () => import('../StaffActivity/Leave'),
    loading: Loading
});
const AsyncClaim = Loadable({
    loader: () => import('../StaffActivity/Claim'),
    loading: Loading
});
const AsyncOvertime = Loadable({
    loader: () => import('../StaffActivity/Overtime'),
    loading: Loading
});
const AsyncStaffReportActivity = Loadable({
    loader: () => import('../StaffActivity/Report'),
    loading: Loading
});


// COMPANY MENU//
const AsyncCompanyMenu = Loadable({
    loader: () => import('../CompanyMenu'),
    loading: Loading
});
const AsyncCompanyMenuProfile = Loadable({
    loader: () => import('../CompanyMenu/CompanyProfile'),
    loading: Loading
});
const AsyncCompanyWorkFlow = Loadable({
    loader: () => import('../CompanyMenu/WorkFlow'),
    loading: Loading
});
const AsyncCompanyRulesRegulation = Loadable({
    loader: () => import('../CompanyMenu/RulesRegulation'),
    loading: Loading
});
const AsyncCompanyDayoff = Loadable({
    loader: () => import('../CompanyMenu/DayOff'),
    loading: Loading
});
const AsyncCompanyOrganizationStructure = Loadable({
    loader: () => import('../CompanyMenu/OrganizationStructure'),
    loading: Loading
});
const AsyncCompanyRewardGuide = Loadable({
    loader: () => import('../CompanyMenu/RewardGuide'),
    loading: Loading
});
const AsyncCompanyPunishmentGuide = Loadable({
    loader: () => import('../CompanyMenu/PunishmentGuide'),
    loading: Loading
});
const AsyncCompanyAnnouncement = Loadable({
    loader: () => import('../CompanyMenu/Announcement'),
    loading: Loading
});
const AsyncCompanyNews = Loadable({
    loader: () => import('../CompanyMenu/News'),
    loading: Loading
});


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sidebarDrawerVisible: false,
            sidebarCollapsed: false,
            match: props.match,
            redirectLogin: false
        };
    }

    componentWillMount() {
        const loginStatus = localStorage.getItem('loginStatus');
        const staff_register_complete = localStorage.getItem('staff_register_complete');
        let state = {};

        if (loginStatus && staff_register_complete === 'yes') {
            state = {
                redirectLogin: true
            };
        } else {
            state = {
                redirectLogin: false
            }
        }
        this.setState(state);
    }

    render() {

        let loginStatus = localStorage.getItem('loginStatus');
        let staff_register_complete = localStorage.getItem('staff_register_complete');

        const {match, sidebarDrawerVisible, sidebarCollapsed} = this.state;
        if (!loginStatus || staff_register_complete === 'no') {
            return <Redirect to={""}/>
        }

        // console.log('applayout', `${match.url}/${DASHBOARD_HRD.route}/${LOG_LIST_PERSONAL.route}`);

        return (

            <Layout className="app-layout">
                <Sidebar sidebarDrawerVisible={sidebarDrawerVisible}
                            sidebarCollapsed={sidebarCollapsed}
                            match={match}
                            sidebarDrawerEvent={this.sidebarDrawerEvent.bind(this)}
                            sidebarDrawerEventClose={this.sidebarDrawerEventClose.bind(this)}/>
                <Layout className="app-content-layout">
                    <Header
                        sidebarCollapsed={sidebarCollapsed}
                        match={match}
                        sidebarDrawerEvent={this.sidebarDrawerEvent.bind(this)}
                        sidebarEvent={this.sidebarEvent.bind(this)}/>

                    <Content className="app-content-wrapper">
                        {/* DASHBOARD */}
                        <Route path={`${match.url}/${DASHBOARD_HRD.route}`} component={AsyncDashboardHrd}/>
                        <Route path={`${match.url}/${DASHBOARD_STAFF.route}`} component={AsyncDashboardStaff}/>

                        {/*//SOP MANAGEMENT*/}
                        <Route path={`${match.url}/${HRD_CONFIG.route}`} component={AsyncConfigHrd}/>
                        <Route path={`${match.url}/${COMPANY_PROFILE.route}`} component={AsyncCompanyProfile}/>
                        <Route path={`${match.url}/${ORGANIZATION_STRUCTURE.route}`}
                               component={AsyncOrganizationStructure}/>
                        <Route path={`${match.url}/${JOB_DESCRIPTION.route}`} component={AsyncJobDescription}/>
                        <Route path={`${match.url}/${WORK_FLOW.route}`} component={AsyncWorkFlow}/>
                        <Route path={`${match.url}/${REWARD.route}`} component={AsyncReward}/>
                        <Route path={`${match.url}/${PUNISHMENT.route}`} component={AsyncPunishment}/>
                        <Route path={`${match.url}/${RULES_REGULATION.route}`} component={AsyncRulesRegulation}/>
                        <Route path={`${match.url}/${SOP_MANAGEMENT.route}`} exact={true}
                               component={AsyncSopManagement}/>

                        {/*//MENU PERSONAL*/}
                        <Route path={`${match.url}/${PROFIL_STAFF.route}`} component={AsyncProfilStaff}/>
                        <Route path={`${match.url}/${KPI.route}`} component={AsyncKPI}/>
                        <Route path={`${match.url}/${GAJI.route}`} component={AsyncGaji}/>
                        <Route path={`${match.url}/${ABSEN.route}`} component={AsyncAbsen}/>
                        <Route path={`${match.url}/${DESKRIPSI_PEKERJAAN.route}`} component={AsyncDeskripsiPekerjaan}/>
                        <Route path={`${match.url}/${KONTRAK_PEKERJAAN.route}`} component={AsyncKontrakPekerjaan}/>
                        <Route path={`${match.url}/${DATA_KELUARGA.route}`} component={AsyncDataKeluarga}/>
                        <Route path={`${match.url}/${DATA_RIWAYAT_PENDIDIKAN.route}`}
                               component={AsyncDataRiwayatPendidikan}/>
                        <Route path={`${match.url}/${DATA_RIWAYAT_PEKERJAAN.route}`}
                               component={AsyncDataRiwayatPekerjaan}/>
                        <Route path={`${match.url}/${DATA_SERTIFIKASI_STAFF.route}`}
                               component={AsyncDataSertifikasiStaff}/>
                        <Route path={`${match.url}/${DOKUMEN_STAFF.route}`} component={AsyncDokumenStaff}/>
                        <Route path={`${match.url}/${RESIGN.route}`} component={AsyncResign}/>
                        <Route path={`${match.url}/${PERSONAL_REWARD.route}`} component={AsyncPersonalReward}/>
                        <Route path={`${match.url}/${PERSONAL_PUNISHMENT.route}`} component={AsyncPersonalPunishment}/>
                        <Route path={`${match.url}/${PERSONAL_LEARNING_DEVELOPMENT.route}`}
                               component={AsyncPersonalLearningDevelopment}/>
                        <Route path={`${match.url}/${PERSONAL_BENEFIT.route}`}
                               component={AsyncPersonalBenefit}/>
                        <Route path={`${match.url}/${PERSONAL_MENU.route}`} exact={true} component={AsyncPersonalMenu}/>
                        <Route path={`${match.url}/${SHIFT.route}`} exact={true} component={AsyncShift}/>


                        {/*STAFF MANAGEMENT*/}
                        <Route path={`${match.url}/${STAFF_DATA.route}`} exact={true} component={AsyncStaffData}/>
                        <Route path={`${match.url}/${STAFF_DATA.route}/create`} component={AsyncStaffDataCreate}/>
                        <Route path={`${match.url}/${STAFF_DATA.route}/edit`} component={AsyncStaffDataEdit}/>
                        <Route path={`${match.url}/${STAFF_POSITION.route}`} component={AsyncStaffPosition}/>
                        <Route path={`${match.url}/${STAFF_CONTRACT.route}`} component={AsyncStaffContract}/>
                        <Route path={`${match.url}/${STAFF_JOB_DESCRIPTION.route}`}
                               component={AsyncStaffJobDescription}/>
                        <Route path={`${match.url}/${STAFF_BENEFIT.route}`} component={AsyncStaffBenefit}/>
                        <Route path={`${match.url}/${STAFF_REPORT.route}`} exact={true} component={AsyncStaffReport}/>
                        <Route path={`${match.url}/${STAFF_REPORT.route}/staff`} component={AsyncStaffReportDetil}/>
                        <Route path={`${match.url}/${STAFF_SALARY.route}`} component={AsyncStaffSalary}/>
                        <Route path={`${match.url}/${STAFF_CLAIM.route}`} component={AsyncStaffClaim}/>
                        <Route path={`${match.url}/${STAFF_LEARNING_DEVELOPMENT.route}`}
                               component={AsyncStaffLearningDevelopment}/>
                        <Route path={`${match.url}/${STAFF_KPI.route}`} component={AsyncStaffKPI}/>
                        <Route path={`${match.url}/${STAFF_RESIGN.route}`} component={AsyncStaffResign}/>
                        <Route path={`${match.url}/${STAFF_ANNOUNCEMENT.route}`} component={AsyncStaffAnnouncement}/>
                        <Route path={`${match.url}/${STAFF_NEWS.route}`} component={AsyncStaffNews}/>
                        <Route path={`${match.url}/${STAFF_FAMILY.route}`} component={AsyncStaffFamily}/>
                        <Route path={`${match.url}/${STAFF_ACADEMIC.route}`} component={AsyncStaffAcademic}/>
                        <Route path={`${match.url}/${STAFF_JOB_HISTORY.route}`} component={AsyncStaffJobHistory}/>
                        <Route path={`${match.url}/${STAFF_CERTIFICATION.route}`} component={AsyncStaffCertification}/>
                        <Route path={`${match.url}/${STAFF_DOCUMENT.route}`} component={AsyncStaffDocument}/>
                        <Route path={`${match.url}/${STAFF_MANAGEMENT.route}`} exact={true}
                               component={AsyncStaffManagement}/>


                        {/*STAFF ATTENDENCE*/}
                        <Route path={`${match.url}/${SCHEDULE.route}`} component={AsyncSchedule}/>
                        <Route path={`${match.url}/${DAY_OFF.route}`} component={AsyncDayOff}/>
                        <Route path={`${match.url}/${ABSENCE.route}`} component={AsyncAbsence}/>
                        <Route path={`${match.url}/${LEAVE.route}`} component={AsyncLeave}/>
                        <Route path={`${match.url}/${OVER_TIME.route}`} component={AsyncOverTime}/>
                        <Route path={`${match.url}/${SHIFT_ATTANDENCE.route}`} component={AsyncShiftAttendence}/>
                        <Route path={`${match.url}/${STAFF_ATTENDENCE.route}`} exact={true}
                               component={AsyncStaffAttendence}/>

                        {/*GENERAL DATA*/}
                        {/*<Route path={`${match.url}/${ANNOUNCEMENT.route}`} component={AsyncAnnouncement}/>*/}
                        {/*<Route path={`${match.url}/${NEWS.route}`} component={AsyncNews}/>*/}
                        <Route path={`${match.url}/${ROLE_PERMISSION.route}`} component={AsyncRolePermission}/>
                        <Route path={`${match.url}/${ROLE_USER.route}`} component={AsyncRoleUser}/>
                        <Route path={`${match.url}/${BANNER.route}`} component={AsyncBanner}/>
                        <Route path={`${match.url}/${GENERAL_DATA.route}`} exact={true} component={AsyncGeneralData}/>

                        {/*//AKTIVITAS STAFF*/}
                        {/*<Route path={`${match.url}/${APPROVAL_LIST.route}`} component={AsyncApprovalList}/>*/}
                        <Route path={`${match.url}/${LEAVE_STAFF.route}`} component={AsyncLeaveStaff}/>
                        <Route path={`${match.url}/${CLAIM.route}`} component={AsyncClaim}/>
                        <Route path={`${match.url}/${OVERTIME.route}`} component={AsyncOvertime}/>
                        <Route path={`${match.url}/${STAFF_REPORT_ACTIVITY.route}`}
                               component={AsyncStaffReportActivity}/>
                        <Route path={`${match.url}/${STAFF_ACTIVITY.route}`} exact={true}
                               component={AsyncStaffActivity}/>


                        {/*//MENU PERUSAHAAN*/}
                        <Route path={`${match.url}/${COMPANY_MENU_PROFILE.route}`} component={AsyncCompanyMenuProfile}/>
                        <Route path={`${match.url}/${COMPANY_WORKFLOW.route}`} component={AsyncCompanyWorkFlow}/>
                        <Route path={`${match.url}/${COMPANY_RULES_REGULATION.route}`}
                               component={AsyncCompanyRulesRegulation}/>
                        <Route path={`${match.url}/${COMPANY_DAYOFF.route}`} component={AsyncCompanyDayoff}/>
                        <Route path={`${match.url}/${COMPANY_ORGANIZATION_STRUCTURE.route}`}
                               component={AsyncCompanyOrganizationStructure}/>
                        <Route path={`${match.url}/${COMPANY_REWARD.route}`} component={AsyncCompanyRewardGuide}/>
                        <Route path={`${match.url}/${COMPANY_PUNISHMENT.route}`} component={AsyncCompanyPunishmentGuide}/>
                        <Route path={`${match.url}/${COMPANY_ANNOUNCEMENT.route}`} component={AsyncCompanyAnnouncement}/>
                        <Route path={`${match.url}/${COMPANY_NEWS.route}`} component={AsyncCompanyNews}/>
                        <Route path={`${match.url}/${COMPANY_MENU.route}`} exact={true} component={AsyncCompanyMenu}/>

                        <Route path={`${match.url}/${PROFILE.route}`} exact={true} render={() => <Profile userLevel={userLevel}/>}/>
                        {/*<Route path={`${match.url}/${PROFILE_DETAIL.route}`} component={AsyncProfileDetail}/>*/}
                        <Route path={`${match.url}/${SETTINGS.route}`} component={AsyncSettings}/>
                        <Route path={`${match.url}/${RELEASE_NOTE.route}`} component={AsyncReleaseNote}/>
                        <Route path={`${match.url}/${LOG_LIST_PERSONAL.route}`} component={AsyncLogListPersonal}/>
                        <Route path={`${match.url}/${LOG_LIST_ALL.route}`} component={AsyncLogListAll}/>

                        {/*<Route path={`${match.url}`} exact={true} key="Dashboard" component={AsyncDashboardHrd}/>*/}

                        {/*<Route path={`${match.url}/${PROFILE_DETAIL.route}`} component={AsyncProfileDetail}/>*/}
                        {/*<Route path={`${match.url}/${SETTINGS.route}`} component={AsyncSettings}/>*/}
                        {/*<Route path={`${match.url}/${RELEASE_NOTE.route}`} component={AsyncReleaseNote}/>*/}
                        {/*<Route path={`${match.url}/${LOG_LIST_PERSONAL.route}`} component={AsyncLogListPersonal}/>*/}
                        {/*<Route path={`${match.url}/${LOG_LIST_ALL.route}`} component={AsyncLogListAll}/>*/}
                        {/*<Route path={`${match.url}/${PROFILE.route}`} exact={true} render={() => <Profile userLevel={userLevel}/>}/>*/}

                    </Content>
                    <Footer/>
                </Layout>
            </Layout>
        )

    }


    sidebarDrawerEvent() {
        this.setState({
            sidebarDrawerVisible: !this.state.sidebarDrawerVisible
        })
    }

    sidebarDrawerEventClose() {
        this.setState({
            sidebarDrawerVisible: false
        })
    }

    sidebarEvent() {
        this.setState({
            sidebarCollapsed: !this.state.sidebarCollapsed
        });
    }

}

export default Index;