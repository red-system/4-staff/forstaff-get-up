import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Edit from "../Edit";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RoleUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            staff: [],
            roleMaster: props.roleMaster,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
        this.setState({
            roleMaster: nextProps.roleMaster,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "department_title") != null) {
                departmentTitle = get(record, "department_title").toString().match(reg);
            }
            var posittionName = false;
            if (get(record, "structure_name") != null) {
                posittionName = get(record, "structure_name").toString().match(reg);
            }
            var roleMasterTitle = false;
            if (get(record, "role_master_title") != null) {
                roleMasterTitle = get(record, "role_master_title").toString().match(reg);
            }
            if (!staffName && !posittionName && !roleMasterTitle && !departmentTitle) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("general_data", "role_user", "list");
        let menuEdit = menuActionViewAccess("general_data", "role_user", "edit");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Nama Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Departemen", dataIndex: "department_title", key: "department_title" },
            { title: "Struktur", dataIndex: "structure_name", key: "structure_name" },
            { title: "Role & Permision", dataIndex: "role_master_title", key: "role_master_title" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 80,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} roleMaster={this.state.roleMaster} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} bordered={true} dataSource={dataList} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        GetData("/role-user").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RoleUser;
