import React, {Component} from 'react';
import {Col, Row} from "antd";

import Create from './Create';
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

class Announcement extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Create {...this.state}/>
                                <List {...this.state}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

let pageNow = GENERALDATA.primaryMenu.general_data.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.general_data;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];


export default Announcement;