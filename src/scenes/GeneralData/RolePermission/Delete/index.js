import {Button, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";

class Delete extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }
    }

    render() {

        const text = 'Yakin hapus data ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger" icon={"close"} loading={this.state.isLoading}>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        DeleteData(`/role-permission/${props.record.role_master_id}`)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
                setTimeout(hide, 0);
            });
    }
}


export default Delete;