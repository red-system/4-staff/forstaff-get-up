import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Checkbox, Tabs, message} from "antd";
import {PostData} from "../../../../services/api";

const {TextArea} = Input;

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            data: this.data(),

            formError: [],
            formValue: {
                role_master_title: '',
                role_master_description: ''
            },
        };
        this.handleChange = this.handleChange.bind(this);
    }

    render() {

        const {menu} = this.props;
        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Role Baru
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Role Baru
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={"90%"}
                    onOk={this.modalStatus(true)}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Nama Role"
                            {...this.state.formError.role_master_title}>
                            <Input onChange={this.onChange('role_master_title')}/>
                        </Form.Item>
                        <Form.Item
                            label="Keterangan Role"
                            {...this.state.formError.role_master_description}>
                            <TextArea onChange={this.onChange('role_master_description')}/>
                        </Form.Item>
                        <Form.Item
                            label="Role"
                            {...this.state.formError.role_master_list}>
                            <table className={"table-bordered"}>
                                <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <th width="150">Nama Menu</th>
                                    <th>Akses Tombol Aksi</th>
                                </tr>
                                </thead>
                                {
                                    menu.map((item, key) => {
                                        return (
                                            <tbody>
                                            <tr>
                                                <td><strong>{key + 1}.</strong></td>
                                                <td><strong>{item['label']}</strong></td>
                                                <td></td>
                                            </tr>
                                            {
                                                Object.keys(item['sub_menu']).map((item2, key2) => {
                                                    // console.log('level-2', item2, key2);
                                                    return (
                                                        <tr className={item['sub_menu'][item2]['label'] == 'Change Staff'? 'hide':''}>
                                                            <td></td>
                                                            <td>{item['sub_menu'][item2]['label']}</td>
                                                            <td>
                                                                {
                                                                    item['sub_menu'][item2]['action'].map((item3, key3) => {
                                                                        return (
                                                                            <Checkbox
                                                                                onChange={this.onChangeAccess(item['variable'], item2, item['sub_menu'][item2]['variable'], key3, item3)}>
                                                                                {this.rename(item3)}
                                                                            </Checkbox>
                                                                        )
                                                                    })
                                                                }
                                                            </td>
                                                        </tr>
                                                    );
                                                })
                                            }
                                            </tbody>
                                        )
                                    })
                                }
                            </table>

                        </Form.Item>
                        <div className={"wrapper-button-float"}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                    loading={this.state.isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </div>

                    </Form>

                </Modal>
            </div>
        )
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    data() {
        const {menu} = this.props;
        let data = {};

        menu.map((item, key) => {

            let variable1 = item['variable'];

            data[variable1] = [];

            Object.keys(item['sub_menu']).map((item2, key2) => {

                let action2 = item['sub_menu'][item2]['action'];
                let variable2 = item['sub_menu'][item2]['variable'];
                let action_re = [];

                action2.map((item3, key3) => {
                    action_re[key3] = {
                        [item3]: false
                    };
                });

                data[variable1] = {
                    [item2]: {
                        [variable2]: action_re
                    },
                    ...data[variable1],
                };
            });
        });

        return data;
    }

    handleChange(value) {
        this.setState({berita: value})
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };

    onChangeAccess = (menu, sub_menu_key, sub_menu_variable, action_key, action_variable) => (e) => {
        let checked = e.target.checked;
        let data = this.state.data;

        data[menu][sub_menu_key][sub_menu_variable][action_key][action_variable] = checked;

        this.setState({
            data: data
        });

    };


    rename(str) {
        return str.replace(/_/g, ' ').capitalize();
    }

    insert = (e) => {

        let data = {
            ...this.state.formValue,
            role_master_list: this.state.data,
        };

        PostData('/role-permission', data).then((result) => {
            const data = result.data;
            if (data.status === 'success') {
                message.success(data.message);
                this.props.listRefreshRun(true);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                    data: this.data()
                });

            } else {
                message.warning(data.message);
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

String.prototype.capitalize = function () {
    return this.replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
    });
};

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 3},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 21},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 3,
        },
    },
};