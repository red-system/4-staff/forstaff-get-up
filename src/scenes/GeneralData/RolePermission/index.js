import React, { Component } from "react";
import { Button, Col, Row, Table, Input, Icon } from "antd";

import Create from "./Create";
import Edit from "./Edit";
import Delete from "./Delete";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { GetData } from "../../../services/api";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: [],
            listRefresh: true,
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("did", this.state, prevState);
        if (this.state.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var roleMasterTitle = false;
            if (get(record, "role_master_title") != null) {
                roleMasterTitle = get(record, "role_master_title").toString().match(reg);
            }
            var roleMasterDescription = false;
            if (get(record, "role_master_description") != null) {
                roleMasterDescription = get(record, "role_master_description").toString().match(reg);
            }
            if (!roleMasterTitle && !roleMasterDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("general_data", "role_permission", "list");
        let menuCreate = menuActionViewAccess("general_data", "role_permission", "create");
        let menuEdit = menuActionViewAccess("general_data", "role_permission", "edit");
        let menuDelete = menuActionViewAccess("general_data", "role_permission", "delete");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Nama Role",
                dataIndex: "role_master_title",
                key: "role_master_title",
            },
            {
                title: "Keterangan Role",
                dataIndex: "role_master_description",
                key: "role_master_description",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit menu={this.menu()} record={record} listRefreshRun={this.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <span className={menuCreate.class}>
                                    <Create listRefreshRun={this.listRefreshRun} menu={this.menu()} />
                                </span>
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    dataSource={data}
                                    bordered={true}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
            listRefresh: false,
        });

        GetData("/role-permission").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
                listRefresh: false,
            });
        });
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };

    menu() {
        let DASHBOARD_HRD = GENERALDATA.primaryMenu.dashboard_hrd;
        let DASHBOARD_STAFF = GENERALDATA.primaryMenu.dashboard_staff;
        let SOP_MANAGEMENT = GENERALDATA.primaryMenu.sop_management;
        let STAFF_MANAGEMENT = GENERALDATA.primaryMenu.staff_management;
        let STAFF_ATTENDENCE = GENERALDATA.primaryMenu.staff_attendence;
        let GENERAL_DATA = GENERALDATA.primaryMenu.general_data;
        let PERSONAL_MENU = GENERALDATA.primaryMenu.personal_menu;
        let KEY_ACTIVIITY = GENERALDATA.primaryMenu.key_activity;
        let MY_COMPANY = GENERALDATA.primaryMenu.my_company;

        return [DASHBOARD_HRD, DASHBOARD_STAFF, SOP_MANAGEMENT, STAFF_MANAGEMENT, STAFF_ATTENDENCE, GENERAL_DATA, PERSONAL_MENU, KEY_ACTIVIITY, MY_COMPANY];
    }
}

let pageNow = GENERALDATA.primaryMenu.general_data.sub_menu["3"];
let pageParent = GENERALDATA.primaryMenu.general_data;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];
