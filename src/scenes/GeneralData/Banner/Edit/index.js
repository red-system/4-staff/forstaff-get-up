import React, {Component} from 'react';
import {Alert, Button, Col, Form, Icon, Input, message, Modal, Radio, Row, Upload} from "antd";
import {PostData} from "../../../../services/api";

const {TextArea} = Input;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record
        };
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul"
                        {...this.state.formError.banner_title}>
                        <Input defaultValue={this.state.formValue.banner_title}
                               onChange={this.onChange("banner_title")}/>
                    </Form.Item>
                    <Form.Item
                        label="Deksripsi"
                        {...this.state.formError.banner_description}>
                        <TextArea defaultValue={this.state.formValue.banner_description}
                                  onChange={this.onChange("banner_description")}/>
                    </Form.Item>
                    <Form.Item
                        label="Publikasi"
                    >
                        <Radio.Group defaultValue={this.state.formValue.banner_publish}
                                     onChange={this.onChange("banner_publish")}
                                     buttonStyle="solid"
                                     key={this.state.formValue.banner_publish}
                                     name={"banner_publish"}>
                            <Radio.Button value="yes">Ya</Radio.Button>
                            <Radio.Button value="no">Tidak</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        label="File Deksripsi Pekerjaan"
                        extra="Ukuran file gambar : 960px x 350px"
                        {...this.state.formError.banner_filename}>
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File Banner</button>
                            <input type="file" name="myfile" onChange={this.handleChange}/>
                        </div>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        ]
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                banner_filename: event.target.files[0],
                banner_filename_change: true
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        formData.append('banner_title', this.state.formValue.banner_title);
        formData.append('banner_description', this.state.formValue.banner_description);
        formData.append('banner_publish', this.state.formValue.banner_publish);
        if (this.state.formValue.banner_filename_change) {
            formData.append('banner_filename', this.state.formValue.banner_filename);
        }

        PostData(`/banner/${this.props.record.key}`, formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;