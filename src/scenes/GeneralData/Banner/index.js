import React, {Component} from 'react';
import {Col, Row, Alert} from "antd";

import Create from './Create';
import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {menuActionViewAccess} from "../../../services/app/General";

class Banner extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        let menuList = menuActionViewAccess('general_data', 'banner', 'list');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Alert message="Data Banner ini digunakan untuk ditampilkan pada aplikasi mobile" type="info" showIcon />
                                <br />
                                <span className={menuList.class}>
                                    <Create {...this.state}/>
                                </span>
                                <List {...this.state}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

let pageNow = GENERALDATA.primaryMenu.general_data.sub_menu["7"];
let pageParent = GENERALDATA.primaryMenu.general_data;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];


export default Banner;