import React, {Component} from 'react';
import {Button, Table} from "antd";

import Edit from '../Edit';
import Delete from '../Delete';
import {GetData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

class Banner extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.listRefresh) {
            this.list();
        }
    }

    render() {

        let menuList = menuActionViewAccess('general_data', 'banner', 'list');
        let menuEdit = menuActionViewAccess('general_data', 'banner', 'edit');
        let menuDelete = menuActionViewAccess('general_data', 'banner', 'delete');

        const columns = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Judul', dataIndex: 'banner_title', key: 'banner_title'},
            {title: 'Deskripsi', dataIndex: 'banner_description', key: 'banner_description'},
            {title: 'Gambar', dataIndex: 'banner_filename_url', key: 'banner_filename_url',
                render: (value) => (
                    <img src={value} width={100}  />
                )
            },
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', className:"th-350", width: 200,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefreshRun={this.props.listRefreshRun}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun}/>
                        </span>
                    </Button.Group>

                )
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['banner_id'],
                no: key + 1,
                ...item
            }
        });

        return <Table
                className={menuList.class}
                columns={columns}
                      dataSource={dataList}
                      bordered={true}
                      loading={this.state.isLoading}
                      size='small'/>
    }

    list() {
        GetData('/banner').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}


export default Banner;