import React from 'react';
import QueueAnim from 'rc-queue-anim';
import {Form, Icon, Input, Button, Tooltip, Checkbox} from 'antd';
import {Redirect} from "react-router-dom"
import signupBackgroundForStaff from '../../images/signup-background-forStaff.jpg';
import appLogo from '../../images/app-logo.png';


// 3rd
import '../../styles/antd.less';
import '../../styles/bootstrap/bootstrap.scss'
// custom
import "../../styles/layout.scss"
import "../../styles/theme.scss"
import "../../styles/ui.scss"

const FormItem = Form.Item;


const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 14,
            offset: 6,
        },
    },
};

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            match: props.match,
            redirectLogin: false
        }
    }

    redirectDashboard = (e) => {
        e.preventDefault();
        this.setState({
            redirectLogin: true
        })
    }

    render() {

        let { redirectLogin } = this.state;

        if(redirectLogin) {
            return <Redirect to={``} />
        }

        return (

            <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <section className="form-card-page form-card row no-gutters" style={{height: '100vh'}}>
                        <div className="form-card__img form-card__img--left col-lg-6"
                             style={{backgroundImage: `url('${signupBackgroundForStaff}')`}}></div>
                        <div className="form-card__body col-lg-6 p-5 px-lg-8 d-flex align-items-center">
                            <section className="form-v1-container">

                                <div className={"text-center"}>
                                    <img src={appLogo} style={{width: 'auto'}} alt={"app logo"} />
                                    <br /><br />
                                </div>
                                <h2>Daftar AKun</h2>
                                <p className="lead">Silahkan.</p>
                                <Form className="form-v1" onSubmit={this.redirectDashboard}>
                                    <FormItem
                                        {...formItemLayout}
                                        label={(
                                            <span> Nickname&nbsp; <Tooltip title="What do you want other to call you?">
                                                  <Icon type="question-circle-o"/>
                                                </Tooltip>
                                              </span>
                                        )}
                                        hasFeedback
                                    >
                                            <Input/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="E-mail"
                                        hasFeedback
                                    >
                                            <Input/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Password"
                                        hasFeedback
                                    >
                                            <Input type="password"/>
                                    </FormItem>
                                    <FormItem
                                        {...formItemLayout}
                                        label="Confirm Password"
                                        hasFeedback
                                    >
                                            <Input type="password" onBlur={this.handleConfirmBlur}/>
                                    </FormItem>
                                    <FormItem {...tailFormItemLayout} style={{marginBottom: 8}}>
                                            <Checkbox>I have read the <a href="http://google.com">agreement</a></Checkbox>
                                    </FormItem>
                                    <FormItem {...tailFormItemLayout}>
                                        <Button type="primary" htmlType="submit" className="btn-cta">Sign Up</Button>
                                    </FormItem>
                                </Form>
                            </section>
                        </div>
                    </section>
                </div>
            </QueueAnim>
        );
    }
}

export default Signup;
