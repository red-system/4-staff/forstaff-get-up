import React, {Component} from 'react';
import {Row, Col, Spin, Popover, Button, Alert} from "antd";
import {GetData} from "../../../../services/api";

import OrgChart from 'react-orgchart';
import 'react-orgchart/index.css';

import StructureCreate from "../StructureCreate";
import StructureEdit from "../StructureEdit";
import StructureDelete from "../StructureDelete";

class Overview extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: [],
            listDepartment: []
        };
    }


    componentWillMount() {
        this.data();
        this.listDepartment();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.data();
        }
    }

    render() {

        const {data} = this.state;
        const MyNodeComponent = ({node}) => {
            return (
                <div className="initechNode-posisi">
                    <Popover content={this.content(node)} title="" trigger="hover">
                        <Row>
                            <Col span={24}>
                                <div className="job-title">
                                    {node.structure_name}
                                </div>
                            </Col>
                        </Row>
                    </Popover>
                </div>
            );
        };

        return (
            <div>
                <Alert message="Perhatian ..." description="Untuk mengubah data Posisi, Sorot item di setiap posisi" type="info" showIcon/>
                <br />
                <br />
                <div className="overview-wrapper">
                    <Spin spinning={this.state.isLoading} tip={"Loading ..."}>
                        <OrgChart tree={data} NodeComponent={MyNodeComponent}/>
                    </Spin>
                </div>
            </div>
        )
    }

    data() {
        GetData('/structure-overview').then((result) => {
            const json = result.data;

            this.setState({
                data: json.data,
                isLoading: false,
            });
        });
    }

    listDepartment() {
        GetData('/department').then((result) => {
            this.setState({
                listDepartment: result.data.data
            })
        })
    }

    content = (node) => {
        let btn_delete;

        if (node.structure_type !== 'ceo') {
            btn_delete = <StructureDelete
                node={node}
                listDepartment={this.state.listDepartment}
                listRefreshRun={this.props.listRefreshRun} />
        }


        return (
            <div>
                <StructureCreate
                    node={node}
                    listDepartment={this.state.listDepartment}
                    listRefreshRun={this.props.listRefreshRun}/> &nbsp;
                <StructureEdit
                    node={node}
                    listDepartment={this.state.listDepartment}
                    listRefreshRun={this.props.listRefreshRun}/> &nbsp;
                {btn_delete}
            </div>
        )
    };

}

export default Overview;