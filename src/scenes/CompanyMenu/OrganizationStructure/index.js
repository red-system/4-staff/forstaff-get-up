import React, {Component} from "react";
import {
    Row, Col
} from 'antd';

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import '../../../styles/ui/_card-icon.scss';
import StructureOverview from "../../SopManagement/OrganizationStructure/Overview";

let pageNow = GENERALDATA.primaryMenu.my_company.sub_menu["8"];
let pageParent = GENERALDATA.primaryMenu.my_company;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class ConfigHrd extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            activeKey: "overview",
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }


    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <StructureOverview/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default ConfigHrd;