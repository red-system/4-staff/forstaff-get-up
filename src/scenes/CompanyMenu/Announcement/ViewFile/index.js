import React, {Component} from 'react';
import {Button, Col, Icon, Modal, Row} from "antd";
import {renderFileType} from "../../../../services/app/General";


class ViewFileAnnouncement extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {

        return [
            <Button type="file" onClick={this.modalStatus(true)}>
                <Icon type="eye"/> View File
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="eye"/> View File
                        </span>
                }
                visible={this.state.modalVisible}
                width={900}
                style={{top: 20}}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row style={{textAlign: 'center'}} key="1">
                        <Col>
                            <a href={this.props.record.announcement_attachment_url} target="_blank">
                                <Button key="submit" type="primary">
                                    <Icon type="download"/> Download File
                                </Button>
                            </a>
                            &nbsp;

                            <Button key="back" type="warning" onClick={this.modalStatus(false)}>
                                <Icon type="rollback"/> Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                {renderFileType(this.props.record.announcement_attachment_type, this.props.record.announcement_attachment_url)}
            </Modal>
        ]
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };
}

export default ViewFileAnnouncement;