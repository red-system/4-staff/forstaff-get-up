import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import ViewFileAnnouncement from "../ViewFile";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class Announcement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var announcementTitle = false;
            if (get(record, "announcement_title") != null) {
                announcementTitle = get(record, "announcement_title").toString().match(reg);
            }
            var announcementDescription = false;
            if (get(record, "announcement_description_view") != null) {
                announcementDescription = get(record, "announcement_description_view").toString().match(reg);
            }
            var createdAt = false;
            if (get(record, "created_at") != null) {
                createdAt = get(record, "created_at").toString().match(reg);
            }
            if (!announcementTitle && !announcementDescription && !createdAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("my_company", "announcement", "list");
        let menuView = menuActionViewAccess("my_company", "announcement", "view_file");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Judul", dataIndex: "announcement_title", key: "announcement_title" },
            { title: "Deskripsi", dataIndex: "announcement_description_view", key: "announcement_description_view" },
            { title: "Tanggal Upload", dataIndex: "created_at", key: "created_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuView.class}>
                            <ViewFileAnnouncement record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["announcement_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        GetData("/announcement-staff").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default Announcement;
