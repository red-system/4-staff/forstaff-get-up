import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import ViewFile from "../ViewFile";
import { GetData } from "../../../../services/api";
import renderHtml from "react-render-html";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var newsTitle = false;
            if (get(record, "news_title") != null) {
                newsTitle = get(record, "news_title").toString().match(reg);
            }
            var newsDescription = false;
            if (get(record, "news_description") != null) {
                newsDescription = get(record, "news_description").toString().match(reg);
            }
            var createdAt = false;
            if (get(record, "created_at") != null) {
                createdAt = get(record, "created_at").toString().match(reg);
            }
            if (!newsTitle && !newsDescription && !createdAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("my_company", "news", "list");
        let menuView = menuActionViewAccess("my_company", "news", "view_file");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Judul", dataIndex: "news_title", key: "news_title" },
            { title: "Deskripsi", dataIndex: "news_description", key: "news_description", className: "th-350" },
            { title: "Tanggal Upload", dataIndex: "created_at", key: "created_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                className: "th-350",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuList.class}>
                            <ViewFile record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["news_id"],
                no: key + 1,
                ...item,
                news_description: renderHtml(item["news_description"]),
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        GetData("/news-staff").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default News;
