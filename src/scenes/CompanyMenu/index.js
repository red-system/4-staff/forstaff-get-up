import React, {Component} from "react";
import {Row, Col} from 'antd';
import GENERALDATA from "../../constants/generalData";
import Breadcrumb from '../../components/Breadcrumb';
import {Link} from 'react-router-dom';

import '../../styles/ui/_card-icon.scss';
import {menuViewAccess} from "../../services/app/General";

let menuMyCompany = GENERALDATA.primaryMenu.my_company;
let subMyCompany = GENERALDATA.primaryMenu.my_company.sub_menu;
let gutter = GENERALDATA.gutter;

const menuGrid = {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 3
};

const breadcrumb = [
    {
        label: menuMyCompany.label,
        route: `${menuMyCompany.route}`
    },
];

class SopManagement extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="-1"/>)]
        })
    }

    render() {

        const menuList = Object.keys(subMyCompany).map(function (key) {

            let menuView = menuViewAccess(menuMyCompany.variable, subMyCompany[key].variable);

            if (menuView) {
                return (
                    <Col {...menuGrid} className="text-center" key={key}>
                        <Link to={subMyCompany[key].route}>
                            <img src={subMyCompany[key].img} alt={subMyCompany[key].label}/>
                            <h1>{subMyCompany[key].label}</h1>
                        </Link>
                    </Col>
                )
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row gutter={gutter} className="menu-list" type="flex" align="top">
                    {menuList}
                </Row>
            </div>
        )

    }

}

export default SopManagement;