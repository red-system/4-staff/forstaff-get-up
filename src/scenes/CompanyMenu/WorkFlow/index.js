import React, { Component } from "react";
import { Button, Col, Row, Table, Input, Icon } from "antd";

import ViewFile from "./ViewFile";
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { GetData } from "../../../services/api";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};

let pageNow = GENERALDATA.primaryMenu.my_company.sub_menu["2"];
let pageParent = GENERALDATA.primaryMenu.my_company;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

class RulesRegulation extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });

        GetData("/work-flow").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var workTitle = false;
            if (get(record, "workf_title") != null) {
                workTitle = get(record, "workf_title").toString().match(reg);
            }
            var workDescription = false;
            if (get(record, "workf_description") != null) {
                workDescription = get(record, "workf_description").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!workTitle && !workDescription && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("my_company", "workflow", "list");
        let menuPreview = menuActionViewAccess("my_company", "workflow", "preview");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Judul",
                dataIndex: "workf_title",
                key: "workf_title",
            },
            {
                title: "Deskripsi",
                dataIndex: "workf_description",
                key: "workf_description",
            },
            {
                title: "Tanggal Upload",
                dataIndex: "updated_at",
                key: "updated_at",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuPreview.class}>
                            <ViewFile record={record} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="row mb-1">
                                    <div className="col-9"></div>
                                    <div className="col-3 pull-right">
                                        <Search
                                            size="default"
                                            ref={(ele) => (this.searchText = ele)}
                                            suffix={suffix}
                                            onChange={this.onSearch}
                                            placeholder="Search Records"
                                            value={this.state.searchText}
                                            onPressEnter={this.onSearch}
                                        />
                                    </div>
                                </div>
                                <Table
                                    className={menuList.class}
                                    columns={columns}
                                    dataSource={data}
                                    bordered={true}
                                    loading={this.state.isLoading}
                                    size="small"
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RulesRegulation;
