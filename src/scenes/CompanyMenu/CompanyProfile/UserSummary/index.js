import React, {Component} from 'react';
import {Col, Icon, Row, Button} from "antd";
import { Link } from 'react-router-dom';
import GENERALDATA from "../../../../../constants/generalData";

const gutter = GENERALDATA.gutter;

class AnnouncementNews extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userLevel: props.userLevel
        }
    }


    render() {

        const {userLevel} = this.state;

        return(
            <Row gutter={gutter}>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-success">
                                        <Icon type="line-chart"/>
                                    </span>
                        <div className="box-info">
                            <p className="box-num">16 <span className="size-h4">%</span></p>
                            <p className="text-muted" style={{marginBottom: '6px'}}>KPI Terakhir</p>
                            <Link to={{
                                pathname: `../${userLevel}/${GENERALDATA.primaryMenu.profileDetil.route}`,
                                state: {
                                    tabActive: 'kpi'
                                }
                            }}>
                                <Button type="primary" size="small">
                                    Detil
                                    <Icon type="info-circle" />
                                </Button>
                            </Link>
                        </div>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-info">
                                        <Icon type="team"/>
                                    </span>
                        <div className="box-info">
                            <p className="box-num">22 <span className="size-h4">%</span></p>
                            <p className="text-muted">Approval</p>
                        </div>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-warning">
                                        <Icon type="credit-card"/>
                                    </span>
                        <div className="box-info">
                            <p className="box-num">51 <span className="size-h4">k</span></p>
                            <p className="text-muted">Reward</p>
                        </div>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-primary">
                                        <Icon type="shopping-cart"/>
                                    </span>
                        <div className="box-info">
                            <p className="box-num">21 <span className="size-h4">k</span></p>
                            <p className="text-muted">Punishment</p>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }

}

export default AnnouncementNews;