import React, {Component} from 'react';
import {Spin} from "antd";
import {GetData} from "../../../../services/api";
import {dateFormatterApp} from "../../../../services/app/General";
import GENERALDATA from "../../../../constants/generalData";

class AnnouncementNews extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            dataCompany: [],
            isLoading: true
        }
    }

    componentWillMount() {
        GetData('/company-profile')
            .then((result) => {
                this.setState({
                    dataCompany: result.data.data.company,
                    isLoading: false
                })
            })
    }

    render() {

        const {dataCompany} = this.state;

        return (
            <Spin {...GENERALDATA.loading} spinning={this.state.isLoading} >
                <section className="form-card row no-gutters">
                    <div className="form-card__img form-card__img--left col-lg-3 company-profile-logo-banner"
                         style={{backgroundImage: `url('${dataCompany.company_logo_banner_url}')`}}>

                        <img src={dataCompany.company_logo_url} className={"company-profile-logo"} />
                    </div>
                    <div className="form-card__body col-lg-9 p-4">
                        <section className="form-v1-container left-text">
                            <h3>{dataCompany.company_name}</h3>
                            <div className="form-text">
                                <div>
                                    <span>Inisial</span> {dataCompany.company_inisial}
                                </div>
                                <div>
                                    <span>Website</span> <a href={dataCompany.company_website}>{dataCompany.company_website}</a>
                                </div>
                                <div>
                                    <span>Deskripsi Perusahaan</span>
                                    <p  style={{textAlign: "left"}}>{dataCompany.company_description}</p>
                                </div>
                                <div>
                                    <span>Tanggal Berdiri</span> {dateFormatterApp(dataCompany.company_found_date)}
                                </div>
                                <div>
                                    <span>Nomer Telepon 1</span> {dataCompany.company_telp_1}
                                </div>
                                <div>
                                    <span>Nomer Telepon 2</span> {dataCompany.company_telp_2}
                                </div>
                                <div>
                                    <span>Nomer Fax</span> {dataCompany.company_fax}
                                </div>
                                <div>
                                    <span>Email 1</span> <a href={`mailto:${dataCompany.company_email_1}`}>{dataCompany.company_email_1}</a>
                                </div>
                                <div>
                                    <span>Email 2</span> <a href={`mailto:${dataCompany.company_email_2}`}>{dataCompany.company_email_2}</a>
                                </div>
                                <div>
                                    <span>Provinsi</span> { dataCompany.province_name}
                                </div>
                                <div>
                                    <span>Kecamatan</span> {dataCompany.district_name }
                                </div>
                                <div>
                                    <span>Alamat Kantor</span> {dataCompany.company_address}
                                </div>
                                {/*<div>*/}
                                {/*<span>Peta</span>*/}
                                {/*<br />*/}
                                {/*<iframe*/}
                                {/*src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.5359247508695!2d115.22603191524327!3d-8.64047019379093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23f7a779a6533%3A0x28f52169c1dbb072!2sPT+Guna+Artha+kencana+%22Red+Consulting%22!5e0!3m2!1sid!2sid!4v1565084953194!5m2!1sid!2sid"*/}
                                {/*width="600" height="450" frameBorder="0" style="border:0" allowFullScreen/>*/}
                                {/*</div>*/}
                            </div>
                        </section>
                    </div>
                </section>
            </Spin>
        )
    }

}

export default AnnouncementNews;