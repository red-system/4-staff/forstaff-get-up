import React, {Component} from 'react';
import './styles.scss';

class AnnouncementNews extends Component {

    render() {
        return(
            <div className="user-info">
                <div className="app-content">
                    <div className="app-content-title">
                        <div className="pull-left">
                            <strong>Jumlah Hari Kerja</strong>
                        </div>
                        <div className="pull-right">
                            <strong>29 Hari</strong>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="app-content">
                    <div className="app-content-title">
                        <div className="pull-left">
                            <strong>Day Off</strong>
                        </div>
                        <div className="pull-right">
                            <strong>1 Hari</strong>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="app-content">
                    <div className="app-content-title">
                        <div className="pull-left">
                            <strong>Absen</strong>
                        </div>
                        <div className="pull-right">
                            <strong>1 Hari</strong>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="app-content">
                    <div className="app-content-title">
                        <div className="pull-left">
                            <strong>Lembur</strong>
                        </div>
                        <div className="pull-right">
                            <strong>10 Jam</strong>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="app-content">
                    <div className="app-content-title">
                        <div className="pull-left">
                            <strong>Cuti</strong>
                        </div>
                        <div className="pull-right">
                            <strong>1/12 Hari</strong>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
        )
    }

}

export default AnnouncementNews;