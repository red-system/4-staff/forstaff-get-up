import React, {Component} from "react";
import {Row, Col} from 'antd';
import Breadcrumb from '../../../components/Breadcrumb';
import GENERALDATA from "../../../constants/generalData";

import UserCard from './UserCard';

import './styles.scss';
import {menuActionViewAccess} from "../../../services/app/General";


const gutter = GENERALDATA.gutter;
let pageNow = GENERALDATA.primaryMenu.my_company.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.my_company;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class Profile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            breadcrumbComponent: null,
            visible: false,
            match: props.match,
            userLevel: props.userLevel
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        let menuList = menuActionViewAccess('my_company', 'company_profile', 'list');
        const { breadcrumbComponent, match, userLevel } =  this.state;

        return (

            <div className="profile-wrapper content-gutter-vertical">
                { breadcrumbComponent }
                <Row gutter={gutter} className={menuList.class}>
                    <Col xs={24} sm={24} md={24} lg={24}>
                        <UserCard match={match} detailVisible={true} userLevel={userLevel} />
                    </Col>
                </Row>

            </div>

        )

    }

}

export default Profile;