import React, {Component} from 'react';
import {Col, Row} from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import List from "./List";

let pageNow = GENERALDATA.primaryMenu.my_company.sub_menu["5"];
let pageParent = GENERALDATA.primaryMenu.my_company;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class RewardTabs extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <List />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    };
}

export default RewardTabs;