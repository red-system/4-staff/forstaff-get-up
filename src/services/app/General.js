import moment from "moment"
import React, {useState} from "react";
import GENERALDATA from "../../constants/generalData";
import Tag from "antd/lib/tag";
import ImageEditorRc from "react-cropper-image-editor";

export function dateFormatterApi(params) {
    // let date = new Date(params);
    // date = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + (date.getDate())).slice(-2);
    //
    // return date;

    return moment(params, "DD-MM-YYYY").format('YYYY-MM-DD');
}

export function dateFormatterApp(params) {
    let date = new Date(params);
    date = ("0" + (date.getDate())).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
    // let date = moment(params, 'DD-MM-YYYY').format('DD-MM-YYYY');
    return date;
}

export function dateTimeFormatterApi(params) {
    let date = new Date(params);
    date = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + (date.getDate())).slice(-2) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    return date;
}

export function dateTimeFormatterApp(params) {
    // let date = new Date(params);

    let date = moment(params, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');

    //console.log('general 1', params);

    //date = ("0" + (date.getDate())).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();

    //console.log('general 2', date);
    return date;
}

export function dayId(day) {
    switch (day) {
        case "Sunday":
            return "Minggu";
            break;
        case "Monday":
            return "Senin";
            break;
        case "Tuesday":
            return "Selasa";
            break;
        case "Wednesday":
            return "Rabu";
            break;
        case "Thursday":
            return "Kamis";
            break;
        case "Friday":
            return "Jumat";
            break;
        case "Saturday":
            return "Sabtu";
            break;
    }
}

export function formatMoney(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

export function dateFormatApp() {
    return 'DD-MM-YYYY';
}

export function dateFormatApi() {
    return 'YYYY-MM-DD';
}

export function dateTimeFormatApp() {
    return 'DD-MM-YYYY HH:mm:ss';
}

export function dateTimeFormatApi() {
    return 'YYYY-MM-DD HH:mm:ss';
}

export function dateToDay() {
    // var today = new Date();
    // var dd = String(today.getDate()).padStart(2, '0');
    // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = today.getFullYear();
    //
    // today = yyyy + '-' + mm + '-' + dd;
    return moment().format("YYYY-MM-DD");
}

export function dateToDay2() {
    // var today = new Date();
    // var dd = String(today.getDate()).padStart(2, '0');
    // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = today.getFullYear();
    //
    // today = yyyy + '-' + mm + '-' + dd;
    return moment().format("DD-MM-YYYY");
}

export function monthToDay() {
    return moment().format("MM");
}

export function yearToDay() {
    return moment().format("YYYY");
}

export function dateTimeToDay() {
    return moment().format("YYYY-MM-DD HH:mm:ss");
}

export function strRandom(length = 5) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export function menuViewAccess(menuRolesVariable, menuViewVariable) {

    let roles = localStorage.getItem('roles');
    roles = JSON.parse(roles);
    let rolesMenu = roles[menuRolesVariable];
    let menuView = false;

    Object.keys(rolesMenu).map((item2, key2) => {
        let menu = rolesMenu[item2][menuViewVariable];
        if (menu != null) {
            Object.keys(menu).map((item3, key3) => {
                Object.keys(menu[item3]).map((item4, key4) => {
                    // console.log(menuRolesVariable, menuViewVariable, menu, item3, item4, menu[item3][item4]);
                    if (menu[item3][item4]) {
                        menuView = true;
                    }
                })
            });
        }
    });

    // console.log(menuView);

    return menuView;
}

export function menuActionViewAccess(menuRolesVariable, menuViewVariable, menuAction) {
    let roles = localStorage.getItem('roles');
    roles = JSON.parse(roles);
    let rolesMenu = roles[menuRolesVariable];
    let menuActionView = false;

    Object.keys(rolesMenu).map((item2, key2) => {
        let menu = rolesMenu[item2][menuViewVariable];
        if (menu != null) {
            Object.keys(menu).map((item3, key3) => {
                Object.keys(menu[item3]).map((item4, key4) => {
                    // console.log(menuRolesVariable, menuViewVariable, menu, item3, item4, menu[item3][item4]);
                    if (menu[item3][item4] && item4 === menuAction) {
                        menuActionView = true;
                    }
                })
            });
        }
    });

    // console.log(menuActionView);

    let menuActionClass = !menuActionView ? 'hide' : '';

    return {
        class: menuActionClass,
        status: menuActionView
    };
}

export function renderFileType(fileType, fileUrl) {

    let preview = ''; //fileType+' - '+fileUrl;
    switch (fileType) {
        case "pdf":
            preview =
                <object
                    width="100%"
                    height="700"
                    data={fileUrl}
                    type="application/pdf"
                    >
                </object>;
            break;
        case "image":
            preview =
                <div className={"text-center"}>
                    <img src={fileUrl} className={"img-responsive"}/>
                </div>;
            break;
        case "excel":
        case "word":
        case "power_point":
            preview =
                <iframe
                    src={`https://view.officeapps.live.com/op/embed.aspx?src=${fileUrl}`}
                    width='100%' height='600px' frameBorder='0'>
                    This is an embedded
                    <a target='_blank' href='http://office.com'>
                        Microsoft Office
                    </a>
                    document, powered by
                    <a target='_blank' href='http://office.com/webapps'>
                        Office Online
                    </a>.
                </iframe>;
            break;
    }

    return preview;
}

export function yearList() {
    let yearList = [];
    for (let i = GENERALDATA.yearFirst; i <= GENERALDATA.yearUntil; i++) {
        yearList[i] = i;
    }

    return yearList;
}

export function notificationRedirect(notification_id, notification_type) {

}


export function status_view(status, status_label = null) {
    var color = "";
    var status_view = "";
    switch (status) {
        case "unpublish":
            color = "volcano";
            status_view = "Belum Publish";
            break;
        case "publish":
            color = "blue";
            status_view = "Publish";
            break;
        case "canceled":
            color = "orange";
            status_view = 'Dibatalkan';
            break;
        case  "confirm":
            color = "green";
            status_view = 'Dikonfirmasi';
            break;
        case  "confirmed":
            color = "green";
            status_view = 'Dikonfirmasi';
            break;
        case "reject":
            color = "red";
            status_view = 'Ditolak';
            break;
        case "rejected":
            color = "red";
            status_view = 'Ditolak';
            break;
        case "accepted":
            color = "green";
            status_view = 'Diterima';
            break;
        case "accept":
            color = "green";
            status_view = 'Diterima';
            break;
        case "approved":
            color = "green";
            status_view = 'Disetujui';
            break;
        case "unpaid":
            color = "green";
            status_view = 'Tidak Bayar';
            break;
        case "paid":
            color = "blue";
            status_view = 'Bayar';
            break;
        case "yes":
            color = "green";
            status_view = 'Ya';
            break;
        case "custom":
            color = "geekblue";
            status_view = 'Custom';
            break;
        case "no":
            color = "red";
            status_view = 'Tidak';
            break;
        case "revise":
            color = "orange";
            status_view = 'Direvisi';
            break;
        case "process":
            color = "orange";
            status_view = 'Proses';
            break;
        case "active":
            color = "green";
            status_view = 'Aktif';
            break;
        case "not_active":
            color = "red";
            status_view = 'Tidak Aktif';
            break;
        default :
            color = "purple";
            status_view = "Nihil";
            break
    }

    return <Tag color={color}>{status_label === null ? status_view : status_label}</Tag>
}

export function staff_avatar_rules_upload_image() {
    return (
        <div>
            <div className="ant-form-extra">- jika ingin memotong gambar, tekan tombol "<strong>Crop Gambar</strong>"
            </div>
            <div className="ant-form-extra">- jika ingin memperbesar gambar, lakukan "<strong>Scroll</strong>" pada
                gambar lalu tekan "<strong>Crop Gambar</strong>"
            </div>
            <div className="ant-form-extra">- browse foto dengan ukuran <strong>500px x 500px</strong></div>
            <div className="ant-form-extra">- browse foto dengan ukuran <strong>5OO KB</strong></div>
        </div>
    );
}

export function staff_avatar_config_upload_image() {
    return {
        ref: 'cropper',
        crossOrigin: 'true', // boolean, set it to true if your image is cors protected or it is hosted on cloud like aws s3 image server
        style: {height: 297, width: 297},
        aspectRatio: 16 / 16,
        viewMode: 1,
        modal: true,
        className: "image-preview-register",
        guides: true,
        center: false,
        highlight: false,
        background: false,
        restore: false,
        autoCropArea: 0.9,
        autoCrop: true,
        rotatable: true,
        imageName: 'test'
    }
}