import React from "react";

import GENERAL_DATA from "../../constants/generalData";
import Axios from "axios";
import { message, Modal, notification } from "antd";

const axiosCancelToken = Axios.CancelToken;
const cancelSource = axiosCancelToken.source();

let baseUrl = `${GENERAL_DATA.apiUrl}`;
let authPrefix = "bearer";

export function PostData(type, data) {
    return new Promise((resolve, reject) => {
        let token = localStorage.getItem("token");

        Axios({
            method: "post",
            url: `${baseUrl}${type}`,
            data: data,
            headers: {
                Authorization: `${authPrefix} ${token}`,
            },
            cancelToken: cancelSource.token,
        }).then((result) => {
            if (result.data.status === "auth_error") {
                AlertLogout();
            } else {
                resolve(result);
            }
        });
    });
}

export function PutData(type, data) {
    return new Promise((resolve, reject) => {
        let token = localStorage.getItem("token");

        Axios({
            method: "put",
            url: `${baseUrl}${type}`,
            data: data,
            headers: {
                Authorization: `${authPrefix} ${token}`,
            },
            cancelToken: cancelSource.token,
        }).then((result) => {
            if (result.data.status === "auth_error") {
                AlertLogout();
            } else {
                resolve(result);
            }
        });
    });
}

export function GetData(type) {
    return new Promise((resolve, reject) => {
        let token = localStorage.getItem("token");
        Axios({
            method: "get",
            url: `${baseUrl}${type}`,
            headers: {
                Authorization: `${authPrefix} ${token}`,
            },
            cancelToken: cancelSource.token,
        }).then((result) => {
            if (result.data.status === "auth_error") {
                AlertLogout();
            } else {
                resolve(result);
            }
        });
    });
}

export function DeleteData(type) {
    return new Promise((resolve, reject) => {
        let token = localStorage.getItem("token");

        Axios({
            method: "delete",
            url: `${baseUrl}${type}`,
            headers: {
                Authorization: `${authPrefix} ${token}`,
            },
            cancelToken: cancelSource.token,
        }).then((result) => {
            if (result.data.status === "auth_error") {
                AlertLogout();
            } else {
                resolve(result);
            }
        });
    });
}

export function GenerateLink(link) {
    let token = localStorage.getItem("token");
    return `${baseUrl}${link}?prefix=${authPrefix}&token=${token}`;
}

export default function CheckLogin() {
    const greeting = "Hello Function Component!";

    return <h1>{greeting}</h1>;
}

export function AlertLogout() {
    // message.error('Sesi Login Kadaluarsa, Mohon Login Ulang Kembali', 2, function() {
    //     localStorage.clear();
    //     window.location.href = '';
    // });
    Modal.error({
        title: "Sesi Gagal",
        content: "Sesi Login Kadaluarsa, Mohon Login Ulang Kembali",
        onOk: function () {
            localStorage.clear();
            window.location.href = "";
        },
    });
}

export function CancelFetch() {
    cancelSource.cancel("Operation cancelled");
}
